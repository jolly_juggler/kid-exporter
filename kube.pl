#!/usr/bin/perl -w

use strict;
use Cwd qw(cwd);
use File::Basename;

my @stage = (
	"build",
	"compose",
	"push",
	"deploy",
);

my $dir = basename(cwd);
$dir =~ s/_/-/g;

$ENV{BUILD_ID}=10 if (!defined $ENV{BUILD_ID});
$ENV{PATH} .= ":." if (!defined $ENV{SystemRoot});

my $deploy = "deploy.yml";
my $tmpfile = "$deploy.$ENV{BUILD_ID}";

my ($arg, $project) = @ARGV;
$project=$dir if (!defined $project);
my $tag="ccp-harbor.tank.local/madaus/$project:$ENV{BUILD_ID}";

sub run {
	my ($arg) = @_;
	my $rc = 0;
	print "******************************* run $arg\n";
	if ($arg eq $stage[0]) {
		$rc = 0;
		$rc = system("plugin.pl") if ( -f "plugin.pl");
		exit($rc) if ($rc);
		$rc = system("mvn install") if ( -d "src");
	}
	elsif ($arg eq $stage[1]) {
		$rc = system("docker build -t $tag .");
	}
	elsif ($arg eq $stage[2]) {
		if ($ENV{EDITOR} eq "vi") {
			$rc = system("kind load docker-image $tag");
		} else {
			$rc = system("docker push $tag");
		}
	}
	elsif ($arg eq $stage[3]) {
		open(my $fp, $deploy);
		open(my $fo, ">$tmpfile");
		while(<$fp>) {
			if (/image:/) {
				s/image:.*/image: $tag/;
				print $fo $_;
			}
			else { print $fo $_; }
		}
		close($fp);	
		close($fo);	
		$rc = system("kubectl apply -f $tmpfile");
	}
	elsif ($arg eq "logs") {
		my @out = `kubectl get pods`;
		foreach (@out) {
			if (/($project[^ ]+)/) {
				system("kubectl logs $1");
			}
		}
	}
	elsif ($arg eq "forward") {
		my @out = `kubectl get services`;
		foreach (@out) {
			if (/$project.*[^\d]+(\d+)\/TCP/) {
				system("kubectl port-forward service/$project $1:$1");
				last;
			}	
		}
	}
	return $rc;
}
my $rc = 0;
if (!defined $arg) {
	foreach my $step (@stage) {
		$rc = run($step);
	}
}
else {
	$rc = run($arg);
}
exit($rc);

