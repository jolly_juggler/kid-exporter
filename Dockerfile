FROM adoptopenjdk:11-jre-hotspot
RUN mkdir /opt/app
COPY target/kid-exporter-0.0.1-SNAPSHOT-spring-boot.war /opt/app
CMD ["java", "-jar", "/opt/app/kid-exporter-0.0.1-SNAPSHOT-spring-boot.war"]

