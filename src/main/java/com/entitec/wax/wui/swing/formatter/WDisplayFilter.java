package com.entitec.wax.wui.swing.formatter;

import java.io.Serializable;
import java.util.StringTokenizer;

import javax.swing.SwingConstants;

import com.entitec.wax.wdm.IWAttribute;
import com.entitec.wax.wdm.IWDataItem;
import com.entitec.wax.wdm.WDMTypChecker;
import com.entitec.wax.wdm.WDataItem;
import com.entitec.wax.wglobal.WRuntimeException;

public class WDisplayFilter extends WAbstractFormatter implements Serializable {
  public static final String NO_SIGN = "NoSign";
  public static final String TRIM_LEFT_ZERO = "TrimLeftZero";
  public static final String FILL_LEFT_ZERO = "FillLeftZero";
	private char mFormat = IWAttribute.FRM_CHAR;
  private String mPattern = null;
	private String mInValue = null;
	private boolean mIsEditable = false;

  public WDisplayFilter(String pattern, IWAttribute a, boolean editable) {
    super(a);
    mFormat = a.getFrm();
    mPattern = pattern;
    mIsEditable = editable;
	}
	public int getHorizontalAlignment() {
		if (super.mAttribute.getFrm() == IWAttribute.FRM_CHAR) {
			return SwingConstants.LEFT;
		} else {
			return SwingConstants.RIGHT;
		}
	}
	public void setEditable(boolean b) {
		mIsEditable = b;
	}
	public boolean checkValue(String value) {
		boolean res = true;
		if (mIsEditable == true) {
			if (value == null) return true;
//			Bug #468 mInValue muss auch gel�scht werden k�nnen
			if (value.length() == 0){
				mInValue = "";
				return true;	
			}
			String formattedInValue = (String)getFormattedValue(mInValue).getValue();
			if (value.equals(formattedInValue)) return true;

			String v = value;
			if (mAttribute.getFrm() == IWAttribute.FRM_DEC || mAttribute.getFrm() == IWAttribute.FRM_NUM) {
				int pos = v.indexOf('.');
				while (pos > -1) {
					v = v.substring(0, pos) + v.substring(pos +1);
					pos = v.indexOf('.');
				}
			}
			switch(mAttribute.getFrm()) {
				case IWAttribute.FRM_CHAR:
					res = WDMTypChecker.checkCharacter(mAttribute.getLen(), v) != null; 
					break;
				case IWAttribute.FRM_NUM:
					res = WDMTypChecker.checkNumeric(mAttribute.getLen(), v) != null; 
					break;
				case IWAttribute.FRM_DEC:
					res = WDMTypChecker.checkDecimal(mAttribute.getLen(), mAttribute.getDec(),
													 mAttribute.getSign(), v) != null; 
					break;
				default:
					throw new WRuntimeException("Value is not Attribute conform.");
			}
			if (res == true) mInValue = v;
		}
		return res;
	}
	@Override
	public boolean hasInputFinishedAndCanTextControlBeLeft(String value) {
		return false;
	}
	public String fillupValue(String value) {
		return value;
	}
	public IWDataItem getFormattedValue(String value) {
	  WDataItem di = new WDataItem();
	  if (value != null) {
			switch(mAttribute.getFrm()) {
				case IWAttribute.FRM_CHAR:
					mInValue = WDMTypChecker.fillupCharRight(mAttribute.getLen(), value);
					break;
				case IWAttribute.FRM_NUM:
					mInValue = WDMTypChecker.fillupNumericLeft(mAttribute.getLen(), value);
					break;
				case IWAttribute.FRM_DEC:
					mInValue = WDMTypChecker.fillupNumericLeft(mAttribute.getLen(), value);
					break;
				default:
					throw new WRuntimeException("Format corrupt.");
			}
			String v = value;
			if (value.length() > 0){
				try {
					v = WDisplayFilter.filterValue(mInValue, mPattern, mFormat, mAttribute.getLen());
				} catch (Exception e) {
					throw new IllegalArgumentException("DisplayFiler '"+ mPattern + "' corrupt for control '" + this.mAttribute.getName() + "'.", e);
				}
			}
			
			if (v != null) {
				di.setState(IWDataItem.ENABLED);
				di.setValue(v);
			} else {
				di.setState(IWDataItem.ERROR);
				di.setValue(value);
			}
		}
	  return di;
	}
	public String getUnformattedValue(String value) {
		if (mInValue == null) {
			return mAttribute.getDefaultValue(); 
		} else {
			return mInValue;
		}
	}
	//*************************************************************************************
	// filter-Mehtodes
	//*************************************************************************************
	/*
	 * Arbeitsweise:
	 * 1. Das Pattern wird in seine SubPatterns zerlegt
	 * 2. Der Value wird dann entsprechend der Angaben im SubPattern von links nach rechts aufbereitet
	 * 3. Die Ergebnisse der Aufbereitung werden dann konkateniert.
	 */
	public static synchronized String filterValue(String value, String pattern, char format, int length) {
		String value_orig = value;
	  String res = "";
	  if ((value == null) || (value.length() == 0)) return res;

	  //Bei DecimalValues ist darauf zu achten, ob der Ausschnitt aus dem Value vor oder nach dem Komma geschehen soll
	  //Grund: Im alten KIDFormat gab es kein Komma
	  //Daher sind die Displayfilter auf die rein numerischen Werte hin erstellt worden
	  //In Wax werden Dezimalwerte aber mit Komma im DataModel gehalten. Dieses wird hier ausgeschnitten
	  if (format == IWAttribute.FRM_DEC){
	  	int pos = value.indexOf(',');
	  	if (pos > -1) {
			StringBuffer buf = new StringBuffer(value.length()-1);
			buf.append(value.substring(0, pos));
			buf.append(value.substring(pos+1));
			value = buf.toString();
	  	}
	  	pos = value.indexOf('+');
	  	if (pos > -1) {
			StringBuffer buf = new StringBuffer(value.length()-1);
			buf.append(value.substring(0, pos));
			buf.append(value.substring(pos+1));
			value = buf.toString();
	  	}
	  	pos = value.indexOf('-');
	  	if (pos > -1) {
			StringBuffer buf = new StringBuffer(value.length()-1);
			buf.append(value.substring(0, pos));
			buf.append(value.substring(pos+1));
			value = buf.toString();
	  	}
	  }
	  value = WDMTypChecker.fillupNumericLeft(length, value);

 	  String func = null;
 	  String range = null;
 	  String separator = null;
	  if (firstSyntaxCheck(pattern, format) == false) return null;
	  String workPattern = WDisplayFilter.translateKIDtoWAX(pattern);
	  workPattern = workPattern.substring(workPattern.indexOf("("));
	  StringTokenizer st = new StringTokenizer(workPattern, ")");
	  while(st.hasMoreTokens()) {
	  	func = null; 
	  	range = null;
	 	separator = null;
	    String singlePattern = st.nextToken().substring(1);
  	    StringTokenizer st2 = new StringTokenizer(singlePattern, ";");
  	    int countTokens = st2.countTokens();
	    switch (countTokens) {
	      case 1:
			//Es ist ein Intervall definiert, keine TrimFunktion, keine F�llzeichen
	        range = st2.nextToken();
	        break;
	      case 2:
			//Es sind zwei Intervalle definiert ...
	        if (singlePattern.charAt(0) == ';') {
				//... keine TrimFunktion, und F�llzeichen
				range = st2.nextToken();
				separator = st2.nextToken();
	        } else {
				//... eine TrimFunktion, keine F�llzeichen
				func = st2.nextToken();
				range = st2.nextToken();
	        }
	        break;
	      case 3:
			func = st2.nextToken();
			range = st2.nextToken();
			separator = st2.nextToken();
	        break;
	      default:
	      	return null;
	    }
	    if (range == null || range.length() == 0) return null;
        if (func == null && separator == null) {
	        String s = WDisplayFilter.filterSubValue(value, range);
	        if (s == null) return null; 
  		  	res = res + s;
        } else if (func == null && separator != null) {
	        String s = WDisplayFilter.filterAndFillSubValue(value, range, separator);
	        if (s == null) return null; 
  		  	res = res + s;
        } else if (func != null && separator == null) {
	        if ((format == IWAttribute.FRM_DEC)
	         || (format == IWAttribute.FRM_NUM)) {
				String s = WDisplayFilter.filterAndTrimSubValue(res, value, func, range, length);
		        if (s == null) return null; 
	  		  	res = res + s;
	        } else {
	          return null;
	        }
        } else if (func != null && separator != null) {
	        if ((format == IWAttribute.FRM_DEC)
	         || (format == IWAttribute.FRM_NUM)) {
				String s = WDisplayFilter.filterAndTrimAndFillSubValue(res, value, func, range, separator, length);
		        if (s == null) return null; 
	  		  	res = res + s;
	        } else {
	          return null;
	        }
        } else {
	  	    return null;
        }
	  }
	  if ((format == IWAttribute.FRM_DEC) 
	   && (pattern.startsWith(WDisplayFilter.NO_SIGN) == false)) {
	    switch (value_orig.charAt(value_orig.length()-1)) {
	      case '+':
	        res = "+" + res;
	        break;
	      case '-':
	        res = "-" + res;
	        break;
	      default:
	        res = "+" + res;
	    }
	  }
	  return res;
	}
	public static synchronized String filterSubValue(String value, String range) {
	  int i = range.indexOf("-");
	  if (i == 0) return null;
	  if (i > 0) {
		  StringTokenizer st = new StringTokenizer(range, "-");
		  if (st.countTokens() != 2) return null;
		  int from, to;
	    try {
  		  from = Integer.parseInt(st.nextToken())-1;
	    } catch (NumberFormatException e) {
	      return null;
	    }
	    try {
  		  to = Integer.parseInt(st.nextToken())-1;
	    } catch (NumberFormatException e) {
	      return null;
	    }
		  if (from >= to) return null;
		  return value.substring(from, to+1);
	  } else {
		  i = range.indexOf(",");
		  if (i == 0) return null;
		  if (i > 0) {
			  StringTokenizer st = new StringTokenizer(range, ",");
			  String res = "";
			  while(st.hasMoreTokens()) {
			    try {
				    res = res + value.charAt(Integer.parseInt(st.nextToken())-1);
			    } catch (NumberFormatException e) {
			      return null;
			    }
			  }
			  return res;
		  } else {
		    try {
		      return ""+value.charAt(Integer.parseInt(range)-1);
		    } catch (NumberFormatException e) {
		      return null;
		    }
		  }
	  }
	}
	public static synchronized String filterAndFillSubValue(String value, String range, String separator) {
		String res = WDisplayFilter.filterSubValue(value, range);
		if (res != null) {
			res = res + separator;
		}
		return res;
	}
	public static synchronized String filterAndTrimSubValue(String curResult, String value, String func, String range, int length) {
	  String res = WDisplayFilter.filterSubValue(value, range);
	  if (res != null) {
	    if (func.equals(WDisplayFilter.TRIM_LEFT_ZERO)) {
	    	if (curResult.length() == 0) {
	    		res = WDisplayFilter.trimLeftZero(res);
	    	}
	    } else if (func.equals(WDisplayFilter.FILL_LEFT_ZERO)) {
	      res = WDisplayFilter.fillLeftZero(res, length);
	    } else {
	      return null;
	    }
	  }
	  return res;
	}
	public static synchronized String filterAndTrimAndFillSubValue(String curResult, String value, String func, String range, String separator, int length) {
	  String res = WDisplayFilter.filterAndTrimSubValue(curResult, value, func, range, length);
	  if (res != null && res.length() > 0) {
	    res = res + separator;
	  } 
	  if ((res != null && res.length() == 0) 
	   && (func.equals(WDisplayFilter.TRIM_LEFT_ZERO))
	   && (separator != null && separator.length() > 0)) {
		  switch (separator.charAt(0)) {
			case ',':
				  if (curResult.length() > 0) {
					  for (int i = 0; i < getRangeLength(range); i++) res = res + "0";
					  res = res + separator;
				  } else {
					  res = "0" + separator;
				  }
				  break;
			case '.':
				  if (curResult.length() > 0) {
					  for (int i = 0; i < getRangeLength(range); i++) res = res + "0";
					  res = res + separator;
				  }
				  break;
			default:
				  break;
		  }
	  }
	  return res;
	}
	public static synchronized String trimLeftZero(String value) {
    int len = value.length();
    int i;
    for (i=0;i<len;i++) {
      if (value.charAt(i) != '0') break;
    }
	  return value.substring(i, len);
	}
	public static synchronized String fillLeftZero(String value, int length) {
    int len = value.length();
	  StringBuffer sb = new StringBuffer(length);
	  int i;
	  for (i=0;i<length-len;i++) {
	    sb.append('0');
	  }
	  if (len > 0) {
	    sb.replace(i, length, value);
		} 
    return sb.toString();
	}
	public static synchronized boolean firstSyntaxCheck(String pattern, char format) {
	  char c = pattern.charAt(0);
	  String s = pattern.substring(0,pattern.indexOf("("));
	  switch (format) {
	    case IWAttribute.FRM_DEC:
	    case IWAttribute.FRM_NUM:
    	  if ((c != '(') || (c == 'N')) return true;
	    case IWAttribute.FRM_CHAR:
    	  if (c == '(') return true;
	  }
 	  return false;
	}
	public static String translateKIDtoWAX(String pattern) {
	  String s = pattern.replaceAll(",", ";");
	  s = s.replaceAll("#", ",");
	  s = s.replaceAll(";,", ";");
	  s = s.replaceAll("-,", "-");
	  s = s.replaceAll(";;", ";,");
	  return s;
  }
	/*
	 * *************************************************************************************
	 * countFiller-Mehtode
	 * *************************************************************************************
	 */
	public static synchronized int countFiller(String pattern, char format) {
		int res = 0;
		if ((pattern == null) || (pattern.length() == 0)) return -1;

		String token1 = null;
		String token2 = null;
		String token3 = null;
		if (firstSyntaxCheck(pattern, format) == false) return -1;
		String workPattern = WDisplayFilter.translateKIDtoWAX(pattern);
		workPattern = workPattern.substring(workPattern.indexOf("("));
		StringTokenizer st = new StringTokenizer(workPattern, ")");
		String del = ";";
		while(st.hasMoreTokens()) {
			String singlePattern = st.nextToken().substring(1);
			StringTokenizer st2 = new StringTokenizer(singlePattern, del);
			switch (st2.countTokens()) {
				case 0:
					return -1;
				case 1:
				  //Es ist nur ein Intervall definiert, keine TrimFunktion, keine F�llzeichen
					if ((singlePattern.charAt(0) == ';') 
					 && (singlePattern.charAt(1) == ';')) {
						return -1;
					} else {
						token1 = st2.nextToken();
						break;
					}
				case 2:
					//Es sind zwei Intervalle definiert, keine TrimFunktion, aber F�llzeichen
					if (singlePattern.charAt(0) == ';') {
						token1 = st2.nextToken();
						token2 = st2.nextToken();
						if (token2 != null) {
							res = res + token2.length();
							break;
						} else {
							return -1;
						}
					//Es sind zwei Intervalle definiert, TrimFunktion, keine F�llzeichen
					} else {
						// do nothing;
						break;
					}
				case 3:
					//Es sind drei Intervalle definiert, TrimFunktion und F�llzeichen
					if ((format == IWAttribute.FRM_DEC)
					 || (format == IWAttribute.FRM_NUM)) {
							token1 = st2.nextToken();
							token2 = st2.nextToken();
							token3 = st2.nextToken();
						 if (token3 != null) {
							 res = res + token3.length();
							 break;
						 } else {
							 return -1;
						 }
					 } else {
						 token1 = st2.nextToken();
					 	 token2 = st2.nextToken();
						 token3 = st2.nextToken();
						 break;
					 }
					default:
						return -1;
			}
		}
		return res;
	}
	/*
	 * *************************************************************************************
	 * unfilter-Mehtodes
	 * ACHTUNG: noch nicht vollst�ndig implementiert, da nicht z.Zt. ben�tigt.
	 * *************************************************************************************
	 */
	public static synchronized String unfilterValue(String value, String pattern, char format, IWAttribute attrib) {
		String res = "";
		if ((value == null) || (value.length() == 0)) return attrib.getDefaultValue();

		//Bei DecimalValues ist darauf zu achten, ob der Ausschnitt aus dem Value vor oder nach dem Komma geschehen soll
		//Grund: Im alten KIDFormat gab es kein Komma
		//Daher sind die Displayfilter auf die rein numerischen Werte hin erstellt worden
		//In Wax werden Dezimalwerte aber mit Komma im DataModel gehalten. Dieses wird hier ausgeschnitten
		if (format == IWAttribute.FRM_DEC){
		  	int pos = value.indexOf(',');
		  	if (pos > -1) {
				StringBuffer buf = new StringBuffer(value.length()-1);
				buf.append('0');
				buf.append(value.substring(0, pos));
				buf.append(value.substring(pos+1));
				value = buf.toString();
		  	}
		  	pos = value.indexOf('+');
		  	if (pos > -1) {
				StringBuffer buf = new StringBuffer(value.length()-1);
				buf.append('0');
				buf.append(value.substring(0, pos));
				buf.append(value.substring(pos+1));
				value = buf.toString();
		  	}
		  	pos = value.indexOf('-');
		  	if (pos > -1) {
				StringBuffer buf = new StringBuffer(value.length()-1);
				buf.append('0');
				buf.append(value.substring(0, pos));
				buf.append(value.substring(pos+1));
				value = buf.toString();
		  	}
		}

		String func = "";
		String range = "";
		String separator = "";
		String token1 = null;
		String token2 = null;
		String token3 = null;
		if (firstSyntaxCheck(pattern, format) == false) return null;
		String workPattern = WDisplayFilter.translateKIDtoWAX(pattern);
		workPattern = workPattern.substring(workPattern.indexOf("("));
		StringTokenizer st = new StringTokenizer(workPattern, ")");
		String del = ";";
		int length = 0;
		while(st.hasMoreTokens()) {
			String singlePattern = st.nextToken().substring(1);
			StringTokenizer st2 = new StringTokenizer(singlePattern, ";");
			switch (st2.countTokens()) {
				case 0:
					return null;
				case 1:
					if ((singlePattern.charAt(0) == ';') 
					 && (singlePattern.charAt(1) == ';')) {
						return null;
					} else {
						token1 = st2.nextToken();
						String s = WDisplayFilter.unfilterSubValue(value, token1, 0);
						if (s != null) {
							res = res + s;
							break;
						} else {
							return null;
						}
					}
				case 2:
					if (singlePattern.charAt(0) == ';') {
						token1 = st2.nextToken();
						token2 = st2.nextToken();
						String s = WDisplayFilter.unfilterSubValue(value, token1, token2, 0);
						if (s != null) {
							res = res + s;
							break;
						} else {
							return null;
						}
					} else {
						if ((format == IWAttribute.FRM_DEC)
						 || (format == IWAttribute.FRM_NUM)) {
							token1 = st2.nextToken();
							token2 = st2.nextToken();
							String s = WDisplayFilter.unfilterSubNumericValue(value, token1, token2, length, 0);
							if (s != null) {
								res = res + s;
								break;
							} else {
								return null;
							}
						} else {
							return null;
						}
					}
				case 3:
				if ((format == IWAttribute.FRM_DEC)
				 || (format == IWAttribute.FRM_NUM)) {
						token1 = st2.nextToken();
						token2 = st2.nextToken();
						token3 = st2.nextToken();
						String s = WDisplayFilter.unfilterSubValue(value, token1, token2, token3, length, 0);
						if (s != null) {
							res = res + s;
							break;
						} else {
							return null;
						}
					} else {
						return null;
					}
				default:
					return null;
			}
		}
		if ((format == IWAttribute.FRM_DEC) 
		 && (pattern.startsWith(WDisplayFilter.NO_SIGN) == false)) {
			switch (value.charAt(0)) {
				case '+':
					res = "+" + res;
					break;
				case '-':
					res = "-" + res;
					break;
				default:
					res = "+" + res;
			}
		}
		return res;
	}
	public static synchronized String unfilterSubValue(String value, String range, int startpos) {
		int i = range.indexOf("-");
		if (i == 0) return null;
		if (i > 0) {
			StringTokenizer st = new StringTokenizer(range, "-");
			if (st.countTokens() != 2) return null;
			int from, to;
			try {
				from = Integer.parseInt(st.nextToken())-1;
			} catch (NumberFormatException e) {
				return null;
			}
			try {
				to = Integer.parseInt(st.nextToken())-1;
			} catch (NumberFormatException e) {
				return null;
			}
			if (from >= to) return null;
			return value.substring(startpos, startpos+(to+1-from));
		} else {
			i = range.indexOf(",");
			if (i == 0) return null;
			if (i > 0) {
				StringTokenizer st = new StringTokenizer(range, ",");
				String res = "";
				while(st.hasMoreTokens()) {
					try {
						int ii = Integer.parseInt(st.nextToken())-1;
						res = res + value.charAt(startpos+ii);
					} catch (NumberFormatException e) {
						return null;
					}
				}
				return res;
			} else {
				try {
					return ""+value.charAt(startpos);
				} catch (NumberFormatException e) {
					return null;
				}
			}
		}
	}
	public static synchronized String unfilterSubValue(String value, String range, String separator, int startpos) {
		String res = WDisplayFilter.unfilterSubValue(value, range, startpos);
		if (!(value.substring(startpos+res.length()).startsWith(separator))) {
			return null;
		} else {
			return res;
		}
	}
	public static synchronized String unfilterSubNumericValue(String value, String func, String range, int length, int startpos) {
		String res = WDisplayFilter.unfilterSubValue(value, range, startpos);
		if (res != null) {
			if (func.equals(WDisplayFilter.TRIM_LEFT_ZERO)) {
				res = WDisplayFilter.trimLeftZero(res);
				//Wenn das trim alle Nullen abgeschnitten hat, muss wieder eine hinzugef�gt werden
				// Nur so k�nnen f�hrende Nullen bei Dezimalwerten gehalten werden
				if (("").equals(res)) res = "0";
			} else if (func.equals(WDisplayFilter.FILL_LEFT_ZERO)) {
				res = WDisplayFilter.fillLeftZero(res, length);
			} else {
				return null;
			}
		}
		return res;
	}
	public static synchronized String unfilterSubValue(String value, String func, String range, String separator, int length, int startpos) {
		String res = WDisplayFilter.unfilterSubNumericValue(value, func, range, length, startpos);
		if (!(value.substring(startpos+res.length()).startsWith(separator))) {
			return null;
		} else {
			return res;
		}
	}
	public static int getRangeLength(String range) {
		  int i = range.indexOf("-");
		  if (i == 0) return 0;
		  if (i > 0) {
			  StringTokenizer st = new StringTokenizer(range, "-");
			  if (st.countTokens() != 2) return -1;
			  int from, to;
		    try {
	  		  from = Integer.parseInt(st.nextToken())-1;
		    } catch (NumberFormatException e) {
		      return -1;
		    }
		    try {
	  		  to = Integer.parseInt(st.nextToken())-1;
		    } catch (NumberFormatException e) {
		      return -1;
		    }
			  if (from >= to) return -1;
			  return to-from+1;
		  } else {
			  i = range.indexOf(",");
			  if (i == 0) return -1;
			  if (i > 0) {
				  StringTokenizer st = new StringTokenizer(range, ",");
				  int count=0;
				  while(st.hasMoreTokens()) {
				    try {
					    Integer.parseInt(st.nextToken());
					    count++;
				    } catch (NumberFormatException e) {
				      return -1;
				    }
				  }
				  return count;
			  } else {
			    try {
			      Integer.parseInt(range);
			      return 1;
			    } catch (NumberFormatException e) {
			      return -1;
			    }
			  }
		  }
	}
	/*
	public static void main(String[] args) {
	  WAttribute a = new WAttribute("TEST1", IWAttribute.FRM_CHAR, 10, 0, false);
	  String pattern = "(,#1-#3, )(,#4-#6, )(,#7#8,)";
	  WDisplayFilter af = new WDisplayFilter(pattern, a, false);
	  String value = "20690500";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));

	  a = new WAttribute("TEST1", IWAttribute.FRM_DEC, 10, 2, false);
	  pattern = "NoSign(TrimLeftZero,#1-#2,.)(TrimLeftZero,#3-#5,.)(TrimLeftZero,#6-#8,,)(,#9#10,)";
	  af = new WDisplayFilter(pattern, a, false);
	  value = "0120030000-";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "0102003000";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "0100200300";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "3000000000";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "0300000000";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "0030000000";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "0003000000";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "0000300000";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "0000030000";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "0000000300";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "0000000030";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "0000000003";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));

	  pattern = "(TrimLeftZero,#1-#2,.)(TrimLeftZero,#3-#5,.)(TrimLeftZero,#6-#8,,)(,#9#10,)";
	  af = new WDisplayFilter(pattern, a, false);
	  value = "0120030000-";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "0102003000";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "0100200300";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "3000000000";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "0300000000";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "0030000000";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "0003000000";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "0000300000";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "0000030000";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "0000000300";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "0000000030";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "0000000003";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));

	  a = new WAttribute("TEST1", IWAttribute.FRM_DEC, 8, 2, false);
	  pattern = "NoSign(TrimLeftZero,#1-#3,.)(TrimLeftZero,#4-#6,,)(,#7#8,)";
	  af = new WDisplayFilter(pattern, a, false);
	  value = "20030000-";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "02003000";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "00200300";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "30000000";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "03000000";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "00300000";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "00030000";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "00000300";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "00000030";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "00000003";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));

	  pattern = "(TrimLeftZero,#1-#3,.)(TrimLeftZero,#4-#6,,)(,#7#8,)";
	  af = new WDisplayFilter(pattern, a, false);
	  value = "20030000-";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "02003000";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "00200300";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "30000000";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "03000000";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "00300000";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "00030000";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "00000300";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "00000030";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "00000003";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));

	  a = new WAttribute("TEST1", IWAttribute.FRM_DEC, 10, 2, true);
	  pattern = "NoSign(TrimLeftZero,#1-#2,.)(TrimLeftZero,#3-#5,.)(TrimLeftZero,#6-#8,,)(,#9#10,)";
	  af = new WDisplayFilter(pattern, a, false);
	  value = "0120030000";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "0102003000";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "0100200300";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));

	  pattern = "(TrimLeftZero,#1-#2,.)(TrimLeftZero,#3-#5,.)(TrimLeftZero,#6-#8,)";
	  af = new WDisplayFilter(pattern, new WAttribute("TEST1", IWAttribute.FRM_NUM, 8, 0, false), false);
	  value = "20000000";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "02000000";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "00200000";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "00020000";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "00002000";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "00000200";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "00000020";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "00000002";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "00002001";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "00002002";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));

	  pattern = "(TrimLeftZero,#1-#3,.)(TrimLeftZero,#4-#6,)";
	  af = new WDisplayFilter(pattern, new WAttribute("TEST1", IWAttribute.FRM_NUM, 6, 0, false), false);
	  value = "200000";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "020000";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "002000";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "000200";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "000020";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "000002";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "002001";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	  value = "002002";
	  System.out.println(value +" -> " + pattern + " = "+af.getFormattedValue(value));
	}
	*/
}

