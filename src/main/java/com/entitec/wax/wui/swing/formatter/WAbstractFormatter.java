package com.entitec.wax.wui.swing.formatter;

import java.io.Serializable;

import javax.swing.SwingConstants;

import com.entitec.wax.wdm.IWAttribute;
import com.entitec.wax.wdm.IWDataItem;
import com.entitec.wax.wdm.WAttribute;
import com.entitec.wax.wdm.WDMTypChecker;
import com.entitec.wax.wglobal.WRuntimeException;

//public abstract class WAbstractFormatter extends AbstractFormatter implements Serializable {
public abstract class WAbstractFormatter implements Serializable {
	private static final long serialVersionUID = 1L;
	protected static final String dummy = "DUMMY";
	protected IWAttribute mAttribute;
	
	private int mChangedLen = -1;
	
  public WAbstractFormatter(IWAttribute a) {
		mAttribute = new WAttribute(a.getName(), a.getFrm(), a.getLen(), a.getDec(), a.getSign());
	}


  public abstract boolean    checkValue(String value);
  public abstract String     fillupValue(String value);
  public abstract IWDataItem getFormattedValue(String value);
  public abstract String     getUnformattedValue(String value);
  
  public int getHorizontalAlignment() {
		return SwingConstants.LEFT;
  }
  public final char getFrm() {
		return mAttribute.getFrm();
  }
  public final boolean getSigned() {
    return mAttribute.getSign();
  }
  public final int getDez() {
    return mAttribute.getDec();
  }
  public final int getLength() {
    return mAttribute.getLen();
  }
	public int getMaxFormattedLength() {
		if ( mChangedLen == -1 ) 
			return getLength();
		else
			return mChangedLen;
		
	}
  public int getColumns() {
    return getLength();
  }
  public final void setChangedLength(int len) {
  	mChangedLen = len;
  }
  public void setDec(int dec) {
	  mAttribute.setDec(dec);
  }
  public void setSign(boolean sign) {
	  mAttribute.setSign(sign);
  }
  public boolean isEqual(String a, String b) {
		if ( (a==null) || (b==null) ) {
		  if ( (a==null) && (b==null) ) return true;
		  if ( (a==null) && b.equals("") ) return true;
		  if ( (b==null) && a.equals("") ) return true;
		  return false;
		} else {
			return a.equals(b);
		}
  }
  public String tranformCharacterOfValue(String s) { 
  	return s;
  }
 
	protected boolean isValueAttributeConform(String value) {
		if (value.length() == 0) return true;
		String filledValue = fillupValue(value);
		if (filledValue == null) return false;

		String unformattedValue = this.getUnformattedValue(filledValue);
		if (unformattedValue == null) return false;

		switch(mAttribute.getFrm()) {
			case IWAttribute.FRM_CHAR:
				return WDMTypChecker.checkCharacter(mAttribute.getLen(), unformattedValue) != null; 
			case IWAttribute.FRM_NUM:
				return WDMTypChecker.checkNumeric(mAttribute.getLen(), unformattedValue) != null; 
			case IWAttribute.FRM_DEC:
				return WDMTypChecker.checkDecimal(mAttribute.getLen(), 
																					 mAttribute.getDec(),
																					 mAttribute.getSign(),
																					 unformattedValue) != null; 
			default:
				throw new WRuntimeException("Value is not Attribute conform.");
		}
	}
	
	/**
	 * Wenn die Eingabe entsprechend dem Format vollst�ndig ist, 
	 * wird true f�r die Focusweitergabe zur�ckgegeben.
	 * 
	 * @return
	 */
	public abstract boolean hasInputFinishedAndCanTextControlBeLeft(String value);

	/**
	 * Vergleich mit einem anderen Formatter.
	 * Wenn der aktuelle Formatter mit dem �ber 'formatter' �bergebenen 
	 * in der konkreten Klasse, dem AttributFormat, der L�nge, 
	 * der Anzahl Dezimalstellen und im Vorzeichen �bereinstimmt, 
	 * dann liefert die Methode true, 
	 * sonst false.
	 * 
	 * @param formatter    Formatter, der auf Gleichheit gepr�ft werden soll.
	 * @return             true, wenn gleich, sonst false.
	 */
	public boolean isEqualTo(WAbstractFormatter formatter) {
		return getClass().isInstance(formatter)
			&& mAttribute.getFrm() == formatter.getFrm() 
			&& mAttribute.getLen() == formatter.getLength() 
			&& mAttribute.getDec() == formatter.getDez() 
			&& mAttribute.getSign() == formatter.getSigned();
	}
}



