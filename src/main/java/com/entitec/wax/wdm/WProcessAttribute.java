package com.entitec.wax.wdm;

import java.util.Enumeration;
import java.util.Hashtable;

public class WProcessAttribute extends WMaskAttribute implements IWMaskAttribute{
	private Hashtable mProperties;
	public static final String ATTRIBUTE_DEFAULT = "default";
	public static final String ATTRIBUTE_GBL = "gbl";
	public static final String ATTRIBUTE_SES = "ses";

// Ohne zusätzliche Attribute
public WProcessAttribute(String name, String alias, char frm,
					int len, int dec, boolean sign) {
	super(name, alias, frm, len, dec, sign, "", IWMaskAttribute.FMT_DEFAULT, IWMaskAttribute.DEFAULT);
}
// Attribut zur PM-Nachricht

public WProcessAttribute(String name, String alias, char frm,
					int len, int dec, boolean sign, 
					int mode, String gbl, String ses) {
	super(name, alias, frm, len, dec, sign, "", IWMaskAttribute.FMT_DEFAULT, mode);
	setProperty(ATTRIBUTE_GBL, gbl);
	setProperty(ATTRIBUTE_SES, ses);
}
/**
 * WProcessAttribute - Attribut mit Default (Request des Processes)
 */
public WProcessAttribute(String name, String alias, char frm,
					int len, int dec, boolean sign, String def) {
	super(name, alias, frm, len, dec, sign, "", IWMaskAttribute.FMT_DEFAULT, IWMaskAttribute.DEFAULT);
	setProperty(ATTRIBUTE_DEFAULT, def);
}
// FKT-Nachricht/ VG-VG-Nachricht / VG-Nachricht

public WProcessAttribute(String name, String alias, char frm,
					int len, int dec, boolean sign, 
					String gbl, String ses) {
	super(name, alias, frm, len, dec, sign, "", IWMaskAttribute.FMT_DEFAULT, IWMaskAttribute.DEFAULT);
	setProperty(ATTRIBUTE_GBL, gbl);
	setProperty(ATTRIBUTE_SES, ses);
}

public WProcessAttribute(String name, String alias, char frm,
	int len, int dec, boolean sign, String def, String gbl, String ses) {
	super(name, alias, frm, len, dec, sign, "", IWMaskAttribute.FMT_DEFAULT, IWMaskAttribute.DEFAULT);
	setProperty(ATTRIBUTE_GBL, gbl);
	setProperty(ATTRIBUTE_SES, ses);
	setProperty(ATTRIBUTE_DEFAULT, def);
}
/**
 * 
 */
public String getProperty(String key) {
	if (mProperties == null) {
		mProperties = new Hashtable();
	}
	return (String)mProperties.get(key);
}
/**
 * 
 */
public Enumeration properties() {
	if (mProperties == null) {
		mProperties = new Hashtable();
	}
	return mProperties.elements();
}
/**
 * 
 */
public void setProperty(String key, String value) {
	if (mProperties == null) {
		mProperties = new Hashtable();
	}
	mProperties.put(key, value);
}
}
