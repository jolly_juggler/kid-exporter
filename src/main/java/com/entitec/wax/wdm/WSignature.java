package com.entitec.wax.wdm;

import java.util.Enumeration;
import java.util.Vector;

import com.entitec.wax.wglobal.WRuntimeException;


/**
 * Date:    03.07.2003
 * <p>Description:</p>
 * Die Klasse WSignature enth�lt die Spaltendefinitionen eines WDataModels. 
 * Je Spalte enth�lt eine WSignature ein WAttribute. Das WAttriubte definiert 
 * die Eigenschaften der IWDataItems in der Spalte des WDataModels.
 * @author AB0043

 *  * @see com.entitec.wax.wdm.WAttriubte
 */
public class WSignature implements IWSignature {
	/*
	 * Vector in dem die WAttriubte der WSignature gehalten werden.
	 */
  protected Vector mAttributes = new Vector();
	/*
	 * Vector in dem die Namen der WAttriubte der WSignature gehalten werden.
	 */
  protected Vector mNames = new Vector();
	/*
	 * Vector in dem die Aliase der WAttriubte der WSignature gehalten werden.
	 */
  protected Vector mAlias = new Vector();
	/*
	 * Schalter, �ber den gesteuert wird, ob beim Zugriff auf ein IWAttriubte 
	 * mit dem Namen oder mit dem Alias gelesen wird. 
	 */
  protected boolean mAliasIsName = false;
	/*
	 * Schalter, �ber den gesteuert wird, ob der WSignature noch IWAttribute 
	 * hinzugef�gt werden k�nnen. Wenn das unterliegende WDataModel bereits 
	 * Daten enth�lt ist dies nicht mehr m�glich. 
	 * In diesem Fall w�re die Methode addAttribute() im WDataModelWriter 
	 * zu verwenden. 
	 */
	protected boolean mIsInitialized = false;
	/*
	 * Variable, die die Summe der L�ngen alle in der WSignature enthaltenen
	 * WAttribute enth�lt. 
	 */
  protected int mDataLength = 0;
	/**
	 * Konstruiert eine WSignature.
	 */
  public WSignature() {
  	super();
  }
  /**
   * Erweitert die WSignature um eine Spalte durch hinzuf�gen 
   * eines IWAttributes zur IWSignature.
   * ACHTUNG:
   * Das Erweitern ist nur m�glich, wenn das unterliegende 
   * WDataModel noch keine Daten enth�lt.
   * @param attrib
   * @throws WDublicateAttributeException
   */
  public synchronized void addAttribute(IWAttribute a) {
		if (mIsInitialized == false) {
			String name = a.getName();
			String alias = a.getAlias();
			if (mNames.contains(name)) {
//�nderung, AB0043: Doppelte Aliase sind erlaubt, da mehrfach Zuordnung im 
//Vorgangsstep beim OutView m�glich.
//			if (mNames.contains(name) || mAlias.contains(alias)) {
				throw new WDublicateAttributeException(
					"Name: " + name + " / Alias: " + alias);
			} else {
				mNames.addElement(name);
				mAlias.addElement(alias);
				mAttributes.addElement(a);
				switch (a.getFrm()) {
					case IWAttribute.FRM_CHAR :
						mDataLength = mDataLength + a.getLen();
						break;
					case IWAttribute.FRM_NUM :
						mDataLength = mDataLength + a.getLen();
						break;
					case IWAttribute.FRM_DEC :
						if (a.getSign() == true) {
							if (a.getDec() > 0) {
								mDataLength = mDataLength + a.getLen() + 2;
							} else {
								mDataLength = mDataLength + a.getLen() + 1;
							}
						} else {
							if (a.getDec() > 0) {
								mDataLength = mDataLength + a.getLen() + 1;
							} else {
								mDataLength = mDataLength + a.getLen();
							}
						}
						break;
				}
			}
		} else {
			throw new WRuntimeException("AddAttribute not allowed because WDataModel has data.");
		}
  }
  /** 
   * Liefert eine Aufz�hlung der IWAttribute der WSignature.
   * @return
   */
  public Enumeration attributes() {
		return mAttributes.elements();
  }
  /**
   * Liefert die Anzahl der IWAttribute der WSignature.
   * @return
   */
  public int cols() {
  	return mAttributes.size();
  }  
  /**
   * Pr�ft, ob die WSignature mit dem Alias auf die IWAttribute beim
   * Lesen zu greift.
   * @return
   */
  public boolean getAliasIsName() {
		return mAliasIsName;
  }
	/**
	 * Liefert eine Aufz�hlung der IWAttribute deren Alias = 'alias' ist.
	 * @param alias
	 * @return Aufz�hlung der IWAttribute-Namen deren Alias =  'alias' ist.
	 */
  public Enumeration getNamesForAlias(String alias) {
  	Vector v = new Vector();
  	for (Enumeration e=this.attributes();e.hasMoreElements();) {
  		IWAttribute a = (IWAttribute)e.nextElement();
  		if (a.getAlias().equals(alias)) {
  			v.addElement(a.getName());
  		}
  	}
		return v.elements();
  }      
	/**
	 * Liefert das IWAttribute aus der Spalte mit dem Namen 'name' aus
	 * der WSignature.
	 * @param name
	 * @return
	 */
	public IWAttribute getAttribute(String name) {
		return (IWAttribute)mAttributes.elementAt(indexOf(name));
	}      
	/**
	 * Liefert den Namen des IWAttributes aus der Spalte mit dem Index 'col' 
	 * aus der WSignature.
	 * @param col
	 * @throws WAttributeNotFoundException
	 * @return
	 */
  public String getAttributeName(int col) {
		if ((col < 0) || (col > mAttributes.size()-1)) {
		  throw new WAttributeNotFoundException("Attribut-Index: "+col);
		} else {
	      if (mAliasIsName) {
	        return new String((String)mAlias.elementAt(col));
	      } else {
			return new String((String)mNames.elementAt(col));
	      }
		}
  }
	/**
	 * Liefert die L�nge des Datenbereichs (Summe der IWAttribute L�ngen)
	 * der WSignature.
	 * @return
	 */
  public int getDataLength() {
  	return mDataLength;
  }
  /**
   * Pr�ft, ob die WSignature das IWAttribute mit dem Namen 'name' enth�lt.
   * @param name
   * @return
   */
  public boolean hasAttribute(String name) {
		if (mAliasIsName) {
	      return mAlias.contains(name);
		} else {
      return mNames.contains(name);
    }
  }
	/**
	 * Pr�ft, ob die WSignature IWAttribute enth�lt.
	 * @return
	 */
  public boolean hasAttributes() {
		return mAttributes.size() > 0;
  }  
  /**
   * Liefert den Index des IWAttributes mit dem Namen 'name'.
   * @param name
   * @throws WAttributeNotFoundException
   * @return
   */
  public int indexOf(String name){
  	return indexOf(name, 0);
  }
  public int indexOf(String name, int idx) {
		if (mAliasIsName) {
      if (!mAlias.contains(name)) {
        throw new WAttributeNotFoundException("Attribut: "+name);
      } else {
        return mAlias.indexOf(name, idx);
      }
		} else {
      if (!mNames.contains(name)) {
        throw new WAttributeNotFoundException("Attribut: "+name);
      } else {
        return mNames.indexOf(name, idx);
      }
		}
  }  
	/** 
	 * Liefert eine Aufz�hlung der Namen der IWAttribute in Abh�ngigkeit
	 * von dem Schalter mAliasIsName.
	 * @return
	 */
  public Enumeration keys() {
		if (mAliasIsName) {
      return mAlias.elements();
		} else {
      return mNames.elements();
    }
  }  
  /**
   * Entfernt aus der WSignature das IWAttribute mit dem Namen 'name'
   * @param name
   */
	protected synchronized void removeAttribute(String name) {
  	int i = indexOf(name);
  	IWAttribute a = (IWAttribute)mAttributes.elementAt(i);
		switch (a.getFrm()) {
			case IWAttribute.FRM_CHAR:
		    mDataLength = mDataLength - a.getLen();
		    break;
			case IWAttribute.FRM_NUM:
		    mDataLength = mDataLength - a.getLen();
		    break;
			case IWAttribute.FRM_DEC:
				if (a.getSign() == true) {
					if (a.getDec() > 0) {
				    mDataLength = mDataLength - a.getLen() - 2;
					} else {
				    mDataLength = mDataLength - a.getLen() - 1;
					}
				} else {
					if (a.getDec() > 0) {
				    mDataLength = mDataLength - a.getLen() - 1;
					} else {
				    mDataLength = mDataLength - a.getLen();
					}
				}
				break;
		}
		mAttributes.removeElementAt(i);
		mNames.removeElement(name);
		mAlias.removeElement(name);
  }  
  /**
   * Setzt den Schalter mAliasIsName auf 'true' oder 'false'
	 * Der Schalter steuert, ob beim Zugriff auf ein IWAttriubte 
	 * mit dem Namen oder mit dem Alias gelesen wird. 
	 * @param b
   */
  public void setAliasIsName(boolean b) {
		mAliasIsName = b;
  }
	protected WAttribute getIndexAttribute() {
		if (this.hasAttribute(INDEX_COLUMN_NAME)) {
			throw new WRuntimeException("Fatal-Error: Index-Column already exists.");
		} else {
			return new WAttribute(INDEX_COLUMN_NAME,IWAttribute.FRM_NUM,8,0,false);
		}
	}
	/**
	 * R�ckgabe der WSignature als String.
	 * @return
	 */
  public String toString() {
  	StringBuffer sb = new StringBuffer(this.getStringSize(0));
  	this.toString(sb, 0);
		return sb.toString();
  }  
	/**
	 * R�ckgabe der WSignature als String mit dem Einr�ckungslevel 'level'.
	 * @param level
	 * @return
	 */
  public void toString(StringBuffer sb, int level) {
  	String tab = "    ";
    char[] c = new char[8*level];
    for (int i=0;i<8*level;i++) { c[i] = ' '; }
    String filler = new String(c);
  	sb.append(filler);
		sb.append("  <WSIGNATURE AliasIsName=\"");
		sb.append(mAliasIsName);
		sb.append("\">\n");
		Enumeration e = this.attributes();
		while (e.hasMoreElements()) {
		  IWAttribute a = (IWAttribute)e.nextElement();
			sb.append(filler);
			sb.append(tab);
			sb.append(a.toString());
		}
		sb.append("  </WSIGNATURE>\n");
  }  
	public int getStringSize(int level){
		int colCount = this.cols();
		int anzZeilen = 2+colCount;
		int offsetForHierarchyLevel = 8*level*anzZeilen;
		int offsetForSignature = 30;
		int attributeStringSize = 120*colCount;
		int offsetForCrLf = (2+colCount)*2;
		int size =  offsetForHierarchyLevel+
								offsetForSignature +
								attributeStringSize +
								offsetForCrLf;
		return size;
	}
}
