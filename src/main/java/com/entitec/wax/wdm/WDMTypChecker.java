package com.entitec.wax.wdm;

import com.entitec.wax.wglobal.WRuntimeException;



public final class WDMTypChecker {
	private static final String DECIMAL_SIGN_DOT = ".";
	private static final String DECIMAL_SIGN_COMMA = ",";
	private static final String ZERO = "0";

	public static synchronized String getTypConformValue(
		IWAttribute a,
		String value) {
		switch (a.getFrm()) {
			case IWAttribute.FRM_CHAR :
			case IWAttribute.FRM_ADDR:
			case 'Z':
				return checkCharacter(a.getLen(), value);
			case IWAttribute.FRM_NUM :
				return checkNumeric(a.getLen(), value);
			case IWAttribute.FRM_DEC :
				return checkDecimal(a.getLen(), a.getDec(), a.getSign(), value);
			default :
				return null;
		}
	}
	
	public static synchronized String getTypConformFilledValue(
		IWAttribute a,
		String value) {
		switch (a.getFrm()) {
			case IWAttribute.FRM_CHAR :
			case IWAttribute.FRM_ADDR:
			case 'Z':
				return fillupCharRight(a.getLen(), checkCharacter(a.getLen(), value));
			case IWAttribute.FRM_NUM :
				return checkNumeric(a.getLen(), value);
			case IWAttribute.FRM_DEC :
				return checkDecimal(a.getLen(), a.getDec(), a.getSign(), value);
			default :
				return null;
		}
	}
	/**
	 * Transformiert einen value in einen g�ltige Wert eines Kid-Typs Character mit der 
	 * L�nge len (nicht rechtsb�ndig mit spaces aufgef�llt).
	 * 
	 * @param len
	 * @param value
	 * @return
	 */
	public static String checkCharacter(int len, String value) {
		String v = WDMTypChecker.trimCharRight(value);
		if ( v.length() > len ){
				return null;
		}else{
			return v;
		}
	}
	
	public static String checkNumeric(int len, String value) {
		String num = WDMTypChecker.getNumeric(value);
		if ( num == null ) return null;
		num = WDMTypChecker.fillupNumericLeft(len, num);
		if ((num == null) || (num.length() > len)) {
			return null;
		} else {
			return num;
		}
	}
	
	public static String truncateNumeric(int len, String value){
		if ( value.length() > len )
			return value.substring(0, len);
		else
			return value;
	}
	
	public static String truncateCharacter(int len, String value){
		if ( value.length() > len )
			return value.substring(0, len);
		else
			return value;
	}
	public static IWDataItem transformDecimal(int fromLen, int fromInt, int fromDec, boolean fromSign, 
	                                     int toLen, int toInt, int toDec, boolean toSign, 
	                                     String value){
		WDataItem di = new WDataItem();
		di.setState(IWDataItem.ENABLED);
		String res = null;
		String fromIntValue = value.substring(0, fromInt);
		String fromDecValue = null;
		String toIntValue = null;
		String toDecValue = null;
		char toSignChar = '+';
		if (fromDec > 0) fromDecValue = value.substring(fromInt+1, fromInt+1+fromDec); else fromDecValue = "";
		if (fromSign == true) toSignChar = value.charAt(value.length()-1); 
		//IntValue bestimmen
		if (toInt == fromInt) {
			toIntValue = fromIntValue;
		} else if (toInt > fromInt) {
			toIntValue = WDMTypChecker.fillupNumericLeft(toInt, fromIntValue);
		} else {
			toIntValue = WDMTypChecker.getNumeric(fromIntValue);
			if (toIntValue.length() > toInt) {
				toIntValue = toIntValue.substring(0, toInt);
				di.setState(IWDataItem.TRUNCATED);
			} else {
				toIntValue = WDMTypChecker.fillupNumericLeft(toInt, toIntValue);
			}
		}
		//DecValue bestimmen
		if (toDec == fromDec) {
			toDecValue = fromDecValue;
		} else if (toDec > fromDec) {
			toDecValue = WDMTypChecker.fillupNumericRight(toDec, fromDecValue);
		} else {
			toDecValue = WDMTypChecker.trimNumericRight(fromDecValue);
			if (toDecValue.length() > toDec) {
				toDecValue = toDecValue.substring(0, toDec);
				di.setState(IWDataItem.TRUNCATED);
			} else {
				toDecValue = WDMTypChecker.fillupNumericRight(toDec, toDecValue);
			}
		}
		//R�ckgabewert zusammenbauen
		StringBuffer sb = null;
		if (toSign == true) {
			if (toDec > 0) {                     //DecimalWithSignWithDec
				sb = new StringBuffer(toLen + 2);
				sb.append(toIntValue); sb.append(DECIMAL_SIGN_COMMA);	sb.append(toDecValue); sb.append(toSignChar);
			} else {                           //DecimalWithSignWithoutDec
				sb = new StringBuffer(toLen + 1);
				sb.append(toIntValue); sb.append(toSignChar);
			}
		} else {
			if (toDec > 0) {                     //DecimalWithoutSignWithDec
				sb = new StringBuffer(toLen + 1);
				sb.append(toIntValue); sb.append(DECIMAL_SIGN_COMMA); sb.append(toDecValue);
			} else {                           //DecimalWithoutSignWithoutDec
				sb = new StringBuffer(toLen);
				sb.append(toIntValue);
			}
		}
		di.setValue(sb.toString());
		return di;
	}
	public static String checkDecimal(int len,
																		int dec,
																		boolean sign,
																		String value) {
		String res = null;
		int decPos = -1;
		int signPos = -1;
		int valueLength = value.length();
		for (int i=0;i<valueLength;i++) {
			if ((value.charAt(i) != '0')
			 && (value.charAt(i) != '1')
			 && (value.charAt(i) != '2')
			 && (value.charAt(i) != '3')
			 && (value.charAt(i) != '4')
			 && (value.charAt(i) != '5')
			 && (value.charAt(i) != '6')
			 && (value.charAt(i) != '7')
			 && (value.charAt(i) != '8')
			 && (value.charAt(i) != '9')
			 && (value.charAt(i) != '+')
			 && (value.charAt(i) != '-')
			 && (value.charAt(i) != ',')
			 && (value.charAt(i) != '.')) return null;
		}
		//Wenn value nur Leerzeichen enth�lt, dann R�ckgabe 0,0+
		if (value.trim().length() == 0) {
			StringBuffer sb = new StringBuffer(len+2);
			for (int i=0;i<len-dec;i++) sb.append('0');
			if (dec > 0) {
				sb.append(DECIMAL_SIGN_COMMA);
				for (int ii=0;ii<dec;ii++) sb.append('0');
			}
			if (sign == true) sb.append('+');
			res = sb.toString();
		} else {
			//Position des Dezimaltrenners ermitteln
			decPos = value.indexOf(DECIMAL_SIGN_COMMA);
			if (decPos == -1)	decPos = value.indexOf(DECIMAL_SIGN_DOT);
			//Position des Vorzeichens ermitteln
			signPos = WDMTypChecker.getSignPos(value);
			if (sign == true) {
				if (dec > 0) { 
					res = checkDecimalWithSignWithDec(len, dec, sign, valueLength, decPos, signPos, value);
				} else {
					res = checkDecimalWithSignWithoutDec(len, dec, sign, valueLength, decPos, signPos, value);
				}
			} else {
				if (dec > 0) { 
					res = checkDecimalWithoutSignWithDec(len, dec, sign, valueLength, decPos, signPos, value);
				} else {
					res = checkDecimalWithoutSignWithoutDec(len, dec, sign, valueLength, decPos, signPos, value);
				}
			}
		}
		return res;
  }
	private static String checkDecimalWithSignWithDec(int len,
																								 int dec,
																								 boolean sign,
																								 int valueLength,
																								 int decPos, 
																								 int signPos,  
																								 String value) {
		char signChar = '+';
		String decValue = null;
		String intValue = null;
		int startPos = 0;
		int endPos = valueLength-1;
		if (decPos == valueLength-1) endPos = decPos-1;

		if (signPos == 0) {
			if (value.charAt(0) == '-') signChar = '-';
			startPos = 1;
		} else if (signPos == valueLength-1) {
			if (value.charAt(valueLength-1) == '-') signChar = '-';
			startPos = 0;
			if (decPos == valueLength-2) { 
				endPos = decPos-1;
			} else {
				endPos = signPos-1;
			}
		} else if (signPos == -1) {
			startPos = 0;
		} else {
			return null;
		}
		if (decPos == valueLength - 1) { // Dezimaltrenner auf letzter Stelle 
			decValue = WDMTypChecker.fillupNumericRight(dec, ZERO);
			intValue = WDMTypChecker.checkNumeric(len-dec, value.substring(startPos, decPos));
		} else if ((decPos == valueLength - 2) && (signPos == valueLength-1)) {// Dezimaltrenner auf vorletzter Stelle und Vorzeichen auf letzter Stelle
			decValue = WDMTypChecker.fillupNumericRight(dec, ZERO);
			intValue = WDMTypChecker.checkNumeric(len-dec, value.substring(startPos, endPos+1));
		} else if (decPos == -1) { // Dezimaltrenner NICHT vorhanden
			decValue = WDMTypChecker.fillupNumericRight(dec, ZERO);
			intValue = WDMTypChecker.checkNumeric(len-dec, value.substring(startPos, endPos+1));
		} else { // Dezimaltrenner nicht auf letzter Stelle
			decValue = WDMTypChecker.checkDecimalPart(dec,value.substring(decPos+1, endPos+1));
			if (decValue == null) return null;
			if (decValue.length() < dec) { //fillup with 0
				decValue = WDMTypChecker.fillupNumericRight(dec, decValue);
			}
			intValue = WDMTypChecker.checkNumeric(len-dec, value.substring(startPos, decPos));
		}
		if (intValue == null)	return null;
		//R�ckgabewert zusammenbauen
		StringBuffer sb = new StringBuffer(len + 2);
		sb.append(intValue);
		sb.append(DECIMAL_SIGN_COMMA);
		sb.append(decValue);
		sb.append(signChar);
		return sb.toString();
	}
	public static String checkDecimalWithSignWithoutDec(int len,
																								 int dec,
																								 boolean sign,
																								 int valueLength,
																								 int decPos, 
																								 int signPos,  
																								 String value) {
		char signChar = '+';
		String decValue = null;
		String intValue = null;
		int startPos = 0;
		int endPos = valueLength-1;
		if (decPos == valueLength-1) endPos = decPos-1;

		if (signPos == 0) {
			if (value.charAt(0) == '-') signChar = '-';
			startPos = 1;
		} else if (signPos == valueLength-1) {
			if (value.charAt(valueLength-1) == '-') signChar = '-';
			startPos = 0;
			if (decPos == valueLength-2) { 
				endPos = decPos-1;
			} else {
				endPos = signPos-1;
			}
		} else if (signPos == -1) {
			startPos = 0;
		} else {
			return null;
		}
		if (decPos == valueLength - 1) { // Dezimaltrenner auf letzter Stelle 
			intValue = WDMTypChecker.checkNumeric(len-dec, value.substring(startPos, decPos));
		} else if ((decPos == valueLength - 2) && (signPos == valueLength-1)) {// Dezimaltrenner auf vorletzter Stelle und Vorzeichen auf letzter Stelle
			intValue = WDMTypChecker.checkNumeric(len-dec, value.substring(startPos, endPos+1));
		} else if (decPos == -1) {// Dezimaltrenner NICHT vorhanden
			intValue = WDMTypChecker.checkNumeric(len-dec, value.substring(startPos, endPos+1));
		} else { // Dezimaltrenner vorhanden
			decValue = WDMTypChecker.getNumeric(value.substring(decPos + 1, endPos+1));
			if (decValue == null)	return null;
			if (!(decValue.equals(ZERO)))	return null;
			intValue = WDMTypChecker.checkNumeric(len-dec,value.substring(startPos, decPos));
		}
		if (intValue == null)	return null;
		//R�ckgabewert zusammenbauen
		StringBuffer sb = new StringBuffer(len + 1);
		sb.append(intValue);
		sb.append(signChar);
		return sb.toString();
	}
	public static String checkDecimalWithoutSignWithDec(int len,
																								 int dec,
																								 boolean sign,
																								 int valueLength,
																								 int decPos, 
																								 int signPos,  
																								 String value) {
		char signChar = '+';
		String decValue = null;
		String intValue = null;
		int startPos = 0;
		int endPos = valueLength-1;

		if (signPos != -1) {
			//Tollerierung von + an Stelle 0, auch wenn laut Format ohne Vorzeichen
			if ((value.charAt(0) == '+') || (value.charAt(0) == ' ')) {
				startPos = 1;
				//Tollerierung von + an Stelle endPos, auch wenn laut Format ohne Vorzeichen
			} else if ((value.charAt(endPos) == '+') || (value.charAt(endPos) == ' ')) {
					endPos = endPos-1;
			} else {
				return null;
			}
		}
		if (decPos == endPos) {
			//Wert enth�lt keine Nachkommastellen, daher mit Null auff�llen 
			decValue = WDMTypChecker.fillupNumericRight(dec, ZERO);
			intValue = WDMTypChecker.checkNumeric(len-dec, value.substring(startPos, endPos));
		} else if (decPos == -1) {
			//Wert enth�lt keine Nachkommastellen, daher mit Null auff�llen 
			decValue = WDMTypChecker.fillupNumericRight(dec, ZERO);
			intValue = WDMTypChecker.checkNumeric(len-dec, value.substring(startPos, endPos+1));
		} else {
			//Wert enth�lt Nachkommastellen
			decValue = WDMTypChecker.checkDecimalPart(dec, value.substring(decPos + 1, endPos+1));
			if (decValue == null) return null;
			if (decValue.length() < dec) { //fillup with 0
				decValue = WDMTypChecker.fillupNumericRight(dec, decValue);
			}
			intValue = WDMTypChecker.checkNumeric(len-dec, value.substring(startPos, decPos));
		}
		if (intValue == null) return null;
		//R�ckgabewert zusammenbauen
		StringBuffer sb = new StringBuffer(len + 1);
		sb.append(intValue);
		sb.append(DECIMAL_SIGN_COMMA);
		sb.append(decValue);
		return sb.toString();
	}
	public static String checkDecimalWithoutSignWithoutDec(int len,
																								 int dec,
																								 boolean sign,
																								 int valueLength,
																								 int decPos, 
																								 int signPos,  
																								 String value) {
		char signChar = '+';
		String decValue = null;
		String intValue = null;
		int startPos = 0;
		int endPos = valueLength-1;

		if (signPos != -1) {
			//Tollerierung von + an Stelle 0, auch wenn laut Format ohne Vorzeichen
			if ((value.charAt(0) == '+') || (value.charAt(0) == ' ')) {
				startPos = 1;
				//Tollerierung von + an Stelle endPos, auch wenn laut Format ohne Vorzeichen
			} else if ((value.charAt(endPos) == '+') || (value.charAt(endPos) == ' ')) {
					endPos = endPos-1;
			} else {
				return null;
			}
		}
		if (decPos == endPos) {
			//Wert enth�lt keine Nachkommastellen, aber Dezimaltrenner am Ende
			intValue = WDMTypChecker.checkNumeric(len-dec, value.substring(startPos, endPos));
		} else if (decPos == -1) {
			//Wert enth�lt keine Nachkommastellen 
			intValue = WDMTypChecker.checkNumeric(len-dec, value.substring(startPos, endPos+1));
		} else {
			//Wert enth�lt doch Nachkommastellen, dann pr�fen, ob null
			decValue = WDMTypChecker.getNumeric(value.substring(decPos + 1, endPos+1));
			if (decValue == null) return null;
			if (!(decValue.equals(ZERO))) return null;
			intValue = WDMTypChecker.checkNumeric(len-dec, value.substring(startPos, decPos));
		}
		if (intValue == null) return null;
		//R�ckgabewert zusammenbauen
		StringBuffer sb = new StringBuffer(len);
		sb.append(intValue);
		return sb.toString();
	}
	private static String checkDecimalPart(
		int len,
		String value) {
		char[] source = value.toCharArray();
		int sourceLength = source.length;
		if (sourceLength == 0)
			return null;
		int endPos = sourceLength - 1;
		while ((endPos > 0) && (source[endPos] == '0'))
			endPos--;
		if ((endPos == 0) && (source[endPos] == '0')) {
			return ZERO;
		} else {
			for (int i = 0; i < endPos + 1; i++) {
				if (!(Character.isDigit(source[i])))
					return null;
			}
			if (endPos + 1 > len) {
				return null;
			} else {
				return value.substring(0, endPos + 1);
			}
		}
	}
	private static int getDecimalPos(String value) {
		return value.indexOf(DECIMAL_SIGN_COMMA);
	}
	public static int getSignPos(String value) {
		int i = -1;
		i = value.indexOf(' ');
		if (i > -1)	{ 
			throw new WRuntimeException("getSignPos mit Leerwerten");
		} 
		i = value.indexOf('+');
		if (i > -1)	return i;
		i = value.indexOf('-');
		if (i > -1)	return i;
		return i;
	}
	public static String getIntegerPart(String value) {
		String v = "";
		int signPos = WDMTypChecker.getSignPos(value);
		int deciPos = WDMTypChecker.getDecimalPos(value);
		if (signPos == -1) {
			if (deciPos == -1) {
				v = value;
			} else {
				v = value.substring(0, deciPos);
			}
		} else if (signPos == 0) {
			if (deciPos == -1) {
				v = value.substring(1);
			} else {
				v = value.substring(1, deciPos);
			}
		} else if (signPos == value.length() - 1) {
			if (deciPos == -1) {
				v = value.substring(0, value.length() - 1);
			} else {
				v = value.substring(0, deciPos);
			}
		} else {
			return null;
		}
		return WDMTypChecker.getNumeric(v);
	}
	/**
	 * liefert zu einem korrekt formatiereten Dezimalwert (####,####[+-]) den Dezimalanteil.
	 * Ist kein Dezimalanteil vorhanden, so wird ein "" String zur�ckgeliefert, ist der Wert
	 * kein Dezimalwert liefert die Funktion null.
	 */
	public static String getDecimalPart(String value) {
		int signPos = WDMTypChecker.getSignPos(value);
		int deciPos = WDMTypChecker.getDecimalPos(value);

		if ((signPos == -1) || (signPos == 0) || (signPos == value.length()-1)) {
			if (deciPos == -1) {
				return "";
			} else if (deciPos == value.length()-1) {
				return "";
			} else if ((deciPos == value.length()-2) && (signPos == value.length()-1)) {
				return "";
			} else {
				if ( signPos == -1 || signPos == 0 ) {
					return WDMTypChecker.checkNumeric(value.substring(deciPos + 1).length(), value.substring(deciPos + 1));
				} else {
					return WDMTypChecker.checkNumeric(value.substring(deciPos + 1, value.length()-1).length(), value.substring(deciPos + 1, value.length()-1));
				}
			}
		} else {
			return null;
		}
	}
	/**
	 * Pr�ft den Wert value auf eine numerische Zahl [0-9]+. *
	 * Handelt es sich bei value um keine numerische Zahl, so wird null 
	 * zur�ckgegeben, andernfalls der Wert ohne f�hrende Nullen und ohne
	 * abschliessende Leerzeichen.
	 * 
	 * @param value
	 * @return numerischer Wert ohne f�hrenden Nullen, ohne abschliessende Leerzeichen
	 */
	public static String getNumeric(String value) {
		//Wenn value nur Leerzeichen enth�lt, dann R�ckgabe 0
		if (value.trim().length() == 0) return "0";
		//Rechtsseitige Leerzeichen entfernen
		char[] source = WDMTypChecker.trimCharRight(value).toCharArray();
		// char[] source = value.toCharArray();
		int len = source.length;
		if (len == 0) return null;
		for (int i = 0; i < len; i++) {
			if (!(Character.isDigit(source[i]))) return null;
		}
		int startPos = 0;
		while ((startPos < len ) && (source[startPos] == '0'))
			startPos++;
		if (startPos == len ) {
			return ZERO;
		} else {
			return value.substring(startPos, len);
		}
	}
	/**
	 * Pr�ft den Wert value auf eine dezimale Zahl [+|-][0-9],[0-9]+. *
	 * Handelt es sich bei value um keine dezimale Zahl, so wird null 
	 * zur�ckgegeben, andernfalls der Wert ohne f�hrende Nullen, mit '.' 
	 * als Dezimaltrennzeichen, ohne aufgef�llte Nullen in den 
	 * Dezimalstellen, mit Vorzeichen und ohne abschliessende Leerzeichen.
	 * 
	 * @param value
	 * @return dezimaler Wert
	 */
	public static String getDecimal(String value) {
		StringBuffer sb = new StringBuffer(value.length());
		int p = WDMTypChecker.getSignPos(value);
		if ( p > -1 ){
			sb.append(value.charAt(p));
		} else {
			sb.append('+');
		}
		String intPart = WDMTypChecker.getIntegerPart(value);
		if (intPart == null) return null; 
		if (intPart.length() > 0) {
			sb.append(intPart);
		} else { 
			sb.append('0');
		}
		String decPart = WDMTypChecker.getDecimalPart(value);
		if (decPart == null) return null; 
		if (decPart.length() > 0) {
				sb.append('.');
				sb.append(decPart);
		} 
		return sb.toString();
	}
	/** Wert mit abschlie�enden Nullen auff�llen
	 * 
	 * @param len	l�nge des neuen Wertes
	 * @param value	aufzuf�llender Wert 
	 * @return mit abschlie�enden Nullen aufgef�llter Wert
	 */
	public static String fillupNumericRight(
		int len,
		String value) {
		StringBuffer sb = new StringBuffer(len);
		sb.append(value);
		for (int i = value.length(); i < len; i++) {
			sb.append('0');
		}
		return sb.toString();
	}
	/** Wert mit abschlie�enden Leerzeichen auff�llen
	 * 
	 * @param len	l�nge des neuen Wertes
	 * @param value	aufzuf�llender Wert 
	 * @return mit abschlie�enden Leerzeichen aufgef�llter Wert
	 */
	public static String fillupCharRight(
			int len,
			String value) {
			StringBuffer sb = new StringBuffer(len);
			sb.append(value);
			for (int i = value.length(); i < len; i++) {
				sb.append(' ');
			}
			return sb.toString();
		}
	/** Wert mit f�hrenden Nullen auff�llen
	 * 
	 * @param len	l�nge des neuen Wertes
	 * @param value	aufzuf�llender Wert 
	 * @return mit f�hrenden Nullen aufgef�llter Wert
	 */
	public static String fillupNumericLeft(int len, String value) {
		if (value == null) throw new WRuntimeException("Leerer String in WDMTypChecker.fillupNumericLeft() ist nicht zul�ssig. "); 
		StringBuffer sb = new StringBuffer(len);
		sb.append(value);
		for (int i = value.length(); i < len; i++) {
			sb.insert(0, '0');
		}
		return sb.toString();
	}
	/** Wert mit f�hrenden Leerzeichen auff�llen
	 * 
	 * @param len	l�nge des neuen Wertes
	 * @param value	aufzuf�llender Wert 
	 * @return mit f�hrenden Leerzeichen aufgef�llter Wert
	 */
	public static String fillupCharLeft(int len, String value) {
		StringBuffer sb = new StringBuffer(len);
		sb.append(value);
		for (int i = value.length(); i < len; i++) {
			sb.insert(0, ' ');
		}
		return sb.toString();
	}
	/** F�hrende Nullen entfernen
	 * 
	 * @param value	zu trimmender Wert 
	 * @return getrimmter Wert
	 */
	public static String trimNumericLeft(String value) {
		int pos = 0;
		while ( (pos < value.length()) && (value.charAt(pos)=='0') ) pos++;
		//TODO Bug #326: Hinzugef�gt am 18.11.2003 von KK0080
		// Wenn "value" nur Nullen enth�lt, w�rde ein leerer STring zur�ckgegeben
		// Somit wird dann ein "0" zur�ckgegeben 
		if (pos == value.length()) return "0";
		return value.substring(pos);
	}
	/** Abschlie�ende Nullen entfernen
	 * 
	 * @param value	zu trimmender Wert 
	 * @return getrimmter Wert
	 */
	public static String trimNumericRight(String value) {
		int pos = value.length()-1;
		while ( (pos >= 0) && (value.charAt(pos)=='0') ) pos--;
		if (pos == -1) return "0";
		return value.substring(0, pos+1);
	}
	/** F�hrende Nullen entfernen
	 * 
	 * @param value	zu trimmender Wert 
	 * @return getrimmter Wert
	 */
	public static String trimDecimalLeft(String value) {
		int pos = 0;
		int len = value.length();
		while ( (pos < len) && (value.charAt(pos)=='0') ) pos++;
		if (pos < len && value.charAt(pos)==',') return '0'+value.substring(pos); 
		if (pos == len) return "0";
		return value.substring(pos);
	}
	/** Abschlie�ende Nullen entfernen
	 * 
	 * @param value	zu trimmender Wert 
	 * @return getrimmter Wert
	 */
	public static String trimDecimalRight(String value) {
		if (value.indexOf(',') == -1) return value;
		int pos = value.length()-1;
		while ( (pos >= 0) && (value.charAt(pos)=='0') ) pos--;
		if (pos >= 0 && value.charAt(pos)==',') return value.substring(0, pos+1)+'0'; 
		if (pos == -1) return "0";
		return value.substring(0, pos+1);
	}
	/** F�hrende Leerzeichen entfernen
	 * 
	 * @param value	zu trimmender Wert 
	 * @return getrimmter Wert
	 */
	public static String trimCharLeft(String value) {
		int pos = 0;
		while ( (pos < value.length()) && (value.charAt(pos)==' ') ) pos++;
		return value.substring(pos);
	}
	/** Abschlie�ende Leerzeichen entfernen
	 * 
	 * @param value	zu trimmender Wert 
	 * @return getrimmter Wert
	 */
	public static String trimCharRight(String value) {
		int pos = value.length()-1;
		while ( (pos >= 0) && (value.charAt(pos)==' ') ) {
			pos--;
		} 
		return value.substring(0, pos+1);
	}
	/*
	public static void main(String s[]) {
//		String[] values = {"01"}    ;
		String[] values = { "1,1+",  "1,1-",   "1,1 ",   "+1,1",   "-1,1",  "1,1",    
											  "12,1+",  "12,1-",  "12,1 ",  "+12,1",  "-12,1", "12,12",
												"1,12+",  "1,12-",  "1,12 ",  "+1,12",  "-1,12", "1,12",
												"12,12+", "12,12-", "12,12 ", "+12,12", "-12,12","12,12",
											  "1,+",    "1,-",    "1, ",    "+1,",    "-1,",   "1,",
											  "12,+",   "12,-",   "12, ",   "+12,",   "-12,",  "12,",
												"1+",     "1-",     "1 ",     "+1",     "-1",    "1",
												"12+",    "12-",    "12 ",    "+12",    "-12",   "12",
			                  "01,1+",  "01,1-",   "01,1 ",   "+01,1",   "-01,1",  "01,1",    
												"012,1+",  "012,1-",  "012,1 ",  "+012,1",  "-012,1", "012,12",
												"01,12+",  "01,12-",  "01,12 ",  "+01,12",  "-01,12", "01,12",
												"012,12+", "012,12-", "012,12 ", "+012,12", "-012,12","012,12",
												"01,+",    "01,-",    "01, ",    "+01,",    "-01,",   "01,",
												"012,+",   "012,-",   "012, ",   "+012,",   "-012,",  "012,",
												"01+",     "01-",     "01 ",     "+01",     "-01",    "01",
												"012+",    "012-",    "012 ",    "+012",    "-012",   "012"};
		System.out.println("DEC,16,0,true");
		for (int i=0;i<values.length;i++) {
			try{
				System.out.println(values[i]+" = "+WDMTypChecker.getTypConformValue(new WAttribute("DEC1","DEC1",IWAttribute.FRM_DEC,16,0,true,false),values[i]));
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
		System.out.println("DEC,16,0,false");
	  for (int i=0;i<values.length;i++) {
			try{
				System.out.println(values[i]+" = "+WDMTypChecker.getTypConformValue(new WAttribute("DEC1","DEC1",IWAttribute.FRM_DEC,16,0,false,false),values[i]));
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
	  }
//		String[] values = {"1,1"};
		System.out.println("DEC,16,4,true");
		for (int i=0;i<values.length;i++) {
			try{
				System.out.println(values[i]+" = "+WDMTypChecker.getTypConformValue(new WAttribute("DEC1","DEC1",IWAttribute.FRM_DEC,16,4,true,false),values[i]));
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
		System.out.println("DEC,16,4,false");
		for (int i=0;i<values.length;i++) {
			try{
			 System.out.println(values[i]+" = "+WDMTypChecker.getTypConformValue(new WAttribute("DEC1","DEC1",IWAttribute.FRM_DEC,16,4,false,false),values[i]));
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
	}
	*/
}
