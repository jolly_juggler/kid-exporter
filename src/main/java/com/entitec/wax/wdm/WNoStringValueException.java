package com.entitec.wax.wdm;

import com.entitec.wax.wglobal.WRuntimeException;


/**
 * Date:    03.07.2003
 * <p>Description:</p>
 * Die Klasse WNoStringValueException erweitert die allgemeine WAX 
 * WRuntimeException und wird geworfen, wenn der Wert (value) eines
 * IWDataItems als String verarbeitet werden soll, es sich aber nicht 
 * um einen String handelt.
 * @author AB0043
 */
public class WNoStringValueException extends WRuntimeException {
  public WNoStringValueException(String message) {
    super(message);
  }
}
