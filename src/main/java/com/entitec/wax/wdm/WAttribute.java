package com.entitec.wax.wdm;

import java.math.BigDecimal;
import java.math.BigInteger;

import com.entitec.wax.wglobal.WRuntimeException;


/**
 * Date:    03.07.2003
 * <p>Description:</p>
 * Die Klasse WAttribute definiert die Eigenschaften der IWDataItems in
 * einer Spalte eines WDataModels.
 * @author AB0043
 */
public class WAttribute implements IWAttribute {
	//	Supplementary definitions to ease the use of the herein defined functions
	public static final boolean OPTIONAL = true;
	public static final boolean MANDATORY = false;
	public static final boolean SIGNED = true;
	public static final boolean UNSIGNED = false;

	/*
	 * Name des WAttriubtes.
	 */
  protected String mName;
	/*
	 * Alias des WAttriubtes.
	 */
  protected String mAlias;
	/*
	 * Format des WAttriubtes.
	 */
  protected char mFrm;
	/*
	 * L�nge des WAttriubtes.
	 */
  protected int mLen;
	/*
	 * Anzahl der Dezimalstellen des WAttriubtes.
	 */
  protected int mDec;
	/*
	 * Vorzeichenbehandlung des WAttriubtes.
	 */
  protected boolean mSign;
	/*
	 * Kennzeichen f�r die homogene F�llung der Spalte des WAttriubtes
	 * im WDataModel.
	 */
  protected boolean mHomo;
	/*
	 * Default-Wert des WAttriubtes.
	 */
  private String mDefaultValue = null;
  private static final String DEFAULT_CHAR = "";
  /**
   * Erzeugen eines WAttriubtes unter Angabe des Names (name), des Formats (frm),
   * der L�nge (len), der Anzahl der Dezimalstellen (dec) und ob mit oder ohne
   * Vorzeichen (sign). Der Alias des Attributs ist dann mit dem Namen identisch.
   * Die Daten der Spalte diese Attributs werden bei einem homogenen WDataModel
   * nicht homogen gehalten, da die Variable homo mit dem default (false)
   * belegt wird.
   * @param name
   * @param frm
   * @param len
   * @param dec
   * @param sign
   */
	public WAttribute(String name, char frm, int len, int dec, boolean sign) {
		this(name, name, frm, len, dec, sign, false);
	}
	/**
   * Erzeugen eines WAttriubtes unter Angabe des Names (name), des Alias 
   * (alias), des Formats (frm), der L�nge (len), der Anzahl der Dezimal-
   * stellen (dec) und ob mit oder ohne Vorzeichen (sign). 
   * Die Daten der Spalte diese Attributs werden bei einem homogenen WDataModel
   * nicht homogen gehalten, da die Variable homo mit dem default (false)
   * belegt wird.
	 * @param name
	 * @param alias
	 * @param frm
	 * @param len
	 * @param dec
	 * @param sign
	 */
	public WAttribute(String name, String alias, char frm, int len, int dec, boolean sign) {
		this(name, alias, frm, len, dec, sign, false);
	}
	/**
   * Erzeugen eines WAttriubtes unter Angabe des Names (name), des Alias 
   * (alias), des Formats (frm), der L�nge (len), der Anzahl der Dezimal-
   * stellen (dec), ob mit oder ohne Vorzeichen (sign) und �ber die Variable 
   * homo ob die Daten der Spalte diese Attributs bei einem homogenen WDataModel
   * homogen oder inhomogen gehalten werden sollen.
	 * @param name
	 * @param alias
	 * @param frm
	 * @param len
	 * @param dec
	 * @param sign
	 * @param homo
	 */
	public WAttribute(String name, String alias, char frm, int len, int dec, 
	                  boolean sign, boolean homo) {
		this.setName(name);
		if (alias.equals("")) {
      this.setAlias(name);
		}	else {
	    this.setAlias(alias);
		}
		mFrm = frm;
		mLen = len;
		switch (frm) {
			case IWAttribute.FRM_CHAR:
			case IWAttribute.FRM_NUM:
			case IWAttribute.FRM_ADDR:
			case IWAttribute.FRM_COLOR:
			case IWAttribute.FRM_DATAMODEL:
			case 'Z':
				mDec = 0;
				mSign = false;
		    break;
			case IWAttribute.FRM_DEC:
				mDec = dec;
				mSign = sign;
		    break;
		  default:
			  throw new WRuntimeException("Format is not correct.");
		}
		mHomo = homo;
		setDefaultValue();
	}            
	/**
	 * Vergleichsmethode f�r zwei IWDataItems unter Ber�cksichtigung
	 * der WAttribute-Eigenschaften.
	 * @param a
	 * @param b
	 */
  public int compare(IWDataItem a, IWDataItem b) {
		Object o1 = a.getValue();
		Object o2 = b.getValue();
		int i = 0;
		if (!(o1 instanceof String)) {
		  throw new WNoStringValueException("Value: "+o1);
		}
		if (!(o2 instanceof String)) {
		  throw new WNoStringValueException("Value: "+o2);
		}
		String s1 = (String)o1;
		String s2 = (String)o2;
  	switch (mFrm) {
  		case IWAttribute.FRM_ADDR:
				i = s1.compareTo(s2);
				break;
			case IWAttribute.FRM_CHAR:
	    	  // i = s1.compareToIgnoreCase(s2);
			  i = (s1.trim()).compareTo(s2.trim()); //jdk1.1
    	  break;
  		case IWAttribute.FRM_NUM:
				if (s1.length() == 0) {
					s1 = "0";
				} else {
					s1 = WDMTypChecker.getNumeric((String)s1);
				}
				if (s2.length() == 0) {
					s2 = "0";
				} else {
					s2 = WDMTypChecker.getNumeric((String)s2);
				}
				BigInteger b1 = new BigInteger(s1);
				BigInteger b2 = new BigInteger(s2);
				
				i = b1.compareTo(b2);
    		break;
  		case IWAttribute.FRM_DEC:
				if (s1.length() == 0) {
					s1 = "0.0";
				} else {
					s1 = WDMTypChecker.getDecimal((String)s1);
				}
				if (s2.length() == 0) {
					s2 = "0.0";
				} else {
					s2 = WDMTypChecker.getDecimal((String)s2);
				}
			  BigDecimal d1 = new BigDecimal(s1);
				BigDecimal d2 = new BigDecimal(s2);
				i = d1.compareTo(d2);
    	  break;
    	default:
    	  i=0;
    	  break;
	  }
  	return i;
  }  
  /**
   * R�ckgabe des Aliasnamens des WAttributes.
   */
  public String getAlias() {
  	return mAlias;
  }        
  
  
  /**
   * <p>R�ckgabe einer Description des Attributs mit dem Alias-Namen als String.</p>
   * <ul>
   * <li>Alias-Name, 16 Zeichen, linksb�ndig, mit Blanks aufgef�llt.</li>
   * <li>Typ, 1 Zeichen {'C', 'N', 'D', 'Z'}</li>
   * <li>L�nge, 4 Zeichen, numerisch, rechtsb�ndig, mit Nullen aufgef�llt.</li>
   * <li>Dezimalstellen, 1 Zeichen, numerisch.</li>
   * <li>Vorzeichen, 1 Zeichen, {'J', 'N'}.</li>
   * </ul>
   * @return Die (Alias-)Description des Attributs.
   */
  public String getAliasDescription() {
	StringBuffer description = new StringBuffer(23);
	StringBuffer buffer = new StringBuffer(23);
  	
	// Alias linksb�ndig, auf 16 Zeichen aufgef�llt.
	buffer.append(this.mAlias);
	buffer.append("                ");
	description.append(buffer.substring(0, 16));
  	
	// Typ
	description.append(this.mFrm);
  	
	// L�nge rechtsb�ndig, auf 4 Zeichen aufgef�llt.
	buffer.replace(0, buffer.length(), "0000"+mLen);
	description.append(buffer.substring(buffer.length()-4));
  	
	// Dezimalstellen
	description.append(""+mDec);

	// Vorzeichen
	if (mSign) {
		description.append('J');
	} else {
		description.append('N');
	}
  	
	return description.toString();
  } // End of getAliasDescription


	/**
	 * R�ckgabe der Anzahl der Dezimalstellen des WAttributes.
	 */
  public int getDec() {
  	return mDec;
  }  
  
  
  /**
   * <p>R�ckgabe einer Description des Attributs als String.</p>
   * <ul>
   * <li>Name, 16 Zeichen, linksb�ndig, mit Blanks aufgef�llt.</li>
   * <li>Typ, 1 Zeichen {'C', 'N', 'D', 'Z'}</li>
   * <li>L�nge, 4 Zeichen, numerisch, rechtsb�ndig, mit Nullen aufgef�llt.</li>
   * <li>Dezimalstellen, 1 Zeichen, numerisch.</li>
   * <li>Vorzeichen, 1 Zeichen, {'J', 'N'}.</li>
   * </ul>
   * @return Die Description des Attributs.
   */
  public String getDescription() {
  	StringBuffer description = new StringBuffer(23);
  	StringBuffer buffer = new StringBuffer(23);
  	
  	// Name linksb�ndig, auf 16 Zeichen aufgef�llt.
  	buffer.append(this.mName);
  	buffer.append("                ");
  	description.append(buffer.substring(0, 16));
  	
  	// Typ
  	description.append(this.mFrm);
  	
	// L�nge rechtsb�ndig, auf 4 Zeichen aufgef�llt.
	buffer.replace(0, buffer.length(), "0000");
	buffer.append(""+mLen);
	description.append(buffer.substring(buffer.length()-4));
  	
	// Dezimalstellen
	description.append(""+mDec);

	// Vorzeichen
	if (mSign) {
		description.append('J');
	} else {
		description.append('N');
	}
  	
  	return description.toString();
  } // End of getDescription
  
  
	/**
	 * R�ckgabe des Formats des WAttributes.
	 */
  public char getFrm() {
  	return mFrm;
  }  
	/**
	 * R�ckgabe der L�nge des WAttributes.
	 */
  public int getLen() {
  	return mLen;
  }  
	/**
	 * R�ckgabe des Namens des WAttributes.
	 */
  public String getName() {
	return mName;
  }
	/**
	 * R�ckgabe ob das WAttribute mit oder ohne Vorzeichen definiert ist.
	 */
  public boolean getSign() {
  	return mSign;
  }  
	/**
	 * R�ckgabe ob die Spalte im WDataModel, die das WAttributes definiert,
	 * homogen oder inhomogen gepflegt werden soll.
	 */
  public boolean getHomo() {
  	return mHomo;
  }  
	/**
	 * R�ckgabe des Standardwerts des WAttributes in Abh�ngigkeit seiner 
	 * Definition (Format, L�nge etc.).
	 */
  public String getDefaultValue() {
  	return mDefaultValue;
  }
  public String getDefaultValueOld() {
	  switch (mFrm) {
		  case IWAttribute.FRM_CHAR:
			case IWAttribute.FRM_ADDR:
			  return DEFAULT_CHAR;
		  case IWAttribute.FRM_NUM:
		  StringBuffer sb = new StringBuffer(mLen);
		  for (int i = 0; i < mLen;i++) sb.append('0');
			  return sb.toString();
		  case IWAttribute.FRM_DEC:
				if (mSign == true) {
			  sb = new StringBuffer(mLen+2);
			  sb.append('+');
				} else {
			  sb = new StringBuffer(mLen+1);
				}
		  if (mDec == 0) {
				for (int i = 0; i < mLen;i++) sb.append('0');
		  } else {
			  if (mLen-mDec == 0) {
				  sb.append('0');
			  } else {
					for (int i = 0; i < mLen-mDec;i++) sb.append('0');
			  }
				sb.append(',');
				for (int i = 0; i < mDec;i++) sb.append('0');
		  }
			  return sb.toString();
		  case IWAttribute.FRM_COLOR:
			  return null;
		  case IWAttribute.FRM_DATAMODEL:
			  return null;
			  default:
				throw new WRuntimeException("Format is not correct.");
		}
	}
  /**
   * Setzen des Aliasnamens des WAttributes.
   * @param a
   */
  public void setAlias(String a) {
		if (!a.equals("")) mAlias = a;
  }  
	/**
	 * Setzen der Anzahl der Dezimalstellen des WAttributes.
	 * @param d
	 */
  public void setDec(int d) {
  	mDec = d;
		setDefaultValue();
  }  
	/**
	 * Setzen des Formats des WAttributes. Die zul�ssigen Formate sind in
	 * IWAttribute als Konstante definiert.
	 * @param f
	 * @see com.entitec.wax.wdm.IWAttriubte
	 */
  public void setFrm(char f) {
  	mFrm = f;
		setDefaultValue();
  }  
	/**
	 * Setzen der L�nge des WAttributes.
	 * @param l
	 */
  public void setLen(int l) {
  	if ((l - mDec) < 0) throw new WRuntimeException("Fehler beim �ndern von Attribut '"+mName+"': Neue L�nge < Anzahl Dezimalstellen."); 
  	mLen = l;
  	setDefaultValue();
  }  
	/**
	 * Setzen des Namens des WAttributes.
	 * @param n
	 */
  public void setName(String n) {
  	mName = n;
  }  
	/**
	 * Setzen ob das WAttribute mit oder ohne Vorzeichen definiert ist.
	 * @param s
	 */
  public void setSign(boolean s) {
  	mSign = s;
		setDefaultValue();
  }  
	/**
	 * Setzen ob die Spalte im WDataModel, die das WAttributes definiert,
	 * homogen oder inhomogen gepflegt werden soll.
	 * @param h
	 */
  public void setHomo(boolean h) {
  	mHomo = h;
  }  
	/**
	 * Setzen des Standardwerts des WAttributes in Abh�ngigkeit seiner 
	 * Definition (Format, L�nge etc.).
	 */
	private void setDefaultValue() {
		switch (mFrm) {
			case IWAttribute.FRM_CHAR:
			case IWAttribute.FRM_ADDR:
			case 'Z':
				mDefaultValue = WDMTypChecker.checkCharacter(mLen, DEFAULT_CHAR);
				break;
			case IWAttribute.FRM_NUM:
				mDefaultValue = WDMTypChecker.checkNumeric(mLen, "0");
				break;
			case IWAttribute.FRM_DEC:
				mDefaultValue = WDMTypChecker.checkDecimal(mLen, mDec, mSign, "0");
				break;
			case IWAttribute.FRM_COLOR:
				mDefaultValue = null;
				break;
			case IWAttribute.FRM_DATAMODEL:
				mDefaultValue = null;
				break;
			default:
				throw new WRuntimeException("Format is not correct.");
		}
	}
	/**
	 * R�ckgabe des WAttributes als String.
	 * @return
	 */
  public String toString() {
  	StringBuffer sb = new StringBuffer(120);
  	this.toString(sb);
	 	return sb.toString();
  }  
  /**
   * Aufl�sen des WAttributes in den �bergebenen StringBuffer.
   * @param sb
   */
	public String toMultiLineString() {
	  	StringBuffer sb = new StringBuffer(120);
		sb.append("<WATTRIBUTE");
		sb.append("\nName=\"");
		sb.append(mName);
		sb.append("\"\nAlias=\"");
		sb.append(mAlias);
		sb.append("\"\nFrm=\"");
		sb.append(mFrm);
		sb.append("\"\nLen=\"");
		sb.append(mLen);
		sb.append("\"\nDec=\"");
		sb.append(mDec);
		sb.append("\"\nSign=\"");
		sb.append(mSign);
		sb.append("\"\nHomo=\"");
		sb.append(mHomo);
		sb.append("\"/>\n");
		return sb.toString();
	}  
	  /**
	   * Aufl�sen des WAttributes in den �bergebenen StringBuffer.
	   * @param sb
	   */
		public void toString(StringBuffer sb) {
			sb.append("<WATTRIBUTE Name=\"");
			sb.append(mName);
			sb.append("\"");
			for(int i=0;i<16-mName.length();i++) sb.append(' ');
			sb.append(" Alias=\"");
			sb.append(mAlias);
			sb.append("\"");
			for(int i=0;i<16-mAlias.length();i++) sb.append(' ');
			sb.append(" Frm=\"");
			sb.append(mFrm);
			sb.append("\" Len=\"");
			if (mLen < 10) {
				sb.append('0');
				sb.append('0');
				sb.append('0');
			} else if (mLen < 100) {
				sb.append('0');
				sb.append('0');
			} else if (mLen < 1000) {
				sb.append('0');
			}
			sb.append(mLen);
			sb.append("\" Dec=\"");
			sb.append(mDec);
			sb.append("\" Sign=\"");
			sb.append(mSign);
			if (mSign == true) sb.append(' ');
			sb.append("\" Homo=\"");
			sb.append(mHomo);
			sb.append("\"/>\n");
		}  
}
