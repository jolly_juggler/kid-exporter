package com.entitec.wax.wdm;

import com.entitec.wax.wglobal.WRuntimeException;


/**
 * Date:    03.07.2003
 * <p>Description:</p>
 * Die Klasse WRowNotFoundException erweitert die allgemeine WAX 
 * WRuntimeException und wird geworfen, wenn in einem WDataModel auf 
 * eine nicht vorhandene Zeile zugegriffen wird.
 * @author AB0043
 */
public class WRowNotFoundException extends WRuntimeException {
  public WRowNotFoundException(String message) {
    super(message);
  }
}
