package com.entitec.wax.wdm;

public interface IWMaskAttribute extends IWAttribute {
	//DisplayFormate
	public static final char FMT_FILLUPZERO = 'A';
	public static final char FMT_CHAR = 'C';
	public static final char FMT_NUM = 'N';
	public static final char FMT_DEC = 'D';
	public static final char FMT_DATE = 'E';
	public static final char FMT_TIME = 'U';
	public static final char FMT_REV_INPUT = 'R';
	public static final char FMT_NO_ZERO = 'M';
	public static final char FMT_FLOWTEXT = 'T';
	public static final char FMT_PASSWORD = 'P';
	public static final char FMT_DYNAMIC = 'X';
	public static final char FMT_NO_ZERO_IND = 'Y';
	public static final char FMT_LOWERTEXT = 'l';
	public static final char FMT_UPPERTEXT = 'u';
	public static final char FMT_NO_ZERO_NUM = 'n';
	public static final char FMT_NO_ZERO_DEC = 'd';
	public static final char FMT_NO_ZERO_IND_NUM = '1';
	public static final char FMT_NO_ZERO_IND_DEC = '2';
	public static final char FMT_FILLUPNUM = '0';
	public static final char FMT_YESNO = 'y';
	public static final char FMT_CBO_TEXT = 't';
	public static final char FMT_COLOR = 'c';
	public static final char FMT_DEFAULT = 'B';
	//DisplayModes
	public static final int CHANGE = 8; //GRDEDIT = C
	public static final int VISIBLE = 9; //GRDEDIT = V
	public static final int INVISIBLE = 10; //GRDEDIT = I
	public static final int DEFAULT = 11;
	//VerarbeitungModes
	public static final int PVM_DEFAULT= 20;
	public static final int PVM_NOTHING = 21;
	public static final int PVM_FIRST = 22; //GRDOUTM = 1
	public static final int PVM_ALL = 23; //GRDOUTM = 4
	public static final int PVM_CHANGED = 24; //GRDOUTM = 3
	public static final int PVM_SELECTED = 25; //GRDOUTM = 2 und 5
	public static final int PVM_NOT_SET= 26;
	public static final int PVM_CELL = 27; //GRDOUTM = 6


  public String getCap();  
  public char getDisp();  
  public int getMode();  
  public int getPvermo();  
  public boolean isFirstFocused();
  
}
