package com.entitec.wax.wdm;

class WIndexedValue {
  private int mIndex = 0;
  private boolean mSelected = false;
  private IWDataItem mValue = null;

  public WIndexedValue(int index, IWDataItem value) {
    this(index, false, value);
  }
  public WIndexedValue(int index, boolean selected, IWDataItem value) {
		mIndex = index;
		mSelected = selected;
		mValue = value;
  }  
  public int getIndex() { 
    return mIndex; 
  }
  public IWDataItem getValue() { 
    return mValue;
  }  
  public boolean isSelected() { 
    return mSelected; 
  }
  public void setSelected(boolean b) { 
    mSelected = b; 
  }
  public String toString() {
    if (mSelected == true) {
      return mValue.toString() + ", Index: " + mIndex + ", Selected: TRUE";
    } else {
      return mValue.toString() + ", Index: " + mIndex + ", Selected: FALSE";
    }
  }
}
