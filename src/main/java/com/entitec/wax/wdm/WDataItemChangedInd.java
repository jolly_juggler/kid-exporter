package com.entitec.wax.wdm;


/**
 *  merkt sich den initialen Set-Wert. Bei getState wird der aktuelle Wert gegen den
 *  initial eingestellten Wert verglichen.
 *  Sind beide Werte gleich und der State IWDataItem.ENABLED, so wird als State IWDataItem.UNCHANGED 
 *  zurueckgegeben.
 */
 
public class WDataItemChangedInd implements IWDataItem {
  protected Object mValue;
  protected short mState;
  protected Object mInitValue;
  private boolean mInitCompleted = false;

  public WDataItemChangedInd() {
    mValue = null;
    mInitValue = null;
    mState = 0;
  }
  
  public void init() {
    mInitValue = null;
    mValue = null;
    mState = 0;
  }

  /** �bernimmt den aktuellen Wert als Initialwert. */
  public void initCompleted() {
	  mInitValue = mValue;
  }
  
  public short getState() {
	if (mValue instanceof String) {
		if ( (mState == IWDataItem.ENABLED) && (mInitValue != null) && (mValue != null)) {
			if ( ((String) mValue).equals((String) mInitValue) ) 
				return IWDataItem.UNCHANGED;
		}
	}
	return mState;
  }
 
  public char getStateAsChar() {
    switch (getState()) {
      case IWDataItem.DISABLED:
        return '0';
      case IWDataItem.ENABLED:
        return '1';
      case IWDataItem.TRUNCATED:
        return '2';
      case IWDataItem.ERROR:
        return '3';
      case IWDataItem.UNCHANGED:
        return '4';
      case IWDataItem.INVALID:
        return '9';
      default:
        return '0';
    }
  }
  public Object getValue() {
    if (mValue == null) {
      return "";
    } else {
      if (mValue instanceof String) {
        return new String((String)mValue);
      } else {
        return WDMToolbox.deepCopy(mValue);
      }
    }
  }  
  public void setState(short state) {
	mState = state;
  }  
  
  public void setValue(Object value) {
	  mValue = value;
  }
  
  public String toString() {
		return toString("WDATAITEM");
  }  
	/**
	 * R�ckgabe des WDataItems als String.
	 * @param name Name des Attributes der Spalte.
	 * @return
	 */
	public String toString(String name) {
		StringBuffer sb = new StringBuffer(this.getStringSize());
		toString(sb, name);
		return sb.toString();
	}  
	/**
	 * Aufl�sen des WDataItems in den �bergebenen StringBuffer.
	 * @param sb.
	 * @param name Name des Attributes der Spalte.
	 */
	public void toString(StringBuffer sb, String name) {
		sb.append("<WDATAITEM Name=\"");
		sb.append(name);
		sb.append("\" Ind=\"");
		sb.append(mState);
		sb.append("\">");
		if (mValue instanceof WDataModel) {
			sb.append("\n");
			sb.append(mValue);
			sb.append("</WDATAITEM>\n");
		} else {
			sb.append(mValue);
			sb.append("</WDATAITEM>\n");
		}
	}  
	public int getStringSize(){
		if (mValue instanceof String) {
			return 56+((String)mValue).length();
		} else if (mValue instanceof WDataModel) {
			return 56+((WDataModel)mValue).getStringSize();
		} else {
			return 156;
		}
	}
}
