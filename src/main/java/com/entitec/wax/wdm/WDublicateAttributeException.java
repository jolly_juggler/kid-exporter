package com.entitec.wax.wdm;

import com.entitec.wax.wglobal.WRuntimeException;


/**
 * Date:    03.07.2003
 * <p>Description:</p>
 * Die Klasse WDublicateAttributeException erweitert die allgemeine WAX 
 * WRuntimeException und wird geworfen, wenn der Name eines WAttributes, 
 * das einer WSignature hinzugefügt werden soll, bereits unter einem 
 * anderen WAttriubte vorhanden ist. 
 * @author AB0043
 */
public class WDublicateAttributeException extends WRuntimeException {
  public WDublicateAttributeException(String message) {
    super(message);
  }
}
