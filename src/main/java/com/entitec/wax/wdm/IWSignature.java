package com.entitec.wax.wdm;

import java.io.Serializable;
import java.util.Enumeration;

public interface IWSignature extends Serializable {
	/*
	 * Name der Index-Spalte
	 */
	public static final String INDEX_COLUMN_NAME = "$ROWIDX$";
  
  public void addAttribute(IWAttribute a);  
  public Enumeration attributes();  
  public int cols();  
  public boolean getAliasIsName();       
	public Enumeration getNamesForAlias(String alias);
  public IWAttribute getAttribute(String name);  
  public String getAttributeName(int col);  
  public int getDataLength();
  public boolean hasAttribute(String Name);  
  public boolean hasAttributes();  
  public int indexOf(String name);  
  public Enumeration keys();  
  public void setAliasIsName(boolean b) ;    
  public String toString();  
  public void toString(StringBuffer sb, int level);  
}    
