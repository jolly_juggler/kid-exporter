package com.entitec.wax.wdm;

import java.io.Serializable;

public interface IWDataItem extends Serializable {
	/**
	 * Konstante f�r den IWDataItem Zustand disabled (nicht aktiviert).
	 */
  public final static short DISABLED = 0;
	/**
	 * Konstante f�r den IWDataItem Zustand enabled (aktiviert).
	 */
  public final static short ENABLED = 1;
	/**
	 * Konstante f�r den IWDataItem Zustand truncated (gek�rzt).
	 */
  public final static short TRUNCATED = 2;
	/**
	 * Konstante f�r den IWDataItem Zustand error (Fehler).
	 */
  public final static short ERROR = 3;
	/**
	 * Konstante f�r den IWDataItem Zustand unchanged (unver�ndert).
	 */
  public final static short UNCHANGED = 4;
	/**
	 * Konstante f�r den IWDataItem Zustand invalid (ung�ltig).
	 */
  public final static short INVALID = 9;

  public void init();
  public short getState();  
  public char getStateAsChar();  
  public Object getValue();  
  public void setState(short state);  
  public void setValue(Object value);  
  public String toString();  
	public String toString(String name);
  public void toString(StringBuffer sb, String name); 
  public void initCompleted();
}
