package com.entitec.wax.wdm;


public class WFunctionAttribute extends WAttribute implements IWAttribute{
	private String mCaption;
	private short mDefaultIndicator = DEFAULT_INDICATOR_DISABLED;
	private int mDefaultType = 0;
	private String mDefaultValue = null;

	public static final int DEFAULT_TYPE_NONE = 0;
	public static final int DEFAULT_TYPE_VALUE = 1;
	public static final int DEFAULT_TYPE_RULE = 2;
	public static final short DEFAULT_INDICATOR_DISABLED = 0;
	public static final short DEFAULT_INDICATOR_ENABLED = 1;
	public static final short DEFAULT_INDICATOR_UNCHANGED = 4;
	

	
// Ohne zusätzliche Attribute
public WFunctionAttribute(String name, String alias, char frm,
					int len, int dec, boolean sign) {
	super(name, alias, frm, len, dec, sign);
}
// Attribut mit Vorbelegung

public WFunctionAttribute(String name, String alias, char frm,
					int len, int dec, boolean sign, 
					int defaultType, String defaultValue, short defaultIndicator) {
	super(name, alias, frm, len, dec, sign);
	setDefaultType(defaultType);
	setDefaultIndicator(defaultIndicator);
	setDefaultValue(defaultValue);
}
public String getDefaultValue() {
	if (mDefaultValue == null) {
		return super.getDefaultValue();
	} else {
		return mDefaultValue;
	}
}
public int getDefaultType() {
	return mDefaultType;
}
public short getDefaultIndicator() {
	return mDefaultIndicator;
}
public void setDefaultValue(String defaultValue) {
	mDefaultValue = defaultValue;
}
public void setDefaultIndicator(short defaultIndicator) {
	mDefaultIndicator = defaultIndicator;
}
public void setDefaultType(int defaultType) {
	mDefaultType = defaultType;
}
}
