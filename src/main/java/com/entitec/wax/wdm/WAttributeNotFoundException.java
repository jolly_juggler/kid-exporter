package com.entitec.wax.wdm;

import com.entitec.wax.wglobal.WRuntimeException;


/**
 * Date:    03.07.2003
 * <p>Description:</p>
 * Die Klasse WAttributeNotFoundException erweitert die allgemeine WAX 
 * WRuntimeException und wird geworfen, wenn in einem WDataModel auf 
 * ein in der WSignature nicht vorhandenes WAttribute zugegriffen wird.
 * @author AB0043
 */
public class WAttributeNotFoundException extends WRuntimeException {
  public WAttributeNotFoundException(String message) {
    super(message);
  }
}
