package com.entitec.wax.wdm;

import java.util.Enumeration;

/**
 * 
 * Date:    08.07.2003
 * <p>Description:</p>
 * Die Klasse WDataModelWriter erm�glicht den schreibenden Zugriff auf 
 * ein WDataModel. Sie enth�lt Methoden die den Zugriff auf die gesamte
 * Struktur eines WDataModels (WDataModel, WSignature, WAttribute, WDataItem)
 * in einer Klasse b�ndelt.
 * Die Methoden f�r den lesenden Zugriff erbt diese Klasse von der Klasse 
 * WDataModelReader.
 * @author ab0043
 * 
 * @see com.entitec.wax.wdm.WDataModelReader
 */
public class WDataModelWriter extends WDataModelReader {
	/**
	 * Konstruiert einen 'leeren' WDataModelWriter. �ber die Methode init()
	 * kann dann dem Writer ein WDataModel zugewiesen werden. Dies erm�glicht 
	 * die Wiederverwendung eines WDataModelWriters. 
	 */
  public WDataModelWriter() {
		super();
  }  
	/**
	 * Konstruiert einen WDataModelWriter f�r das angegebene WDataModel.
	 * @param dm 
	 */
  public WDataModelWriter(WDataModel dm) {
  	super(dm);
  }  
	/**
	 * Liefert einen WDataModelWriter auf ein 'leeres' WDataModel zur�ck.
	 * Dies spart Tipparbeit.
	 * @return
	 */
  public static WDataModelWriter getInstance() {
		return new WDataModelWriter(new WDataModel(new WSignature()));
  }      
	/**
	 * Erweitert das WDataModel um eine Spalte durch Hinzuf�gen 
	 * eines IWAttributes zur IWSignature.
	 * @param a
	 * @throws WDublicateAttributeException
	 */
  public void addAttribute(IWAttribute a) {
		mDataModel.addCol(a);
  }      
	/**
	 * F�gt dem WDataModel nach der letzten Zeile eine Zeile hinzu.
	 * Die Zeile enth�lt dann je IWAttribute eine 'leeres' IWDataItem.
	 * Der Zeilenzeiger (curRowNr()) steht dann auf dieser Zeile.
	 */
  public void addRow() {
  	mDataModel.addRow();
  }  
	/**
	 * F�gt in das WDataModel in der Zeile 'row' eine Zeile ein.
	 * Die Zeile enth�lt dann je IWAttribute eine 'leeres' IWDataItem.
	 * Der Zeilenzeiger steht dann auf dieser Zeile.
	 */
	public void addRow(int row, int count) {
		mDataModel.addRow(row, count);
	}  
	/**
	 * Entfernt die Zeile row aus dem WDataModel.
	 * @param row
	 */
  public void removeRow(int row) {
  	mDataModel.removeRow(row);
  }  
	/**
	 * Verschiebt im WDataModel die Zeile row um die in distance angegebene 
	 * Anzahl Zeilen.
	 * @param row
	 * @param distance
	 */
  public void moveRow(int row, int distance) {
    mDataModel.moveRow(row, distance);
  }
	/**
	 * L�scht die Daten des WDataModels, d.h. anschlie�end enth�lt das
	 * WDataModel keine IWDataItems mehr.
	 * Die IWSignature bleibt erhalten.
	 */
  public void clear() {
  	mDataModel.clear();
  }  
	/**
     * L�scht die Daten und die Spalten des WDataModels, d.h. anschlie�end enth�lt das
     * WDataModel keine IWDataItems und keine IWAttributes mehr.
     * Die IWSignature ist leer.
	 */
	public void clearAll() {
		mDataModel.clearAll();
	}  
	/**
	 * L�scht die Daten der Spalte 'name' des WDataModels, d.h. 
	 * anschlie�end enth�lt die Spalte keine IWDataItems mehr.
	 * Das IWAttribute in der IWSignature bleibt erhalten.
	 */
  public void clearCol(String name) throws WAttributeNotFoundException {
  	mDataModel.clearCol(name);
  }  
	/**
	 * Entfernt aus dem WDataModel die Spalte mit dem WAttributnamen 'name'.
	 * In der WSignature wird das IWAttribute mit dem Namen 'name' ebenfalls
	 * entfernt.
	 * @param name
	 */
  public void removeAttribute(String name) throws WAttributeNotFoundException {
  	mDataModel.removeCol(name);
  }  
	/**
	 * Setzt den Schalter mAliasIsName auf 'true' oder 'false'
	 * Der Schalter steuert, ob beim Zugriff auf ein IWAttriubte 
	 * mit dem Namen oder mit dem Alias gelesen wird. 
	 * @param b
	 */
	public void setAliasIsName(boolean b) {
		mSignature.setAliasIsName(b);
	}  
	/**
	 * Setzt den Zustand des WDataItems in der aktuellen Zeile und 
	 * der Spalte mit dem Attributnamen 'name' auf den Wert von 'state'.
	 * M�gliche Zust�nde:
	 * @see com.entitec.wax.wdm.IWDataItem
	 * @param name
	 * @param state
	 * @throws WAttributeNotFoundException
	 */
  public void setState(String name, short state) throws WAttributeNotFoundException {
		if (mSignature.getAliasIsName() == true) {
			//Lesen mit Alias, d.h. Mehrdeutig:
			//1. Schalter auf Name und Ermitteln aller Namen zu dem Alias.
			//2. �ndern aller States zu den gefundenen Namen
			mSignature.setAliasIsName(false);
			for (Enumeration e=mSignature.getNamesForAlias(name);e.hasMoreElements();) {
				mDataModel.getDataItem((String)e.nextElement()).setState(state);
			}
			mSignature.setAliasIsName(true);
		} else {
			//Lesen mit Name, d.h. eindeutig:
			//2. �ndern des States zu dem Namen
			mDataModel.getDataItem(name).setState(state);
		}
  }  
	/**
	 * Setzt den Zustand aller WDataItems des WDataModels auf den Wert 
	 * von 'state'.
	 * M�gliche Zust�nde:
	 * @see com.entitec.wax.wdm.IWDataItem
	 * @param state
	 */
  public void setAllStates(short state) {
  	mDataModel.setAllStates(state);
  }  
	/**
	 * Setzt den Wert des WDataItems in der aktuellen Zeile und 
	 * der Spalte mit dem Attributnamen 'name' auf den Wert von 'value'.
	 * @param name
	 * @param value
	 * @throws WAttributeNotFoundException
	 * @throws WValueNotTypConformException
	 */
  public void setValue(String name, Object value) 
    throws WAttributeNotFoundException, WValueNotTypConformException {
    if (value instanceof String) {
    	if (mSignature.getAliasIsName() == true) {
    		//Lesen mit Alias, d.h. Mehrdeutig:
    		//1. Schalter auf Name und Ermitteln aller Namen zu dem Alias.
    		//2. �ndern aller Values zu den gefundenen Namen
    		mSignature.setAliasIsName(false);
    		for (Enumeration e=mSignature.getNamesForAlias(name);e.hasMoreElements();) {
					this.checkAndSetValue((String)e.nextElement(), value);
    		}
				mSignature.setAliasIsName(true);
    	} else {
				//Lesen mit Name, d.h. eindeutig:
				//2. �ndern des Values zu dem Namen
				this.checkAndSetValue(name, value);
			}
    } else {
     	mDataModel.getDataItem(name).setValue(value);
    }
  }  
	private void checkAndSetValue(String name, Object value) { 
		IWAttribute a = mSignature.getAttribute((String)name);
		
		String v = WDMTypChecker.getTypConformValue(a, (String)value);
		if (v != null) {
			mDataModel.getDataItem(name).setValue(v);
		} else {
			throw new WValueNotTypConformException("\nAttribute: " + a.toMultiLineString() + "Value: '" + value + "'");
		}
	}  
	/**
	 * Setzt den Wert und den Zustand des WDataItems in der aktuellen 
	 * Zeile und der Spalte mit dem Attributnamen 'name' auf den Wert 
	 * von 'value'.
	 * ACHTUNG:
	 * Wenn 'state' als IWDataItem.DISABLED �bergeben wird, dann wird 
	 * der Wert des IWDataItems auf null gesetzt.
	 * M�gliche Zust�nde:
	 * @see com.entitec.wax.wdm.IWDataItem
	 * @param name
	 * @param value
	 * @param state
	 * @throws WAttributeNotFoundException
	 * @throws WValueNotTypConformException
	 */
  public void setValue(String name, Object value, short state) 
    throws WAttributeNotFoundException, WValueNotTypConformException {
		if (state == IWDataItem.DISABLED) {
			this.setValue(name, null);
		} else {
	    this.setValue(name, value);
		}
		this.setState(name, state);
  }  
	/**
	 * Setzt den Zeilenzeiger auf die �ber 'row' angegebene Zeile.
	 * ACHTUNG:
	 * Ist 'row' gleich der Zeilenanzahl, dies entspricht der ersten
	 * Zeile nach der letzten, dann wird automatisch mit addRow() 
	 * eine Zeile angef�gt.
	 * @param row
	 * @throws WRowNotFoundException
	 */
  public void setRow(int row) throws WRowNotFoundException {
  	mDataModel.setRow(row);
  }  
	/**
	 * Setzt den Modus mit dem das WDataModel bearbeitet wird.
	 * Ist der Modus auf 'homogen' eingestellt, so wird das WDataModel
	 * beim hinzuf�gen von WAttributen gleichm��ig aufgef�llt.
	 * Die default-Einstellung ist 'inhomogen'.
	 * @param modus
	 */
  public void setModus(int modus) {
    mDataModel.mModus = modus;
  }
	/**
	 * Pr�fen, ob DataModel indiziert ist.
	 * @return
	 */
	public boolean isIndexed() {
		return mDataModel.isIndexed();
	}
	/**
	 * Indizierung der DataModels aktivieren.
	 * Die Spalte IWSignature.INDEX_COLUMN_NAME wird der Signature
	 * hinzugef�gt und mit den Zeilennummern initialisiert.
	 */
	public void setIndexed() {
		mDataModel.setIndexed();
	}
	/**
	 * Indizierung der DataModels deaktivieren.
	 * Die Spalte IWSignature.INDEX_COLUMN_NAME wird aus der Signature
	 * entfernt.
	 */
	public void removeIndex() {
		mDataModel.removeIndex();
	}
	/**
	 * Liefert den Index der aktuellen Zeile.
	 * @return
	 */
	public int getIndexOfCurentRow() {
		return mDataModel.getIndexOfCurentRow();
	}
	/**
	 * Setzt den Zeilenzeiger auf die Row mit dem Index 'index'.
	 * @param index
	 */
	public int setRowToIndex(int index) {
		return mDataModel.setRowToIndex(index);
	}
}
