package com.entitec.wax.wdm;

import java.io.Serializable;

public interface IWAttribute extends Serializable {
	/**
	 * Konstante f�r das WAttriubte-Format charakter.
	 */
  public static final char FRM_CHAR = 'C';
	/**
	 * Konstante f�r das WAttriubte-Format numerisch.
	 */
  public static final char FRM_NUM = 'N';
	/**
	 * Konstante f�r das WAttriubte-Format decimal.
	 */
  public static final char FRM_DEC = 'D';
	/**
	 * Konstante f�r das WAttriubte-Format address.
	 */
	public static final char FRM_ADDR = 'A';
	/**
	 * Konstante f�r das WAttriubte-Format Color.
	 */
  public static final char FRM_COLOR = 'O';
	/**
	 * Konstante f�r das WAttriubte-Format WDataModel.
	 */
  public static final char FRM_DATAMODEL = 'M';

  public int compare(IWDataItem a, IWDataItem b);  
  public String getAlias();  
  public String getAliasDescription();
  public int getDec();  
  public String getDescription();
  public char getFrm();  
  public int getLen();  
  public String getName();  
  public boolean getSign();  
  public boolean getHomo();  
  public String getDefaultValue();
  public void setAlias(String a);  
  public void setDec(int d);  
  public void setFrm(char f);  
  public void setLen(int l);  
  public void setName(String n);  
  public void setSign(boolean s);  
  public void setHomo(boolean h);  
  public String toMultiLineString();
  public String toString();
}
