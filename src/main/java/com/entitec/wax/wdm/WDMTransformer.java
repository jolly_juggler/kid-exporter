package com.entitec.wax.wdm;

import com.entitec.wax.wcall.WSuccess;
import com.entitec.wax.wglobal.WRuntimeException;

public class WDMTransformer {
	private static final String EMPTY_STRING = "";
	private static final String DECIMAL_SIGN = ",";
	private static final String ZERO = "0";

  public static synchronized String transform(IWAttribute fromAttrib,
                                            IWDataItem  fromData,
                                            IWAttribute toAttrib, 
                                            IWDataItem  toData) {
                                            	
    /* pruefen der eingangswerte */
    if (fromData.getState() == IWDataItem.ERROR || 
        fromData.getState() == IWDataItem.INVALID){
    	throw new WRuntimeException ("K999", "Indikator Fehler", fromAttrib.getName());
    }
    if ((fromData.getValue() == null)||(fromData.getState() == IWDataItem.DISABLED)) {
    	toData.setValue(null); // ?? initialwert
    	toData.setState(IWDataItem.DISABLED);
    	return WSuccess.WRK_KEINE_FELDER;
    }

    /* Transformation */
    switch (fromAttrib.getFrm()) {
    	case IWAttribute.FRM_CHAR:
			case IWAttribute.FRM_ADDR:
		    switch (toAttrib.getFrm()) {
		    	case IWAttribute.FRM_CHAR:
					case IWAttribute.FRM_ADDR:
		    	  transformCharToChar(fromAttrib, fromData, toAttrib, toData);
		    	  break;
		    	case IWAttribute.FRM_NUM:
		    	  transformCharToNum(fromAttrib, fromData, toAttrib, toData);
		    	  break;
		    	case IWAttribute.FRM_DEC:
		    	  transformCharToDec(fromAttrib, fromData, toAttrib, toData);
		    	  break;
		    	default:
					throw new WRuntimeException ("K892");
		    }
    	  break;
    	case IWAttribute.FRM_NUM:
		    switch (toAttrib.getFrm()) {
		    	case IWAttribute.FRM_CHAR:
					case IWAttribute.FRM_ADDR:
		    	  transformNumToChar(fromAttrib, fromData, toAttrib, toData);
		    	  break;
		    	case IWAttribute.FRM_NUM:
		    	  transformNumToNum(fromAttrib, fromData, toAttrib, toData);
		    	  break;
		    	case IWAttribute.FRM_DEC:
		    	  transformNumToDec(fromAttrib, fromData, toAttrib, toData);
		    	  break;
		    	default:
					throw new WRuntimeException ("K892");
		    }
    	  break;
    	case IWAttribute.FRM_DEC:
		    switch (toAttrib.getFrm()) {
		    	case IWAttribute.FRM_CHAR:
					case IWAttribute.FRM_ADDR:
		    	  transformDecToChar(fromAttrib, fromData, toAttrib, toData);
		    	  break;
		    	case IWAttribute.FRM_NUM:
		    	  transformDecToNum(fromAttrib, fromData, toAttrib, toData);
		    	  break;
		    	case IWAttribute.FRM_DEC:
		    	  transformDecToDec(fromAttrib, fromData, toAttrib, toData);
		    	  break;
		    	default:
					throw new WRuntimeException ("K892");
		    }
    	  break;
    	default:
    		toData.setValue(fromData.getValue());
    		toData.setState(fromData.getState());
// AB0043: Keine Exception, da andere Attributformat (FRM_COLOR) nicht
//         �ber den Transformator laufen sollen. Daher direkte Wertzuweisung.
//			throw new WRuntimeException ("K892");
    }
    
    /* pruefen der transformation */
	if ((fromData.getState() == IWDataItem.INVALID)) {
		toData.setState(IWDataItem.DISABLED);
		toData.setValue(null); //?? Initial
		return WSuccess.WRK_FORMAT_FEHLER;
	}

	if ((toData.getState() == IWDataItem.TRUNCATED)) {
//		throw new WRuntimeException ("K999", "gek�rztes Feld", 
//			fromAttrib.getName()+"="+fromData.getValue(), toAttrib.getName()+"="+toData.getValue());
		return WSuccess.WRK_GEKUERZTE_FELDER;
	}
	
	return WSuccess.WRK_OK;
  }
  /**
   * Transformiert den Kid-Typ Character nach Character. Dabei wird der Wert nicht mit 
   * abschliessenden Spaces, entsprechend seiner L�nge, aufgef�llt.<p>
   * 
   * Ist die signifikante L�nge von fromData gr��er als von toData, so wird 
   * rechtsseitig gek�rzt und der Indikator in toData auf '2' (TRUNCATED) gesetzt.<p>
   * 
   * Bei erfolgreicher Transformation wird der Indiktor auf '1' (ENABLED) gesetzt. <p>
   * 
   * Bei fehlerhaften Format im fromData wird der Indikator von fromData auf '9' gesetzt.
   * Das toData bleibt unber�hrt.
   * 
   * @param fromAttrib	Attributbeschreibung von
   * @param fromData	Daten von
   * @param toAttrib	Attributbeschreibung Ziel
   * @param toData		Daten Ziel
   */
  private static void transformCharToChar(IWAttribute fromAttrib,
	IWDataItem  fromData, IWAttribute toAttrib, IWDataItem  toData) {
	  int fromLen = fromAttrib.getLen();
	  String v = (String)fromData.getValue();
	  v = WDMTypChecker.checkCharacter(fromLen, v);
	  if ( v == null ){
	  	fromData.setState(IWDataItem.INVALID);
	  	return;
	  }
	  
	  int toLen = toAttrib.getLen();
	  String vv = WDMTypChecker.checkCharacter(toLen, v);
	  if ( vv == null ){
		toData.setValue(WDMTypChecker.truncateCharacter(toLen, v));
		toData.setState(IWDataItem.TRUNCATED);
	  } else {
		toData.setValue(vv);
		toData.setState(fromData.getState());	  	
	  }
  }
  
  /**
   * Transformiert das Kid-Format Charakter nach Numerisch. 
   * @param fromAttrib
   * @param fromData
   * @param toAttrib
   * @param toData
   */
  
  private static void transformCharToNum(IWAttribute fromAttrib,
	IWDataItem  fromData, IWAttribute toAttrib, IWDataItem  toData) {
		// pruefe eingang
		int fromLen = fromAttrib.getLen();
		String v = ((String)fromData.getValue()).trim();
		v = WDMTypChecker.checkCharacter(fromLen, v);
		if ( v == null ){
		  fromData.setState(IWDataItem.INVALID);
		  return;
		}
	  
	  	if ( v.length() == 0 ) v = ZERO;
	  	
	  	// transformiere
		int toLen = toAttrib.getLen();
		String vv = WDMTypChecker.checkNumeric(toLen, v);
		if ( vv == null ){
		  vv = WDMTypChecker.truncateNumeric(toLen, v);
		  vv = WDMTypChecker.checkNumeric(toLen, vv);
		  if ( vv == null ){
			  fromData.setState(IWDataItem.INVALID);
			  return;
		  }
		  toData.setValue(vv);
		  toData.setState(IWDataItem.TRUNCATED);
		} else {
		  toData.setValue(vv);
		  toData.setState(fromData.getState());	  	
		}
  }
  
  public static void transformCharToDec(IWAttribute fromAttrib,
		IWDataItem  fromData, IWAttribute toAttrib, IWDataItem  toData) {
	
		//toAttribute Kennzeichen
		int toLen = toAttrib.getLen();
		int toDec = toAttrib.getDec();	
		int toInt = toLen - toDec;
		boolean toSign = toAttrib.getSign();
		//Korrektheit gegen fromAttribute pr�fen
		String v = ((String)fromData.getValue()).trim();
		v = WDMTypChecker.checkCharacter(fromAttrib.getLen(), v);
		if ( v == null ){                               //fromData ist NICHT g�ltig
			fromData.setState(IWDataItem.INVALID);
		} else {                                        //fromData ist g�ltig
			//fromIntValue aufbereiten
			String fromIntValue = WDMTypChecker.getIntegerPart(v);
			if (fromIntValue == null) { fromData.setState(IWDataItem.INVALID); return; }
			if (fromIntValue.length() == 0) fromIntValue = "0";
			//fromDecValue aufbereiten
			String fromDecValue = WDMTypChecker.getDecimalPart(v);
			if (fromDecValue == null) { fromData.setState(IWDataItem.INVALID); return; }
			if (fromDecValue.length() == 0) fromDecValue = "0";
			//fromSignValue aufbereiten
			int fromSignPos     = WDMTypChecker.getSignPos(v);
			char fromSignValue  = '+';
			if (fromSignPos > -1) fromSignValue  = v.charAt(fromSignPos);
			//Einheitliches fromValue zusammensetzen
			int fromInt = fromIntValue.length();
			int fromDec = fromDecValue.length();
			boolean fromSign = true;
			StringBuffer sb = new StringBuffer(fromInt+fromDec+2);
			sb.append(fromIntValue);sb.append(WDMTransformer.DECIMAL_SIGN);sb.append(fromDecValue);sb.append(fromSignValue);
			
			if ( fromInt  == toInt 
				&& fromDec  == toDec 
				&& fromSign == toSign ) {                   //Format gleich
				toData.setValue(sb.toString());
				toData.setState(fromData.getState());	  	
			} else {                                      //Format NICHT gleich
				IWDataItem di = WDMTypChecker.transformDecimal(fromInt+fromDec, fromInt, fromDec, fromSign, 
																											 toLen, toInt, toDec, toSign, sb.toString());
				toData.setValue(di.getValue());
				if (di.getState() == IWDataItem.ENABLED) {
					toData.setState(fromData.getState());
				} else {
					toData.setState(di.getState());
				}
			}
		}
  }
  
  /**
   * Transformiert das Kid-Format Numerisch nach Charakter.
   * 
   * @param fromAttrib
   * @param fromData
   * @param toAttrib
   * @param toData
   */
  private static void transformNumToChar(IWAttribute fromAttrib,
																 IWDataItem  fromData,
																 IWAttribute toAttrib, 
																 IWDataItem  toData) {
  
		// pruefe eingang
		int fromLen = fromAttrib.getLen();
		String v = (String)fromData.getValue();
		v = WDMTypChecker.checkNumeric(fromLen, v);
		if ( v == null ){
		  fromData.setState(IWDataItem.INVALID);
		  return;
		}
	  
		// transformiere
		int toLen = toAttrib.getLen();
		String vv = WDMTypChecker.checkCharacter(toLen, v);
		if ( vv == null ){
			String numFromChar = WDMTypChecker.checkNumeric(toLen, v);
			if ( numFromChar == null ){
				numFromChar = v.substring(v.length() - toLen, v.length());
		  	toData.setValue(WDMTypChecker.truncateCharacter(toLen, numFromChar));
		  	toData.setState(IWDataItem.TRUNCATED);
			} else {
				toData.setValue(numFromChar);
				toData.setState(fromData.getState());	  	
			}
		} else {
		  toData.setValue(vv);
		  toData.setState(fromData.getState());	  	
		}
	}
	
/**
 * Transformiert den Kid-Typ Numerisch nach Numerisch. Dabei wird der Wert 
 * linksseitig mit f�hrenden Nullen, entsprechend seiner L�nge, aufgef�llt.<p>
 * Ist der Wert nicht numerisch, so wird eine WRuntimeException erzeugt.
 * 
 * @param fromAttrib	Attributbeschreibung Quelle
 * @param fromData	Attributwert Quelle
 * @param toAttrib	Attributbeschreibung Ziel
 * @param toData	Attributwert Ziel
 */
  private static void transformNumToNum(IWAttribute fromAttrib, IWDataItem  fromData,
	IWAttribute toAttrib, IWDataItem  toData) {
	  // pruefe eingang
	  int fromLen = fromAttrib.getLen();
	  String v = (String)fromData.getValue();
	  v = WDMTypChecker.checkNumeric(fromLen, v);
	  if ( v == null ){
		fromData.setState(IWDataItem.INVALID);
		return;
	  }
	  
	  // transformiere
	  int toLen = toAttrib.getLen();
	  String vv = WDMTypChecker.checkNumeric(toLen, v);
	  if ( vv == null ){
		toData.setValue(WDMTypChecker.truncateNumeric(toLen, v));
		toData.setState(IWDataItem.TRUNCATED);
	  } else {
		toData.setValue(vv);
		toData.setState(fromData.getState());	  	
	  }
  }
  
  private static void transformNumToDec(IWAttribute fromAttrib,
    IWDataItem  fromData, IWAttribute toAttrib, IWDataItem  toData) {
		//fromAttribute Kennzeichen
		int fromLen = fromAttrib.getLen();
		int fromDec = fromAttrib.getDec();
		int fromInt = fromLen - fromDec;
		boolean fromSign = fromAttrib.getSign();
		//toAttribute Kennzeichen
		int toLen = toAttrib.getLen();
		int toDec = toAttrib.getDec();	
		int toInt = toLen - toDec;
		boolean toSign = toAttrib.getSign();
		//Korrektheit gegen fromAttribute pr�fen
		String v = (String)fromData.getValue();
		v = WDMTypChecker.checkNumeric(fromLen, v);
		if ( v == null ){                               //fromData ist NICHT g�ltig
			fromData.setState(IWDataItem.INVALID);
		} else {                                        //fromData ist g�ltig
			if ( fromInt  == toInt 
				&& fromDec  == toDec 
				&& fromSign == toSign ) {                   //Format gleich
				toData.setValue(v);
				toData.setState(fromData.getState());	  	
			} else {                                      //Format NICHT gleich
				IWDataItem di = WDMTypChecker.transformDecimal(fromLen, fromInt, fromDec, fromSign, 
				                                               toLen, toInt, toDec, toSign, v);
				toData.setValue(di.getValue());
				if (di.getState() == IWDataItem.ENABLED) {
					toData.setState(fromData.getState());
				} else {
					toData.setState(di.getState());
				}
			}
		}
  }
  
  private static void transformDecToChar(IWAttribute fromAttrib, 
  	IWDataItem  fromData, IWAttribute toAttrib, IWDataItem  toData) {
					                                           	
		int fromLen = fromAttrib.getLen();
		int fromDec = fromAttrib.getDec();
		boolean fromSign = fromAttrib.getSign();
		
		String v = (String)fromData.getValue();
		v = WDMTypChecker.checkDecimal(fromLen, fromDec, fromSign, v);
		if ( v == null ){
		  fromData.setState(IWDataItem.INVALID);
		  return;
		}
		
		int toLen = toAttrib.getLen();
	
		String vv = WDMTypChecker.checkCharacter(toLen, v);
		if ( vv == null ){
		  toData.setValue(WDMTypChecker.truncateCharacter(toLen, v));
		  toData.setState(IWDataItem.TRUNCATED);
		} else {
		  toData.setValue(vv);
		  toData.setState(fromData.getState());	  	
		}
  }
  private static void transformDecToNum(IWAttribute fromAttrib,
  	IWDataItem  fromData, IWAttribute toAttrib, IWDataItem  toData) {

	  int fromLen = fromAttrib.getLen();
	  int fromDec = fromAttrib.getDec();
	  boolean fromSign = fromAttrib.getSign();
		
	  String v = (String)fromData.getValue();
	  v = WDMTypChecker.checkDecimal(fromLen, fromDec, fromSign, v);
	  if ( v == null ){
		fromData.setState(IWDataItem.INVALID);
		return;
	  }
		
	  int toLen = toAttrib.getLen();
	
	  String vv = WDMTypChecker.checkDecimal(toLen, 0, false, v);
	  if ( vv == null ){
	  	
		vv = WDMTypChecker.truncateNumeric(toLen, v);
		vv = WDMTypChecker.checkDecimal(toLen, 0, false, vv);
		if ( vv == null ){
			fromData.setState(IWDataItem.INVALID);
			return;
		}
		toData.setValue(vv);
		toData.setState(IWDataItem.TRUNCATED);
	  } else {
		toData.setValue(vv);
		toData.setState(fromData.getState());	  	
	  }

  }
  private static void transformDecToDec(IWAttribute fromAttrib,
	  IWDataItem  fromData, IWAttribute toAttrib, IWDataItem  toData) {
		//fromAttribute Kennzeichen
	  int fromLen = fromAttrib.getLen();
	  int fromDec = fromAttrib.getDec();
		int fromInt = fromLen - fromDec;
	  boolean fromSign = fromAttrib.getSign();
		//toAttribute Kennzeichen
		int toLen = toAttrib.getLen();
		int toDec = toAttrib.getDec();	
		int toInt = toLen - toDec;
		boolean toSign = toAttrib.getSign();
		//Korrektheit gegen fromAttribute pr�fen
	  String v = (String)fromData.getValue();
	  v = WDMTypChecker.checkDecimal(fromLen, fromDec, fromSign, v);
	  if ( v == null ){                               //fromData ist NICHT g�ltig
			fromData.setState(IWDataItem.INVALID);
	  } else {                                        //fromData ist g�ltig
			if ( fromInt  == toInt 
			  && fromDec  == toDec 
			  && fromSign == toSign ) {                   //Format gleich
				toData.setValue(v);
				toData.setState(fromData.getState());	  	
			} else {                                      //Format NICHT gleich
				IWDataItem di = WDMTypChecker.transformDecimal(fromLen, fromInt, fromDec, fromSign, 
																											 toLen, toInt, toDec, toSign, v);
				toData.setValue(di.getValue());
				if (di.getState() == IWDataItem.ENABLED) {
					toData.setState(fromData.getState());
				} else {
					toData.setState(di.getState());
				}
			}
	  }
  }

}

