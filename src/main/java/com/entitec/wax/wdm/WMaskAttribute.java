package com.entitec.wax.wdm;

import com.entitec.wax.wglobal.WRuntimeException;


/**
 * Date:    03.07.2003
 * <p>Description:</p>
 * Die Klasse WMaskAttribute erweitert die Klasse WAttriubte um die 
 * Eigenschalften von Maskenattributen. Hierzu z�hlen:
 * - mCap, die Caption des WAttributes
 * - mDisp, das DisplayFormat des WAttributes
 * - mMode, der DisplayMode des WAttributes
 * - mPvermo, der Verarbeitungsmodus
 * @author AB0043
 */
public class WMaskAttribute extends WAttribute implements IWMaskAttribute{
  protected String mCap;
  protected char mDisp;
  protected int mMode;
  protected int mPvermo;
  private boolean mFirstFocusedControl = false;
  
  public WMaskAttribute(String name, String alias, char frm, int len, int dec,
					   boolean sign) {
		this(name, alias, frm, len, dec, sign, false);
  }  
  public WMaskAttribute(String name, String alias, char frm, int len, int dec,
					   boolean sign, boolean homo) {
		super(name, alias, frm, len, dec, sign, homo);
		this.setCap("");
		this.setMode(VISIBLE);
		this.setPvermo(IWMaskAttribute.PVM_DEFAULT);
		if (frm == IWAttribute.FRM_CHAR) {
		  this.setDisp(FMT_CHAR);
		} else if (frm == IWAttribute.FRM_NUM) {
		  this.setDisp(FMT_NUM);
		} else if (frm == IWAttribute.FRM_DEC) {
		  this.setDisp(FMT_DEC);
		} else {
		  throw new WRuntimeException("AttributFormat '" +  frm + "' not supported.");
		}
  }  
  public WMaskAttribute(String name, String alias, char frm, int len, int dec,
					   boolean sign, String cap, char disp, int mode) {
		this(name, alias, frm, len, dec, sign, false, cap, disp, mode);
  }  
  public WMaskAttribute(String name, String alias, char frm, int len, int dec,
					   boolean sign, boolean homo, String cap, char disp, int mode) {
		this(name, alias, frm, len, dec, sign, homo, cap, disp, mode, IWMaskAttribute.PVM_DEFAULT);
  }  
	public WMaskAttribute(String name, String alias, char frm, int len, int dec,
						 boolean sign, boolean homo, String cap, char disp, int mode, int vermo) {
		super(name, alias, frm, len, dec, sign, homo);
		this.setCap(cap);
		this.setDisp(disp);
		this.setMode(mode);
		this.setPvermo(vermo);
	}  
  public String getCap() {
	return mCap;
  }  
  public char getDisp() {
	return mDisp;
  }  
  public int getMode() {
	return mMode;
  }  
  public int getPvermo() {
	return mPvermo;
  }  
  public void setCap(String c) {
	mCap = c;
  }  
  public void setDisp(char d) {
	mDisp = d;
  }  
  public void setMode(int m) {
	mMode = m;
  } 
  public void setPvermo(int pvermo) {
	mPvermo = pvermo;
  }
  
  public void setFirstFocused(boolean focus) {
  	mFirstFocusedControl = focus;
  }
  
  public boolean isFirstFocused() {
  	return mFirstFocusedControl;
  }
	/**
	 * R�ckgabe des WAttributes als String.
	 * @return
	 */
	public String toString() {
		StringBuffer sb = new StringBuffer(160);
		this.toString(sb);
		return sb.toString();
	}  
	/**
	 * Aufl�sen des WAttributes in den �bergebenen StringBuffer.
	 * @param sb
	 */
	public void toString(StringBuffer sb) {
		sb.append("<WATTRIBUTE Name=\"");
		sb.append(mName);
		sb.append("\"");
		for(int i=0;i<16-mName.length();i++) sb.append(' ');
		sb.append(" Alias=\"");
		sb.append(mAlias);
		sb.append("\"");
		for(int i=0;i<16-mAlias.length();i++) sb.append(' ');
		sb.append(" Frm=\"");
		sb.append(mFrm);
		sb.append("\" Len=\"");
		if (mLen < 10) {
			sb.append('0');
			sb.append('0');
			sb.append('0');
		} else if (mLen < 100) {
			sb.append('0');
			sb.append('0');
		} else if (mLen < 1000) {
			sb.append('0');
		}
		sb.append(mLen);
		sb.append("\" Dec=\"");
		sb.append(mDec);
		sb.append("\" Sign=\"");
		sb.append(mSign);
		if (mSign == true) sb.append(' ');
		sb.append("\" Homo=\"");
		sb.append(mHomo);
		sb.append("\" Cap=\"");
		sb.append(mCap);
		sb.append("\" Disp=\"");
		sb.append(mDisp);
		sb.append("\" Mode=\"");
		sb.append(mMode);
		sb.append("\" Vermo=\"");
		sb.append(mPvermo);
		sb.append("\"/>\n");
  }    
}
