package com.entitec.wax.wdm;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Vector;

import com.entitec.wax.wglobal.WRuntimeException;

/**
 * Date:    03.07.2003
 * <p>Description:</p>
 * Die Klasse WDMToolbox enth�lt eine Sammlung statischer Methoden
 * zur Bearbeitung von WDataModels. Hierzu z�hlen folgende Methoden:
 * - Die Signature eines WDataModels zu einem anderen hinzuf�gen
 * - Die Daten eines WDataModels zu einem anderen hinzuf�gen
 * - Mit der Signature und den Daten eines WDataModels ein anderes aktualisieren
 * - Mit den Daten eines WDataModels die eines anderen aktualisieren
 * - Mit den Daten einer Zeile eines WDataModels die Zeile eines anderen aktualisieren
 * - Mit den Daten eines WDataModels unter Ber�cksichtigung des Zustands 
 *   die eines anderen aktualisieren
 * - Ein WDataModel sortieren
 * - Ein WDataModel als String im Format 'csv' (Komma separiert) ausgeben
 * - Ein WDataModel als String im Format 'skid' ausgeben
 * @author ab0043
 */
public final class WDMToolbox {
	public static final char UNSORTED = 'N';
	public static final char ASCENDING = 'A';
	public static final char DESCENDING = 'D';

	public static final String SUCCESS_OK = "00";
	public static final String SUCCESS_KEINE_FELDER = "01";
	public static final String SUCCESS_GEKUERZTE_FELDER = "02";
	public static final String SUCCESS_FELDINHALT_FEHLER = "03";
	public static final String SUCCESS_NOT_OK = "07";
	
	private static final String CENTURY_PREFIX = "20";
	private static final String DATE_DELIMITER = ".";
	private static final String TIME_DELIMITER = ":";
	private static final String NULL_SECONDS = "00";
	/**
	 * Hinzuf�gen von nicht vorhandenen Spalten 
	 * aus dem from-DataModel in das to-DataModel. 
	 * @param from Von-WDataModel
	 * @param to Nach-WDataModel
	 */
	public static synchronized void appendSig(WDataModel from, WDataModel to) {
		if (from != to) {
			IWSignature fromSig = from.getSignature();
			IWSignature toSig = to.getSignature();
			Enumeration keys = fromSig.keys();
			WDataModelWriter dmw = new WDataModelWriter(to);
			while (keys.hasMoreElements()) {
				String name = (String) keys.nextElement();
				if (!(toSig.hasAttribute(name))) {
					IWAttribute attrib = (IWAttribute)WDMToolbox.deepCopy(
																						fromSig.getAttribute(name));
					dmw.addAttribute(attrib);
				}
			}
		}
	}
	/**
	 * Hinzuf�gen von nicht vorhandenen Spalten mit Indikator IWDataItem.ENABLED oder
	 * IWDataItem.UNCHANGED aus dem from-DataModel in das to-DataModel. 
	 * (Analog zu KID: EXPAND-DESCRIPTION)
	 * @param from Von-WDataModel
	 * @param to Nach-WDataModel
	 */
	public static synchronized void appendSigByIndicator(WDataModel from, WDataModel to) {
		if (from != to) {
			IWSignature fromSig = from.getSignature();
			IWSignature toSig = to.getSignature();
			Enumeration keys = fromSig.keys();
			WDataModelReader dmr = new WDataModelReader(from);
			WDataModelWriter dmw = new WDataModelWriter(to);
			int curRow = from.mCurRow;
			dmr.setRow(0);
			while (keys.hasMoreElements()) {
				String name = (String) keys.nextElement();
				if (!(toSig.hasAttribute(name)) && ((dmr.getState(name) == IWDataItem.ENABLED) ||
				(dmr.getState(name) == IWDataItem.UNCHANGED))) {
					IWAttribute attrib = (IWAttribute)WDMToolbox.deepCopy(
																						fromSig.getAttribute(name));
					dmw.addAttribute(attrib);
				}
			}
			from.mCurRow = curRow;
		}
	}
	/**
	 * Hinzuf�gen von Spalten aus from mit Indikator 1 o. 4 nach to nebst Umsetzung von
	 * Alias auf Name um Duplicate Problematik zu vermeiden.
	 */
	public static synchronized void appendSigWithoutAlias(WDataModel from, WDataModel to) {
			if (from != to) {
				IWSignature fromSig = from.getSignature();
				IWSignature toSig = to.getSignature();
				Enumeration keys = fromSig.keys();
				WDataModelWriter dmw = new WDataModelWriter(to);
				WDataModelReader dmr = new WDataModelReader(from);
				int curRow = from.mCurRow;
				dmr.setRow(0);
				while (keys.hasMoreElements()) {
					String name = (String) keys.nextElement();
					if (!(toSig.hasAttribute(name)) && ((dmr.getState(name) == IWDataItem.ENABLED) ||
					(dmr.getState(name) == IWDataItem.UNCHANGED))) {
						IWAttribute attrib =
							(IWAttribute) WDMToolbox.deepCopy(
								fromSig.getAttribute(name));
						attrib.setAlias(name);
						attrib.setName(name);
						dmw.addAttribute(attrib);
					}
				}
				from.mCurRow = curRow;
			}
		}
	/**
	 * Anf�gen der Daten aus dem from-DataModel in das to-DataModel
	 * nach der letzten Zeile �ber den Transformator (WDMTransformer). 
	 * @param from Von-WDataModel
	 * @param to Nach-WDataModel
	 * @return
	 */
	public static synchronized String append(WDataModel from, WDataModel to) {
		String succ = SUCCESS_OK;
		if (from != to) {
			IWDataItem fromDi = null;
			IWDataItem toDi = null;
			IWSignature fromSig = from.getSignature();
			IWSignature toSig = to.getSignature();
			succ = SUCCESS_KEINE_FELDER;

			boolean aliasIsName = toSig.getAliasIsName(); //AliasIsName merken
			toSig.setAliasIsName(false);                  //Denn  hier wird er ver�ndert
			//Enumeration �ber alle Attribute
			Enumeration keys = toSig.keys();
			int curRowfrom = from.mCurRow;
			int fromRowCount = from.mRowCount;
			int toRowCount = to.mRowCount;
			boolean rowAdded = false;
			while (keys.hasMoreElements()) {
				String name = (String) keys.nextElement();
				//Index-Spalte in to wird automatisch gesetzt, daher �berspringen.
				if (name.equals(IWSignature.INDEX_COLUMN_NAME)) continue;
				IWAttribute toAttrib = toSig.getAttribute(name);
				String toName = name;
				if (aliasIsName) toName = toAttrib.getAlias();
				if (fromSig.hasAttribute(toName)) {
					IWAttribute fromAttrib = fromSig.getAttribute(toName);
					if (rowAdded == false) {
						for (int i = 0; i < fromRowCount; i++) {
							to.addRow();
						}
						rowAdded = true;
					}
					to.mCurRow = toRowCount - 1;
					for (int i = 0; i < fromRowCount; i++) {
						from.setRow(i);
						to.setRow(toRowCount + i);
						fromDi = from.getDataItem(toName);
						toDi = to.getDataItem(name);
						WDMTransformer.transform(
							fromAttrib,
							fromDi,
							toAttrib,
							toDi);
						String s = getSuccess(toDi);
						if (!SUCCESS_GEKUERZTE_FELDER.equals(succ) && SUCCESS_GEKUERZTE_FELDER.equals(s)){
							succ = s;							
						} else if (!SUCCESS_OK.equals(succ) &&(!SUCCESS_GEKUERZTE_FELDER.equals(succ)&& SUCCESS_OK.equals(s))){
							succ = s;
						}
					}
				}
			}
			from.mCurRow = curRowfrom;
			toSig.setAliasIsName(aliasIsName);
		}
		return succ;
	}
	/**
	 * @deprecated Achtung, die Methode arbeitet OHNE Transformator
	 * (WDMTransformer) und �ndert ggf. die Signature vorhandener 
	 * Attribute im to-DataModel.  
	 * �berschreiben der Signature und der Daten aus dem 
	 * from-DataModel in dem to-DataModel. 
	 * @param from Von-WDataModel
	 * @param to Nach-WDataModel
	 * @return
	 */

	public static synchronized String updateAll(WDataModel from, WDataModel to) {
		String succ = SUCCESS_OK;
		if (from != to) {
			IWDataItem fromDi = null;
			IWDataItem toDi = null;
			IWSignature fromSig = from.getSignature();
			IWSignature toSig = to.getSignature();
			succ = SUCCESS_KEINE_FELDER;
			boolean aliasIsName = toSig.getAliasIsName(); //AliasIsName merken
			toSig.setAliasIsName(false);                  //Denn  hier wird er ver�ndert
			//Enumeration �ber alle Attribute
			Enumeration keys = toSig.keys();
			int curRowfrom = from.mCurRow;
			int fromRowCount = from.mRowCount;
			while (keys.hasMoreElements()) {
				String name = (String) keys.nextElement();
				//Index-Spalte in to wird automatisch gesetzt, daher �berspringen.
				if (name.equals(IWSignature.INDEX_COLUMN_NAME)) continue;
				IWAttribute toAttrib = toSig.getAttribute(name);
				String toName = name;
				if (aliasIsName) toName = toAttrib.getAlias();
				if (fromSig.hasAttribute(toName)) {
					IWAttribute fromAttrib = fromSig.getAttribute(toName);
					toAttrib.setName(fromAttrib.getName());
					toAttrib.setAlias(fromAttrib.getAlias());
					toAttrib.setFrm(fromAttrib.getFrm());
					toAttrib.setLen(fromAttrib.getLen());
					toAttrib.setDec(fromAttrib.getDec());
					toAttrib.setSign(fromAttrib.getSign());
					//Daten von from nach to �bertragen
					for (int i = 0; i < fromRowCount; i++) {
						from.setRow(i);
						to.setRow(i);
						fromDi = from.getDataItem(toName);
						toDi = to.getDataItem(name);
						toDi.setValue(fromDi.getValue());
						toDi.setState(fromDi.getState());
						String s = getSuccess(toDi);
						if (!SUCCESS_GEKUERZTE_FELDER.equals(succ) && SUCCESS_GEKUERZTE_FELDER.equals(s)){
							succ = s;							
						} else if (!SUCCESS_OK.equals(succ) &&(!SUCCESS_GEKUERZTE_FELDER.equals(succ)&& SUCCESS_OK.equals(s))){
							succ = s;
						}
					}
				}
			}
			from.mCurRow = curRowfrom;
			toSig.setAliasIsName(aliasIsName);
		}
		return succ;
	}
	/**
	 * �bernehmen der Daten aus dem from-DataModel in das to-DataModel
	 * in im Attributnamen �bereinstimmenden Spalten �ber den 
	 * Transformator (WDMTransformer). 
	 * @param from Von-WDataModel
	 * @param to Nach-WDataModel
	 * @return
	 */
	public static synchronized String updateData(
		WDataModel from,
		WDataModel to) {
		return WDMToolbox.updateData(from, to, true);
	}
	/**
	 * �bernehmen der Daten aus dem from-DataModel in das to-DataModel
	 * in im Attributnamen �bereinstimmenden Spalten optional �ber den 
	 * Transformator (WDMTransformer). 
	 * @param from Von-WDataModel
	 * @param to Nach-WDataModel
	 * @param withTransform Schalter, ob �bernehmen der Daten mit Transformation
	 * @return
	 */
	public static synchronized String updateData(
		WDataModel from,
		WDataModel to,
		boolean withTransform) {
		String succ = SUCCESS_OK;
		if (from == null || to == null){
			if (from == null && to != null){
				to.clear();
			}
			return SUCCESS_KEINE_FELDER;
		}
		if (from != to) {
			IWDataItem fromDi = null;
			IWDataItem toDi = null;
			IWSignature fromSig = from.getSignature();
			IWSignature toSig = to.getSignature();
			succ = SUCCESS_KEINE_FELDER;
      boolean aliasIsName = toSig.getAliasIsName(); //AliasIsName merken
      toSig.setAliasIsName(false);                  //Denn  hier wird er ver�ndert
			//Enumeration �ber alle Attribute
			Enumeration keys = toSig.keys();
			int curRowfrom = from.mCurRow;
			int fromRowCount = from.mRowCount;
			while (keys.hasMoreElements()) {
				String name = (String) keys.nextElement();
				//Index-Spalte in to wird automatisch gesetzt, daher �berspringen.
				if (name.equals(IWSignature.INDEX_COLUMN_NAME)) continue;
				IWAttribute toAttrib = toSig.getAttribute(name);
				String toName = name;
				if (aliasIsName) toName = toAttrib.getAlias();
				if (fromSig.hasAttribute(toName)) {
					IWAttribute fromAttrib = fromSig.getAttribute(toName);
					to.clearCol(name);
					for (int i = 0; i < fromRowCount; i++) {
						from.setRow(i);
						to.setRow(i);
						fromDi = from.getDataItem(toName);
						toDi = to.getDataItem(name);
						if (withTransform) {
								WDMTransformer.transform(
								fromAttrib,
								fromDi,
								toAttrib,
								toDi);
							String s = getSuccess(toDi);
							
							if (!SUCCESS_GEKUERZTE_FELDER.equals(succ) && SUCCESS_GEKUERZTE_FELDER.equals(s)){
								succ = s;							
							} else if (!SUCCESS_OK.equals(succ) &&(!SUCCESS_GEKUERZTE_FELDER.equals(succ)&& SUCCESS_OK.equals(s))){
								succ = s;
							}
						} else {
							toDi.setValue(fromDi.getValue());
							toDi.setState(fromDi.getState());
							succ = SUCCESS_OK;
						}
					}
				} else{
					to.clearCol(name);
				}
			}
			from.mCurRow = curRowfrom;
			toSig.setAliasIsName(aliasIsName);
		}
		return succ;
	}
/**
 * �bernehmen der Daten aus Zeile 'from' aus dem from-DataModel 
 * in Zeile 'to' in das to-DataModel in im Attributnamen 
 * �bereinstimmenden Spalten �ber den Transformator (WDMTransformer). 
 * Bedingung:
 * Die Ausf�hrung ist nur m�glich, wenn die Zeilen aus unterschiedlichen
 * DataModels stammen oder wenn die DataModels identisch, m�ssen die 
 * Zeilennummern unterschiedlich sein.
 * @param from Von-WDataModel
 * @param from Von-WDataModelRow
 * @param to Nach-WDataModel
 * @param to Nach-WDataModelRow
 * @return
 */
	public static synchronized String updateDataRowByRow(
		WDataModel from,
		int fromRow,
		WDataModel to,
		int toRow) {
		return WDMToolbox.updateDataRowByRow(from, fromRow, to, toRow, true);
	}
	/**
	 * �bernehmen der Daten aus Zeile 'from' aus dem from-DataModel 
	 * in Zeile 'to' in das to-DataModel in im Attributnamen 
	 * �bereinstimmenden Spalten optional �ber den Transformator (WDMTransformer). 
	 * Bedingung:
	 * Die Ausf�hrung ist nur m�glich, wenn die Zeilen aus unterschiedlichen
	 * DataModels stammen oder wenn die DataModels identisch, m�ssen die 
	 * Zeilennummern unterschiedlich sein.
	 * @param from Von-WDataModel
	 * @param from Von-WDataModelRow
	 * @param to Nach-WDataModel
	 * @param to Nach-WDataModelRow
	 * @param withTransform Schalter, ob �bernehmen der Daten mit Transformation
	 * @return
	 */
 	public static synchronized String updateDataRowByRow(
		WDataModel from,
		int fromRow,
		WDataModel to,
		int toRow,
		boolean withTransform) {
		String succ = SUCCESS_OK;
		if (from != to) {
			IWDataItem fromDi = null;
			IWDataItem toDi = null;
			IWSignature fromSig = from.getSignature();
			IWSignature toSig = to.getSignature();
			boolean aliasIsName = toSig.getAliasIsName();
			//Achtung, wenn fromSig ist Referenz auf toSig d.h. fromSig = toSig
			//Dann gibt das Probleme !!! KB0026 18.8.2003
			toSig.setAliasIsName(false);
			succ = SUCCESS_KEINE_FELDER;

			Enumeration keys = toSig.keys();
			int curRow = from.mCurRow;
			//int fromRowCount = from.mRowCount;
			if ((fromRow < 0) || (fromRow >= from.mRowCount))
				throw new WRuntimeException("fromRow out of range.");
			if ((toRow < 0) || (toRow > to.mRowCount))
				throw new WRuntimeException("toRow out of range.");
			from.setRow(fromRow);
			to.setRow(toRow);
			while (keys.hasMoreElements()) {
				String name = (String) keys.nextElement();
				//Index-Spalte in to wird automatisch gesetzt.
				if (name.equals(IWSignature.INDEX_COLUMN_NAME)) continue;
				IWAttribute toAttrib = toSig.getAttribute(name);
				String toName = name;
				if (aliasIsName) toName = toAttrib.getAlias();
				if (fromSig.hasAttribute(toName)) {
					//to Attribut ist in from Signatur enthalten
					IWAttribute fromAttrib = fromSig.getAttribute(toName);
					fromDi = from.getDataItem(toName);
					toDi = to.getDataItem(name);
					if (withTransform) {
						WDMTransformer.transform(
							fromAttrib,
							fromDi,
							toAttrib,
							toDi);
						String s = getSuccess(toDi);
						if (!SUCCESS_GEKUERZTE_FELDER.equals(succ) && SUCCESS_GEKUERZTE_FELDER.equals(s)){
							succ = s;							
						} else if (!SUCCESS_OK.equals(succ) &&(!SUCCESS_GEKUERZTE_FELDER.equals(succ)&& SUCCESS_OK.equals(s))){
							succ = s;
						}
					} else {
						toDi.setValue(fromDi.getValue());
						toDi.setState(fromDi.getState());
						succ = SUCCESS_OK;
					}
				} else {
					// to Attribut ist nicht in from enthalten -> Wert wird null State wird DISABLED(0)
					toDi = to.getDataItem(name);
					toDi.setValue(null);
					toDi.setState(IWDataItem.DISABLED);
				}
				
			}
			from.mCurRow = curRow;
			toSig.setAliasIsName(aliasIsName);
		} else if (fromRow != toRow) {
			WDataModelWriter dmw = WDataModelWriter.getInstance();
			WDMToolbox.appendSig(from, dmw.getDataModel()); 
			succ = WDMToolbox.updateDataRowByRow(from, fromRow, dmw.getDataModel(), 0);
			if (SUCCESS_OK.equals(succ)) succ = WDMToolbox.updateDataRowByRow(dmw.getDataModel(), 0, to, toRow);
		}
		return succ;
	}
	/**
	 * �bernehmen der Daten aus Zeile 'from' aus dem from-DataModel 
	 * in Zeile 'to' in das to-DataModel in im Attributnamen 
	 * �bereinstimmenden Spalten optional �ber den Transformator (WDMTransformer). 
	 * Bedingung:
	 * Die Ausf�hrung ist nur m�glich, wenn die Zeilen aus unterschiedlichen
	 * DataModels stammen.
	 * @param from Von-WDataModel
	 * @param from Von-WDataModelRow
	 * @param to Nach-WDataModel
	 * @param to Nach-WDataModelRow
	 * @param withTransform Schalter, ob �bernehmen der Daten mit Transformation
	 * @return
	 */
 	public static synchronized String updateCell(
		WDataModel from,
		int fromRow,
		String columnName,
		WDataModel to,
		int toRow,
		boolean withTransform) {
		String succ = SUCCESS_OK;
		if (from != to) {
			IWDataItem fromDi = null;
			IWDataItem toDi = null;
			IWSignature fromSig = from.getSignature();
			IWSignature toSig = to.getSignature();
			boolean aliasIsName = toSig.getAliasIsName();
			//Achtung, wenn fromSig ist Referenz auf toSig d.h. fromSig = toSig
			//Dann gibt das Probleme !!! KB0026 18.8.2003
			toSig.setAliasIsName(false);
			succ = SUCCESS_KEINE_FELDER;

			int curRow = from.mCurRow;
			//int fromRowCount = from.mRowCount;
			if ((fromRow < 0) || (fromRow >= from.mRowCount))
				throw new WRuntimeException("fromRow out of range.");
			if ((toRow < 0) || (toRow > to.mRowCount))
				throw new WRuntimeException("toRow out of range.");
			from.setRow(fromRow);
			to.setRow(toRow);
			if ((toSig.hasAttribute(columnName)) 
			 && (columnName.equals(IWSignature.INDEX_COLUMN_NAME) == false)) {
				String name = columnName;
				IWAttribute toAttrib = toSig.getAttribute(name);
				String toName = name;
				if (aliasIsName) toName = toAttrib.getAlias();
				if (fromSig.hasAttribute(toName)) {
					//to Attribut ist in from Signatur enthalten
					IWAttribute fromAttrib = fromSig.getAttribute(toName);
					fromDi = from.getDataItem(toName);
					toDi = to.getDataItem(name);
					if (withTransform) {
						WDMTransformer.transform(
							fromAttrib,
							fromDi,
							toAttrib,
							toDi);
						String s = getSuccess(toDi);
						if (!SUCCESS_GEKUERZTE_FELDER.equals(succ) && SUCCESS_GEKUERZTE_FELDER.equals(s)){
							succ = s;							
						} else if (!SUCCESS_OK.equals(succ) &&(!SUCCESS_GEKUERZTE_FELDER.equals(succ)&& SUCCESS_OK.equals(s))){
							succ = s;
						}
					} else {
						toDi.setValue(fromDi.getValue());
						toDi.setState(fromDi.getState());
						succ = SUCCESS_OK;
					}
				} else {
					// to Attribut ist nicht in from enthalten -> Wert wird null State wird DISABLED(0)
					toDi = to.getDataItem(name);
					toDi.setValue(null);
					toDi.setState(IWDataItem.DISABLED);
				}
				
			}
			from.mCurRow = curRow;
			toSig.setAliasIsName(aliasIsName);
		}
		return succ;
	}
	/** Ueberlagern/Ueberschreiben von Zeilen aus einem Quell-Modell in ein Ziel-Modell.
	 *  es werden nur die Spalten aus from uebernommen die in to vorhanden sind
	 *  Zeilen muessen bereits im Ziel-Modell vorhanden sein.
	 *
	 * @param from Von-WDataModel - Quell Modell
	 * @param from Von-WDataModelStartRow - erste Zeile aus Quell-Modell die uebernommen wird.
	 * @param from Von-WDataModelRows - anzahl der zu uebernehmenden Zeilen ab erste Zeile
	 * @param to Nach-WDataModel - Ziel Modell
	 * @param to Nach-WDataModelRow - erste Zeile im Ziel-Modell die ueberschrieben/ueberlagert wird
	 * @param bOverlay - ueberschreiben(false)/ueberlagern(true). �berlagen: wenn im Quell Modell ind = 0 ist, so
	 * wird der Attributwert im Ziel Modell nicht ver�ndert.
	 * @param bIgnoreErrorInd - bei true wird Indikator im Ziel-Modell auf IWDataItem.ENABLED gesetzt
	 * wenn im Quell-Modell IWDataItem.ERROR gesetzt ist.
	 * @param bIgnoreUnchangedInd - bei true wird Indikator im Ziel-Modell auf IWDataItem.ENABLED gesetzt
	 * wenn im Quell-Modell IWDataItem.UNCHANGED gesetzt ist.
	 * @return
	 */
	public static synchronized String updateData(
		WDataModel from,
		int fromStartRow,
		int fromRows,
		WDataModel to,
		int toRow,
		boolean bOverlay,
		boolean bIgnoreErrorInd,
		boolean bIgnoreUnchangedInd) {
		return WDMToolbox.updateData(from, fromStartRow, fromRows, 
																 to, toRow, bOverlay, 
																 bIgnoreErrorInd, bIgnoreUnchangedInd, true);
	}
	/** Ueberlagern/Ueberschreiben von Zeilen aus einem Quell-Modell in ein Ziel-Modell.
	 *  es werden nur die Spalten aus from uebernommen die in to vorhanden sind
	 *  Zeilen muessen bereits im Ziel-Modell vorhanden sein.
	 *
	 * @param from Von-WDataModel - Quell Modell
	 * @param from Von-WDataModelStartRow - erste Zeile aus Quell-Modell die uebernommen wird.
	 * @param from Von-WDataModelRows - anzahl der zu uebernehmenden Zeilen ab erste Zeile
	 * @param to Nach-WDataModel - Ziel Modell
	 * @param to Nach-WDataModelRow - erste Zeile im Ziel-Modell die ueberschrieben/ueberlagert wird
	 * @param bOverlay - ueberschreiben(false)/ueberlagern(true). �berlagen: wenn im Quell Modell ind = 0 ist, so
	 * wird der Attributwert im Ziel Modell nicht ver�ndert.
	 * @param bIgnoreErrorInd - bei true wird Indikator im Ziel-Modell auf IWDataItem.ENABLED gesetzt
	 * wenn im Quell-Modell IWDataItem.ERROR gesetzt ist.
	 * @param bIgnoreUnchangedInd - bei true wird Indikator im Ziel-Modell auf IWDataItem.ENABLED gesetzt
	 * wenn im Quell-Modell IWDataItem.UNCHANGED gesetzt ist.
	 * @param withTransform Schalter, ob �bernehmen der Daten mit Transformation
	 * @return
	 */
	public static synchronized String updateData(
		WDataModel from,
		int fromStartRow,
		int fromRows,
		WDataModel to,
		int toRow,
		boolean bOverlay,
		boolean bIgnoreErrorInd,
		boolean bIgnoreUnchangedInd, 
		boolean bWithTransform) {
		String succ = SUCCESS_OK;
		if (from != to) {
			IWDataItem fromDi = null;
			IWDataItem toDi = null;
			IWSignature fromSig = from.getSignature();
			IWSignature toSig = to.getSignature();
			boolean aliasIsName = toSig.getAliasIsName();
			toSig.setAliasIsName(false);
			succ = SUCCESS_KEINE_FELDER;
			int curRow = from.mCurRow;
			Enumeration keys = toSig.keys();
			while (keys.hasMoreElements()) {
				String name = (String) keys.nextElement();
				//Index-Spalte in to wird automatisch gesetzt.
				if (name.equals(IWSignature.INDEX_COLUMN_NAME)) continue;
				IWAttribute toAttrib = toSig.getAttribute(name);
				String toName = name;
				if (aliasIsName) toName = toAttrib.getAlias();
				if (fromSig.hasAttribute(toName)) {
					// wenn in quellmodel attribut vorhanden ...
					IWAttribute fromAttrib = fromSig.getAttribute(toName);
					for (int i=fromStartRow;i<(fromRows + fromStartRow);i++) {
						from.setRow(i); //????
						to.setRow(toRow + (i - fromStartRow));
						//???? veraender DM
						fromDi = from.getDataItem(toName);
						if (bOverlay
							&& fromDi.getState() == IWDataItem.DISABLED) {
							// bei overlay wird wert nicht veraendert wenn ind=0 im Quell-Modell    
							;
						} else if (
							bIgnoreErrorInd
								&& (fromDi.getState() == IWDataItem.ERROR)) {
							toDi = to.getDataItem(name);
							if (bWithTransform) {
								WDMTransformer.transform(
								fromAttrib,
								fromDi,
								toAttrib,
								toDi);
								String s = getSuccess(toDi);
								if (!SUCCESS_GEKUERZTE_FELDER.equals(succ) && SUCCESS_GEKUERZTE_FELDER.equals(s)){
									succ = s;							
								} else if (!SUCCESS_OK.equals(succ) &&(!SUCCESS_GEKUERZTE_FELDER.equals(succ)&& SUCCESS_OK.equals(s))){
									succ = s;
								}
							} else {
								toDi.setValue(fromDi.getValue());
								succ = SUCCESS_OK;
							}
							toDi.setState(IWDataItem.ENABLED);
						} else if (
							bIgnoreUnchangedInd
								&& (fromDi.getState() == IWDataItem.UNCHANGED)) {
							toDi = to.getDataItem(name);
							if (bWithTransform) {
								WDMTransformer.transform(
								fromAttrib,
								fromDi,
								toAttrib,
								toDi);
								String s = getSuccess(toDi);
								if (!SUCCESS_GEKUERZTE_FELDER.equals(succ) && SUCCESS_GEKUERZTE_FELDER.equals(s)){
									succ = s;							
								} else if (!SUCCESS_OK.equals(succ) &&(!SUCCESS_GEKUERZTE_FELDER.equals(succ)&& SUCCESS_OK.equals(s))){
									succ = s;
								}
							} else {
								toDi.setValue(fromDi.getValue());
								succ = SUCCESS_OK;
							}
							toDi.setState(IWDataItem.ENABLED);
						} else {
							toDi = to.getDataItem(name);
							if (bWithTransform) {
								WDMTransformer.transform(
								fromAttrib,
								fromDi,
								toAttrib,
								toDi);
								String s = getSuccess(toDi);
								if (!SUCCESS_GEKUERZTE_FELDER.equals(succ) && SUCCESS_GEKUERZTE_FELDER.equals(s)){
									succ = s;							
								} else if (!SUCCESS_OK.equals(succ) &&(!SUCCESS_GEKUERZTE_FELDER.equals(succ)&& SUCCESS_OK.equals(s))){
									succ = s;
								}
							} else {
								toDi.setValue(fromDi.getValue());
								toDi.setState(IWDataItem.ENABLED);
								succ = SUCCESS_OK;
							}
						}
					}
				}else{
					if (!bOverlay){
						for (int i = fromStartRow;
							i < (fromRows + fromStartRow);
							i++) {
							from.setRow(i); //????
							to.setRow(toRow + (i - fromStartRow));
							toDi = to.getDataItem(name);
							toDi.setValue(null);
							toDi.setState(IWDataItem.DISABLED);
							succ = SUCCESS_OK;
						}
					}
					
				}
				
			}
			to.initCompleted();
			from.mCurRow = curRow;
			toSig.setAliasIsName(aliasIsName);
		}
		return succ;
	}
	/**
	 * �bernehmen der Daten aus dem from-DataModel in das to-DataModel
	 * in im Attributnamen �bereinstimmenden Spalten �ber den 
	 * Transformator (WDMTransformer) wenn der Indikator im 
	 * from-DataModel ungleich 0 ist. 
	 * @param from Von-WDataModel
	 * @param to Nach-WDataModel
	 * @return
	 */
	public static synchronized String updateOverlay(
		WDataModel from,
		WDataModel to) {
		return WDMToolbox.updateOverlay(from, to, true);
	}
	/**
	 * �bernehmen der Daten aus dem from-DataModel in das to-DataModel
	 * in im Attributnamen �bereinstimmenden Spalten optional �ber den 
	 * Transformator (WDMTransformer) wenn der Indikator im 
	 * from-DataModel ungleich 0 ist. 
	 * @param from Von-WDataModel
	 * @param to Nach-WDataModel
	 * @param withTransform Schalter, ob �bernehmen der Daten mit Transformation
	 * @return
	 */
	public static synchronized String updateOverlay(
		WDataModel from,
		WDataModel to,
		boolean withTransform) {
		String succ = SUCCESS_OK;
		if (from != to) {
			IWDataItem fromDi = null;
			IWDataItem toDi = null;
			IWSignature toSig = to.getSignature();
			IWSignature fromSig = from.getSignature();
			succ = SUCCESS_KEINE_FELDER;
			boolean aliasIsName = toSig.getAliasIsName(); //AliasIsName merken
			toSig.setAliasIsName(false);                  //Denn  hier wird er ver�ndert
			int curRow = from.mCurRow;
			int fromRowCount = from.mRowCount;
			Enumeration keys = toSig.keys();
			while (keys.hasMoreElements()) {
				String name = (String) keys.nextElement();
				//Index-Spalte in to wird automatisch gesetzt.
				if (name.equals(IWSignature.INDEX_COLUMN_NAME)) continue;
				IWAttribute toAttrib = toSig.getAttribute(name);
				String toName = name;
				if (aliasIsName) toName = toAttrib.getAlias();
				if (fromSig.hasAttribute(toName)) {
					// wenn in quellmodel attribut vorhanden ...
					IWAttribute fromAttrib = fromSig.getAttribute(toName);
					for (int i = 0; i < fromRowCount; i++) {
						from.setRow(i);
						to.setRow(i);
						fromDi = from.getDataItem(toName);
						if (fromDi.getState() != IWDataItem.DISABLED) {
							toDi = to.getDataItem(name);
							if (withTransform) {
									WDMTransformer.transform(
									fromAttrib,
									fromDi,
									toAttrib,
									toDi);
								String s = getSuccess(toDi);
								if (!SUCCESS_GEKUERZTE_FELDER.equals(succ) && SUCCESS_GEKUERZTE_FELDER.equals(s)){
									succ = s;							
								} else if (!SUCCESS_OK.equals(succ) &&(!SUCCESS_GEKUERZTE_FELDER.equals(succ)&& SUCCESS_OK.equals(s))){
									succ = s;
								}
							} else {
								toDi.setValue(fromDi.getValue());
								toDi.setState(fromDi.getState());
								succ = SUCCESS_OK;
							}
						}
					}
				}
			}
			from.mCurRow = curRow;
			toSig.setAliasIsName(aliasIsName);
		}
		return succ;
	}
	/**
	 * �bernehmen der Indikatoren aus dem from-DataModel in das to-DataModel
	 * f�r alle IWDataItems, unabh�ngig vom Wert des Indikators.
	 * @param from Von-WDataModel
	 * @param to Nach-WDataModel
	 * @return
	 */
	public static synchronized String updateIndicator(
		WDataModel from,
		WDataModel to) {
		String succ = SUCCESS_OK;
		if (from != to) {
			IWDataItem fromDi = null;
			IWDataItem toDi = null;
			IWSignature fromSig = from.getSignature();
			IWSignature toSig = to.getSignature();
			succ = SUCCESS_KEINE_FELDER;
			boolean aliasIsName = toSig.getAliasIsName(); //AliasIsName merken
			toSig.setAliasIsName(false);                  //Denn  hier wird er ver�ndert
			int fromCurRow = from.mCurRow;
			int toCurRow = to.mCurRow;
			int fromRowCount = from.mRowCount;
			Enumeration keys = toSig.keys();
			while (keys.hasMoreElements()) {
				String name = (String) keys.nextElement();
				//Index-Spalte in to wird automatisch gesetzt.
				if (name.equals(IWSignature.INDEX_COLUMN_NAME)) continue;
				IWAttribute toAttrib = toSig.getAttribute(name);
				String toName = name;
				if (aliasIsName) toName = toAttrib.getAlias();
				if (fromSig.hasAttribute(toName)) {
					// wenn in quellmodel attribut vorhanden ...
					for (int i = 0; i < fromRowCount; i++) {
						from.setRow(i);
						to.setRow(i);
						fromDi = from.getDataItem(toName);
						toDi = to.getDataItem(name);
						toDi.setState(fromDi.getState());
						String s = getSuccess(toDi);
						if (!SUCCESS_GEKUERZTE_FELDER.equals(succ) && SUCCESS_GEKUERZTE_FELDER.equals(s)){
							succ = s;							
						} else if (!SUCCESS_OK.equals(succ) &&(!SUCCESS_GEKUERZTE_FELDER.equals(succ)&& SUCCESS_OK.equals(s))){
							succ = s;
						}
					}
				}
			}
			from.mCurRow = fromCurRow;
			if (toCurRow != -1) to.mCurRow = toCurRow;
			toSig.setAliasIsName(aliasIsName);
		}
		return succ;
	}
	/**
	 * �bernehmen der Indikatoren aus dem from-DataModel in das to-DataModel
	 * f�r alle IWDataItems einer Row, unabh�ngig vom Wert des Indikators.
	 * @param from Von-WDataModel
	 * @param fromRow RowNumber
	 * @param to Nach-WDataModel
	 * @param toRow RowNumber
	 * @return
	 */
	public static synchronized String updateIndicatorRowByRow(
		WDataModel from, int fromRow,
		WDataModel to, int toRow) {

		String succ = SUCCESS_OK;
		if (from != to) {
			IWDataItem fromDi = null;
			IWDataItem toDi = null;
			IWSignature fromSig = from.getSignature();
			IWSignature toSig = to.getSignature();
			succ = SUCCESS_OK;

			boolean aliasIsName = toSig.getAliasIsName(); //AliasIsName merken
			toSig.setAliasIsName(false);                  //Denn  hier wird er ver�ndert
			int fromCurRow = from.mCurRow;
			int toCurRow = to.mCurRow;
			int fromRowCount = from.mRowCount;
			if ((fromRow < 0) || (fromRow >= from.mRowCount))
				throw new WRuntimeException("fromRow out of range.");
			if ((toRow < 0) || (toRow > to.mRowCount))
				throw new WRuntimeException("toRow out of range.");
			from.setRow(fromRow);
			to.setRow(toRow);


			Enumeration keys = toSig.keys();
			while (keys.hasMoreElements()) {
				String name = (String) keys.nextElement();
				//Index-Spalte in to wird automatisch gesetzt.
				if (name.equals(IWSignature.INDEX_COLUMN_NAME)) continue;
				IWAttribute toAttrib = toSig.getAttribute(name);
				String toName = name;
				if (aliasIsName) toName = toAttrib.getAlias();
				if (fromSig.hasAttribute(toName)) {
					// wenn in quellmodel attribut vorhanden ...
					IWAttribute fromAttrib = fromSig.getAttribute(toName);
					fromDi = from.getDataItem(toName);
					toDi = to.getDataItem(name);
					toDi.setState(fromDi.getState());
					String s = getSuccess(toDi);
				}
			}
			from.mCurRow = fromCurRow;
			to.mCurRow = toCurRow;
			toSig.setAliasIsName(aliasIsName);
		} else if (fromRow != toRow) {
			WDataModelWriter dmw = WDataModelWriter.getInstance();
			WDMToolbox.appendSig(from, dmw.getDataModel()); 
			succ = WDMToolbox.updateDataRowByRow(from, fromRow, dmw.getDataModel(), 0);
			if (SUCCESS_OK.equals(succ)) succ = WDMToolbox.updateIndicatorRowByRow(dmw.getDataModel(), 0, to, toRow);
		}
		return succ;
	}


	/**
	 * �ndern der Indikatoren in einem DataModel f�r alle IWDataItems, 
	 * deren Indikator (State) gleich 'fromState' ist, auf 'toState'.
	 * (Analog zu KID: TRANSFORM-INDIKATOR)
	 * @param fromState
	 * @param toState
	 * @return
	 */
	public static synchronized String changeIndicator(
		WDataModel dm,
		short fromState,
		short toState) {
		String succ = SUCCESS_KEINE_FELDER;
		IWDataItem di = null;
		IWSignature sig = dm.getSignature();
		succ = SUCCESS_KEINE_FELDER;

		int curRow = dm.mCurRow;
		int fromRowCount = dm.mRowCount;
		for (Enumeration e=sig.keys(); e.hasMoreElements();) {
			String name = (String) e.nextElement();
			for (int i = 0; i < fromRowCount; i++) {
				dm.setRow(i);
				di = dm.getDataItem(name);
				if (di.getState() == fromState) {
					di.setState(toState);
					succ = SUCCESS_OK;
				}
			}
			dm.mCurRow = curRow;
		}
		return succ;
	}
	/**
	 * Aktualisieren der Indikatoren des to-DataModels, wenn das WAttribute
	 * im from-DataModel vorhanden ist. Dabei werden alle IWDataItems der
	 * Spalte aktualisiert. 
	 * (Analog zu KID: OUT-PARAMETER-VORHANDEN)
	 * @param from Von-WDataModel
	 * @param to Nach-WDataModel
	 * @return
	 */
	public static synchronized String updateIndicatorBySignature(
		WDataModel from,
		WDataModel to) {
		String succ = SUCCESS_OK;
		if (from != to) {
			IWDataItem fromDi = null;
			IWDataItem toDi = null;
			IWSignature fromSig = from.getSignature();
			IWSignature toSig = to.getSignature();
			succ = SUCCESS_KEINE_FELDER;

			if (to.hasData() == false) to.addRow();

			int curRow = from.mCurRow;
			int toRowCount = to.mRowCount;
			for (Enumeration e=fromSig.keys(); e.hasMoreElements();) {
				String name = (String) e.nextElement();
				if (toSig.hasAttribute(name)) {
					for (int i = 0; i < toRowCount; i++) {
						to.setRow(i);
						toDi = to.getDataItem(name);
						toDi.setState(IWDataItem.ENABLED);
						String s = getSuccess(toDi);
						succ = s;
					}
				}
			}
			from.mCurRow = curRow;
		}
		return succ;
	}
	/**
	 * Pr�fen der Indikatoren aller IWDataItems eines DataModels auf den Zustand 
	 * IWDataItem.ENABLED.
	 * Ist der Zustand (state) bei allen IWDataItems gleich IWDataItem.ENABLED, 
	 * dann endet die Methode mit SUCCESS_OK, sonst mit SUCCESS_NOT_OK.  
	 * @param dm WDataModel
	 * @return
	 */
	public static synchronized String checkAllDataItemsEnabled(WDataModel dm) {
		String succ = SUCCESS_OK;
		IWDataItem di = null;
		IWSignature sig = dm.getSignature();

		int curRow = dm.mCurRow;
		int rowCount = dm.mRowCount;
		for (Enumeration e=sig.keys(); e.hasMoreElements();) {
			String name = (String) e.nextElement();
			for (int i = 0; i < rowCount; i++) {
				dm.setRow(i);
				di = dm.getDataItem(name);
				if (di.getState() != IWDataItem.ENABLED) {
					return SUCCESS_NOT_OK;
			  } 
			}
			dm.mCurRow = curRow;
		}
		return succ;
	}
	/**
	 * Pr�fen der Indikatoren aller IWDataItems eines DataModels auf den Zustand 
	 * IWDataItem.ENABLED.
	 * Ist der Zustand (state) bei mindestens einem IWDataItem gleich 
	 * IWDataItem.ENABLED, dann endet die Methode mit SUCCESS_OK, sonst mit 
	 * SUCCESS_NOT_OK.  
	 * @param dm WDataModel
	 * @param short state
	 * @return
	 */
	public static synchronized String checkIfOneDataItemHasState(WDataModel dm, short state) {
		String succ = SUCCESS_NOT_OK;
		IWDataItem di = null;
		IWSignature sig = dm.getSignature();

		int curRow = dm.mCurRow;
		int rowCount = dm.mRowCount;
		for (Enumeration e=sig.keys(); e.hasMoreElements();) {
			String name = (String) e.nextElement();
			for (int i = 0; i < rowCount; i++) {
				dm.setRow(i);
				di = dm.getDataItem(name);
				if (di.getState() == state) {
					return SUCCESS_OK;
				} 
			}
			dm.mCurRow = curRow;
		}
		return succ;
	}
	/**
	 * Erzeugt zu einem WDataModel einen Key, der alle Attribute umfasst,
	 * bei denen wenigstens ein IWDataItem der zugeh�rigen Spalte im 
	 * Zustand (state) IWDataItem.ENABLED ist.
	 * @param dm WDataModel
	 * @return
	 */
	public static synchronized String getDataModelAsKey(WDataModel dm) {
		Vector v = new Vector();
		IWDataItem di = null;
		IWSignature sig = dm.getSignature();

		int curRow = dm.mCurRow;
		int dataLen = sig.getDataLength();
		int rowCount = dm.mRowCount;
		int colCount = sig.cols();

		StringBuffer sb = new StringBuffer((colCount*16) + (rowCount*dataLen));
		//Ermitteln der zu ber�cksichtigen Attribute
		for (Enumeration e=sig.keys(); e.hasMoreElements();) {
			String name = (String) e.nextElement();
			int rowCountInColumn = dm.getColVector(sig.indexOf(name)).size();
			for (int i = 0; i < rowCountInColumn; i++) {
				dm.setRow(i);
				di = dm.getDataItem(name);
				if (di.getState() == IWDataItem.ENABLED) {
					v.add(name);
					break;
				} 
			}
		}
		//�bertragen der ber�cksichtigten Attribute und Werte aller Zeilen in den Key
		for (Enumeration ee=v.elements(); ee.hasMoreElements();) {
			String name = (String) ee.nextElement();
			sb.append(name);
			int rowCountInColumn = dm.getColVector(sig.indexOf(name)).size();
			for (int i = 0; i < rowCountInColumn; i++) {
				dm.setRow(i);
				di = dm.getDataItem(name);
				sb.append((String)di.getValue());
			}
		}
		dm.mCurRow = curRow;
		return sb.toString();
	}
	/**
	 * Formatierung eines numerischen Werts 'value' in einen String 
	 * entsprechend der Musterdefinition in 'pattern'.
	 * Beispiele:
	 * value   = 34758.9865;
	 * pattern = "#0.000");
	 * pattern = "000000.000");
	 * pattern = "#.000000");
	 * pattern = "#,###,##0.000");
	 * pattern = "0.000E00");
	 * pattern = "#,##0.00");
	 * Ergebnisse:
   * 34758,986
   * 034758,986
   * 34758,986500
   * 34.758,986
   * 3,476E04
   * 34.758,99
   * 
   * Beispiele:
	 * value   = 34758;
	 * pattern = "#0");
	 * pattern = "000000");
	 * pattern = "#");
	 * pattern = "#,###,##0");
	 * pattern = "0E00");
	 * pattern = "#,##0");
	 * Ergebnisse:
   * 34758
   * 034758
   * 34758
   * 34.758
   * 3E04
   * 34.758
	 * 
	 * 0  = Eine einzelne Ziffer. 
	 * #  = Eine einzelne Ziffer. Wird ausgelassen, falls f�hrende Null.  
	 * .  = Dezimaltrennzeichen (sprachunabh�ngig)
	 * ,  = Tausendertrennzeichen (sprachunabh�ngig)  
	 * -  = Minuszeichen  
	 * ;  = Trennt positive und negative Sub-Muster
	 * E  = Exponentialdarstellung.  
	 * %  = Multiplikation mit 100 und Anzeige in Prozent  
	 * � (\u00A4) = Als Prefix oder Suffix  zur Angabe des W�hrungszeichens.
	 * '  Als Prefix oder Suffix zur Definition fester Texte. Der Text muss 
	 *    von ' eingeschlossen sein.
	 * 
	 * @param pattern
	 * @param value
	 * @return R�ckgabe des formatierten values
	 */
	public static synchronized String getFormattedNumericValue(String pattern, String value) {
		String res = null;
		DecimalFormat df = null;
		try {
			df = new DecimalFormat(pattern);
		} catch (IllegalArgumentException e) {
			throw new WRuntimeException("Illegal pattern: "+pattern);
		}
		try {
			double d = Double.parseDouble(value);
			res = df.format(d);
		} catch (NumberFormatException e) {
			throw new WRuntimeException("Illegal number format: "+value);
		}
		return res;
	}
	/**
	 * Formatierung eines numerischen Werts 'value' in einen String 
	 * entsprechend der Musterdefinition in 'pattern' rechtsb�ndig.
	 * Weitere Beschreibung siehe 'getFormettedNumericValue()'.
	 * @param pattern
	 * @param value
	 * @return
	 */
	public static synchronized String getFormattedNumericValueRight(String pattern, String value) {
		int len = pattern.length();
		StringBuffer res = new StringBuffer(len);
		String v = WDMToolbox.getFormattedNumericValue(pattern, value);
		for (int i=0;i<len-v.length(); i++) res.append(' ');
		res.append(v);
		return res.toString();
	}
	/**
	 * Formatierung eines KID-Datumwerts (JJJJMMTT) 'value' in einen String 
	 * entsprechend der Musterdefinition in 'pattern'.
	 * Beispiele:
	 * value   = 20030701;
	 * pattern = "dd.MM.yy");
	 * pattern = "dd.MM.yyyy");
	 * pattern = "dd.MMM.yyyy");
	 * pattern = "dd.MMMM.yyyy");
	 * Ergebnisse:
	 * 01.07.03
	 * 01.07.2003
	 * 01. Jul 2003
	 * 01. Juli 2003
	 * 
	 * Pattern-Symbole:
	 * y = Jahr 
	 * M = Monat
	 * w = Woche im Jahr
	 * W = Woche im Monat
	 * D = Tag im Jahr
	 * d = Tag im Monat
	 * F = Tag in der Woche im Monat
	 * E = Wochentag
	 * a = Am/pm marker
	 * H = Stunde (0-23)
	 * k = Stunde (1-24)
	 * K = Stunde in am/pm (0-11)
	 * h = Stunde in am/pm (1-12)
	 * m = Minute 
	 * s = Sekunde
	 * S = Millisekunde
	 * z = Zeitzone
	 * Z = Zeitzone nach RFC822
	 * '  Als Prefix oder Suffix zur Definition fester Texte. Der Text muss 
	 *    von ' eingeschlossen sein.
	 * 
	 * @param pattern
	 * @param value
	 * @return R�ckgabe des formatierten values
	 */
	public static synchronized String getFormattedDateValue(String pattern, String value) {
		String res = null;
		String v = null;
		SimpleDateFormat sdf = null;
		DateFormat df = DateFormat.getDateInstance();
		 
		try {
			sdf = new SimpleDateFormat(pattern);
		} catch (IllegalArgumentException e) {
			throw new WRuntimeException("Illegal pattern: "+pattern);
		}
		switch (value.length()) {
			case 8:
			  v = value.substring(6,8)+DATE_DELIMITER+value.substring(4,6)+DATE_DELIMITER+value.substring(0,4);
				break;
			case 6:
			  v = value.substring(4,6)+DATE_DELIMITER+value.substring(2,4)+DATE_DELIMITER+CENTURY_PREFIX+value.substring(0,2);
				break;
			default:
			  throw new WRuntimeException("Illegal date format: "+value);
		}
		try {
			res = sdf.format(df.parse(v));
		} catch (ParseException e) {
			throw new WRuntimeException("Illegal date format: "+value);
		}
		return res;
	}
	/**
	 * Formatierung eines KID-Uhrzeitwerts (HHMMSS) 'value' in einen String 
	 * entsprechend der Musterdefinition in 'pattern'.
	 * Beispiele:
	 * value   = 123015;
	 * pattern = "HH:mm");
	 * pattern = "HH:mm:ss");
	 * Ergebnisse:
	 * 12:30
	 * 12:30:15
	 * 
	 * Pattern-Symbole:
	 * a = Am/pm marker
	 * H = Stunde (0-23)
	 * k = Stunde (1-24)
	 * K = Stunde in am/pm (0-11)
	 * h = Stunde in am/pm (1-12)
	 * m = Minute 
	 * s = Sekunde
	 * S = Millisekunde
	 * z = Zeitzone
	 * Z = Zeitzone nach RFC822
	 * '  Als Prefix oder Suffix zur Definition fester Texte. Der Text muss 
	 *    von ' eingeschlossen sein.
	 * 
	 * @param pattern
	 * @param value
	 * @return R�ckgabe des formatierten values
	 */
	public static synchronized String getFormattedTimeValue(String pattern, String value) {
		String res = null;
		String v = null;
		SimpleDateFormat sdf = null;
		DateFormat df = DateFormat.getTimeInstance();
		 
		try {
			sdf = new SimpleDateFormat(pattern);
		} catch (IllegalArgumentException e) {
			throw new WRuntimeException("Illegal pattern: "+pattern);
		}
		switch (value.length()) {
			case 6:
				v = value.substring(0,2)+TIME_DELIMITER+value.substring(2,4)+TIME_DELIMITER+value.substring(4,6);
				break;
			case 4:
				v = value.substring(0,2)+TIME_DELIMITER+value.substring(2,4)+TIME_DELIMITER+NULL_SECONDS;
				break;
			default:
				throw new WRuntimeException("Illegal time format: "+value);
		}
		try {
			res = sdf.format(df.parse(v));
		} catch (ParseException e) {
			throw new WRuntimeException("Illegal time format: "+value);
		}
		return res;
	}
	/**
	 * Ermittelt zu einem IWDataItem anhand des Zustands (state)
	 * den Erfolg. 
	 * M�gliche Erfolge sind:
	 * @see com.entitec.wax.wdm.WDMToolbox 
	 * @param di
	 * @return
	 */
	public static synchronized String getSuccess(IWDataItem di) {
		String succ = null;
		switch (di.getState()) {
			case IWDataItem.TRUNCATED:
				succ = SUCCESS_GEKUERZTE_FELDER;
				break;
			case IWDataItem.INVALID:
				succ = SUCCESS_FELDINHALT_FEHLER;
				break;
			case IWDataItem.DISABLED:
				succ = SUCCESS_KEINE_FELDER;
				break;
			default:
				// OK(1) und UNCHANGED(4)
				succ = SUCCESS_OK;
		}
		return succ;
	}
	/**
	 * Erzeugen einer tiefen Kopie von einem Objekt.
	 * @param o
	 */
	public static synchronized Object deepCopy(Object o) {
		Object newObject = null;
		try {
			ByteArrayOutputStream outStream = new ByteArrayOutputStream();
			ObjectOutputStream os = new ObjectOutputStream(outStream);
			os.writeObject(o);
			byte[] b = outStream.toByteArray();
			os.close();
			ByteArrayInputStream inStream = new ByteArrayInputStream(b);
			ObjectInputStream is = new ObjectInputStream(inStream);
			newObject = is.readObject();
		} catch (IOException e) {
			throw new WRuntimeException(e.getClass() + ": " + e.getMessage());
		} catch (ClassNotFoundException ee) {
			throw new WRuntimeException(ee.getClass() + ": " + ee.getMessage());
		}
		return newObject;
	}
	/**
	 * Auf- oder Absteigendes Sortieren eines WDataModels nach 
	 * einer Spalte.  
	 * @param dm
	 * @param attributeName
	 * @param ascending
	 * @return
	 */
	public static WDataModel sort(
		WDataModel dm,
		String attributeName,
		boolean ascending) {
		IWDataItem fromDi = null;
		IWDataItem toDi = null;
		int curRow = dm.mCurRow;
		Vector v = dm.getColVector(attributeName);
		WIndexedValue[] a = new WIndexedValue[v.size()];
		int i = 0;
		Enumeration e = v.elements();
		while (e.hasMoreElements()) {
			a[i] = new WIndexedValue(i, (IWDataItem) e.nextElement());
			i++;
		}
		WDMToolbox.quickSort(
			a,
			null,
			0,
			v.size() - 1,
			ascending,
			dm.getSignature().getAttribute(attributeName));
		WDataModel tempDm = new WDataModel(dm.getSignature());
		for (i = 0; i < a.length; i++) {
			int index = a[i].getIndex();
			dm.setRow(index);
			tempDm.addRow();
			Enumeration keys = tempDm.getSignature().keys();
			while (keys.hasMoreElements()) {
				String name = (String) keys.nextElement();
				fromDi = dm.getDataItem(name);
				toDi = tempDm.getDataItem(name);
				toDi.setValue(fromDi.getValue());
				toDi.setState(fromDi.getState());
			}
		}
		dm.mCurRow = curRow;
		return tempDm;
	}
	/**
	 * Auf- oder Absteigendes Sortieren eines WDataModels nach 
	 * mehreren Spalte. In 'attriubtes' sind die Attriubtnamen
	 * als String-Array zu �bergeben. In korrespondierender 
	 * Reihenfolge ist in 'directions' die Richtung, nach der
	 * die einzelnen Spalten sortiert werden sollen, anzugeben.
	 * �ber die Variable 'selectedIndices' kann ein Array der 
	 * selectierten Zeilenindices �bergeben werden. Nach dem
	 * Abschluss der Sortierung enth�lt die Variable die neuen
	 * Indices der selektierten Spalten.
	 * @param dm
	 * @param attributes
	 * @param directions
	 * @param selectedIndices
	 * @return
	 */
	public static WDataModel sort(
		WDataModel dm,
		IWAttribute[] attributes,
		char[] directions,
		int[] selectedIndices) {
		IWDataItem fromDi = null;
		IWDataItem toDi = null;
		int curRow = dm.mCurRow;
		int rowCount = dm.mRowCount;
		int lastIndex = dm.mLastIndex;
		int modus = dm.mModus;
		boolean isIndexed = dm.mIsIndexed;
		boolean isInitCompleted = dm.mInitCompleted;
		boolean useChangedInd = dm.mUseChangedInd;
		
		int levelCount = attributes.length;
		if (levelCount == directions.length) {
			WIndexedValue[][] a =
				new WIndexedValue[levelCount][dm.getRowCount()];
			for (int j = 0; j < levelCount; j++) {
				Vector v = dm.getColVector(attributes[j].getName());
				int i = 0;
				Enumeration e = v.elements();
				while (e.hasMoreElements()) {
					a[j][i] =
						new WIndexedValue(i, (IWDataItem) e.nextElement());
					i++;
				}
			}
			for (int i = 0; i < selectedIndices.length; i++) {
				a[0][selectedIndices[i]].setSelected(true);
			}

			//sort column 0
			int level = 0;
			int rowAnz = a[level].length;
			WDMToolbox.quickSort(
				a,
				null,
				level,
				0,
				rowAnz - 1,
				(directions[level] == WDMToolbox.ASCENDING),
				attributes[level]);
			if (levelCount > 1) {		
				for (level = 1; level < levelCount; level++) {
					int beginIndex = 0;
					int endIndex = 0;
					//search for beginIndex, that�s the first element that�s equals the following
					while (beginIndex < rowAnz - 1) {
						//tuning: byte-compare insted of string-compare
						endIndex = beginIndex;
						String s1, s2;
						boolean bEqual = true;
						while ((endIndex < rowAnz - 1) && bEqual){
							for (int l = level -1 ; l >= 0 && bEqual ; l--) {
								s1 = (String) a[l][endIndex].getValue().getValue();
								s2 = (String) a[l][endIndex+1].getValue().getValue();
								if (!s1.equals(s2)){
									bEqual = false;
								}
							}
							if (bEqual) endIndex++;
						}
						if (beginIndex < endIndex) {
							WDMToolbox.quickSort(
								a,
								null,
								level,
								beginIndex,
								endIndex,
								(directions[level] == WDMToolbox.ASCENDING),
								attributes[level]);
						}
						beginIndex = endIndex + 1;
						endIndex = 0;
					}
				}
			}

			//sort the table
			int selectionCounter = 0;
			WDataModel tempDm = new WDataModel(dm.getSignature());
			for (int i = 0; i < a[0].length; i++) {
				int index = a[0][i].getIndex();
				if (a[0][i].isSelected()) {
					selectedIndices[selectionCounter] = i;
					selectionCounter++;
				}
				dm.setRow(index);
				tempDm.addRow();
				Enumeration keys = tempDm.getSignature().keys();
				while (keys.hasMoreElements()) {
					String name = (String) keys.nextElement();
					fromDi = dm.getDataItem(name);
					toDi = tempDm.getDataItem(name);
					toDi.setValue(fromDi.getValue());
					toDi.setState(fromDi.getState());
				}
			}
			tempDm.mCurRow = curRow;
			tempDm.mRowCount = rowCount;
			tempDm.mLastIndex = lastIndex;
			tempDm.mModus = modus;
			tempDm.mIsIndexed = isIndexed;
			tempDm.mInitCompleted = isInitCompleted;
			tempDm.mUseChangedInd = useChangedInd;
			return tempDm;
		} else {
			dm.mCurRow = curRow;
			dm.mRowCount = rowCount;
			dm.mLastIndex = lastIndex;
			dm.mModus = modus;
			dm.mIsIndexed = isIndexed;
			dm.mInitCompleted = isInitCompleted;
			dm.mUseChangedInd = useChangedInd;
			return dm;
		}
	}
	/**
	 * Ausgabe eines WDataModels im CSV Format. Die Daten
	 * der einzelnen IWDataItems werden zeilenweise, wobei die
	 * Spalten durch Semikolon getrennt sind.
	 * @param dm
	 * @return
	 */
	public static String toCSV(WDataModel dm) {
		IWSignature sig = dm.getSignature();
		int curRow = dm.mCurRow;
		int rowCount = dm.getRowCount();
		int colCount = dm.getColCount();
		int totalLen = 0;
		StringBuffer[] csv = new StringBuffer[rowCount];
		char del = '\t';
		String cr = "\n";
//		String cr = System.getProperty("line.separator");
		for (int i = 0; i < rowCount; i++) {
			dm.setRow(i);
			int len = 0;
			for (Enumeration e=sig.keys();e.hasMoreElements();) {
				IWDataItem di = dm.getDataItem((String)e.nextElement());
				if (di.getState() != IWDataItem.DISABLED) {
					len = len + ((String) di.getValue()).length() + 1;
				}
			}
			csv[i] = new StringBuffer(len + 1);
			totalLen = totalLen + len + 1;
			for (Enumeration e=sig.keys();e.hasMoreElements();) {
				IWDataItem di = dm.getDataItem((String)e.nextElement());
				if (di.getState() != IWDataItem.DISABLED) {
					String s = (String)di.getValue();
					int length = s.length();
					if (length > 2) {
						if (s.charAt(length-2) == '\r' && s.charAt(length-1) == '\n') {
							s = s.substring(0, length-2);
						} else if (s.charAt(length-1) == '\r' || s.charAt(length-1) == '\n') {
							s = s.substring(0, length-1);
						}
					}
					csv[i].append(s);
				}
				if (e.hasMoreElements()) csv[i].append(del);
			}
			csv[i].append(cr);
			//		  System.out.println("row: "+i);
		}
		StringBuffer res = new StringBuffer(totalLen);
		for (int i = 0; i < dm.getRowCount(); i++) {
			res.append(csv[i]);
		}
		dm.mCurRow = curRow;
		return res.toString();
	}
	public static String toCSV(TreeMap data) {
		TreeSet columns = new TreeSet();
		TreeSet rows = new TreeSet();
		
		for (Iterator iter = data.keySet().iterator();iter.hasNext();) {
			String key = (String)iter.next();
			int pos = key.indexOf(':');
			columns.add(key.substring(0, pos));
			rows.add(key.substring(pos+1));
		}

		StringBuffer res = new StringBuffer();
		char del = '\t';
		String cr = System.getProperty("line.separator");

		for (Iterator rowIter = rows.iterator();rowIter.hasNext();) {
			String row = (String)rowIter.next();
			for (Iterator colIter = columns.iterator();colIter.hasNext();) {
				String col = (String)colIter.next();
				String key = col+":"+row;
				if (data.keySet().contains(key)) res.append(data.get(key));
				res.append(del);
			}
			res.deleteCharAt(res.length()-1);
			res.append(cr);
		}
		return res.toString();
	}
	/**
	 * Ausgabe eines WDataModels im SKID Format. Die Daten
	 * der einzelnen IWDataItems werden zeilenweise, wobei die
	 * Spalten ebenfals zeilenweise inklusive der Attribut-
	 * beschreibung ausgegeben werden.
	 * @param view
	 * @param dm
	 * @return
	 */
	public static String toSkid(char view, WDataModel dm) {
		IWSignature sig = dm.getSignature();
		int curRow = dm.mCurRow;
		int rowCount = dm.getRowCount();
		int colCount = dm.getColCount();
		if (dm != null) {
			int size = 0;
			String lineSeperator = System.getProperty("line.separator");
			boolean aliasIsName = dm.getSignature().getAliasIsName();
			StringBuffer sb = null;
			if (dm.mRowCount > 0) {
				for (int i = 0; i < dm.getRowCount(); i++) {
					dm.setRow(i);
					for (Enumeration e=sig.keys();e.hasMoreElements();) {
						size =
							size
								+ 27
								+ ((String) dm.getDataItem((String)e.nextElement()).getValue())
									.trim()
									.length();
					}
				}
				sb = new StringBuffer(size);
				for (int i = 0; i < dm.mRowCount; i++) {
					dm.setRow(i);
					for (Enumeration e = dm.getSignature().attributes();
						e.hasMoreElements();
						) {
						sb.append(view);
						IWAttribute a = (IWAttribute) e.nextElement();
						String name = null;
						if (aliasIsName == true) {
							name = a.getAlias();
						} else {
							name = a.getName();
						}
						sb.append(name.toUpperCase());
						for (int j = name.length(); j < 16; j++) {
							sb.append(' ');
						}
						sb.append(a.getFrm());
						String len = "" + a.getLen();
						for (int j = 0; j < 4 - len.length(); j++) {
							sb.append('0');
						}
						sb.append(len);
						sb.append(a.getDec());
						if (a.getSign() == true) {
							sb.append('J');
						} else {
							sb.append('N');
						}
						sb.append(dm.getDataItem(name).getState());
						String s = (String)dm.getDataItem(name).getValue();
						int lastPos = s.length();
						while(lastPos > 0) {
							if (s.charAt(lastPos-1) != ' ') break;
							lastPos--;
						}
						sb.append(s.substring(0, lastPos));
						sb.append(lineSeperator);
					}
				}
			} else {
				sb = new StringBuffer(dm.getRowCount() * dm.getColCount() * 27);
				for (Enumeration e = dm.getSignature().attributes(); e.hasMoreElements();) {
					sb.append(view);
					IWAttribute a = (IWAttribute) e.nextElement();
					String name = null;
					if (aliasIsName == true) {
						name = a.getAlias();
					} else {
						name = a.getName();
					}
					sb.append(name.toUpperCase());
					for (int j = name.length(); j < 16; j++) {
						sb.append(' ');
					}
					sb.append(a.getFrm());
					String len = "" + a.getLen();
					for (int j = 0; j < 4 - len.length(); j++) {
						sb.append('0');
					}
					sb.append(len);
					sb.append(a.getDec());
					if (a.getSign() == true) {
						sb.append('J');
					} else {
						sb.append('N');
					}
					sb.append('0');
					sb.append(lineSeperator);
				}
			}
			dm.mCurRow = curRow;
			return sb.toString();
		} else {
			dm.mCurRow = curRow;
			return "\n";
		}
	}
	/** 
	 * Erzeugt basierend auf dem Inhalt einer Map ein DataModel.
	 * F�r jeden Key wird ein Attribut vom Typ 'C' mit der L�nge
	 * des dazugeh�rigen Wertes erzeugt.
	 */
	public static WDataModel MapToDataModel(Map m) {
		/* Signature erzeugen */
		WSignature sig = new WSignature();
		Iterator i = m.keySet().iterator();
		while (i.hasNext()) {
			String key = (String) i.next();
			String value = (String) m.get(key);
			sig.addAttribute(
				new WAttribute(
					key,
					IWAttribute.FRM_CHAR,
					value.length(),
					0,
					false));
		}
		/* Datamodel mit Inhalt der Map fuellen */
		WDataModelWriter dm = new WDataModelWriter(new WDataModel(sig));
		dm.setRow(0);
		i = m.keySet().iterator();
		while (i.hasNext()) {
			String key = (String) i.next();
			String value = (String) m.get(key);
			dm.setValue(key, value);
			dm.setState(key, IWDataItem.ENABLED);
		}
		return dm.getDataModel();

	}
	private static void quickSort(
		WIndexedValue[][] a,
		WIndexedValue[][] b,
		int level,
		int from,
		int to,
		boolean ascending,
		IWAttribute attrib) {
		//level = columnIndex to sort
		if ((a[level] == null) || (a[level].length < 2))
			return;
		int i = from;
		int j = to;
		IWDataItem center = a[level][(from + to) / 2].getValue();
		do {
			if (ascending) {
				while ((i < to)
					&& (attrib.compare(center, a[level][i].getValue()) > 0))
					i++;
				while ((j > from)
					&& (attrib.compare(center, a[level][j].getValue()) < 0))
					j--;
			} else {
				while ((i < to)
					&& (attrib.compare(center, a[level][i].getValue()) < 0))
					i++;
				while ((j > from)
					&& (attrib.compare(center, a[level][j].getValue()) > 0))
					j--;
			}
			if (i < j) {
				for (int col = 0; col < a.length; col++) {
					WIndexedValue tmp = a[col][i];
					a[col][i] = a[col][j];
					a[col][j] = tmp;
					if (b != null) {
						tmp = b[col][i];
						b[col][i] = b[col][j];
						b[col][j] = tmp;
					}
				}
			}
			if (i <= j) {
				i++;
				j--;
			}
		}
		while (i <= j);
		if (from < j)
			quickSort(a, b, level, from, j, ascending, attrib);
		if (i < to)
			quickSort(a, b, level, i, to, ascending, attrib);
	}
	private static void quickSort(
		WIndexedValue[] a,
		WIndexedValue[] b,
		int from,
		int to,
		boolean ascending,
		IWAttribute attrib) {
		if ((a == null) || (a.length < 2))
			return;
		int i = from;
		int j = to;
		IWDataItem center = a[(from + to) / 2].getValue();
		do {
			if (ascending) {
				while ((i < to)
					&& (attrib.compare(center, a[i].getValue()) > 0))
					i++;
				while ((j > from)
					&& (attrib.compare(center, a[j].getValue()) < 0))
					j--;
			} else {
				while ((i < to)
					&& (attrib.compare(center, a[i].getValue()) < 0))
					i++;
				while ((j > from)
					&& (attrib.compare(center, a[j].getValue()) > 0))
					j--;
			}
			if (i < j) {
				WIndexedValue tmp = a[i];
				a[i] = a[j];
				a[j] = tmp;
				if (b != null) {
					tmp = b[i];
					b[i] = b[j];
					b[j] = tmp;
				}
			}
			if (i <= j) {
				i++;
				j--;
			}
		}
		while (i <= j);
		if (from < j)
			quickSort(a, b, from, j, ascending, attrib);
		if (i < to)
			quickSort(a, b, i, to, ascending, attrib);
	}
}
