package com.entitec.wax.wdm;

import com.entitec.wax.wglobal.WRuntimeException;


/**
 * Date:    03.07.2003
 * <p>Description:</p>
 * Die Klasse WValueNotTypConformException erweitert die allgemeine WAX 
 * WRuntimeException und wird geworfen, wenn in einem WDataModel in ein
 * IWDataItem ein Wert (value) eingetragen werden soll, der nicht der 
 * WAttribute-Definition in der zugehörigen Spalte entspricht. 
 * 
 * @author AB0043
 */
public class WValueNotTypConformException extends WRuntimeException {
  public WValueNotTypConformException(String message) {
    super(message);
  }
}
