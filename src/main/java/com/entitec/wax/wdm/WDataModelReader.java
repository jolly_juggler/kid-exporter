package com.entitec.wax.wdm;

import java.io.Serializable;
import java.util.Enumeration;

/**
 * 
 * Date:    08.07.2003
 * <p>Description:</p>
 * Die Klasse WDataModelReader erm�glicht den lesenden Zugriff auf 
 * ein WDataModel. Sie enth�lt Methoden die den Zugriff auf die gesamte
 * Struktur eines WDataModels (WDataModel, WSignature, WAttribute, WDataItem)
 * in einer Klasse b�ndelt.
 * F�r den schreibenden Zugriff wurde die Klasse WDataModelWriter als
 * Erweiterung von WDataModelReader implementiert.
 * @author ab0043
 * 
 * @see com.entitec.wax.wdm.WDataModelWriter
 */
public  class WDataModelReader implements Serializable{
	/**
	 * Die Konstante MODUS_INHOMOGENOUS setzt den Modus des WDataModels
	 * auf inhomogen, d.h. das WDataModel kann ungleichm��ig belegt sein.
	 */
	public static final int MODUS_INHOMOGENOUS = 0;
	/**
	 * Die Konstante MODUS_HOMOGENOUS setzt den Modus des WDataModels
	 * auf homogen, d.h. das WDataModel kann gleichm��ig belegt sein.
	 */
	public static final int MODUS_HOMOGENOUS = 1;
	protected WDataModel mDataModel;
	protected IWSignature mSignature = null;
  private boolean mRowNotInitialized = false;
	/**
	 * Konstruiert einen 'leeren' WDataModelReader. �ber die Methode init()
	 * kann dann dem Reader ein WDataModel zugewiesen werden. Dies erm�glicht 
	 * die Wiederverwendung eines WDataModelReaders. 
	 */
  public WDataModelReader() {
  }  
	/**
	 * Konstruiert einen WDataModelReader f�r das angegebene WDataModel.
	 * @param dm 
	 */
  public WDataModelReader(WDataModel dm) {
  	init(dm);
  }  
	/**
	 * Initialisiert einen WDataModelReader mit dem angegebene WDataModel.
	 * @param dm 
	 */
  public void init(WDataModel dm) {
  	mDataModel = dm;
	  mSignature = dm.getSignature();
  }
	/**
	 * Liefert eine Aufz�hlung (Enumeration) der IWAttribute aus der 
	 * IWSignature des WDataModels.
	 * Die Enumeration ist folgenderma�en zu benutzen:
	 * <pre>
	 * import java.util.*;
	 *    :
	 * for(Enumeration e=dm.attributes();e.hasMoreElements();) {
	 *   IWAttribute a = (IWAttribute)e.nextElement();
	 *    :
	 * }
	 * </pre>
	 * @return
	 */
  public Enumeration attributes() {
		return mDataModel.getSignature().attributes();
  }  
  /**
   * Liefert die Anzahl der Spalten des WDataModels, dies entspricht der  
   * Anzahl der WAttribute der WSignature. 
   * @return
   */
  public int colCount() {
		return mDataModel.getColCount();
  }  
	/**
	 * Setzt den Schalter mAliasIsName auf 'true' oder 'false'
	 * Der Schalter steuert, ob beim Zugriff auf ein IWAttriubte 
	 * mit dem Namen oder mit dem Alias gelesen wird. 
	 * @param b
	 */
  public boolean getAliasIsName() {
	  return mSignature.getAliasIsName();
  }  
	/**
	 * Setzt an der WSignature des WDatoModels den Schalter 
	 * mAliasIsName auf 'true' oder 'false'.
	 * Der Schalter steuert, ob beim Zugriff auf ein IWAttriubte 
	 * mit dem Namen oder mit dem Alias gelesen wird. 
	 * @param b
	 */
	public void setAliasIsName(boolean b) {
		mSignature.setAliasIsName(b);
	}  
	/**
	 * Liefert eine Aufz�hlung der IWDataItems der Spalte mit
	 * dem Attributnamen 'name'.
	 * Die Enumeration ist folgenderma�en zu benutzen:
	 * import java.util.*;
	 *    :
	 * for(Enumeration e=dm.attributes();e.hasMoreElements();) {
	 *   IWDataItem di = (IWDataItem)e.nextElement();
	 *    :
	 * }
	 * @param name
	 * @return
	 * @throws WAttributeNotFoundException
	 */
  public Enumeration getCol(String name) throws WAttributeNotFoundException {
		return mDataModel.getCol(name);
  }  
	/**
	 * Liefert das WDataModel des WDataModelReaders.
	 * @see com.entitec.wax.wdm.WDataModel
	 * @return
	 */
  public WDataModel getDataModel() {
		return mDataModel;
  }  
	/**
	 * Liefert die IWSignature des WDataModelReaders.
	 * @see com.entitec.wax.wdm.IWSignature
	 * @return
	 */
  public IWSignature getSignature() {
		return mDataModel.getSignature();
  }  
	/**
	 * Liefert den Zustand des WDataItems. M�gliche Zust�nde:
	 * @see com.entitec.wax.wdm.IWDataItem
	 * @return
	 */
  public short getState(String name) throws WAttributeNotFoundException {
    if (mRowNotInitialized == true) {
  	  return IWDataItem.DISABLED;
    } else {
  	  return ((IWDataItem)mDataModel.getDataItem(name)).getState();
    }
  }  
	/**
	 * Liefert den Zustand des WDataItems als Charakter-Wert. M�gliche Zust�nde:
	 * @see com.entitec.wax.wdm.IWDataItem
	 * @param name
	 * @return
	 * @throws WAttributeNotFoundException
	 */
  public char getStateAsChar(String name) throws WAttributeNotFoundException {
  	return ((IWDataItem)mDataModel.getDataItem(name)).getStateAsChar();
  }  
	/**
	 * Liefert den Wert des WDataItems. Ist mValue == null, dann liefert die
	 * Methode "" als Ergebnis. Steht der Zeilenzeiger auf der n�chsten nach
	 * der letzten Zeile, so liefert die Methode ein 'leeres' IWDataItem  zur�ck.
	 * @param name
	 * @return
	 * @throws WAttributeNotFoundException
	 */
  public Object getValue(String name) throws WAttributeNotFoundException {
    Object o = null;
    if (mRowNotInitialized == true) {
      o = this.getSignature().getAttribute(name).getDefaultValue();
    } else {
      o = ((IWDataItem)mDataModel.getDataItem(name)).getValue();
      if (o instanceof String) {
        if (((String)o).length() == 0) {
          o = this.getSignature().getAttribute(name).getDefaultValue();
        }
      }
    }
    return o;
  }  
	/**
	 * Wert des Attributes name in der aktuellen Zeile KID konform zur�ckgeben, d.h. Werte des Kid-Types
	 * Charakter werden bis zur Attributl�nge mit Leerzeichen (Spaces) aufgef�llt.
	 * 
	 * @param name	Attributname
	 * @return KID konformer Wert des Attributes
	 * @throws WAttributeNotFoundException
	 * @throws WRowNotFoundException
	 */
	public String getValueFilled(String name)throws WAttributeNotFoundException, WRowNotFoundException {
		Object o = this.getValue(name);
		IWAttribute a = this.getSignature().getAttribute(name);
		return WDMTypChecker.getTypConformFilledValue(a, (String) o);
	}
	
	/**
	 * Gibt den Wert des Dezimal Attribut name in der Form "-123.5" zur�ck. 
	 * Das Attribut muss vom Typ Decimal sein.
	 * 
	 * @param name	Attributname
	 * @return	Dezimalwert in aufbereiteter Form
	 * @throws WAttributeNotFoundException
	 * @throws WRowNotFoundException
	 */
	public String getDecimalValue(String name )
	throws WAttributeNotFoundException, WRowNotFoundException {
		Object o = this.getValue(name);
		IWAttribute a = this.getSignature().getAttribute(name);
		if ( a.getFrm() == IWAttribute.FRM_DEC){
			return WDMTypChecker.getDecimal((String)o);
		} else {
			throw new WAttributeNotFoundException(name+" ist nicht vom Typ Dezimal");
		}
	}
	/**
	 * Liefert das IWDataItem der aktuellen Zeile in der Spalte mit dem 
	 * Namen 'name'.
	 * @param name
	 * @return
	 * @throws WAttributeNotFoundException
	 */
  public IWDataItem get(String name) throws WAttributeNotFoundException {
    if (mRowNotInitialized == true) {
      return new WDataItem(null, IWDataItem.DISABLED);
    } else {
      return (IWDataItem)mDataModel.getDataItem(name);
    }
  }  
  /**
   * Liefert den Attributnamen der Spalte 'col'.
   * @param col
   * @return
   */
  public String getAttributeName(int col) {
		return mSignature.getAttributeName(col);
  }
	/**
	 * Liefert das IWAttribute mit dem Namen 'name'.
	 * @param name
	 * @return
	 */
	public IWAttribute getAttribute(String name) {
		return mSignature.getAttribute(name);
	}
	/**
	 * Pr�ft, ob das WDataModel ein IWAttribute mit dem Namen 'name'
	 * enth�lt.
	 * @param name
	 * @return
	 */
  public boolean hasAttribute(String name) {
		return mDataModel.getSignature().hasAttribute(name);
  }  
	/**
	 * Pr�ft, ob das WDataModel IWAttribute enth�lt.
	 * @param name
	 * @return
	 */
  public boolean hasAttributes() {
		return mDataModel.getSignature().hasAttributes();
  }  
	/**
	 * Pr�ft, ob das WDataModel IWDataItems enth�lt.
	 * @param name
	 * @return
	 */
  public boolean hasData() {
		return mDataModel.hasData();
  }  
	/** 
	 * Liefert eine Aufz�hlung der Namen der IWAttribute der WSignature 
	 * des WDatoModels in Abh�ngigkeit von dem Schalter mAliasIsName.
	 * Die Enumeration ist folgenderma�en zu benutzen:
	 * import java.util.*;
	 *    :
	 * for(Enumeration e=dm.attributes();e.hasMoreElements();) {
	 *   String s = (String)e.nextElement();
	 *    :
	 * }
	 * @return
	 */
  public Enumeration keys() {
		return mSignature.keys();
  }  
	/** 
	 * Liefert String-Array der Namen der IWAttribute der WSignature 
	 * des WDatoModels in Abh�ngigkeit von dem Schalter mAliasIsName.
	 * Das String-Array ist folgenderma�en zu benutzen:
	 * import java.util.*;
	 *    :
	 * WDataModelReader dmr;
	 *    :
	 * String[] names = dmr.keyNames();
	 * for(int i=0;i<names.length;i++) {
	 *   String s = names[i];
	 *    :
	 * }
	 * @return
	 */
  public String[] keyNames() {
  	String[] names = new String[mSignature.cols()];
  	int i=0;
  	for (Enumeration e=mSignature.keys();e.hasMoreElements();) {
  		names[i] = (String)e.nextElement();
  		i++;
  	}
  	return names;
  }  
	/**
	 * Liefert die Zeilenanzahl des WDataModels.
	 * @return
	 */
  public int rowCount() {
		return mDataModel.getRowCount();
  }  
	/**
	 * Liefert den Index der aktuellen Zeile des WDataModels.
	 * ACHTUNG:
	 * Der Zeilenindex beginnt mit 0 und endet mit rowCount()-1.
	 * @return
	 */
  public int curRowNr() {
		return mDataModel.mCurRow;
  }  
  /**
   * Setzt den Zeilenzeiger auf die Zeile mit dem Index 'row'.
   * ACHTUNG:
   * Soll der Zeiger auf die n�chste Zeile nach der letzten Zeile 
   * gesetzt werden, so gibt es keine WRowNotFoundException, statt-
   * dessen wird der Reader auf 'nicht initialisiert' gesetzt, d.h. 
   * solange der Zeilenzeiger nicht ver�ndert wird, liefert der 
   * Reader beim Lesen ein 'leeres' IWDataItem zur�ck.
   * Dieses Verhalten ist notwendig, da der WDataModelWriter ebenfals
   * das setzen des Zeigers auf die n�chste Zeile nach der letzten zu 
   * l��t, um automatisch Zeilen anf�gen zu k�nnen.
   * @param row
   * @throws WRowNotFoundException
   */
  public void setRow(int row) throws WRowNotFoundException {
    if (row == mDataModel.mCurRow) {
      return;
    } else {
  	  if (row > mDataModel.mRowCount-1 || row < 0) {
  	    if (row == mDataModel.mRowCount) {
	        mRowNotInitialized = true;
	        return;
	  	  } else {
	  	  	throw new WRowNotFoundException("Row: "+row);
	      }
  	  }
  	  mRowNotInitialized = false;
  	  mDataModel.mCurRow = row;
    }
  }  
	/**
	 * Liefert das WDataModels als String zur�ck.
	 * @return
	 */
  public String toString() {
  	if( mDataModel == null ){
  		return "<null/>";
  	}
  	return mDataModel.toString();
  }  
}
