package com.entitec.wax.wdm;

/**
 * Date:    03.07.2003
 * <p>Description:</p>
 * Die Klasse WDataItem umfasst den Wert und den Zustand einer Zelle,
 * markiert durch Zeile und Spalte bzw. WAttribute-Name, in einen WDataModel.
 * @author AB0043
 */
public class WDataItem implements IWDataItem {
	/*
	 * Wert des WDataItems.
	 */
  protected Object mValue;
	/*
	 * Zustand des WDataItems.
	 */
  protected short mState;
	/**
	 * Erzeugen eines 'leeren' WDataItems. Die Eigenschaften Wert und Zustand
	 * werden mit Initialwerten (mValue = null und mState = IWDataItem.DISABLED 
	 * gef�llt.
	 */
  public WDataItem() {
    mValue = null;
    mState = 0;
  }
	/** 
	 * Teilt dem WDataItem mit, das die Initialisierung abgeschlossen ist. 
	 * Erweiterung f�r ChangedInd.
	 */
  public void initCompleted(){;}
	/**
	 * Erzeugen eines initialisierten WDataItems. 
	 * @param value Wert des WDataItems
	 * @param state Zustand des WDataItems
	 */
  public WDataItem(Object value, short state){
    if ( value == null ) {
      mValue = value;
      mState = 0;
    } else {
      mValue = value;
      mState = state;
    }
  }  
  /**
	 * Initialisieren eines WDataItems. Die Eigenschaften Wert und Zustand
	 * werden mit Initialwerten (mValue = null und mState = IWDataItem.DISABLED 
	 * gef�llt.
   */
  public void init() {
    mValue = null;
    mState = 0;
  }
  /**
   * Liefert den Zustand des WDataItems. M�gliche Zust�nde:
   * @see com.entitec.wax.wdm.IWDataItem
   * @return
   */
  public short getState() {
		return mState;
  }  
	/**
	 * Liefert den Zustand des WDataItems als Charakter-Wert. M�gliche Zust�nde:
	 * @see com.entitec.wax.wdm.IWDataItem
	 * @return
	 */
  public char getStateAsChar() {
    switch (mState) {
      case IWDataItem.DISABLED:
        return '0';
      case IWDataItem.ENABLED:
        return '1';
      case IWDataItem.TRUNCATED:
        return '2';
      case IWDataItem.ERROR:
        return '3';
      case IWDataItem.UNCHANGED:
        return '4';
      case IWDataItem.INVALID:
        return '9';
      default:
        return '0';
    }
  }
	/**
	 * Liefert den Wert des WDataItems. Ist mValue == null, dann liefert die
	 * Methode "" als Ergebnis.
	 * @return
	 */
  public Object getValue() {
    if (mValue == null) {
      return "";
    } else {
      if (mValue instanceof String) {
        return mValue;
      } else {
        return WDMToolbox.deepCopy(mValue);
      }
    }
  }  
  /**
   * Setzt den Zustand des WDataItems auf den Wert von state.
   * M�gliche Zust�nde:
	 * @see com.entitec.wax.wdm.IWDataItem
	 * @param state
   */
  public void setState(short state) {
		mState = state;
  }  
	/**
	 * Setzt den Wert des WDataItems auf den Wert von value.
	 * @param value
	 */
  public void setValue(Object value) {
		mValue = value;
  }  
	/**
	 * R�ckgabe des WDataItems als String.
	 * @return
	 */
  public String toString() {
		return toString("WDATAITEM");
  }  
	/**
	 * R�ckgabe des WDataItems als String.
	 * @param name Name des Attributes der Spalte.
	 * @return
	 */
  public String toString(String name) {
  	StringBuffer sb = new StringBuffer(this.getStringSize());
  	toString(sb, name);
    return sb.toString();
  }  
	/**
	 * Aufl�sen des WDataItems in den �bergebenen StringBuffer.
	 * @param sb.
	 * @param name Name des Attributes der Spalte.
	 */
	public void toString(StringBuffer sb, String name) {
		sb.append("<WDATAITEM Name=\"");
		sb.append(name);
		sb.append("\"");
		for(int i=0;i<16-name.length();i++) sb.append(' ');
		sb.append(" Ind=\"");
		sb.append(mState);
		sb.append("\">");
		if (mValue instanceof WDataModel) {
			sb.append("\n");
			sb.append(mValue);
			sb.append("</WDATAITEM>\n");
		} else {
			sb.append("<![CDATA[" + mValue + "]]>");
			sb.append("</WDATAITEM>\n");
		}
	}  
	public int getStringSize(){
		if (mValue instanceof String) {
			return 56+((String)mValue).length();
		} else if (mValue instanceof WDataModel) {
			return 56+((WDataModel)mValue).getStringSize();
		} else {
			return 156;
		}
	}
}
