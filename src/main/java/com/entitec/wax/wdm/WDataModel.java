package com.entitec.wax.wdm;

import java.io.Serializable;
import java.util.Enumeration;
import java.util.Vector;

import com.entitec.wax.wglobal.WRuntimeException;


/**
 * Date:    03.07.2003
 * <p>Description:</p>
 * Die Klasse WDataModel ist der Kopf einer Klassenstruktur in
 * der Daten in tabellenartiger Form gespeichert und verwaltet
 * werden. Die Daten sind in Spalten organisiert, wobei das 
 * Format der Daten jeder Spalte in einem WAttribut, das in 
 * der WSignature abgelegt wird, beschrieben ist.
 * 
 * Das WDataModel ist somit ein selbstbeschreibender Datencontainer,
 * auch dataview genannt.
 * 
 * Die Struktur stellt sich folgenderma�en da:
 * 
 * WDataModel:
 * <pre>
 *   
 *                         |- IWAttribute -|
 *                         V               V
 *  | -------------- |-|------------|------------|----
 *  | -> IWSignature:| | NAME       | VORNAME    | ...
 *  |                | | |-Name     | |-Name     | ...
 *  |                |I| |-Alias    | |-Alias    | ...
 *  |                |N| |-Format   | |-Format   | ...
 *  |                |D| |-Length   | |-Length   | ...
 *  |                |E| |-Decimel  | |-Decimel  | ...
 *  |                |X| |-Signed   | |-Signed   | ...
 *  | -------------- |-|------------|------------|----
 *  | -> Vector:     |0| IWDataItem | IWDataItem | ...
 *  |   (of Vectors) |-|------------|------------|----
 *  |                |1| IWDataItem | IWDataItem | ...
 *  |                |-|------------|------------|----
 *  |                | |     :      |     :      | ...
 *                          A              A
 *                          |--- Vector ---|
 *                          (of IWDataItems)
 * </pre>
 * 
 * Die Erzeugung und Bearbeitung eines WDataModels erfolgt �ber
 * WDataModelReader f�r den lesenden Zugriff und WDataModelWriter f�r
 * den schreibenden Zugriff.
 * @author AB0043
 * 
 * @see WSignature
 * @see WAttribute
 * @see WDataItem
 * @see WDataModelReader
 * @see WDataModelWriter
 */
public class WDataModel implements Serializable {
	/**
	 * Die Konstante MODUS_INHOMOGENOUS setzt den Modus des WDataModels
	 * auf inhomogen, d.h. das WDataModel kann ungleichm��ig belegt sein.
	 */
  public static final int MODUS_INHOMOGENOUS = 0;
	/**
	 * Die Konstante MODUS_HOMOGENOUS setzt den Modus des WDataModels
	 * auf homogen, d.h. das WDataModel kann gleichm��ig belegt sein.
	 */
  public static final int MODUS_HOMOGENOUS = 1;
  /*
   * Vector in dem die Vectoren f�r IWDataItems je IWAttribute
   * gehalten werden. 
   */
  private Vector<Vector<IWDataItem>> mData = new Vector<Vector<IWDataItem>>();
  /*
   * IWSignature des WDataModel, enth�lt die IWAttributes
   */
  private WSignature mSig = null;
	/*
	 *  mUseChangedInd bestimmt welches IWDataItem verwendet 
	 *  wird (false=WDataItem, true=WDataItemChangedInd)
	 */
	protected boolean mUseChangedInd = false;
  /*
   * Wenn die Initialisierung des WDataModel abgeschlossen ist,
   * steht mInitCompleted auf true.
   */
	protected boolean mInitCompleted = false;
  
	/*
	 * Zeilenzeiger (Cursor) f�r die aktuelle Zeile
	 */
  protected int mCurRow = -1;
	/*
	 * Zeilenz�hler (Counter) f�r die Anzahl Zeilen
	 */
  protected int mRowCount = 0;
  /*
   *Der Hierarchie-Level des WDataModels, wird f�r toString ben�tigt. 
   */
  protected int mHierarchyLevel = 0;
  /*
   *Der Modus des WDataModels 
   */
  protected int mModus = MODUS_INHOMOGENOUS;
	/*
	 *Wird das DataModel mit Index verwaltet (ja/nein)
	 */
	protected boolean mIsIndexed = false;
	/*
	 *Letzter vergebener Index des DataModels
	 */
	protected int mLastIndex = -1;
  /**
   * Konstruiert ein WDataModel mit der angegebenen IWSignature.
	 * @param s 
	 */
	public WDataModel(IWSignature s) {
		if (s instanceof WSignature) {
			mSig = (WSignature)s;
			init();
		} else {
			throw new WRuntimeException("A WDataModel can only be initialized with a WSignature");
		}
  }

  /**
   * Konstruiert ein WDataModel mit der angegebenen IWSignature.
	 * @param s 
   * @param useChangedInd bestimmt welches IWDataItem verwendet 
   * wird (false=WDataItem, true=WDataItemChangedInd)
   */
  public WDataModel(IWSignature s, boolean useChangedInd) {
		if (s instanceof WSignature) {
			mUseChangedInd = useChangedInd;
			mSig = (WSignature)s;
			init();
		} else {
			throw new WRuntimeException("A WDataModel can only be initialized with a WSignature");
		}
  }
  /**
   * Erweitert das WDataModel um eine Spalte durch Hinzuf�gen 
   * eines IWAttributes zur IWSignature.
   * @param attrib
   * @throws WDublicateAttributeException
   */
  protected synchronized void addCol(IWAttribute attrib) throws WDublicateAttributeException {
  	mSig.mIsInitialized = false;
		mSig.addAttribute(attrib);
		mSig.mIsInitialized = true;
		Vector v = (new Vector());
    for (int i = 0; i < this.mRowCount; i++) {
	    if ( mUseChangedInd ) { v.addElement(new WDataItemChangedInd()); }
	    else {v.addElement(new WDataItem());}
    }
    mData.addElement(v);
  }
  /**
   * Setzt f�r alle IWDataItems den State (Indikator) auf den Wert 
   * der �bergebenen Variablen state.
	 * @param state
	 * M�gliche states sind in IWDataItem als Konstante definiert.
	 * @see IWDataItem
	 */
	public void setAllStates(short state) {
  	Enumeration e = mData.elements();
		while(e.hasMoreElements()) {
		  Vector v = (Vector)e.nextElement();
		  Enumeration ee = v.elements();
  	  while(ee.hasMoreElements()) {
//	  for ( int i = 0; i < mRowCount; i++ ) {
//	    ((IWDataItem)v.elementAt(i)).setState(state);
  	    ((IWDataItem)ee.nextElement()).setState(state);
      }
  	}
  }
  /** 
   * Teilt allen IWDataItem's mit, das die Initialisierung abgeschlossen ist. 
   * Erweiterung f�r ChangedInd.
   */
  public void initCompleted() {
	if (mInitCompleted) return;
  	Enumeration e = mData.elements();
		while(e.hasMoreElements()) {
		  Vector v = (Vector)e.nextElement();
		  Enumeration ee = v.elements();
		  while(ee.hasMoreElements()) {
			  ((IWDataItem)ee.nextElement()).initCompleted();
		  }
  	}
    mInitCompleted = true;
  }
  /**
	 * F�gt dem WDataModel nach der letzten Zeile eine Zeile hinzu.
	 * Die Zeile enth�lt dann je IWAttribute eine 'leeres' IWDataItem.
	 * Der Zeilenzeiger (curRowNr()) steht dann auf dieser Zeile.
	 */
	protected synchronized void addRow() {
  	if (mData.size() == 0) {
			for (int i=0;i<mSig.cols(); i++) {
				Vector v = (new Vector());
	      if ( mUseChangedInd ) { v.addElement(new WDataItemChangedInd()); }
	      else {v.addElement(new WDataItem());}
		    mData.addElement(v);
			}
			mSig.mIsInitialized = true;
  	} else {
			Enumeration e = mData.elements();
			while(e.hasMoreElements()) {
				Vector v = (Vector)e.nextElement();
				if ( mUseChangedInd ) { v.addElement(new WDataItemChangedInd()); }
				else {v.addElement(new WDataItem());}
			}
  	}
  	//Setzen der currentRow auf die angef�gte Zeile
		mCurRow = mRowCount;
		//Erh�hen des Zeilenz�hlers
		mRowCount++;
		//Wenn DataModel indiziert, dann initialisieren des Index in der neuen Zeile
		if (this.mIsIndexed == true) initIndex(mCurRow, 1);
  }
  /** 
   * Fuegt in Zeile row weitere Zeilen in das WDataModel ein. 
   * Die Variable count gibt an, wieviele Zeilen eingef�gt werden. 
   * Die urspruengliche Zeile row und alle nachfolgenden werden 
   * nach unten verschoben.
   */
  public synchronized void addRow(int row, int count) {
    if (mData.size()==0) {
      for (int i=0;i<mSig.cols();i++) mData.addElement(new Vector());
    }
    Enumeration e = mData.elements();
    while (e.hasMoreElements()) {
	    Vector v = (Vector)e.nextElement();
	    for (int i=0; i<count; i++) {
		    if ( mUseChangedInd ) { v.insertElementAt(new WDataItemChangedInd(), row); }
		    else {v.insertElementAt(new WDataItem(), row);}
	    }
    }
    mRowCount+=count;
		//Wenn DataModel indiziert, dann initialisieren des Index in den neuen Zeilen
		if (this.mIsIndexed == true) initIndex(row, count);
  }
  /**
   * Entfernt die Zeile row aus dem WDataModel.
	 * @param row
	 */
	public synchronized void removeRow(int row) {
    if ((row >= 0) && (row < mRowCount)) {
      Enumeration e = mData.elements();
      while(e.hasMoreElements()) {
          Vector v = (Vector)e.nextElement();
          v.removeElementAt(row);
      }
			mRowCount--;
      if (mCurRow >= mRowCount) mCurRow = mRowCount-1;
    }
  }  
  /**
   * Verschiebt im WDataModel die Zeile row um die in distance angegebene 
   * Anzahl Zeilen.
	 * @param row
	 * @param distance
	 */
	public synchronized void moveRow(int row, int distance) {
    int newRow = row + distance;
    if ((row >= 0) && (row < mRowCount) 
     && (newRow >= 0) && (newRow < mRowCount)) {
      Enumeration e = mData.elements();
      while(e.hasMoreElements()) {
	      Vector v = (Vector)e.nextElement();
	      Object o = v.elementAt(row);
	      v.removeElementAt(row);
				v.insertElementAt(o, newRow);
        mCurRow = newRow;
      }
    }
  }
  /**
   * L�scht die Daten des WDataModels, d.h. anschlie�end enth�lt das
   * WDataModel keine IWDataItems mehr.
   * Die IWSignature bleibt erhalten.
   */
  protected void clear() {
    init();
  }
  /**
   * L�scht die Daten und die Spalten des WDataModels, d.h. anschlie�end enth�lt das
   * WDataModel keine IWDataItems und keine IWAttributes mehr.
   * Die IWSignature ist leer.
   */
  protected void clearAll() {
    init();
	mSig = new WSignature();
  }
	/**
	 * L�scht die Daten der Spalte 'name' des WDataModels, d.h. 
	 * anschlie�end enth�lt die Spalte keine IWDataItems mehr.
	 * Das IWAttribute in der IWSignature bleibt erhalten.
	 */
  protected void clearCol(String name) throws WAttributeNotFoundException{
		Vector v = getColVector(name);
		if (v != null) {
			for ( int i = 0; i < mRowCount; i++ ) {
		      ((IWDataItem)v.elementAt(i)).init();
			}
		}
  }  
  /**
   * Liefert eine Aufz�hlung der IWDataItems der Spalte mit
   * dem Attributnamen 'name'.
   * Die Enumeration ist folgenderma�en zu benutzen:
   * import java.util.*;
   *    :
   * for(Enumeration e=dm.attributes();e.hasMoreElements();) {
   *   IWDataItem di = (IWDataItem)e.nextElement();
   *    :
   * }
   * @param name
   * @return
   * @throws WAttributeNotFoundException
   */
  protected Enumeration getCol(String name) throws WAttributeNotFoundException {
    return getColVector(name).elements();
  }
  /**
   * Liefert die Anzahl der Spalten des WDataModels.
   * @return
   */  
  protected int getColCount() {
		return mSig.cols();
  }
  /**
   * Liefert den Attributnamen der Spalte mit dem Index 'col'.
   * @param col
   * @return
   * @throws WAttributeNotFoundException
   */
  protected String getColName(int col) throws WAttributeNotFoundException {
		return mSig.getAttributeName(col);
  }
  /**
   * Liefert einen Vector mit den IWDataItems der Spalte mit dem Index 'col'.
   * @param col
   * @return
   * @throws WAttributeNotFoundException
   */
  protected Vector getColVector(int col) throws WAttributeNotFoundException{
    if ((col >= 0) && (col < mData.size())) {
      return (Vector)mData.elementAt(col);
    } else {
      throw new WAttributeNotFoundException("Attriubt-Index: "+col);
    }
  }
	/**
	 * Liefert einen Vector mit den IWDataItems der Spalte mit dem 
	 * Attributnamen 'name'.
	 * @param name
	 * @return
	 * @throws WAttributeNotFoundException
	 */
  protected Vector getColVector(String name) throws WAttributeNotFoundException{
  	if (mData.size() > 0) {
  		int i = mSig.indexOf(name);
  		return (Vector)mData.elementAt(i);
  	} else {
	  	return null;
  	}
  }
	/**
	 * Liefert das IWDataItem der aktuellen Zeile in der Spalte 'col'.
	 * @param name
	 * @return
	 * @throws WAttributeNotFoundException
	 */
  protected IWDataItem getDataItem(String name) throws WAttributeNotFoundException {
  		int idx = mSig.indexOf(name);
		Vector v = getColVector(idx);
		IWDataItem di = (IWDataItem)v.elementAt(mCurRow);
		// bug#:385 bei alias-duplikaten wird ein gesetzter Wert, (ind <> 0) gesucht
		if (di.getState() == IWDataItem.DISABLED && mSig.mAliasIsName ){
			while ( idx < (mSig.cols()-1) ){
				idx = mSig.indexOf(name, idx+1);
				if ( idx == -1 ) break;
				v = getColVector(idx);
				di = (IWDataItem)v.elementAt(mCurRow);
				if ( di.getState() != IWDataItem.DISABLED ) break;
			}
		}
		
		if (mModus == WDataModel.MODUS_INHOMOGENOUS) {
			return di;
		} else {
			if (mSig.getAttribute(name).getHomo()) {
				if (di.getState() == WDataItem.DISABLED) {
					return (IWDataItem)v.elementAt(0);
				} else {
					return di;
				}
			} else {
				return di;
			}
		}
  }
  /**
   * Liefert eine Aufz�hlung der IWDataItems der aktuellen Zeile.
   * Die Enumeration ist folgenderma�en zu benutzen:
   * import java.util.*;
   *    :
   * for(Enumeration e=dm.attributes();e.hasMoreElements();) {
   *   IWDataItem di = (IWDataItem)e.nextElement();
   *    :
   * }
   * @return
   */
  protected Enumeration getRow() {
		Vector v = new Vector();
		Enumeration e = mData.elements();
		while (e.hasMoreElements()) {
			Vector col = (Vector)e.nextElement();
			v.addElement(col.elementAt(mCurRow));
		}
		return v.elements();
  }
  /**
   * Liefert die Zeilenanzahl des WDataModels.
   * @return
   */
  protected int getRowCount() {
		return mRowCount;
  }
  /**
   * Liefert die IWSignature des WDataModels. 
   * @return
   */
  protected IWSignature getSignature() {
		return mSig;
  }
  /**
   * Pr�ft, ob das WDataModel IWDataItems enth�lt.
   * @return
   */
  protected boolean hasData() {
		return mSig.hasAttributes() && mRowCount>0;
  }
  /**
   * Liefert den Index des IWAttributes der WSignature 
	 * des WDatoModels mit dem Namen 'name'.
   * @param name
   * @return
   * @throws WAttributeNotFoundException
   */
  protected int indexOf(String name) throws WAttributeNotFoundException {
		return mSig.indexOf(name);
  }
  /**
   * Initialisiert das WDataModel auf der Basis der vorhandenen
   * WSignature. Die IWDataItems werden gel�scht und der Zeilenz�hler
   * zur�ckgesetzt.
   */
  private synchronized void init() {
    if (mSig != null) {
			mInitCompleted = false;
      mCurRow = -1;
      mRowCount = 0;
      mData.removeAllElements();
      Enumeration e = mSig.attributes();
      while (e.hasMoreElements()) {
        IWAttribute a = (IWAttribute)e.nextElement();
        mData.addElement(new Vector());
      }
    } else {
      throw new WRuntimeException("Initialization failed, WSignature expected.");
    }
  }  
	/**
	 * Entfernt aus dem WDataModel die Spalte mit dem WAttributnamen 'name'.
	 * In der WSignature wird das IWAttribute mit dem Namen 'name' ebenfalls
	 * entfernt.
	 * @param name
	 */
  protected synchronized void removeCol(String name) throws WAttributeNotFoundException {
		int index = this.indexOf(name);
		mSig.removeAttribute(name);
		mData.removeElementAt(index);
  }
  /**
   * Setzt die Membervariable mRowCount auf den �ber 'row' angegebenen
   * Wert. Die Membervariable mRowCount legt die Zeilenanzahl fest. 
   * @param row
   */
  protected void setRowCount(int row) {
  	mRowCount = row;
  }
  /**
   * Setzt den Zeilenzeiger auf die �ber 'row' angegebene Zeile.
   * ACHTUNG:
   * Ist 'row' gleich der Zeilenanzahl, dies entspricht der ersten
   * Zeile nach der letzten, dann wird automatisch mit addRow() 
   * eine Zeile angef�gt.
   * @param row
   * @throws WRowNotFoundException
   */
  protected void setRow(int row) throws WRowNotFoundException {
	  if (row < 0) {
	    throw new WRowNotFoundException("RowNr.: "+row);
// �nderung durch AB0043, 28.07.2001: Keine Exception wenn ich ein setRow auf
// die n�chste nach mRowCount-1 folgende Zeile ausf�hre, sondern ein addRow.
		} else if (row > mRowCount-1) {
		  if (row == mRowCount) {
			  this.addRow();
		  } else {
			  throw new WRowNotFoundException("RowNr.: "+row);
		  }
		} else {
		  mCurRow = row;
		}
	}  
	/**
	 * Liefert den Hierarchie-Level f�r die Ausgabe des WDataModels, 
	 * um die korrekte Einr�ckung zu gew�hrleisten.
	 */
  public int getHierachyLevel() {
    return mHierarchyLevel;
  }
  /**
   * Setzt den Hierarchie-Level f�r die Ausgabe des WDataModels
   * als String, um die korrekte Einr�ckung zu gew�hrleisten.
   * @param level
   */
  public void setHierachyLevel(int level) {
    mHierarchyLevel = level;
  }
  /**
   * Pr�fen, ob DataModel indiziert ist.
   * @return
   */
	protected boolean isIndexed() {
		return mIsIndexed;
	}
	/**
	 * Indizierung der DataModels aktivieren.
	 * Die Spalte IWSignature.INDEX_COLUMN_NAME wird der Signature
	 * hinzugef�gt und mit den Zeilennummern initialisiert.
	 */
	protected void setIndexed() {
		if (mIsIndexed == false) {
			//Index-Spalte hinzuf�gen
			this.addCol(mSig.getIndexAttribute());
			if (this.mRowCount > -1) {
				//Index-Spalte initialisieren
				initIndex(0, this.mRowCount);
			}
			//Index-Schalter auf an
			mIsIndexed = true;
		}
	}
	/**
	 * Indizierung der DataModels deaktivieren.
	 * Die Spalte IWSignature.INDEX_COLUMN_NAME wird aus der Signature
	 * entfernt.
	 */
	protected void removeIndex() {
		if (mIsIndexed == true) {
			//Index-Spalte entfernen
			this.removeCol(IWSignature.INDEX_COLUMN_NAME);
			//Index-Schalter auf aus
			mIsIndexed = false;
		}
	}
	/**
	 *Iterieren �ber die Zeilen von row bis row+count (anz)
	 *und initialisieren der Spalte IWSignature.INDEX_COLUMN_NAME
	 *mit dem Index mLastIndex+1.
	 * @param row
	 * @param count
	 */
	protected void initIndex(int row, int count) {
		int anz = row+count;
		Vector v = this.getColVector(IWSignature.INDEX_COLUMN_NAME);
		//Iterieren �ber die Zeilen von row bis row+count (anz)
		//und initialisieren der Spalte IWSignature.INDEX_COLUMN_NAME
		for (int i=row; i<anz; i++) {
			IWDataItem di = (IWDataItem)v.elementAt(i);
			di.setState(IWDataItem.ENABLED);
			//Erh�hen des letztvergebenen Indexes
			mLastIndex++;
			di.setValue(""+mLastIndex);
		}
	}
	/**
	 * Liefert den Index der aktuellen Zeile.
	 * @return
	 */
	protected int getIndexOfCurentRow() {
		if (mCurRow > -1) {
			String value = (String)this.getDataItem(IWSignature.INDEX_COLUMN_NAME).getValue();
			return Integer.parseInt(value);
		}
		return -1;
	}
	/**
	 * Setzt den Zeilenzeiger auf die Row mit dem Index 'index'.
	 * Zur�ckgegeben wird die Zeilennummer, die den Index enth�lt.
	 * Wird keine Zeile mit dem angegebenen Index gefunden, so
	 * ist die R�ckgabe gleich -1.
	 * @param index
	 */
	protected int setRowToIndex(int index) {
		Vector v = this.getColVector(IWSignature.INDEX_COLUMN_NAME);
		/* Aus Performance-Gr�nden: Wenn index < mRowCount/2,
		 *                          dann Iteration von 0 aufsteigend, 
		 *                          sonst von mRowCount-1 absteigend.
		 */
		if (index < mRowCount/2) {
			//Iterieren in der Index-Spalte �ber die Zeilen 
			//von 0 bis rowCount-1 (Zeilenanzahl)
			for (int i=0; i<mRowCount; i++) {
				//Zeilenzeiger auf die n�chste Zeile setzen
				mCurRow = i;
				IWDataItem di = (IWDataItem)v.elementAt(i);
				if (index == Integer.parseInt((String)di.getValue())) return i;
			}
		} else {
			//Iterieren in der Index-Spalte �ber die Zeilen 
			//von rowCount-1 bis 0 (Zeilenanzahl)
			for (int i=mRowCount-1; i>=0; i--) {
				//Zeilenzeiger auf die n�chste Zeile setzen
				mCurRow = i;
				IWDataItem di = (IWDataItem)v.elementAt(i);
				if (index == Integer.parseInt((String)di.getValue())) return i;
			}
		}
		return -1;
	}
	/**
	 * Liefert das WDataModels als String zur�ck.
	 * @return
	 */
  public String toString() {
  	String tab = "      ";
		StringBuffer filler = new StringBuffer(0);
    if (mHierarchyLevel > 0) {
		  filler = new StringBuffer(8*mHierarchyLevel);
		  for (int i=0;i<8*mHierarchyLevel;i++) { filler.append(' '); }
    }
    int size = getStringSize();
    StringBuffer sb = new StringBuffer(size);
		sb.append(filler);
		sb.append("\n<WDATAMODEL Modus=\"");
		sb.append(mModus);
		sb.append("\" CurrentRow=\"");
		sb.append(mCurRow);
		sb.append("\" RowCount=\"");
		sb.append(mRowCount);
		sb.append("\">\n");
		mSig.toString(sb, mHierarchyLevel);
		sb.append(filler);
		sb.append("  <WDATA>\n");
		int curRow=mCurRow;
		boolean aliasIsName=mSig.getAliasIsName();
		for (int i=0;i<mRowCount;i++) {
			sb.append(filler);
			sb.append("    <WDATARECORD>\n");
		  this.setRow(i);
		  int j = 0;
		  for (Enumeration e=this.getRow();e.hasMoreElements();) {
		   	String name = mSig.getAttributeName(j);
				Object o = e.nextElement();
		    if (o instanceof IWDataItem) {
					sb.append(filler);
					sb.append(tab);
		      ((IWDataItem)o).toString(sb, name);
		    } else {
					sb.append(filler);
					sb.append(tab);
		      sb.append("POS[");
					sb.append(i);
					sb.append("/");
					sb.append(j);
					sb.append("]: No IWDataItem found.\n");
		    }
			  j++;
		  }
			sb.append(filler);
			sb.append("    </WDATARECORD>\n");
		}
		sb.append(filler);
		sb.append("  </WDATA>\n");
		sb.append(filler);
		sb.append("</WDATAMODEL>\n");
		mCurRow = curRow;
		mSig.setAliasIsName(aliasIsName);
		return sb.toString();
  }  
  public int getStringSize(){
  	int colCount = mSig.cols();
  	int anzZeilen = 6+colCount+(mRowCount*(colCount+2));
  	int offsetForHierarchyLevel = 8*mHierarchyLevel*anzZeilen;
  	int offsetForDataModel = 100;
  	int offsetForSignature = 30;
		int offsetForData = 20;
		int offsetForDataRecord = 40*mRowCount;
  	int attributeStringSize = 120*colCount;
  	long dataItemStringSize = (65*colCount*mRowCount)+(mSig.mDataLength*mRowCount);
		long offsetForCrLf = (6+colCount+((colCount+2)*mRowCount))*2;
  	long size =  offsetForHierarchyLevel+
								offsetForDataModel +
								offsetForSignature +
								offsetForData +
								offsetForDataRecord +
								attributeStringSize +
								dataItemStringSize +
								offsetForCrLf;
		if (size > Integer.MAX_VALUE) {
			System.out.println("WDataModel max stringSize reached.");
			size = Integer.MAX_VALUE;
		} 
		return (int)size;
  }
  
  /**
   * Ermitteln, ob <em>genau ein</em> enthaltenes WDataItem als fehlerhaft gekennzeichnet ist.
   * @return true, falls genau ein enthaltenes WDataItem als fehlerhaft gekennzeichnet ist, sonst false.
   */
  public boolean hasExactlyOneErrorMark() {
	  int nrErrorMarks = 0;
	  for (Vector<IWDataItem> diVector : mData) {
		  for (IWDataItem dataItem : diVector) {
			  if (dataItem.getState() == IWDataItem.ERROR) {
				  nrErrorMarks++;
				  if (nrErrorMarks > 1){
					  return false;
				  }
			  }
		  }
	  }
	  return (nrErrorMarks == 1);
  }
  
  /**
   * Ermitteln, ob <em>mehr als ein</em> enthaltenes WDataItem als fehlerhaft gekennzeichnet ist.
   * @return true, falls mehr als ein enthaltenes WDataItem als fehlerhaft gekennzeichnet ist, sonst false.
   */
  public boolean hasMoreThanOneErrorMark() {
	  int nrErrorMarks = 0;
	  for (Vector<IWDataItem> diVector : mData) {
		  for (IWDataItem dataItem : diVector) {
			  if (dataItem.getState() == IWDataItem.ERROR) {
				  nrErrorMarks++;
			  }
		  }
	  }
	  return (nrErrorMarks > 1);
  }
  
} // End of class WDataModel 
