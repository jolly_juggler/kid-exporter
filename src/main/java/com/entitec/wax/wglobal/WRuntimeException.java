package com.entitec.wax.wglobal;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


//import com.entitec.wax.wcall.WSuccess;


public class WRuntimeException extends RuntimeException {

	private static final String TRENNSTRING = " ";

	private Log log = LogFactory.getLog(WRuntimeException.class);

	/**
	 * Diese Methode dient nur dazu, zu verhindern, dass die WRuntimeException ohne
	 * Parameter verwendet wird.
	 */
	private WRuntimeException() {
	}

	/**
	 * @see WRuntimeException#WRunTimeException(String, String, String, String, String);
	 */
	public WRuntimeException(String textNr) {
		this(textNr, null, null, null, null);
	}

	/**
		* @see WRuntimeException#WRunTimeException(String, String, String, String, String);
		*/
	public WRuntimeException(String textNr, String parm1) {
		this(textNr, parm1, null, null, null);
	}

	/**
	 * @see WRuntimeException#WRunTimeException(String, String, String, String, String);
	 */
	public WRuntimeException(String textNr, String parm1, String parm2) {
		this(textNr, parm1, parm2, null, null);
	}

	/**
	 * @see WRuntimeException#WRunTimeException(String, String, String, String, String);
	 */
	public WRuntimeException(String textNr, String parm1, String parm2, String parm3) {
		this(textNr, parm1, parm2, parm3, null);
	}

	/**
	 * Aus der Y07.properties wird zu der textNr der Fehlertext ermittelt.
	 * Platzhalter (&amp1 bis &amp4) werden durch param ersetzt. Und der 
	 * internen Fehlerliste hinzugef�gt.
	 * @param textNr	FehlerTextnummer
	 * @param parm1	Wert f�r ersten Platzhalter
	 * @param parm2 Wert f�r zweiten Platzhalter
	 * @param parm3 Wert f�r dritten Platzhalter
	 * @param parm4 Wert f�r vierten Platzhalter
		*/
	public WRuntimeException(String textNr, String parm1, String parm2, String parm3, String parm4) {
		/* Achtung: Da WAbstactModule Methoden mit der Reflection-API aufruft werden Exceptions in den 
		 * aufgerufenen Methoden als "InvocationTargetException" weitergereicht.
		 * Um aber nach aussen hin die urspr�ngliche Ursache zu pr�sentieren werden die 
		 * "InvocationTargetException" in WAbstractModule abgefangen und stattdessen wieder WRuntimeExcpetions
		 * (mit dem bereits expandierten Fehlertext der urspr�nglichen Exception) erzeugt.
		 * Dieser bereits expandierte Fehlertext l�uft auf der zweiten Ebene hier als "textNr" rein,
		 * mit der Folge, dass am Ende noch ein (�berfl�ssiger) TRENNSTRING angeh�ngt wird.
		 * Also nicht wundern ;-)
		 */
		// super(textNr + TRENNSTRING + WSuccess.getErrorText(textNr, parm1, parm2, parm3, parm4));
		super(textNr + TRENNSTRING );

		log.fatal(this.getMessage());
		log.error(this.getMessage()+"\n"+getStackTraceAsString(this));
	} 

	public WRuntimeException(Exception e) {
		super();
		log.fatal(e.toString());
		log.error(e.toString()+"\n"+getStackTraceAsString(e));
	}

	public WRuntimeException(Throwable cause) {
		super(cause);
	}

	private String getStackTraceAsString(Throwable cause) {
		StackTraceElement [] elements = cause.getStackTrace();
		StringBuffer buffer = new StringBuffer();
		for ( int i=0; i < elements.length; i++) {
			buffer.append(elements[i].toString()+"\n");
		}
		return buffer.toString();
	}

	private String getStackTraceAsString(Exception e) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream(4096);
		PrintStream ps = new PrintStream(baos);
		e.printStackTrace(ps);
		ps.flush();
		return new String(baos.toString());
	}
}
