package com.entitec.wax.wcall;

import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;

/**
 * Die Beschreibung des Typs hier eingeben.
 * @date: (20.07.2001 12:10:52)
 * @author: Ingo Pelkner
 */
public class WCallErrorList implements java.io.Serializable{
    private static final long serialVersionUID = 8145818674088307903L;
	private java.util.Vector<WCallError> m_errorList;
/**
 * WCallErrorList - Konstruktorkommentar.
 */
public WCallErrorList() {
	super();
	m_errorList = new java.util.Vector<WCallError>();
}


/**
 * Erstellung einer neuen Liste aus einer bestehenden.
 * <br>
 * Die Elemente k&ouml;nnen einfach aus der besthenden Liste &uuml;bernommen werden, da sie unver&auml;nderlich sind.
 * @param original Die bestehende Liste, deren Elemente in die neue Liste aufgenommen werden.
 */
public WCallErrorList(WCallErrorList original) {
	m_errorList = new Vector<WCallError>(original.m_errorList);
}

/**
 * Die Beschreibung der Methode hier eingeben.
 * @date: (20.07.2001 12:13:03)
 * @param error com.entitec.wax.wcall.WCallError
 */
public void add(WCallError error) {
	if ( error != null ){
		m_errorList.addElement(error);
	}
}


/**
 * Hinzuf&uuml;gen aller Elemente der &uuml;bergebenen Collection.
 */
public void addAll(Collection callErrors) {
	m_errorList.addAll(callErrors);
} // End of addAll


/**
 * Hinzuf&uuml;gen aller Elemente der &uuml;bergebenen WCallErrorList.
 */
public void addAll(WCallErrorList errorList) {
	m_errorList.addAll(errorList.m_errorList);
} // End of addAll


/**
 * Die Beschreibung der Methode hier eingeben.
 * @date: (20.07.2001 12:13:22)
 */
public void clear() {
	m_errorList.removeAllElements();
}


/**
 * Die Beschreibung der Methode hier eingeben.
 * @date: (20.07.2001 12:18:22)
 * @return java.util.Enumeration
 */
public java.util.Enumeration<WCallError> errors() {
	return m_errorList.elements();
}


/**
 * Die Beschreibung der Methode hier eingeben.
 * @date: (20.07.2001 12:18:22)
 * @return String[]
 */
public String[] getErrors() {
	String[] res = new String[m_errorList.size()];
	for(int i=0;i<res.length;i++) {
		WCallError e = m_errorList.elementAt(i);
		res[i] = e.getNumber() + ":" + e.getText();
	}
	return res;
}

public WCallError getError() {
	if (m_errorList.size() == 0) return null;
	return m_errorList.remove(0);
}

/**
 * Die Beschreibung der Methode hier eingeben.
 * @date: (20.07.2001 12:19:39)
 * @return boolean
 */
public boolean isEmpty() {
	return m_errorList.isEmpty();
}

/**
 * R&uuml;ckgabe der Anzahl der Eintr&auml;ge in der Liste.
 * @return Die Anzahl der Eintr&auml;ge in der Liste.
 */
public int size() {
	return m_errorList.size();
}

/**
 * <p>R�ckgabe eines Iterators f�r die interne Liste.</p>
 * @return boolean
 * @author TM0015
 */
public Iterator<WCallError> iterator() {
	return m_errorList.iterator();
} // End of iterator

/**
 * Erzeugung eines Strings mit allen (aufbereiteten) Fehlermeldungen.
 * <br>
 * Die Fehlermeldungen werden durch Zeilenwechsel voneinander abgetrennt.
 * @return Der erzeugte String. Falls keine Fehlermeldungen existieren ist der String leer.
 */
public String asFormattedString(){
	StringBuffer result = new StringBuffer(85); 
	for (WCallError error : m_errorList) {
		if (result.length() != 0) {
			result.append(System.getProperty("line.separator")); //$NON-NLS-1$
		}
		result.append(error.asFormattedString());
    }
	return result.toString();
}

/**
 * Die Beschreibung der Methode hier eingeben.
 * @date: (20.07.2001 13:37:34)
 * @return java.lang.String
 */
public String toString() {
	String ret = "<CALL_ERROR_LIST>";
	for ( int i = 0; i < m_errorList.size(); i++ ) {
		ret += "\n" + m_errorList.elementAt(i).toString();
	}
	ret += "\n</CALL_ERROR_LIST>";
	return ret;
}
}
