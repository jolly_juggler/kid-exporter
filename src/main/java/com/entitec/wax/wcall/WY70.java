package com.entitec.wax.wcall;

/**
Zuordnung von Textnummer zu Fehlertext
*/

public class WY70 extends WCascadeProperties {
	private static String rcsid="$Id: WY70.java 1545 2003-11-11 12:09:08Z sd0024 $";
	private static WY70 s = null;

	private WY70() {
		super ("y70.properties");
	}

	/** instanzieren der Klasse nach dem Singleton-Pattern */
	public static synchronized WY70 getInstance() {
		if ( s == null ) {
			s = new WY70();
		}
		return (s);
	}

	/** gibt den Fehlertext zu einer TextNummer zurueck. Ist zu der TextNummer
			kein Fehlertext vorhanden, so wird null zurueckgegeben. */
	public synchronized String getErrorText(String textNr) {
		return (getProperty(textNr));
	}
}
