package com.entitec.wax.wcall;

import com.entitec.wax.wdm.WDataModel;

/**
 * Schnittstelle zur Ausf�hrung von EP/KID Funktionalit�t.
 * 
 * �ber den <em>Communicator</em> werden die nachrichtenbasierten EP/KID Aufrufe verteilt und 
 * ausgef�hrt. Ort der Ausf�hrung bzw. durch welche Instanz die Ausf�hrung erfolgt
 * ist f�r den Aufrufer transparent.
 * 
 * @author SD0024
 *
 */
public interface IWCall {
	
WSuccess process(String function, WDataModel inview, WDataModel outview) throws WCallException;
}
