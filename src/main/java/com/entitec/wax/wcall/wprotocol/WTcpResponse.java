package com.entitec.wax.wcall.wprotocol;

import java.io.UnsupportedEncodingException;

import com.entitec.wax.wcall.WCallError;
import com.entitec.wax.wcall.WCallErrorList;
import com.entitec.wax.wdm.IWAttribute;
import com.entitec.wax.wdm.WDMTypChecker;
import com.entitec.wax.wdm.WDataModelWriter;
import com.entitec.wax.wglobal.WRuntimeException;


public class WTcpResponse {
	
	WDataModelWriter mwin = null;
	int minic = 0;
	int minir = -1;
	
	WDataModelWriter mwout = null;
	int moutic = 0;
	
	WCallErrorList merrors = null;
	
	String merfolg = "";
	String mcida = "";
	String mcide = "";
	
//	public WTcpResponse(WDataModelWriter in, WDataModelWriter out, WCallErrorList errors) {
//		this(in, out, errors, null);
//	}
	public WTcpResponse(WDataModelWriter in, WDataModelWriter out, 
			WCallErrorList errors) {
		mwout = out;
		mwin = in;
		merrors = errors;
	}

	public String getSuccess() {
		return (merfolg);
	}
	
	public String getCide(){ return(mcide);}
	public String getCida(){ return(mcida);}

	protected void set1(String modul, String fnktn, String cida, String cide) {
		mcida = cida;
		mcide = cide;
	}

	protected void set2(String abs, String emp, String erfolg, 
						String fehlerid, String textnummer, String textmessage) {
		
		if ( ("".equals(textnummer.trim()) && "".equals(textmessage.trim())) == false ) {
			// wenn textnummer oder erfolg ungleich leer
			WCallError e = new WCallError(textnummer, textmessage);
			merrors.add(e);
		}
		
		if ("".equals(merfolg)) merfolg = erfolg;
							
	}

	protected void addIn(
		String attribut,
		String typ,
		String len,
		String dec,
		String sign) {
	}

	protected void nextInRow() {
		minir++;
		minic = 0;
	}

	/** Setzt den Indikator im In-View */
	protected void addInValue(String ind) {
		if ( minic >= mwin.colCount() )
			throw new WRuntimeException("Fehler bei addInValue");
		mwin.setRow(minir);
		mwin.setState(mwin.getAttributeName(minic), WTcpToolkit.string2State(ind));
		minic ++;
	}

	protected void nextOutRow() {
		mwout.addRow();
		moutic = 0;
	}

	protected void addOutValue(String value1, String ind) throws UnsupportedEncodingException {
		String value = value1;
		
		if ( moutic >= mwout.colCount() )
			throw new WRuntimeException("Fehler bei addOutValue");
		String name = mwout.getAttributeName(moutic);
		IWAttribute a = mwout.getAttribute(name);
		if (a.getFrm() == IWAttribute.FRM_DEC){
			// korrektur von decimalwerte die als charakter transportiert wurden
			value = WDMTypChecker.trimCharRight(value);
		}
/*
		if ( "2".equals(ind)){
			throw new WRuntimeException ("K999", "gek�rztes Feld", 
					name+"=",value);
		}
*/
		mwout.setValue(name, value, WTcpToolkit.string2State(ind));
		moutic ++;
	}
}
