package com.entitec.wax.wcall.wprotocol;

import java.io.UnsupportedEncodingException;
import java.util.Enumeration;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.entitec.wax.wdm.IWAttribute;
import com.entitec.wax.wdm.IWSignature;
import com.entitec.wax.wdm.WDMTypChecker;
import com.entitec.wax.wdm.WDataModelReader;


public class WTcpRequest {
	private static Log log = LogFactory.getLog(WTcpRequest.class);	
	
	protected StringBuffer prmabsender = new StringBuffer(60);
	protected StringBuffer prm2 = new StringBuffer(50);

	protected StringBuffer indesc = new StringBuffer();
	protected int incount = 0;
	protected int indatalen = 0;
	private Vector inmap = new Vector();
	private Vector inlmap = new Vector();
	private int inmapi = 0;
	private int inlmapi = 0;
	
	protected Vector inind = new Vector();
	protected Vector indata = new Vector();

	protected StringBuffer outdesc = new StringBuffer();
	protected int outcount = 0;
	protected int outdatalen = 0;	
	
	private WDataModelReader mInR = null;
		
	public WTcpRequest(String methode, WDataModelReader in, WDataModelReader out){
		this(in, out);
		set1("WTCPCALL", methode.toUpperCase(),"1", "0");
		set2("RECEI","RECEI","","","","");
	}
	
	/**
	 * erzeugt einen Kid-Request basierend auf in und out View.
	 * Dezimalwerte werden als Charakter in den String übernommen.
	 */
	
	public WTcpRequest(WDataModelReader in, WDataModelReader out) {
		int is = 0;
		mInR = in;
		
		/* In-View */
		IWSignature s = in.getSignature();
		for ( is=0; is<s.cols(); is++ ) {
			IWAttribute a = s.getAttribute(s.getAttributeName(is));

			String name = a.getName();
			String format = ""+a.getFrm();
			int len = a.getLen();
			String dec = ""+a.getDec();
			String sign = (a.getSign())?"J":"N";

			if ( in.getAliasIsName() ) name = a.getAlias();

			if ( "D".equals(format) ) {
				// umwandeln in C, len + vorzeichen und seperator 
				// if ( "J".equals(sign) ) len += 2; else len += 1;
				len = 32;
				format = "C"; dec ="0"; sign = "N"; 
			}
			addIn(name, format, ""+len, dec, sign);	
		}

		for ( int ir=0; ir<in.rowCount(); ir++) {
			nextInRow();
			in.setRow(ir);
			for ( int ic=0; ic<in.colCount(); ic++ ) {
				String value = (String) in.getValue(in.getAttributeName(ic));
				short ind = in.getState(in.getAttributeName(ic));
				addInValue(value, WTcpToolkit.state2String(ind));
			}
		}
	
		/* Out-View */
		s = out.getSignature();
		for ( is=0; is<s.cols(); is++ ) {
			IWAttribute a = s.getAttribute(s.getAttributeName(is));

			String name = a.getName();
			String format = ""+a.getFrm();
			int len = a.getLen();
			String dec = ""+a.getDec();
			String sign = (a.getSign())?"J":"N";

			if ( in.getAliasIsName() ) name = a.getAlias();

			if ( "D".equals(format) ) {
				// if ( "J".equals(sign) ) len += 2; else len += 1;
				len = 32;
				format = "C"; dec ="0"; sign = "N"; 
			}
			addOut(name, format, ""+len, dec, sign);			

		}	

	}
	/** setzt den parameterbereich des kid-strings */	
	public void set1(String modul, String fnktn, String cida, String cide) {
		prmabsender.append( WTcpToolkit.fillRight(modul, ' ', 8) );
		prmabsender.append( WTcpToolkit.fillLeft(cida, '0', 8) );
		prmabsender.append("V001");
		prmabsender.append( WTcpToolkit.fillLeft(cide, '0', 8) );
		prmabsender.append( WTcpToolkit.fillRight(fnktn, ' ', 32) );
		
	}
	/** setzt den absenderbereich des kid-strings */
	public void set2(String abs, String emp, String erfolg, 
						String fehlerid, String textnummer, String textmessage) {
		// prm2.append( WTcpToolkit.fillRight(abs, ' ', 5) );
		// prm2.append( WTcpToolkit.fillRight(emp, ' ', 5) );
		prm2.append( WTcpToolkit.fillLeft(erfolg, '0', 2) );
		prm2.append( WTcpToolkit.fillLeft(fehlerid, '0', 8) );
		prm2.append( WTcpToolkit.fillRight(textnummer, ' ', 4) );
		prm2.append( WTcpToolkit.fillRight(textmessage, ' ', 80) );
	}

	private void addIn(String attribut, String typ, String len, String dec, String sign ) {
		indesc.append( WTcpToolkit.fillRight(attribut, ' ', 16) );
		indesc.append( WTcpToolkit.fillRight(typ, 'C', 1) );
		indesc.append( WTcpToolkit.fillLeft(len, '0', 4) );
		indesc.append( WTcpToolkit.fillLeft(dec, '0', 1) );
		indesc.append( WTcpToolkit.fillLeft(sign, 'N', 1) );
		incount ++;
		indatalen += Integer.parseInt(len);
		inmap.addElement(len);
		inlmap.addElement(typ);
	}
	
	private void nextInRow() {
		inmapi = 0; inlmapi = 0;

		StringBuffer data = new StringBuffer();
		StringBuffer ind = new StringBuffer(incount);
		
		indata.addElement(data);
		inind.addElement(ind);
	}

	private void addInValue(String value, String ind) {
		StringBuffer d = (StringBuffer) indata.lastElement();
		StringBuffer i = (StringBuffer) inind.lastElement();
						
		String typ = (String) inlmap.elementAt(inlmapi++);
		if ( "C".equals(typ) || "c".equals(typ)) {
			//bug#:475
			String name = mInR.getAttributeName(inmapi);
			IWAttribute a = mInR.getAttribute(name);
			if (a.getFrm() == IWAttribute.FRM_DEC){
				// korrektur von decimalwerte die als charakter transportiert wurden (pos sign entfernen)
				if ( value.endsWith("+") ){ 
					value = value.substring(0, value.lastIndexOf("+"));
				}
			}
			//xxxxx bug#:475
			d.append(WTcpToolkit.fillRight(
			WDMTypChecker.trimCharRight(value),
			' ', Integer.parseInt((String) inmap.elementAt(inmapi++))));
		} 
		else if ( "N".equals(typ) || "n".equals(typ) ) {
				d.append(WTcpToolkit.fillLeft(
				WDMTypChecker.trimNumericLeft(value), 
				'0', Integer.parseInt((String) inmap.elementAt(inmapi++))));
		}
		else {
			log.error("addInValue: unknow attribute typ ("+typ+")");
		}

		i.append(ind);
	}

	private void addOut(String attribut, String typ, String len, String dec, String sign ) {
		outdesc.append( WTcpToolkit.fillRight(attribut, ' ', 16) );
		outdesc.append( WTcpToolkit.fillRight(typ, 'C', 1) );
		outdesc.append( WTcpToolkit.fillLeft(len, '0', 4) );
		outdesc.append( WTcpToolkit.fillLeft(dec, '0', 1) );
		outdesc.append( WTcpToolkit.fillLeft(sign, 'N', 1) );
		outcount ++;
		outdatalen += Integer.parseInt(len);
	}

	private void nextOutRow() {;}

	
	/** gibt den Request als TCP-String zurueck 
	 * @throws UnsupportedEncodingException*/
	public String toTcpRequest() {
		Enumeration iind = inind.elements();
		Enumeration idat = indata.elements();

		String gesamt = "";

		do {
		String result = ""; 
		
		if (incount > 0) { // incount=anzahl der attribute 
			result +="                "
				+indesc.toString()
				+"                "
				+((StringBuffer)iind.nextElement()).toString()
				+WTcpToolkit.fillLeft(""+indatalen, '0', 8)
				+((StringBuffer)idat.nextElement()).toString();
		} else {
			result += "                "+"                "
				+WTcpToolkit.fillLeft(""+indatalen, '0', 8);

			// reparieren, falls nextRow ohne Add
			if ( iind.hasMoreElements() ) iind.nextElement();
			if ( idat.hasMoreElements() ) idat.nextElement();
		}

		result += WTcpToolkit.fillLeft(""+outcount, '0', 8);
		
		if (outcount > 0) {	
			result += "                "
				+outdesc.toString()
				+"                "
				+WTcpToolkit.fillLeft(""+outdatalen, '0', 8);			
		} else {
			result += "                "+"                "
				+WTcpToolkit.fillLeft(""+outdatalen, '0', 8);
		}

		if ( iind.hasMoreElements() ) {
			result = prmabsender.toString()
				+"SEND RECEI"+prm2.toString()
				+WTcpToolkit.fillLeft(""+incount, '0', 8)
				+result;
			gesamt += "1"+WTcpToolkit.fillLeft(""+result.length(), '0', 10)+result;
		} else {
			result = prmabsender.toString()
				+"RECEIRECEI"+prm2.toString()
				+WTcpToolkit.fillLeft(""+incount, '0', 8)
				+result;
			gesamt += "2"+WTcpToolkit.fillLeft(""+result.length(), '0', 10)+result;
		}
	} while (iind.hasMoreElements());
		if (log.isDebugEnabled()) log.debug("toTcpRequest:"+gesamt);

		return gesamt;
	}

	
}
