package com.entitec.wax.wcall.wprotocol;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Enumeration;
import java.util.Vector;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.entitec.wax.wdm.IWDataItem;
import com.entitec.wax.wglobal.WRuntimeException;

public class WTcpToolkit {
	private static String rcsid="$Id: WTcpToolkit.java 19068 2019-10-25 09:33:47Z hm0048 $";
	private static Log log = LogFactory.getLog(WTcpToolkit.class);	
		
	private static int READBUF_LEN = 32000;
	private static int MAX_REQUEST_LEN = 32000;

	public static String info() {
		return "$Id: WTcpToolkit.java 19068 2019-10-25 09:33:47Z hm0048 $";
	}
	
	public static void write(OutputStream os, String request, String encoding) throws IOException {
		if (log.isDebugEnabled()) log.debug("done: write");
		OutputStreamWriter osw = null;
		if (encoding == null || "".equals(encoding)) {
			osw = new OutputStreamWriter(os);
		} else {
			osw = new OutputStreamWriter(os, encoding);
		}
		BufferedWriter b = new BufferedWriter(osw, READBUF_LEN);
		b.write(request);
		b.flush();
		if (log.isDebugEnabled()) log.debug("write:"+request);
	}

	//** liest len Zeichen mittels Reader nach buf */
	private static void read2(BufferedReader r, char[] buf, int len) throws IOException { 
		if (log.isDebugEnabled()) log.debug("done: read");
		while ( r.ready() == false ) {
			try {
				Thread.sleep(1);
			} catch ( InterruptedException e ) {;}
		};
		int l = r.read(buf, 0, len);
		int lg = l;
		while ( l < len ) {
			while ( r.ready() == false ) {
				try {
					Thread.sleep(1);
				} catch ( InterruptedException e ) {;}
			}
			
			len = len-l;
			l =r.read(buf, l, len);
			lg+=l;
		}
		if (log.isDebugEnabled()) log.debug("read:"+new String(buf,0,lg));
	}

	private static void read_nok(BufferedReader r, char[] buf, int len) throws IOException {
		if (log.isDebugEnabled()) log.debug("read");
		int l = r.read(buf, 0, len);
		int lg = l;
		while ( l < len ) {
			len = len-l;
			l =r.read(buf, l, len);
			lg+=l;
		}
		if (log.isDebugEnabled()) log.debug("done read:"+new String(buf,0,lg));
	}
	
	private static void read(BufferedReader r, char[] buf, int len) throws IOException {
		if (log.isDebugEnabled()) log.debug("read");
		int lg = r.read(buf, 0, len);
		if ( lg == -1 ) throw new IOException("Der Server steht derzeit nicht zur Verfuegung!");
		int c = 0;
		while ( lg < len ) {
			try {
			lg += r.read(buf, lg, len-lg);
			} catch (IndexOutOfBoundsException e ) {
				throw new WRuntimeException("Protokollfehler: len="+len+", lg="+lg+", c="+c);
			}
			c++;
		}
		if (log.isDebugEnabled()) log.debug("done read:"+new String(buf,0,lg));
	}


	private static void read(String header, BufferedReader r, char[] buf, int len) throws IOException {
		if (header.startsWith("7") || header.startsWith("8")) {
			WTcpToolkit.read(r, buf, len);
			Inflater decompresser = new Inflater();
			byte[] result = new byte[MAX_REQUEST_LEN];
			byte[] input = new byte[len];
			for (int jj=0; jj<len; ++jj) input[jj] = (byte) buf[jj];
			decompresser.setInput(input, 0, len);
			int resultLength = -1;
			try {
				resultLength = decompresser.inflate(result);
				decompresser.end();
				for (int jj=0; jj<resultLength; ++jj) buf[jj] = (char) result[jj];
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		else read(r, buf, len);
	}


	private static void checkHeader(String header) throws WRuntimeException {
		if ( header.startsWith("2") || 
			header.startsWith("1") || 
			header.startsWith("5") || 
			header.startsWith("6") || 
			header.startsWith("7") || 
			header.startsWith("8") )
			return;
		else
			throw new WRuntimeException(" K600 - wrong header received ("+header+")");	
	}
	
	/** parsed den Response-Stream is und versorgt die Modell-Schnittstelle m */
	public static String parseTcpResponse( InputStream is, OutputStream os, WTcpResponse m, String encoding ) 
	throws IOException 
	{
	
		char inbuffer[] = new char[MAX_REQUEST_LEN];
		BufferedReader b = new BufferedReader(new InputStreamReader(is, encoding), READBUF_LEN);
		
		String header = WTcpToolkit.readHeader(b, inbuffer);
		checkHeader(header);		
		
		while ( header.startsWith("5") ) {
			os.write("6000".getBytes());
			os.flush();
			header = WTcpToolkit.readHeader(b, inbuffer);
			checkHeader(header);
		}
		int ii = 0;
		while ( header.startsWith("1") || header.startsWith("7") ){
			WTcpToolkit.read(header, b, inbuffer, Integer.parseInt(header.substring(1)));
			
			work(ii++, inbuffer, m);
			header = WTcpToolkit.readHeader(b, inbuffer);
			checkHeader(header);
			if ( header.startsWith("5") ) {
				os.write("6000".getBytes());
				os.flush();
				header = WTcpToolkit.readHeader(b, inbuffer);
				checkHeader(header);
			}
			
		}
	
		WTcpToolkit.read(header, b, inbuffer, Integer.parseInt(header.substring(1)));
		work(ii++, inbuffer, m);
		
		return (m.getSuccess());
	}
	
	public static boolean parseServerTcp( InputStream is, OutputStream os, IWTcpItem m ) throws IOException {
		
			char inbuffer[] = new char[MAX_REQUEST_LEN];
			BufferedReader b = new BufferedReader(new InputStreamReader(is), READBUF_LEN);
				
			String header = WTcpToolkit.readHeader(b, inbuffer);
			checkHeader(header);		
			
			while ( header.startsWith("5") ) {
				os.write("6000".getBytes());
				os.flush();
				return false;
			}
			int ii = 0;
			while ( header.startsWith("1") || header.startsWith("7")){
				WTcpToolkit.read(header, b, inbuffer, Integer.parseInt(header.substring(1)));
				
				m.parse(ii++, inbuffer);
				header = WTcpToolkit.readHeader(b, inbuffer);
				checkHeader(header);
				if ( header.startsWith("5") ) {
					os.write("6000".getBytes());
					os.flush();
					header = WTcpToolkit.readHeader(b, inbuffer);
					checkHeader(header);
				}
				
			}
		
			WTcpToolkit.read(header, b, inbuffer, Integer.parseInt(header.substring(1)));
			m.parse(ii++, inbuffer);
			return true;
	}
	
	public static void proxyTcpRequest( InputStream is, OutputStream os ) throws IOException {
		char inbuffer[] = new char[MAX_REQUEST_LEN];
		BufferedReader b = new BufferedReader(new InputStreamReader(is), READBUF_LEN);
		BufferedWriter w = new BufferedWriter(new OutputStreamWriter(os), READBUF_LEN);
	
		String header = WTcpToolkit.readHeader(b, inbuffer);		
		int len = Integer.parseInt(header.substring(1));

	
		int ii = 0;
		while ( header.startsWith("1") || header.startsWith("7") ){
			WTcpToolkit.read(header, b, inbuffer, len);

			if (log.isDebugEnabled()) log.debug("proxyTcpRequest:"+header+ new String(inbuffer, 0, len));
			w.write(header); w.write(inbuffer, 0, len);

			header = WTcpToolkit.readHeader(b, inbuffer);
			len = Integer.parseInt(header.substring(1));
		} 
		WTcpToolkit.read(header, b, inbuffer, len);

		if (log.isDebugEnabled()) log.debug("proxyTcpRequest:"+header+ new String(inbuffer, 0, len));
		w.write(header); w.write(inbuffer, 0, len);
		w.flush();
		
	}

	public static void proxyTcpResponse( OutputStream os, InputStream is, OutputStream os2 ) throws IOException {
		char inbuffer[] = new char[MAX_REQUEST_LEN];
		BufferedReader b = new BufferedReader(new InputStreamReader(is), READBUF_LEN);
		BufferedWriter w = new BufferedWriter(new OutputStreamWriter(os), READBUF_LEN);
	
		String header = WTcpToolkit.readHeader(b, inbuffer);		
		int len = Integer.parseInt(header.substring(1));

		while ( header.startsWith("5") ) {
			os2.write("6000".getBytes());
			os2.flush();
			header = WTcpToolkit.readHeader(b, inbuffer);
			len = Integer.parseInt(header.substring(1));
		}
		
		int ii = 0;
		while ( header.startsWith("1") || header.startsWith("7") ){
			WTcpToolkit.read(header, b, inbuffer, len);
			len = Integer.parseInt(header.substring(1));

			if (log.isDebugEnabled()) log.debug("proxyTcpResponse:"+header+ new String(inbuffer, 0, len));
			w.write(header); w.write(inbuffer, 0, len);

			header = WTcpToolkit.readHeader(b, inbuffer);
			len = Integer.parseInt(header.substring(1));
			if ( header.startsWith("5") ) {
				os2.write("6000".getBytes());
				os2.flush();
				header = WTcpToolkit.readHeader(b, inbuffer);
				len = Integer.parseInt(header.substring(1));
			}
	
		} 
		WTcpToolkit.read(header, b, inbuffer, len);

		if (log.isDebugEnabled()) log.debug("proxyTcpResponse:"+header+ new String(inbuffer, 0, len));
		w.write(header); w.write(inbuffer, 0, len);
		w.flush();
	}

	public static void answerTcpRequest( InputStream is, OutputStream os ) throws IOException {
		char inbuffer[] = new char[MAX_REQUEST_LEN];
		BufferedReader b = new BufferedReader(new InputStreamReader(is), READBUF_LEN);
		BufferedWriter w = new BufferedWriter(new OutputStreamWriter(os), READBUF_LEN);
	
		String header = WTcpToolkit.readHeader(b, inbuffer);		
		int len = Integer.parseInt(header.substring(1));
	
		int ii = 0;
		while ( header.startsWith("1") || header.startsWith("7") ){
			WTcpToolkit.read(header, b, inbuffer, len);

			header = WTcpToolkit.readHeader(b, inbuffer);
			len = Integer.parseInt(header.substring(1));
		} 
		WTcpToolkit.read(header, b, inbuffer, len);
try {
		// und ein wenig korrigieren
		StringBuffer buf = new StringBuffer(len);
		buf.append( inbuffer, 0, len);	
		int pos = 164; // abs+aufruf
		int t = Integer.parseInt(buf.substring(pos,pos+8));	// anz. inind
		pos += 8;
		buf.delete(pos, pos+t*23+32); // Indescription entfernen
		pos +=t; // inind ueberspringen
		t = Integer.parseInt(buf.substring(pos,pos+8));	// laenge indata 
		buf.delete(pos, pos+t+8); // laenge indata und indata entfernen 
		t = Integer.parseInt(buf.substring(pos,pos+8));	// anz. outind 
		pos +=8; // anz. outind ueberspringen
		pos += t*23+32; // outdesc ueberspringen
		buf.insert(pos, WTcpToolkit.fillLeft("", '0', t)); // outind hinzufuegen
		pos += t; // outind ueberspringen

		t = Integer.parseInt(buf.substring(pos,pos+8));	// laenge outdata 
		pos += 8; // laenge outdata ueberspringen
		buf.insert(pos, WTcpToolkit.fillRight("", ' ', t)); // Datenbereich anhaengen 
		pos += t;

		buf.delete(65,70); buf.insert(65,"FREE ");
		buf.delete(70,72); buf.insert(70,"99");
		buf.delete(80,84); buf.insert(80,"K639");
		buf.delete(84,115); buf.insert(84,"Verschluesslung kleiner 128 Bit");

		header ="2"+WTcpToolkit.fillLeft(""+pos, '0', 10);
		w.write(header+buf.substring(0,pos));
		w.flush();
} catch (Exception e) {
		log.error(e.toString());
}
		
	}
	private static String readHeader(BufferedReader b, char[] inbuffer) throws IOException {
		
		WTcpToolkit.read(b, inbuffer, 1);
		String top = new String(inbuffer,0,1);

		String header = "";
		if ( "2".equals(top) || "1".equals(top) || "7".equals(top)  || "8".equals(top) ) {
			WTcpToolkit.read(b, inbuffer, 10);
			header = new String(inbuffer,0,10);
		}
		else {
				WTcpToolkit.read(b, inbuffer, 3);
				header = new String(inbuffer,0,3);
		}

		header = top+header;
		return (header);
	}

	/** interne Hilfsfunktion, wird fuer jeden KID-Satz aufgerufen */
	protected static void work( int idx, char[] inbuffer, WTcpResponse m ) throws IOException {

		int p = 0;

		if ( idx == 0) {
		m.set1(new String(inbuffer,0,8), new String(inbuffer, 28, 32),
				new String(inbuffer, 8,8), new String(inbuffer, 20, 8) );
		}

		p = 60;
		if (( idx == 0 )
		 || (idx > 0 && inbuffer[p+20] != ' ')) {
		m.set2(new String(inbuffer,p,5), new String(inbuffer,p+5,5) , 
			new String(inbuffer, p+10,2), new String(inbuffer,p+12,8), 
			new String(inbuffer, p+20,4), new String(inbuffer,p+24,80) );
		}
		p = 164;	
		int anzInInd;
		try {
			anzInInd = Integer.parseInt(new String(inbuffer, p,8));
		} catch (NumberFormatException e){
			throw new WRuntimeException(new String(inbuffer, 80,92));
		}
		p += 8;
		
		String inInd = new String(inbuffer, p, anzInInd);
		p += anzInInd;

		if ( idx == 0 ) {
			// nur beim ersten in-satz werden die indikatoren gesetzt	
			m.nextInRow();
			for ( int ii = 0; ii < anzInInd; ii++) {
				m.addInValue(""+inInd.charAt(ii));
			}
		}

		int anzOutInd;
		try { 
			anzOutInd = Integer.parseInt(new String(inbuffer, p,8));
		} catch (NumberFormatException e){
			throw new WRuntimeException(new String(inbuffer, p,8));
		}
		p += 8;
		p += 16;
		// traversieren der Out-Description:
		Vector vo = new Vector();
		for (int i=0; i<anzOutInd; i++) {
			vo.addElement(new String(inbuffer,p+17,4));
/*
			if ( idx == 0 ) {
			m.addOut (new String(inbuffer,p,16), new String(inbuffer,p+16, 1), 
				new String(inbuffer,p+17,4), new String(inbuffer,p+21,1), 
				new String(inbuffer,p+22,1));
			}
*/
			p += 23; 
		}
		p += 16;

		m.nextOutRow();
		
		// out-indikatoren
		String outInd = new String(inbuffer, p, anzOutInd);
		p += anzOutInd;
		p += 8;

		// out-datenbereich		
		Enumeration voi = vo.elements();
		int ii = 0;
		while (voi.hasMoreElements()) {
			int l=Integer.parseInt((String) voi.nextElement());
			String value = new String(inbuffer, p, l);
			String ind = ""+outInd.charAt(ii++);
			
			m.addOutValue( value, ind);
			p += l;
		}
	}

	/** auffuellen von links mit c, maximal n mal */
	public static String fillLeft(String a, char c, int n) {
		StringBuffer r = new StringBuffer(n);
		for ( int i = 0; i < (n-a.length()); i++) r.append(c);
		return r.toString()+a;
	}
	
	/** auffuellen von rechts mit c, maximal n mal */
	public static String fillRight(String a, char c, int n) {
		
		StringBuffer r = new StringBuffer(n);
		if (a.length() > n) { 
			r.append(a.substring(0,n));
		}
		else {
			r.append(a);
			for ( int i = 0; i < (n-a.length()); i++) r.append(c);
		}
		return r.toString();
	}
	
	public static String state2String(short state) {
		if (state == IWDataItem.DISABLED) return ("0");
		else if (state == IWDataItem.ENABLED) return ("1");
		else if (state == IWDataItem.TRUNCATED) return ("2");
		else if (state == IWDataItem.ERROR) return ("3");
		else if (state == IWDataItem.UNCHANGED) return ("4");
		else if (state == IWDataItem.INVALID) return ("9");
		else return ("9");
	}
			
	public static short string2State(String s) {
		if ("0".equals(s)) return (IWDataItem.DISABLED);
		else if ("1".equals(s) )return (IWDataItem.ENABLED);
		else if ("2".equals(s)) return (IWDataItem.TRUNCATED);
		else if ("3".equals(s)) return (IWDataItem.ERROR);
		else if ("4".equals(s)) return (IWDataItem.UNCHANGED);
		else if ("9".equals(s)) return (IWDataItem.INVALID);
		else return (IWDataItem.INVALID);
	}		

}
