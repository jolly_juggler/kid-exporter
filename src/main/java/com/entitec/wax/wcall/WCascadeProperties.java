/*
 * Created on 11.11.2003
 */
package com.entitec.wax.wcall;

import java.io.InputStream;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 * @author SD0024, AB00434
 * 
 * wax.jar -> properties/wax-%name%
 * waxadmin.jar -> properties/admin-%name%
 * waxwmodule.jar -> properties/wmodule-%name%
 * wax%project%.jar -> properties/%project%-%name%
 * 
 * wax-1.0.2.5.jar -> properties/wax-%name%
 * Erweiterung:
 * Komponenten, die nicht als jar ausgliefert werden (svcclasses), 
 * sondern als Classes-Verzeichnis, werden nach properties durchsucht.
 */
public class WCascadeProperties {
	
	private static String classSeperator = null;
	private static String userClassPath = null;
	private static String constJAR = ".jar";
	private static String constCLASSES = "classes";
	private static String constSUFFIX = "wax";

	private Vector waxComponentsProperties = null;
	
	public WCascadeProperties(String name){
		classSeperator = System.getProperty("path.separator");
		if ( userClassPath == null ) {
			try {
				userClassPath = System.getProperty("java.class.path");
			} catch (Exception e) {
				e.printStackTrace();
				userClassPath = ".";
			}
		}
		waxComponentsProperties = new Vector();
		loadDefaultProperties(name);
		loadClassesProperties(name);
		loadJarProperties(name);
	}
	final static public void setProjects(String [] projects) {
		final String sep = System.getProperty("path.separator");
		final StringBuffer b = new StringBuffer();
		
		for ( int i=0; i < projects.length; i++ ) {
			if ( i == 0 ) {
				b.append(projects[i]);
				b.append(constJAR);
			} else {
				b.append(sep);
				b.append(projects[i]);
				b.append(constJAR);
			}
		}
		userClassPath = b.toString();
	}
	
	public String getProperty(String key) {
		Iterator i = waxComponentsProperties.iterator();
		Properties p;
		String value;
		while (i.hasNext()){
			p = (Properties) i.next();
			value = p.getProperty(key);
			if (value != null ) return value;
		}
		return null;
	}
	
	public boolean containsKey(String key) {
		Iterator i = waxComponentsProperties.iterator();
		Properties p;
		boolean value;
		while (i.hasNext()){
			p = (Properties) i.next();
			value = p.containsKey(key);
			if (value == true ) return true;
		}
		return false;
	}
		
	public Enumeration propertyNames(){
		Vector v = new Vector();
		Iterator i = waxComponentsProperties.iterator();
		Properties p;
		String value;
		while (i.hasNext()){
			p = (Properties) i.next();
			v.addAll(p.keySet());
		}
		return v.elements();
	}
	private void loadDefaultProperties(String name) {
		/* laden der 'normalen' properties, z.B.: y02.properties */
		try {
			InputStream is = getClass().getResourceAsStream("/properties/"+name); 
			if (is != null) {
				Properties p = new Properties();
				p.load(is);
				waxComponentsProperties.add(p);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private void loadJarProperties(String name) {
		/* laden der 'normalen' properties, z.B.: y02.properties */
		try {
			/* laden der 'kaskadierten' properties z.B.: wmodule-y02.properties, admin-y02.properties */
			int i; String s;
			final StringTokenizer st = new StringTokenizer(userClassPath, classSeperator);
			while (st.hasMoreTokens()) {
				s = st.nextToken().toLowerCase();
				if (s.endsWith(constJAR) == false ) continue;
		
				i = s.lastIndexOf('/');
				if ( i == -1 ) i = s.lastIndexOf('\\');
				s = s.substring(i+1);
				if (s.startsWith(constSUFFIX) == false ) continue;
				s = s.replaceFirst(constSUFFIX,"");
				s = s.replaceAll(constJAR, "");
				/* moegliche Version entfernen */
				i = s.lastIndexOf('-');
				if ( i >= 0) s = s.substring(0, i);
				if ("".equals(s)) s = "wax";
				
				String cascadeProperty = "/properties/"+s+"-"+name;
				// waxwmodule.jar -> e.g. /properties/wmodule-y70.properties
				InputStream is = getClass().getResourceAsStream(cascadeProperty); 
				if (is != null) {
					Properties p = new Properties();
					p.load(is);
					waxComponentsProperties.add(p);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private void loadClassesProperties(String name) {
		/* laden der 'normalen' properties, z.B.: y02.properties */
		try {
			/* laden der 'kaskadierten' properties z.B.: svc-y02.properties */
			int i; String s;
			final StringTokenizer st = new StringTokenizer(userClassPath, classSeperator);
			while (st.hasMoreTokens()) {
				s = st.nextToken().toLowerCase();
				if (s.endsWith(constCLASSES) == false ) continue;
		
				i = s.lastIndexOf('/');
				if ( i == -1 ) i = s.lastIndexOf('\\');
				s = s.substring(i+1);
				if (s.endsWith(constCLASSES) == false ) continue;
				s = s.replaceFirst(constCLASSES,"");
				if ("".equals(s)) continue;
				
				String cascadeProperty = "/properties/"+s+"-"+name;
				// svcclasses -> e.g. /properties/svc-y02.properties
				InputStream is = getClass().getResourceAsStream(cascadeProperty); 
				if (is != null) {
					Properties p = new Properties();
					p.load(is);
					waxComponentsProperties.add(p);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
