package com.entitec.wax.wcall;

/**
Zuordnung von Funktionsnamen zu Java-Klassen
*/

public class WY02 extends WCascadeProperties {
	private static String rcsid="$Id: WY02.java 1545 2003-11-11 12:09:08Z sd0024 $";
	private static WY02 s = null;

	private WY02() {
		super ("y02.properties");
	}

	/** instanzieren der Klasse nach dem Singleton-Pattern */
	public static synchronized WY02 getInstance() {
		if ( s == null ) {
			s = new WY02();
		}
		return (s);
	}

	/** gibt den Klassennamen zu einer Funktion zurueck. Ist zu der Funktion
			keine Klasse vorhanden, so wird null zurueckgegeben. */
	public synchronized String getClassname(String fnktn) {
		return (getProperty(fnktn));
	}
}
