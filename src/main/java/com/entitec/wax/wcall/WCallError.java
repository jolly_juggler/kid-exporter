package com.entitec.wax.wcall;

/**
 * Die Beschreibung des Typs hier eingeben.
 * @date: (20.07.2001 12:05:08)
 * @author: Ingo Pelkner
 */
public class WCallError implements java.io.Serializable{
	private java.lang.String m_number = "";
	private java.lang.String m_text = "";
/**
 * WCallError - Konstruktorkommentar.
 */
public WCallError(String number, String text) {
	super();
	if ( number != null ) {
		m_number = number;
	}
	if ( text != null ) {
		m_text = text;
	}
}
/**
 * Die Beschreibung der Methode hier eingeben.
 * @date: (20.07.2001 12:06:05)
 * @return java.lang.String
 */
public String getNumber() {
	return m_number;
}
/**
 * Die Beschreibung der Methode hier eingeben.
 * @date: (20.07.2001 12:05:34)
 * @return java.lang.String
 */
public String getText() {
	return m_text;
}


/**
 * Erzeugung eines (aufbereiteten) Strings f&uuml;r die Fehlermeldung.
 * @return Der erzeugte String.
 */
public String asFormattedString(){
	StringBuffer result = new StringBuffer(85); 
    result.append(m_number);
    result.append(": "); //$NON-NLS-1$
    result.append(m_text.trim());
	return result.toString();
}

/**
 * Die Beschreibung der Methode hier eingeben.
 * @date: (20.07.2001 13:34:19)
 */
public String toString() {
	return ("<call_error number=\"" + m_number + "\">" + m_text + "</call_error>");
}
}
