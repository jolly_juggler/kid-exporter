package com.entitec.wax.wcall;

import java.util.Enumeration;


/** 
 * Erfolgsmeldung<p>
 * Bestehend aus Erfolgsnummer ("99") und m�glicherweise mehreren
 * Fehlertexten/Meldungen.<p>
 * 
 * Jeder Fehlertext/Meldung besteht aus Textnummer ("K001") und
 * Text ("Parameter nicht vorhanden").<p>
 * 
 * Bei Fehlertexten wird der Text zu der Textnummer �ber die Y70.properties
 * ermittelt. Dabei werden auch Platzhalter (&amp1 .. &amp2) unterst�tzt.<p>
 *
 * Bei Meldungen findet keine Umschl�sselung �ber die Y70.properites statt.<p>
 * 
 */

public class WSuccess implements java.io.Serializable {
	 /**
	  * Gibt den Eingabestring mit verdoppelten Backslashes zur�ck.
	  */
	 private static String withDoubledBackslashes(String repl) {
	
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < repl.length(); i++) {
			sb.append(repl.charAt(i));
			if (repl.charAt(i) == '\\') {
				sb.append("\\");
			}
		} // for whole array 
	
		return sb.toString();
	 }

	public final static String WRK_OK = "00";
	public final static String WRK_NEUER_SPEICHER = "01";
	public final static String WRK_KEINE_FELDER = "01";
	public final static String WRK_KEIN_SPEICHERPLATZ = "02";
	public final static String WRK_GEKUERZTE_FELDER = "02";
	public final static String WRK_FORMAT_FEHLER = "03";
	public final static String WRK_WARNUNG = "04";
	public final static String WRK_LETZTER_SATZ = "05";
	public final static String WRK_QUEUE_LEER = "06";
	public final static String WRK_NOT_OK = "07";
	public final static String WRK_GESPERRT = "08";
	public final static String WRK_NICHT_GESPERRT = "09";
	public final static String WRK_FACHLICHER_FEHLER = "10";
	public final static String WRK_GELOESCHT = "11";
	public final static String WRK_SATZ_GELOESCHT = "12";
	public final static String WRK_NICHT_GEFUNDEN = "13";
	public final static String WRK_NICHT_VORHANDEN = "14";
	public final static String WRK_NICHT_ZULAESSIG = "15";
	public final static String WRK_VORHANDEN = "16";
	public final static String WRK_EINE_REGEL = "17";
	public final static String WRK_N_REGELN = "18";
	public final static String WRK_SATZ_INAKTIV = "19";
	public final static String WRK_OK_BESTAETIGEN = "20";
	public final static String WRK_GUELTIG_AB_FALSCH = "21";
	public final static String WRK_GUELTIG_BIS_FALSCH = "22";
	public final static String WRK_GUELTIG_AB_BIS_FALSCH = "23";
	public final static String WRK_OK_GUELTIG_BIS_SPERRE = "24";
	public final static String WRK_OK_KEINE_AENDERUNG = "25";
	public final static String WRK_AENDERN = "30";
	public final static String WRK_AENDERN_UND_DUPLIZIEREN = "31";
	public final static String WRK_SATZ_SCHREIBEN = "32";
	public final static String WRK_AV_GILT = "33";
	public final static String WRK_AV_GILT_NICHT = "34";
	public final static String WRK_KEINE_AV = "35";
	public final static String WRK_LETZTE_AV = "36";
	public final static String WRK_DATEN_IN_ZUKUNFT = "37";
	public final static String WRK_BEREITS_GEAENDERT = "38";
	public final static String WRK_SATZ_UNVOLLSTAENDIG = "40";
	public final static String WRK_NICHT_RELEVANT = "96";
	public final static String WRK_ANY_ERROR = "97";
	public final static String WRK_SECURITY_ERROR = "98";
	public final static String WRK_TECHN_FEHLER = "99";
	private String mSuccess = "00";
	private WCallErrorList mErrorList = new WCallErrorList(); 
  /** Erzeugt Erfolgsmeldung mit Erfolgsnummer.
   * 
   * @param success	Erfolgsnummer
   */
  public WSuccess(String success) {
  	mSuccess = success;
  }
  
/**
 * Erzeugt Erfolgsmeldung mit Erfolgsnummer und bestehender Fehlerliste
 * @param success	Erfolgsnummer
 * @param el	Fehlerliste
 */  
  public WSuccess(String success, WCallErrorList el) {
		mSuccess = success;
		mErrorList = el;
  }

  /**
   * Erzeugt Erfolgsmeldung mit Erfolgnummer und Fehlertext
   * @param success	Erfolgsnummer
   * @param textNr	Fehlertext
   */
  public WSuccess(String success, String textNr) {
  	this(success, textNr, null, null, null, null);
  }
  
  /**
   * Erzeugt Erfolgsmeldung mit Erfolgnummer, Fehlertext und Parameter 1
   * @param success	Erfolgsnummer
   * @param textNr	Fehlertext
   * @param param1	Parameter1
   */
  public WSuccess(String success, String textNr, String param1) {
	mSuccess = success;
	if (textNr != null) {
		this.addError(textNr, param1, null, null, null);
	}
  }
  
  /**
   * Erzeugt Erfolgsmeldung mit Erfolgnummer, Fehlertext und den Parametern 1 und 2
   * @param success	Erfolgsnummer
   * @param textNr	Fehlertext
   * @param param1	Parameter1
   * @param param2	Parameter2
   */
  public WSuccess(String success, String textNr, String param1, String param2) {
	mSuccess = success;
	if (textNr != null) {
		this.addError(textNr, param1, param2, null, null);
	}
  }
  
  /**
   * Erzeugt Erfolgsmeldung mit Erfolgnummer, Fehlertext und den Parametern 1 bis 3
   * @param success	Erfolgsnummer
   * @param textNr	Fehlertext
   * @param param1	Parameter1
   * @param param2	Parameter2
   * @param param3	Parameter3
   */
  public WSuccess(String success, String textNr, String param1, String param2, String param3) {
	mSuccess = success;
	if (textNr != null) {
		this.addError(textNr, param1, param2, param3, null);
	}
  }
  
  
  /**
   * Erzeugt Erfolgsmeldung mit Erfolgnummer, Fehlertext und den Parametern 1 bis 4
   * @param success	Erfolgsnummer
   * @param textNr	Fehlertext
   * @param param1	Parameter1
   * @param param2	Parameter2
   * @param param3	Parameter3
   * @param param4	Parameter4
   */
  public WSuccess(String success, String textNr, String param1, String param2, String param3, String param4) {
	mSuccess = success;
	if (textNr != null) {
		this.addError(textNr, param1, param2, param3, param4);
	}
  }
  
  /** 
   * F�gt einen Fehlertext hinzu.
   * Aus der Y07.properties wird zu der textNr der Fehlertext ermittelt.
   * Platzhalter (&amp1 bis &amp4) werden durch param ersetzt. Und der 
   * internen Fehlerliste hinzugef�gt.
   * 
   * @param textNr	FehlerTextnummer
   * @param param1	Wert f�r ersten Platzhalter
   * @param param2  Wert f�r zweiten Platzhalter
   * @param param3  Wert f�r dritten Platzhalter
   * @param param4  Wert f�r vierten Platzhalter
   */
  public void addError(String textNr, String param1, String param2, String param3, String param4) {
	if (textNr != null) {
		String text = WSuccess.getErrorText(textNr, param1, param2, param3, param4);
		if (mErrorList == null) {
			mErrorList = new WCallErrorList();
		}
		mErrorList.add(new WCallError(textNr, text));
	}
  }
  
  /**
   * F�gt einen Fehlertext hinzu.
   * @see com.entitec.wax.wcall#addError(String, String, String, String, String)
   * @param textNr	FehlerTextnummer
   * @param param1	Wert f�r ersten Platzhalter
   * @param param2  Wert f�r zweiten Platzhalter
   * @param param3  Wert f�r dritten Platzhalter
   */
  public void addError(String textNr, String param1, String param2, String param3) {
	this.addError(textNr, param1, param2, param3, null);
  }
  
  /**
   * F�gt einen Fehlertext hinzu.
   * @see com.entitec.wax.wcall#addError(String, String, String, String, String)
   * @param textNr	FehlerTextnummer
   * @param param1	Wert f�r ersten Platzhalter
   * @param param2  Wert f�r zweiten Platzhalter
   */
  public void addError(String textNr, String param1, String param2) {
	this.addError(textNr, param1, param2, null, null);
  }
  
  /**
   * F�gt einen Fehlertext hinzu.
   * @see com.entitec.wax.wcall#addError(String, String, String, String, String)
   * @param textNr	FehlerTextnummer
   * @param param1	Wert f�r ersten Platzhalter
   */
  public void addError(String textNr, String param1) {
	this.addError(textNr, param1, null, null, null);
  }
  
  /**
   * F�gt einen Fehlertext hinzu.
   * @see com.entitec.wax.wcall#addError(String, String, String, String, String)
   * @param textNr	FehlerTextnummer
   * @param param1	Wert f�r ersten Platzhalter
   */
  public void addError(String textNr) {
  	this.addError(textNr, null, null, null, null);
  }
  
  /**
   * �bernimmt eine Kopie der Fehlertexte aus einer bestehenden Erfolgsmeldung.
   * (SAVE-MSG)
   * @param succ 
   */
  public void addError(WSuccess succ) {
  	if (succ != null) {
  		WCallErrorList l = succ.getErrorList();
  		Enumeration e = l.errors();
  		while (e.hasMoreElements()){
  			WCallError a = (WCallError) e.nextElement();
  			addMessage(a.getNumber(), a.getText());
  		}
  	}
  }

/**
 * F�gt eine Meldung hinzu. Eine Meldung wird nicht �ber die 
 * Y70.properties aufgel�st.
 * @param msgNr	Nr. z.B. "K001"
 * @param msgText	"Paramter GFKNR ist nicht gesetzt"
 */
public void addMessage(String msgNr, String msgText) {
	mErrorList.add(new WCallError(msgNr, msgText));
}

/**
 * Erfolgsnummer ermitteln
 * 
 * @return	Erfolgsnummer, z.B. "14"
 */
public String getSuccess() {
  	return mSuccess;
}
/**
 * Fehlerliste bestehend aus textNr und text ermitteln 
 * @return WCallErrorList
 */
public WCallErrorList getErrorList() {
  	return mErrorList;
  }
  
/**
 * @deprecated 
 * @param errorList
 */
public void addErrorList(WCallErrorList errorList) {
  	if (!errorList.isEmpty()) {
  		if (mErrorList == null) {
  			mErrorList = new WCallErrorList();
  		}
  		for(Enumeration e=errorList.errors();e.hasMoreElements();) {
  			WCallError ce = (WCallError)e.nextElement();
  			mErrorList.add(ce);
  		}
  	}
}
  
  /** 
	* Fehlertext erzeugen.
	* Ermittelt anhand der textNr aus der Y07.properties
	* den Fehlertext. M�gliche Platzhalter 
	* (&amp1 bis &amp2) werden durch param ersetzt. Sollte
	* der Fehlertext nicht in den Y07.properties vorhanden sein,
	* so wird der Fehlertext "" zur�ckgegeben.
	* 
	* @param textNr Textnummer, z.B.: "K001"
	* @return Fehlertext zur Textnummer aus den Y07.properties.
	*/
	 public static synchronized String getErrorText(String textNr) {
	   String text = WY70.getInstance().getErrorText(textNr);
		 if (text != null) {
			 return text;
		 } else {
			 return "";
		 }
	 }
	 
  /** 
   * Fehlertext erzeugen.
   * Ermittelt anhand der textNr aus der Y07.properties
   * den Fehlertext. M�gliche Platzhalter 
   * (&amp1 bis &amp2) werden durch param ersetzt. Sollte
   * der Fehlertext nicht in den Y07.properties vorhanden sein,
   * so wird der Fehlertext "Fehlertext unbekannt." zur�ckgegeben.
   * 
   * @param textNr Textnummer, z.B.: "K001"
   * @param parm1  Wert f�r ersten Platzhalter (&amp1)
   * @param parm2  Wert f�r zweiten Platzhalter (&amp2)
   * @param parm3  Wert f�r dritten Platzhalter (&amp3)
   * @param parm4  Wert f�r dritten Platzhalter (&amp4)
   * @return Fehlertext zur Textnummer.
   */
	public static synchronized String getErrorText(String textNr, 
													String parm1, 
													String parm2, 
	                                                String parm3, 
	                                                String parm4) {
		String text;
		if (textNr != null) {
				// Hier war mal eine Pr�fung auf "richtige" TextNummern,
				// F�r diese gibt es aber keine solch stringenten Konventionen (1 Buchstabe und 3 Ziffern)
				// Daher ist die Pr�fung hier nicht durchzuf�hren
				// die aufgerufene Methode getErrorText(textNr) versucht �ber die Properties einen Text zu finden und liefert sonst "" zur�ck
				// Das reicht vollkommen aus  
				text = getErrorText(textNr);
				if (parm1 != null) {
					String repl = withDoubledBackslashes(parm1);
					text = text.replaceAll("&1", repl);
				}
				if (parm2 != null) {
					String repl = withDoubledBackslashes(parm2);
					text = text.replaceAll("&2", repl);
				}
				if (parm3 != null) {
					String repl = withDoubledBackslashes(parm3);
					text = text.replaceAll("&3", repl);
				}
				if (parm4 != null){
					String repl = withDoubledBackslashes(parm4);
					text = text.replaceAll("&4", repl);
				}
		} else {
			text = "Kein Text vorhanden";
		}
		return text;
	}
	/**
	 * R�ckgabe des WSuccess als String.
	 * @return
	 */
	public String toString() {
		String s = "";
		if (this.mErrorList.isEmpty()) {
			s = "<WSUCCESS Erfolg=\"" + mSuccess + "\"/>\n";
		} else {
			s = "<WSUCCESS Erfolg=\"" + mSuccess + "\">\n";
			s = s + mErrorList + "\n";
			s = s + "<WSUCCESS/>\n";
		}
		return s;
	}  
	
	/**
	 * Setzen des Erfolgs
	 * @param string
	 */
	public void setSuccess(String string) {
		mSuccess = string;
	}

}
