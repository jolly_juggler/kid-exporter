package com.entitec.jepkid;

import com.entitec.jepkid.communicator.Communicator;
import com.entitec.jepkid.communicator.CommunicatorFactory;
import com.entitec.jepkid.communicator.ServiceInvocationHandler;
import com.entitec.jepkid.communicator.Token;
import com.entitec.jepkid.server.LoginHandler;
import com.entitec.jepkid.server.Server;

public interface CoreFactory {

	public CommunicatorFactory getCommunicatorFactory();

	public Token createToken(String user, String group, String password,
			String mandt);

	public LoginHandler createThreadBasedLoginHandler();

	public Server createEPKIDTcpServer(int listenport,
			final Communicator communicator, final LoginHandler loginHandler);

	public ServiceInvocationHandler createGenericServiceFactory(
			Communicator communicator);

}