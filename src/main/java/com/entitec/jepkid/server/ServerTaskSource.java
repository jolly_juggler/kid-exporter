package com.entitec.jepkid.server;

import java.net.Socket;

public interface ServerTaskSource {
	
	Runnable getServerTask(Socket soc);

}
