package com.entitec.jepkid.server;

import com.entitec.jepkid.communicator.CommunicationSource;
import com.entitec.jepkid.communicator.Communicator;
import com.entitec.jepkid.communicator.Token;
import com.entitec.jepkid.communicator.TokenFinder;


/**
 * Threadbasierte Implementierung des LoginHandler.
 * 
 * Beim login() wird das Token in den ThreadLocal Storage des aktuellen
 * Threads eingestellt.
 * 
 * Beim logoff() wird das Token des aktuellen Threads entfernt und f�r 
 * ung�ltig erkl�rt.
 * 
 * @author sd0024
 *
 */
public class UserTokenHandlerImpl implements LoginHandler, TokenFinder {

	protected class ThreadLocalItem {
		Token t = null;
	}
	
	private ThreadLocal<ThreadLocalItem> threadLocal = 
		new ThreadLocal<ThreadLocalItem>();
	
	ThreadLocalItem threadLocalItem = new ThreadLocalItem();
	
	public boolean login(Token epkidLoginToken) {
		threadLocal.remove();
		ThreadLocalItem item = new ThreadLocalItem();
		item.t = epkidLoginToken;
		threadLocal.set(item);
		return false;
	}

	public Token getToken() {
		return threadLocal.get().t;
	}

	public void logoff() {
		threadLocal.get().t.invalidate();
		threadLocal.remove();
	}
	
}
