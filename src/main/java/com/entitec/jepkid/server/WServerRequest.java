/*
 * Created on 18.04.2005
 *
 */
package com.entitec.jepkid.server;

import java.io.IOException;
import java.util.Enumeration;

import com.entitec.wax.wdm.IWAttribute;
import com.entitec.wax.wdm.IWDataItem;
import com.entitec.wax.wdm.WAttribute;
import com.entitec.wax.wdm.WDMTypChecker;
import com.entitec.wax.wdm.WDataModel;
import com.entitec.wax.wdm.WDataModelWriter;

/**
 * @author SD0024
 */
public class WServerRequest implements com.entitec.wax.wcall.wprotocol.IWTcpItem {
	
	private String header = null;
	private String function = null;
	private String prmArea = null;
	private String inInd = null;
	
	private int insCount;
	private int insDataLen;
	private int outsCount;
	
	private int outsDataLen;
	private String outsDescription = null;
	
	private WDataModelWriter mIn = null;
	private WDataModelWriter mOut = null;
	
	public String getFunction() {
		return function.trim();
	}
	
	public WDataModel getIn() {
		return mIn.getDataModel();
	}
	
	public WDataModel getOut() {
		return mOut.getDataModel();
	}
	
	public void parse( int idx, char[] inbuffer ) throws IOException {
		int p = 0;
		
		if ( (idx == 0) && (inbuffer[0] == 'V') && (inbuffer[1] == '0') && 
				           (inbuffer[2] == '0') && (inbuffer[3] == '1') ) {
			header = new String(inbuffer, 0, 76);
		}

		if ( header != null ) p = 76;
		
		if ( idx == 0) {
			prmArea = new String (inbuffer, p, 164);
			p+=28;  // vorheriges
			/* Funktion */
			function = new String (inbuffer, p, 32); p += 32;
			p += 5;  // Absender Status;
			p += 5;  // Empfaenger Status;
			p += 2;  // Erfolg
			p += 8;  // Fehlerid
			p += 4;  // TextNr
			p += 80; // FehlerMessage
			
			/* Anzahl In Parameter */
			insCount = Integer.parseInt(WDMTypChecker.trimNumericLeft(new String (inbuffer, p, 8))); p += 8;
			/* In Description */
			String indesc = new String(inbuffer, p, insCount*23+32); p += (insCount*23+32);
			mIn = WServerRequest.WDMfromDecription(indesc, insCount);
		} else {
			p += (172 + insCount*23+32);
		}
		
		/* in indikatoren */
		inInd = new String(inbuffer, p, insCount); p += insCount;
		
		if ( idx == 0 ) {
			/* in daten laenge */
			insDataLen  = Integer.parseInt(WDMTypChecker.trimNumericLeft(new String (inbuffer, p, 8))); p += 8;
		} else { 
			p += 8; 
		}
		
		/* in daten */
		String indata = new String(inbuffer, p, insDataLen); p += insDataLen;
		
		mIn.addRow();
		int pos = 0; int len = 0; int p2 = 0;
		for ( Enumeration e = mIn.attributes(); e.hasMoreElements();) {
			IWAttribute a = (IWAttribute) e.nextElement();
			len = a.getLen();	
			String value = indata.substring(pos, pos+len);
			
			short ind = IWDataItem.ENABLED;
			mIn.setValue(a.getName(), value, WServerRequest.char2State( inInd.charAt(p2)));
			pos += len;
			p2 ++;
		}
		
		if ( idx == 0 ) {
			/* Anzahl Out Parameter */
			outsCount = Integer.parseInt(WDMTypChecker.trimNumericLeft(new String (inbuffer, p, 8))); p += 8;
			/* Out Description */
			outsDescription = new String(inbuffer, p, outsCount*23+32); p += (outsCount*23+32);
		
			mOut = WServerRequest.WDMfromDecription(outsDescription, outsCount);
			
			/* in Out daten laenge */
			outsDataLen = Integer.parseInt(WDMTypChecker.trimNumericLeft(new String (inbuffer, p, 8))); p += 8;
		} else {
			;
		}
	}

	private static short char2State(char s) {
		if ( s == '0')
			return IWDataItem.DISABLED;
		else if ( s == '1')
			return IWDataItem.ENABLED;
		else if ( s == '2')
			return IWDataItem.TRUNCATED;
		else if ( s == '3')
			return IWDataItem.ERROR;
		else if ( s == '4')
			return IWDataItem.UNCHANGED;
		else if ( s == '9')
			return IWDataItem.ERROR;
		else
			return IWDataItem.ERROR;	
	}
	
	private static WDataModelWriter WDMfromDecription(String desc, int c) {
		WDataModelWriter dm = WDataModelWriter.getInstance();
		int pos = 16;
		for ( int i = 0; i < c; i++ ){
			String name = desc.substring(pos, pos+16).trim(); pos+=16;
			char typ = desc.charAt(pos); pos++;
			int len = Integer.parseInt(WDMTypChecker.trimNumericLeft(desc.substring(pos, pos+4))); pos +=4;
			int dec = desc.charAt(pos) - '0'; pos++;
			boolean verz = (desc.charAt(pos) == 'J'); pos++;
						
			dm.addAttribute(new WAttribute( name, typ, len, dec, verz)) ;
		}
		return dm;
	}
	
	/**
	 * @return PrmAbsender und PrmAufrufer Datenbereich des Requests
	 */
	protected final String getPrmArea() {
		return prmArea;
	}

	/**
	 * @return Runtime Control Header des Requests
	 */
	protected final String getHeader() {
		return header;
	}
	
	
	/**
	 * @return Returns the inInd.
	 */
	protected final String getInInd() {
		return inInd;
	}
	/**
	 * @return Returns the outsCount.
	 */
	protected final int getOutsCount() {
		return outsCount;
	}
	/**
	 * @return Returns the outsDataLen.
	 */
	protected final int getOutsDataLen() {
		return outsDataLen;
	}
	/**
	 * @return Returns the outsDescription.
	 */
	protected final String getOutsDescription() {
		return outsDescription;
	}
	/**
	 * @return Returns the insCount.
	 */
	protected final int getInsCount() {
		return insCount;
	}
}
