package com.entitec.jepkid.server;

import com.entitec.jepkid.communicator.Token;
import com.entitec.jepkid.communicator.TokenFinder;

public interface LoginHandler extends TokenFinder {
	
	boolean login(Token epkidLoginToken);
	void logoff();

}
