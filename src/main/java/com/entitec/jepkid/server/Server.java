package com.entitec.jepkid.server;

public interface Server {
	
	void start();
	void stop();

}
