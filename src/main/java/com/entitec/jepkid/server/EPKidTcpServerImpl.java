package com.entitec.jepkid.server;

import java.net.Socket;

import com.entitec.jepkid.communicator.Communicator;

public class EPKidTcpServerImpl extends TcpServerImpl {
	
	public EPKidTcpServerImpl(
			int listenport, final Communicator communicator, final LoginHandler loginHandler) {
		super(
			listenport,
			new ServerTaskSource() {
				public Runnable getServerTask(final Socket soc) {
					return new WCallServerTask( 
							soc, communicator, loginHandler); 
				}
			}
		);
	}

}
