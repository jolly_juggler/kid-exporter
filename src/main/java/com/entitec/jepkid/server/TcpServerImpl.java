package com.entitec.jepkid.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author SD0024
 *
 * Ist die Implementierung eines einfachen, internen EP/KID Servers. Er nimmt an einen
 * TCP/IP Port EP/KID Anfragen entgegen und f�hrt sie aus.<p/>
 * 
 * Wird kein Port explizit vorgegeben, so wird ein freier Port ermittelt. Dieser Port
 * wird in einer Environmentvariable hinterlegt.
 * 
 */
public class TcpServerImpl implements Runnable, Server {
	private final int port;
	private final ServerTaskSource serverTaskSource;
	private final Thread listenThread;
		
	private ServerSocket srvsoc = null;
	
	public TcpServerImpl(int port, ServerTaskSource serverTaskSource ) {
		this.serverTaskSource = serverTaskSource;
		this.port = port;
		this.listenThread = new Thread(this, this.getClass().getSimpleName()+":listen");
	}
	
	/**
	 * Startet den TCP Server
	 */
	public void run() {
		
		Socket soc = null;
		
		try {
			srvsoc = new ServerSocket(port);
				 
			while (true) {
				soc = srvsoc.accept();
				final Runnable task = serverTaskSource.getServerTask(soc);
				if ( task != null ) {
					soc = null;
					final Thread t = new Thread(task, this.getClass().getSimpleName()+":worker");
					t.start();
				} else {
					soc.close();
					soc = null;
				}
			}
		} catch (IOException e) {
			if ( (srvsoc != null) && srvsoc.isClosed() ) {
				; 
			} else {
				e.printStackTrace();
			}
		} finally {
			try { if ( soc != null ) soc.close(); } catch (IOException e) {;}
			try { if ( srvsoc != null ) srvsoc.close(); } catch (IOException e) {;}
		}
	}
	public void start() { listenThread.start(); }
	public void stop() {
		try {
			if ( srvsoc != null ) {
				srvsoc.close();
			}
		} catch (IOException e) { ; }
	}
	

}
