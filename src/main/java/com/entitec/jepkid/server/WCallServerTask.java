package com.entitec.jepkid.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import com.entitec.jepkid.communicator.Token;
import com.entitec.jepkid.communicator.TokenPasswordImpl;
import com.entitec.wax.wcall.IWCall;
import com.entitec.wax.wcall.WSuccess;
import com.entitec.wax.wcall.wprotocol.WTcpToolkit;
import com.entitec.wax.wdm.WDataModelReader;

/**
 * Nimmt KID-Nachrichten an einer TCP Socket entgegen, leitet die Anfragen
 * an einen IWCall Handler weiter und sendet das Ergebnis �ber die Socket
 * zur�ck.
 * 
 * @author SD0024
 *
 */
public class WCallServerTask implements Runnable {
	private static final String USRID = "USRID";
	private static final String PASSWD = "PASSWD";
	private static final String USRGR = "USRGR";
	private static final String MANDT = "MANDT";
	
	private final Socket soc;
	private final IWCall handler;
	private final LoginHandler epkidLoginHandler;
	
	public WCallServerTask(Socket soc, IWCall handler, LoginHandler epkidLoginHandler) {
		this.soc = soc;
		this.handler = handler;
		this.epkidLoginHandler = epkidLoginHandler;
	}

	public void run() {
		InputStream in;
		OutputStream out;
		try {
			in = soc.getInputStream();
			out = soc.getOutputStream();
			
			WServerRequest req = new WServerRequest();
			while  ( WTcpToolkit.parseServerTcp(in, out, req) ) {
			
				String function = req.getFunction();
				WSuccess suc = null;
				if ( function.startsWith("LOGIN") == true ) {
					if ( epkidLoginHandler != null ) {
						epkidLoginHandler.login(fishingTokenFromServerRequest(req));
					}
				} else 
				{
					try {
						suc = handler.process(function, req.getIn(), req.getOut());
					} catch ( Exception we ){
						suc = new WSuccess("99", we.getMessage());
					}
				} 
				WServerResponse resp = new WServerResponse(req, suc);
				WServerResponse.sendTcp(out, resp);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if ( soc != null )
				try { soc.close(); } catch (IOException e) {;}
				if ( epkidLoginHandler != null ) epkidLoginHandler.logoff();
		}
	}
	
	private static Token fishingTokenFromServerRequest(WServerRequest req) {
		final WDataModelReader r = new WDataModelReader(req.getIn());
		final String user = (String) r.getValue(USRID);
		final String password = (String) r.getValue(PASSWD);
		final String group = (String) r.getValue(USRGR);
		final String mandt = (String) r.getValue(MANDT);
//		return new TokenImpl(user, password, group, mandt);
		return new TokenPasswordImpl(user, password, group, mandt);
	}
}
