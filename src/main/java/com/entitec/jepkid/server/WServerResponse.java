/**
 * Erzeugt eine Server Antwort (Response) auf eine Server Anfrage (Request)
 */
package com.entitec.jepkid.server;

import java.io.IOException;
import java.io.OutputStream;

import com.entitec.wax.wcall.WCallError;
import com.entitec.wax.wcall.WCallErrorList;
import com.entitec.wax.wcall.WSuccess;
import com.entitec.wax.wcall.wprotocol.WTcpToolkit;
import com.entitec.wax.wdm.WDataModelReader;

/**
 * @author SD0024
 */
public class WServerResponse {
	private WServerRequest req = null;
	private String erfolg = "0";
	private String txtnr = "";
	private String text = "";
	private int rows = 0;
	private int currow = -1;
	private WDataModelReader dmrOut = null;

	/**
	 * Erzeugt eine Antwort auf den Request
	 * @param req Request
	 */
	public WServerResponse (WServerRequest req) {
		this.req = req;
		this.dmrOut = new WDataModelReader(req.getOut());
		this.rows = dmrOut.rowCount();
	}
	
	public WServerResponse (WServerRequest req, WSuccess suc) {
		this(req);
		
		/*this.req = req;
		this.dmrOut = new WDataModelReader(req.getOut());
		this.rows = (new WDataModelReader(req.getOut())).rowCount();*/
		
		if ( suc != null ){
			this.erfolg = suc.getSuccess();
			WCallErrorList el = suc.getErrorList();
			if ( el.isEmpty() == false ){
				WCallError err = (WCallError) el.errors().nextElement();
				if ( err != null ) {
					txtnr = err.getNumber();
					text = err.getText();
				}
			}
		}
	}
	
	/** 
	 * gibt einen, dem Request entsprechenden, EP/KID konformen Response Protokollstring zur�ck.
	 * 
	 * Der Satz besitzt folgenden Aufbau:
	 * 
	 * RCTOKEN
	 * PARMABSENDER
	 * PARMAUFRUF
	 * ANZ-IN-ATTR
	 * IN-IND
	 * ANZ-OUT-ATTR
	 * OUT-DESC
	 * OUT-IND
	 * OUT-DATALEN
	 * OUT-DATA
	 * 
	 * @return EP/KID konformer Protokollstring
	 */
	public StringBuffer next() {
		currow++;
		StringBuffer buf = new StringBuffer();
		/* RC Header */
		final String rcHeader = req.getHeader();
		if ( rcHeader != null ) buf.append(rcHeader);
		
		/* PARMABSENDER + PARMAUFRUF */
		if ( isLast() ){
				buf.append( WServerResponse.setPrmArea(new StringBuffer(req.getPrmArea()),
						"FREE", erfolg, txtnr, text));
		} else {
			buf.append( WServerResponse.setPrmArea(new StringBuffer(req.getPrmArea()),
					"SEND", erfolg, txtnr, text));
		}

		/* ANZ-IN-ATTR */
		buf.append(WTcpToolkit.fillLeft(""+req.getInsCount(), '0', 8));
		
		/* IN-IND */
		buf.append(req.getInInd());
		
		/* ANZ-OUT-ATTR */
		buf.append(WTcpToolkit.fillLeft(""+req.getOutsCount(), '0', 8));
		
		/* OUT-DESC */
		buf.append(req.getOutsDescription());
		
		if ( rows == 0 ) {
			/* OUT-IND ( mit NULL Werten ) */
			buf.append(WTcpToolkit.fillLeft("", '0', req.getOutsCount()));
		
			/* OUT-DATALEN */
			buf.append(WTcpToolkit.fillLeft(""+req.getOutsDataLen(), '0',8 ));
		
			/* OUT-DATA */
			buf.append(WTcpToolkit.fillLeft("", ' ', req.getOutsDataLen()));
		
		} else {
			String desc = req.getOutsDescription();
			/* OUT-IND ( mit NULL Werten ) */
			buf.append(WServerResponse.indikatorStringBuffer(dmrOut, currow));
		
			/* OUT-DATALEN */
			buf.append(WTcpToolkit.fillLeft(""+req.getOutsDataLen(), '0',8 ));
		
			/* OUT-DATA */
			buf.append(WServerResponse.datenStringBuffer(dmrOut, currow));

		}
		return buf;
	}
	
	/** wurde mit next() der letzte Satz zur�ckgegeben */
	protected boolean isLast() {
		return ( (rows > 1) && ( currow < (rows-1) )) == false;
	}
	
	/** Werte der Indikatoren aus einer Zeile eines Datamodels ermitteln. Die Indikatoren werden
	 * als StringBuffer zurueckgegeben. Je Zeichen im StringBuffer ein Indiktor. Stelle entspricht
	 * der Reihenfolge ihres Vorkommens in der Signatur des Datamodels.
	 * 
	 * @param dmr	Datamodel
	 * @param row	Zeile
	 * @return	Indikatoren
	 */
	private static StringBuffer indikatorStringBuffer(WDataModelReader dmr, int row) {
		
		dmr.setRow(row);
		int cols = dmr.colCount();
		
		StringBuffer b = new StringBuffer();
		char c;
		for ( int i=0; i < cols; i++ ){
			b.append(dmr.getStateAsChar(dmr.getAttributeName(i)));
		}
		
		return b;
	}
	
	/** Datenwerte der Attribute einer Zeile eines Datamodels ermitteln. Die Daten werden
	 * als StringBuffer im internen Kid Format zurueckgegeben. Reihenfolge der Attributewerte entspricht 
	 * der Reihenfolge ihres Vorkommens in der Signatur des Datamodels.
	 * 
	 * @param dmr	Datamodel
	 * @param row	Zeile
	 * @return	Daten
	 */
	private static StringBuffer datenStringBuffer(WDataModelReader dmr, int row) {
		dmr.setRow(row);
		int cols = dmr.colCount();
		
		StringBuffer b = new StringBuffer();
		String value;
		for ( int i=0; i < cols; i++ ){
			value = dmr.getValueFilled(dmr.getAttributeName(i));
			b.append(value);
		}
		
		return b;
	}

	
	/**
	 * setzt den PRMAREA (b) zur�ck, d.h. den Empf�ngerstatus auf 'FREE', Erfolg auf '00', Textnr auf ''
	 * und Message auf ''.
	 * 
	 * @param b
	 * @return Referenz auf den Buffer 
	 */
	private static StringBuffer setPrmArea(StringBuffer b) {
		return WServerResponse.setPrmArea(b, "FREE", "0", "", ""); 
	}
	
	private static StringBuffer setPrmArea(StringBuffer b, 
			String status, String erfolg, String txtnr, String text) {
		
		b.replace(65, 65+5, WTcpToolkit.fillRight(status, ' ', 5));
		b.replace(70, 70+2, WTcpToolkit.fillLeft(erfolg, '0', 2));
		b.replace(80, 80+4, WTcpToolkit.fillRight(txtnr, ' ', 4));
		b.replace(84, 84+80, WTcpToolkit.fillRight(text, ' ', 80));
	
		return b;
	}
	/**
	 * versendet eine Antwort (response) �ber einen Stream.
	 * 
	 * Es werden alle S�tze der Response mit vorangestellten Header und L�ngenangabe in den 
	 * OutputStream eingestellt. Der abschliessenden Satz wird mit dem Header '2', die 
	 * vorangehenden mit Header '1' eingestellt. (noch nicht implementiert, aktuell wird
	 * nur ein Satz eingestellt)
	 * 
	 * @param so outputstream
	 * @param resp response f�r den Server
	 * @throws IOException
	 */
	public static void sendTcp(OutputStream so, WServerResponse resp ) throws IOException{
		StringBuffer b;
		boolean islast = false;
		
		while (islast == false){
			b = resp.next();
			islast = resp.isLast();
			if ( islast ){
				b.insert(0, "2"+WTcpToolkit.fillLeft(""+b.length(), '0', 10));		
			} else {
				b.insert(0, "1"+WTcpToolkit.fillLeft(""+b.length(), '0', 10));
			}
			WTcpToolkit.write(so, b.toString(), null);
		}				
	}	
}
