package com.entitec.jepkid;

import com.entitec.jepkid.communicator.Communicator;
import com.entitec.jepkid.communicator.CommunicatorFactory;
import com.entitec.jepkid.communicator.CommunicatorFactoryImpl;
import com.entitec.jepkid.communicator.GenericServiceFactoryImpl;
import com.entitec.jepkid.communicator.ServiceInvocationHandler;
import com.entitec.jepkid.communicator.Token;
import com.entitec.jepkid.communicator.TokenPasswordImpl;
import com.entitec.jepkid.server.EPKidTcpServerImpl;
import com.entitec.jepkid.server.LoginHandler;
import com.entitec.jepkid.server.Server;
import com.entitec.jepkid.server.UserTokenHandlerImpl;

public class CoreFactoryImpl implements CoreFactory {
	
	private final CommunicatorFactory communicatorFactory =
		new CommunicatorFactoryImpl();

	public CommunicatorFactory getCommunicatorFactory() {
		return communicatorFactory;
	}
	
	public Token 
	createToken(String user, String group, String password, String mandt){
//		return new TokenImpl(user, group, password, mandt);
		return new TokenPasswordImpl(user, password, group, mandt);
	}
	
	public LoginHandler createThreadBasedLoginHandler() {
		return new UserTokenHandlerImpl();
	}
	
	public Server createEPKIDTcpServer(
			int listenport, final Communicator communicator, final LoginHandler loginHandler) {
		return new EPKidTcpServerImpl(listenport, communicator, loginHandler);
	}
	
	public ServiceInvocationHandler createGenericServiceFactory(Communicator communicator){
		return new GenericServiceFactoryImpl(communicator);
	}
	
}
