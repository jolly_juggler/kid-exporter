package com.entitec.jepkid.session;

public interface Session {
	
	public void setProperty(String propertyName, Object property);
	public Object getProperty(String propertyName);
	public String[] getPropertyNames();
	
	public Object getSessionId();

}
