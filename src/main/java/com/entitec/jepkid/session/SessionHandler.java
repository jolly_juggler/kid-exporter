package com.entitec.jepkid.session;

public interface SessionHandler {

	public Session currentSession();
	public Session getSession(Object sessionId);

}
