package com.entitec.jepkid.session;

import java.util.Collections;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class SessionManagerImpl 
implements SessionManager, SessionHandler
{
	
	private final Map<Object, Session> sessions = Collections.synchronizedMap(new HashMap<Object,Session>());
	private final ThreadLocal<Session> currentSession = new ThreadLocal<Session>();

	public Session closeSession(Object sessionId) {
		if (sessionId == null) return null;
		if (!existSession(sessionId)) {
			throw new RuntimeException("Session not exists");
		}
		// ggf. die aktuelle Session eleminieren
		if (isActiv(sessionId)) {
			currentSession.set(null);
		}
		return sessions.remove(sessionId);
	}

	public Session createSession(Object sessionId) {
		if (existSession(sessionId)) {
			throw new RuntimeException("Session already exists");
		}
		Session s = new SessionImpl(sessionId);
		sessions.put(sessionId, s);
		return s;
	}

	public Session activateSession(Object sessionId) {
		if (!existSession(sessionId)) {
			throw new RuntimeException("Session not exists");
		}
		currentSession.set(null);
		currentSession.set(sessions.get(sessionId));
		return currentSession.get();
	}

	public Session deactivateCurrentSession() {
		Session session = currentSession.get();
		currentSession.set(null);
		return session;
	}

	public Session currentSession() {
		return currentSession.get();
	}
	
	public Session getSession(Object sessionId) {
		return sessions.get(sessionId);
	}

	public boolean existSession(Object sessionId) {
		if (sessionId == null) return false;
		return sessions.containsKey(sessionId);
	}

	public boolean isActiv(Object sessionId) {
		if (sessionId == null) return false;
		return (currentSession.get() != null && 
				currentSession.get().getSessionId().equals(sessionId)) ? true : false;
	}
	
	

	public Session[] getSessions() {
		
		Collection<Session> l = sessions.values();
		if (l == null) return new Session[]{};
		Session[] ret = new Session[l.size()];

		int i = 0;
		for (Iterator<Session> iterator = l.iterator(); iterator.hasNext();) {
			ret[i] = iterator.next();
			i++;
		}
		return ret;
	}

}
