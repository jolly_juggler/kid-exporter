package com.entitec.jepkid.session;

public interface SessionManager {

	/**
	 * Erzeugen einer neuen Session mit der angegebenen SessionId
	 * Diese Session darf noch nicht existieren
	 * @param sessionId
	 * @return neu erzeugte Session
	 */
	public Session createSession(Object sessionId);
	/**
	 * Aktivierung einer bestehenden Session 
	 * Die Session unter der besagten SessionId wird als 
	 * CurrentSession (threadLocal) eingestellt.
	 * @param sessionId
	 * @return aktivierte Session
	 */
	public Session activateSession(Object sessionId);
	/**
	 * Deaktivierung der aktuellen Session
	 * @return deaktivierte Session
	 */
	public Session deactivateCurrentSession();
	
	/**
	 * Schliessen der Session mit der angegebenen SessionId
	 * @param sessionId
	 * @return geschlossenen Session
	 */
	public Session closeSession(Object sessionId);
	
	/**
	 * Pr�ft, ob eine Session mit der angegebenen SessionId vorhanden ist.
	 * @param sessionId
	 * @return true, wenn Session mit der angegebnen SessionId bereits vorhanden ist
	 */
	public boolean existSession(Object sessionId);
	
	/**
	 * Pr�ft, ob die Session mit der angegebenen SessionId die aktuelle Session ist.
	 * @param sessionId
	 * @return true, wenn die gew�nschte Session die aktuelle ist
	 */
	public boolean isActiv(Object sessionId);
	
	/**
	 * Ermitteln aller bekannten Sessions
	 * @return
	 */
	public Session[] getSessions();
	
	public Session getSession(Object sessionId);
	
}
