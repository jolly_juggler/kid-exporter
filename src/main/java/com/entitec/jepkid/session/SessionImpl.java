package com.entitec.jepkid.session;

import java.util.Collections;

import java.util.HashMap;
import java.util.Map;

public class SessionImpl 
	implements Session
{

	private Object sessionId;
	private Map<String, Object> properties = Collections.synchronizedMap(new HashMap<String,Object>());
	
	
	public SessionImpl(Object sessionId) {
		this.sessionId = sessionId;
	}
	
	public String[] getPropertyNames() {
		return (String[])properties.keySet().toArray();
	}

	public Object getProperty(String propertyName) {
		return properties.get(propertyName);
	}

	public Object getSessionId() {
		return sessionId;
	}

	public void setProperty(String propertyName, Object property) {
		properties.put(propertyName, property);
	}

}
