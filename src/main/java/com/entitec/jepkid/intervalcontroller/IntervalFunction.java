package com.entitec.jepkid.intervalcontroller;

import java.io.Serializable;

public class IntervalFunction implements Serializable {
	private int balanceTime;
	private final String functionname;
	private final int elapsedTime;
	
	public IntervalFunction(String functionname, int elapsedTime) {
		this.functionname = functionname;
		this.elapsedTime = elapsedTime;
		this.balanceTime = elapsedTime;
	}
	
	public void resetBalanceTime() {
		this.balanceTime = this.elapsedTime;
	}
	public void decreaseBalanceTime(int seconds) {
		this.balanceTime -= seconds;
	}

	public int getBalanceTime() {
		return balanceTime;
	}

	public int getElapsedTime() {
		return elapsedTime;
	}

	public String getFunctionname() {
		return functionname;
	}
}

