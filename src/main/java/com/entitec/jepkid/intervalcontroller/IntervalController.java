package com.entitec.jepkid.intervalcontroller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import com.entitec.jepkid.communicator.Communicator;

public class IntervalController implements Runnable, IntervalControllerMBean {
	
	Thread t = null;
	
	Communicator communicator;
	final List<IntervalFunction> functionsList = 
		new ArrayList<IntervalFunction>();
	
	final Comparator<IntervalFunction> minBalanceComparator = 
		new Comparator<IntervalFunction>() {
			public int compare(IntervalFunction o1, IntervalFunction o2) {
				return (o1.getBalanceTime() - o2.getBalanceTime());
			}
	};
	
	private String lastPerformedFunction = null;
	private String nextPendingFunction = null;
	private Integer pendingTime = -1;

	public IntervalController(Map<String, String> functions, Communicator communicator) {
		this.communicator = communicator;
		
		for (String f : functions.keySet() ) {
			functionsList.add(
					new IntervalFunction(f, Integer.parseInt(functions.get(f))));
		}
	}
	
	public IntervalController() {
		;
	}
	
	public void setCommunicator(Communicator communicator) {
		this.communicator = communicator;
	}
	
	public void setFunctions(Map<String, String> functions) {
		for (String f : functions.keySet() ) {
			functionsList.add(
					new IntervalFunction(f, Integer.parseInt(functions.get(f))));
		}
	}
	
	
	
	
	public void run() {
		if ((functionsList == null) || functionsList.isEmpty() ) return;
		
		while ( (t != null) && (Thread.currentThread().getId() == t.getId()) ) {
			int waitTime = getMinBalanceTime();
			this.pendingTime = waitTime;
			try {
				Thread.sleep(waitTime * 1000);
			} catch (InterruptedException e) {
				continue;
			}
			if ((t != null) && (Thread.currentThread().getId() == t.getId())) {
				List<String> pendingFunctions = 
					decreaseBalanceTimeAndGetPendingFunctions(waitTime);
			
				for (String f:pendingFunctions) {
					communicator.process(f, null, null);
					this.lastPerformedFunction = f;
					System.out.println(f);
				}
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see com.entitec.good.intervalcontroller.communicator.IntervalControllerMBean#start()
	 */
	public void start() {
		if ( t == null ) {
			t = new Thread(this, "IntervalController");
			t.start();
		}
	}
	
	/* (non-Javadoc)
	 * @see com.entitec.good.intervalcontroller.communicator.IntervalControllerMBean#stop()
	 */
	public void stop() {
		t = null;
		nextPendingFunction = "";
		pendingTime = -1;
	}
	
	private int getMinBalanceTime() {
		Collections.sort(functionsList, minBalanceComparator);
		this.nextPendingFunction = functionsList.get(0).getFunctionname();
		return functionsList.get(0).getBalanceTime();
	}
	
	private List<String> decreaseBalanceTimeAndGetPendingFunctions(int seconds) {
		final List<String> l = new ArrayList<String>();
		for (IntervalFunction f : functionsList) {
			f.decreaseBalanceTime(seconds);
			if ( f.getBalanceTime() <= 0 ) {
				l.add(f.getFunctionname());
				f.resetBalanceTime();
			}
		}
		return l;
	}
	
	public Boolean getStopped() {
		return (t == null);
	}

	public List<String> showInternalList() {
		final List<String> l = new ArrayList<String>();
		for (IntervalFunction f:functionsList ) {
			l.add(""+f.getBalanceTime()+":"+f.getFunctionname()+"="+f.getElapsedTime());
		}
		return l;
	}

	public String getLastPerformedFunction() {
		return lastPerformedFunction;
	}

	public String getNextPendingFunction() {
		return nextPendingFunction;
	}

	public Integer getPendingTime() {
		return pendingTime;
	}
}
