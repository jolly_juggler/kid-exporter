package com.entitec.jepkid.intervalcontroller;

import java.util.List;

public interface IntervalControllerMBean {

	public void start();

	public void stop();
	
	public Boolean getStopped();
	
	public List<String> showInternalList();
	
	public String getLastPerformedFunction();
	
	public String getNextPendingFunction();

	public Integer getPendingTime();

}