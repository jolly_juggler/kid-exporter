package com.entitec.jepkid.communicator;

import com.entitec.wax.wcall.WCallError;
import com.entitec.wax.wcall.WCallErrorList;
import com.entitec.wax.wcall.WSuccess;

public class CommunicatorRuntimeException extends RuntimeException {
	private final String methodName;
	private final int erfolg;
	private final WCallErrorList errorList;
	private WCallError error = null;
	
	public CommunicatorRuntimeException(WSuccess suc, String name) {
		super();
		this.methodName = name;
		
		this.erfolg = Integer.parseInt(suc.getSuccess());
		this.errorList = suc.getErrorList();
	}

	public int getErfolg() {
		return this.erfolg;
	}
	
	public String getTextNummer() {
		if ( error == null ) {
			error =errorList.getError();
		}
		if ( error != null ) {
			return error.getNumber();
		}
		return ("");
	}

	public String getText() {
		if ( error == null ) {
			error =errorList.getError();
		}
		if ( error != null ) {
			return error.getText().trim();
		}
		return ("");
	}

	public String[] getTexte() {
		if ( errorList != null ) {
			return errorList.getErrors();
		}
		return new String[] {};
	}
}
