package com.entitec.jepkid.communicator;

import com.entitec.wax.wcall.IWCall;

public interface CommunicationSource {
	
	IWCall getConnection(String name, Token token);

}
