package com.entitec.jepkid.communicator;

import java.util.List;

public abstract class AbstractTokenImpl implements Token {
	
	public static final String DEFAULT_MANDT = "31";
	
	private String user;
	private String mandt;
	private boolean traceEnabled;
	
	private boolean invalid;
	private List<TokenObserver> observers;	
	
	public AbstractTokenImpl(String user) {
		this(user, DEFAULT_MANDT);
	}
	
	public AbstractTokenImpl(String user, String mandt) {
		this(user, mandt, false);
	}
	
	public AbstractTokenImpl(String user,
			String mandt,
			boolean traceEnabled) {
		this.user = user;
		this.mandt = mandt;
		this.traceEnabled = traceEnabled;
		this.invalid = false;
	}

	@Override
	public String getUser() {
		return this.user;
	}

	@Override
	public String getMandt() {
		return this.mandt;
	}
	
	@Override
	public boolean isTraceEnabled() {
		return traceEnabled;
	}

	@Override
	public void invalidate() {
		this.invalid = true;
		for (TokenObserver tokenObserver : observers) {
			tokenObserver.invalidate(this);
		}
	}

	@Override
	public boolean isInvalid() {
		return this.invalid;
	}

	@Override
	public boolean addObserver(TokenObserver tokenObserver) {
		return this.observers.add(tokenObserver);
	}

	@Override
	public boolean removeObserver(TokenObserver tokenObserver) {
		return this.observers.remove(tokenObserver);
	}

}
