package com.entitec.jepkid.communicator;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Proxy;
import java.lang.reflect.Type;
import java.util.List;

import com.entitec.jepkid.communicator.bean.BeanMappingTools;
import com.entitec.jepkid.communicator.bean.BeanTools;
import com.entitec.jepkid.communicator.bean.annotations.KIDExceptions;
import com.entitec.jepkid.communicator.bean.annotations.KIDFunction;
import com.entitec.jepkid.communicator.bean.annotations.KIDResult;
import com.entitec.jepkid.communicator.bean.mapping.BeanException;
import com.entitec.jepkid.communicator.bean.mapping.BeanUtils2;
import com.entitec.wax.wcall.IWCall;
import com.entitec.wax.wcall.WCallErrorList;
import com.entitec.wax.wcall.WSuccess;


import com.entitec.wax.wdm.WDataModel;

/** Erzeugt f�r einen Communicator einen Factory zur Erzeugung von
 * Service Instanzen.
 *  */
public class GenericServiceFactoryImpl implements ServiceInvocationHandler {
	
	private final IWCall communicator;
	
	public GenericServiceFactoryImpl(IWCall communicator) {
		this.communicator = communicator;
	}

	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		try {
			Class retType = method.getReturnType();
	
			if ( retType == void.class) 
				return invoke_withoutReturnType(method, args);
			else if ( retType.isArray() ) 
				return invoke_Array(retType, method, args);
			else if ( retType.isAssignableFrom(List.class) )
				return invoke_List(method, args);
			else {
				return invoke_Complex(retType, method, args);
			}
		} catch (BeanException e) {
			return null;
		}
	}
	
	final private Object invoke_withoutReturnType(Method method, Object[] args) throws Throwable {

		String function = getKIDFunctionName(method);
		WDataModel in = BeanMappingTools.createInWDataModel(method, args);
		WSuccess suc = communicator.process(function, in, null);
		throwMethodExceptionIfError(method, suc);
		return null;
	}
	
	final private Object invoke_Array(Class retType, Method method, Object[] args) throws Throwable {
		Object r = null;
		
		String function = getKIDFunctionName(method);
		
		WDataModel in = 
			BeanMappingTools.createInWDataModel(method, args);
		
		
		WDataModel out;
		
		if ( BeanUtils2.isBean(retType) ) {
			out = BeanMappingTools.createOutWDataModel(BeanUtils2.getArrayElementClass(method.getReturnType()));
			WSuccess suc = communicator.process(function, in, out );
			throwMethodExceptionIfError(method, suc);
			r = BeanTools.createPojoArray(out, BeanUtils2.getArrayElementClass(method.getReturnType()));
		} else {
			out = BeanMappingTools.createOutWDataModel(method);
			WSuccess suc = communicator.process(function, in, out );
			throwMethodExceptionIfError(method, suc);
			r = BeanTools.createValueArray(out, BeanUtils2.getArrayElementClass(method.getReturnType())
					, method.getAnnotation(KIDResult.class).value());
		}
		return r;
	}
	
	private Object invoke_List(Method method, Object[] args) throws Throwable {
		Object r = null;
		
		if ( method.getReturnType().isAssignableFrom(List.class) == false ) return null;
		Type genericReturnType  = method.getGenericReturnType();
		if ( (genericReturnType instanceof ParameterizedType) == false) return null;
	
		ParameterizedType paraType = (ParameterizedType) genericReturnType;
		Type t[] = paraType.getActualTypeArguments();
		Class listReturnType = (Class) t[0];	
		
		String function = getKIDFunctionName(method);
		
		WDataModel in = 
			BeanMappingTools.createInWDataModel(method, args);
		WDataModel out;
		if ( BeanUtils2.isBean(listReturnType)) {
			
			out = BeanMappingTools.createOutWDataModel(listReturnType);
			WSuccess suc = communicator.process(function, in, out );
			throwMethodExceptionIfError(method, suc);
			r = BeanTools.createPojoList(out, listReturnType);
			
		} else {
			out = BeanMappingTools.createOutWDataModel(method);
			WSuccess suc = communicator.process(function, in, out );
			throwMethodExceptionIfError(method, suc);
			r = BeanTools.createValueList(out, listReturnType, method.getAnnotation(KIDResult.class).value());
		}
		
		return r;
	}
	
	private Object invoke_Complex(Class retType, Method method, Object[] args) throws Throwable {
		Object r = null;
		
		String function = getKIDFunctionName(method);
	
		WDataModel in = BeanMappingTools.createInWDataModel(method, args);
		if ( BeanUtils2.isBean( retType ) ) {
			WDataModel out = BeanMappingTools.createOutWDataModel(method.getReturnType());	
			WSuccess suc = communicator.process(function, in, out );
			throwMethodExceptionIfError(method, suc);
			r = BeanTools.createPojo(out, method.getReturnType());
		} else {
			WDataModel out = BeanMappingTools.createOutWDataModel(method);	
			WSuccess suc = communicator.process(function, in, out );
			throwMethodExceptionIfError(method, suc);
			r = BeanTools.createValueObject(out, method.getReturnType(), 
					method.getAnnotation(KIDResult.class).value());			
		}
		return r;
	}

		private String getKIDFunctionName(Method m) {
		KIDFunction af = m.getAnnotation(KIDFunction.class);
		if ( af != null )
			return af.value();
		else 
			return m.getName();
	}
	private void throwMethodExceptionIfError(Method m, WSuccess suc) throws Throwable {
		if ("00".equals(suc.getSuccess())) return ;

		Class [] exceptions = m.getExceptionTypes();
		
		if ( exceptions.length == 0 ) {
			throw new CommunicatorRuntimeException(suc, m.getName());
		} else if ( "99".equals(suc.getSuccess())) {
			throw new CommunicatorRuntimeException(suc, m.getName());
		} else {
			KIDExceptions e = m.getAnnotation(KIDExceptions.class);
			if ( e != null && e.value().length == exceptions.length ) {
				String s = suc.getSuccess();
				if (s.startsWith("0")) 
					s = s.substring(1);
				int si = Integer.parseInt(s);
				
				for ( int i=0; i < e.value().length; i++ )
					if ( e.value()[i] == si ) {
						throw (Throwable) exceptions[i].getConstructor(
								new Class[]{String.class}).
								newInstance(new Object[] {createMessage(suc, m)}
							);
					}	
				
			} else {
				throw new CommunicatorRuntimeException(suc, m.getName());
			}
			
		}
	}

	private String createMessage(WSuccess suc, Method m ) {
		StringBuffer message = new StringBuffer(m.getName()+":"+suc.getSuccess());
	
		WCallErrorList l = suc.getErrorList();
		if ( l != null ) {
			String [] txts = l.getErrors();
			if ( txts.length > 0 )
				message.append(':'+txts[0]);
		}
		return message.toString();
	}	
		
	public <T> T newInstance(Class<T> serviceInterfaces) {
		return (T) Proxy.newProxyInstance(
				serviceInterfaces.getClassLoader(),
				new Class [] {serviceInterfaces}, 
				this);
	}
}
