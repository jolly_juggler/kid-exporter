package com.entitec.jepkid.communicator;

/**
 * 
 * @author sd0024
 *
 */
@Deprecated
public class TokenImpl
	extends AbstractTokenImpl
	implements Token {
	
	private static String DEFAULT_MANDT = "31";
	private static String DEFAULT_GROUP = "admin";
	
	private String password;
	private String group;
	

	public TokenImpl(String user, String password) {
		this(user, password, DEFAULT_GROUP, DEFAULT_MANDT);
	}

	public TokenImpl(String user, String password, String group, String mandt) 
	{
		super(user, mandt);
		this.password = password;
		this.group = group;
	}
	

	public final String getPassword() {
		return password;
	}

	public String getGroup() {
		return group;
	}


}
