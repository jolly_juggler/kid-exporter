package com.entitec.jepkid.communicator;

import java.lang.reflect.InvocationHandler;

public interface ServiceInvocationHandler extends InvocationHandler {
	<T> T newInstance(Class<T> serviceInterfaces);
}
