package com.entitec.jepkid.communicator;

import com.entitec.jepkid.communicator.source.LDAPHandlerImpl;
import com.entitec.wax.wdm.IWDataItem;
import com.entitec.wax.wdm.WAttribute;
import com.entitec.wax.wdm.WDataModel;
import com.entitec.wax.wdm.WDataModelWriter;
import com.entitec.wax.wdm.WSignature;

public class TokenPasswordWDataModelProcessor implements TokenWDataModelProcessor {

	String userdnsdomain = System.getProperties().getProperty("USERDNSDOMAIN"); 

	public static final String PASSWD_KEY = "PASSWD";
	
	public static final String USRGR_KEY = "USRGR";
	
	private final TokenPassword token;
	
	public TokenPasswordWDataModelProcessor(TokenPassword token) {
		this.token = token;
	}

	@Override
	public WDataModel incomingDataModel() {
		
		WSignature sig = new WSignature();
		WDataModel in;
		WDataModelWriter win;
		
		sig.addAttribute(new WAttribute(MANDT_KEY, MANDT_KEY, 'C', 2, 0, false));
		sig.addAttribute(new WAttribute(USRID_KEY, USRID_KEY, 'C', 8, 0, false));
		sig.addAttribute(new WAttribute(PASSWD_KEY, PASSWD_KEY, 'C', 32, 0, false));
		sig.addAttribute(new WAttribute(USRGR_KEY, USRGR_KEY, 'C', 8, 0, false));

		String uid = token.getUser();
		
		if (userdnsdomain != null) {
			LDAPHandlerImpl ldm = LDAPHandlerImpl.getInstance(userdnsdomain, uid, token.getPassword());
			if (ldm != null) {
				uid = ldm.getUid(uid);
				if (uid == null) uid = token.getUser();
			}
		}

		
		in = new WDataModel(sig);
		win = new WDataModelWriter(in);

		win.addRow();
		win.setValue(MANDT_KEY, token.getMandt());
		win.setState(MANDT_KEY,IWDataItem.ENABLED);
		win.setValue(USRID_KEY, uid);
		win.setState(USRID_KEY,IWDataItem.ENABLED);
		win.setValue(PASSWD_KEY, token.getPassword());
		win.setState(PASSWD_KEY,IWDataItem.ENABLED);
		win.setValue(USRGR_KEY, token.getGroup());
		win.setState(USRGR_KEY,IWDataItem.ENABLED);
			
		return in;
		
	}
	
	@Override
	public WDataModel outgoingDataModel() {
		
		WSignature sig = new WSignature();
		
		sig.addAttribute(new WAttribute(MANDT_KEY, MANDT_KEY, 'C', 2, 0, false));

		return new WDataModel(sig);
		
	}

}
