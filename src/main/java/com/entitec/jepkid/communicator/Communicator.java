package com.entitec.jepkid.communicator;

import com.entitec.wax.wcall.IWCall;

/**
 * Schnittstelle zur Ausf�hrung von EP/KID Funktionalit�t.
 * 
 * �ber den <em>Communicator</em> werden die nachrichtenbasierten EP/KID Aufrufe verteilt und 
 * ausgef�hrt. Ort der Ausf�hrung bzw. durch welche Instanz die Ausf�hrung erfolgt
 * ist f�r den Aufrufer transparent.
 * 
 * @author SD0024
 *
 */
public interface Communicator extends IWCall {
	
	String getUser();
	String getMandt();
	String getSttag();
}
