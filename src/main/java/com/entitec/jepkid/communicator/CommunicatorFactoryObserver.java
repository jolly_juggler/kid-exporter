package com.entitec.jepkid.communicator;

public interface CommunicatorFactoryObserver {
	
	void setCommunicator(Communicator communicator);
	void setDefaultSource(CommunicationSource defaultsource);

}
