package com.entitec.jepkid.communicator;

import com.entitec.wax.wdm.WDataModel;

public interface TokenWDataModelProcessor {
	
	public static final String MANDT_KEY = "MANDT";
	
	public static final String USRID_KEY = "USRID";
	
	WDataModel incomingDataModel();
	
	WDataModel outgoingDataModel();

}
