package com.entitec.jepkid.communicator;

public class TokenJWTImpl extends AbstractTokenImpl implements TokenJWT {
	
	private String jwt;

	public TokenJWTImpl(String user,
			String jwt) {
		super(user);
		this.jwt = jwt;
	}
	
	public TokenJWTImpl(String user,
			String jwt,
			String mandt) {
		super(user, mandt);
		this.jwt = jwt;
	}
	
	public TokenJWTImpl(String user,
			String jwt,
			String mandt,
			boolean traceEnabled) {
		super(user, mandt, traceEnabled);
		this.jwt = jwt;
	}

	@Override
	public String getJWT() {
		return this.jwt;
	}

}
