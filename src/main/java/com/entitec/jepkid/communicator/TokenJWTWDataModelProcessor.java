package com.entitec.jepkid.communicator;

import com.entitec.wax.wdm.IWDataItem;
import com.entitec.wax.wdm.WAttribute;
import com.entitec.wax.wdm.WDataModel;
import com.entitec.wax.wdm.WDataModelWriter;
import com.entitec.wax.wdm.WSignature;

public class TokenJWTWDataModelProcessor implements TokenWDataModelProcessor {
	
	public static final String JWTOKEN_KEY = "JWTOKEN";
	
	private final TokenJWT token;

	public TokenJWTWDataModelProcessor(TokenJWT token) {
		this.token = token;
	}
	
	@Override
	public WDataModel incomingDataModel() {
		
		WSignature sig = new WSignature();
		WDataModel in;
		WDataModelWriter win;
		
		sig.addAttribute(new WAttribute(MANDT_KEY, MANDT_KEY, 'C', 2, 0, false));
		sig.addAttribute(new WAttribute(JWTOKEN_KEY, JWTOKEN_KEY, 'C', 9999, 0, false));

		in = new WDataModel(sig);
		win = new WDataModelWriter(in);

		win.addRow();
		win.setValue(MANDT_KEY, token.getMandt());
		win.setState(MANDT_KEY,IWDataItem.ENABLED);
		win.setValue(JWTOKEN_KEY, token.getJWT());
		win.setState(JWTOKEN_KEY,IWDataItem.ENABLED);
			
		return in;
		
	}
	
	@Override
	public WDataModel outgoingDataModel() {
		
		WSignature sig = new WSignature();
		
		sig.addAttribute(new WAttribute(MANDT_KEY, MANDT_KEY, 'C', 2, 0, false));
		sig.addAttribute(new WAttribute(JWTOKEN_KEY, JWTOKEN_KEY, 'C', 9999, 0, false));

		return new WDataModel(sig);
		
	}

}
