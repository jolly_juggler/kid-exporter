package com.entitec.jepkid.communicator;

import java.util.Collection;

/**
 * Schnittstelle Identifikationsobjektes (Token).
 * 
 * @author sd0024
 * @author ef0052
 *
 */
public interface Token {
	
	/**
	 * Gibt den Benutzernamen des Tokens zur�ck.
	 * 
	 * @return Der Benutzername.
	 */
	String getUser();

	/**
	 * Gibt den Mandanten zur�ck.
	 * 
	 * @return Der Mandant.
	 */
	String getMandt();
	
	/**
	 * @return true, wenn tracing aktiviert ist.
	 */
	boolean isTraceEnabled();
	
	/**
	 * Erkl�rt das Token f�r ung�ltig, entfernt das Password und benachrichtigt die Observer.
	 */
	void invalidate();

	/**
	 * Indiziert, ob das Token ung�ltig ist.
	 * 
	 * @return true, wenn das Token ung�ltig ist.
	 */
	boolean isInvalid();
	
	/**
	 * F�gt einen {@link TokenObserver} hinzu.
	 * 
	 * @param tokenObserver Der Observer.
	 * @return {@link Collection#add(Object)}
	 */
	boolean addObserver(TokenObserver tokenObserver);
	
	/**
	 * Entfernt einen {@link TokenObserver}.
	 * 
	 * @param tokenObserver Der Observer.
	 * @return {@link Collection#remove(Object)}
	 */
	boolean removeObserver(TokenObserver tokenObserver);
	
}