package com.entitec.jepkid.communicator;

import java.util.List;

public interface CommunicatorFactory {

	/**
	 * Communicator zu einem entfernten EP/KID Server.
	 * <p/>
	 * 
	 * <li/>eine Verbindung zur Zeit
	 * <li/>pro Aufruf ein Verbindungsaufbau und -abbau
	 * <li/>Anmeldung mit einen vorgegebenen Benutzer
	 *  
	 * @param user	Name des Benutzers
	 * @param group	Gruppe des Benutzers
	 * @param password	Kennwort des Benutzers
	 * @param mandt	Mandt
	 * @param host	Name des EP/KID Servers
	 * @param port	Port an dem der EP/KID Servers Anfragen erwartet
	 * @return	Communicator
	 */
	public abstract Communicator createDefault(String user, String group,
			String password, String mandt, String host, int port);

	/**
	 * Communicator zu einem entfernten EP/KID Server.
	 * <p/>
	 * 
	 * <li/>eine Verbindung zur Zeit
	 * <li/>pro Aufruf ein Verbindungsaufbau und -abbau
	 * <li/>Benutzer zur Anmeldung wird bei jedem Aufruf ermittelt
	 *  
	 * @param tokenFinder	Ermittlung der Benutzerdaten
	 * @param host	Name des EP/KID Servers
	 * @param port	Port an dem der EP/KID Servers Anfragen erwartet
	 * @return	Communicator
	 */
	public abstract Communicator createDefault(TokenFinder tokenFinder,
			String host, int port);

	public abstract Communicator createDefault(TokenFinder tokenFinder,
			CommunicationSource source, String host, int port);
	
	public abstract Communicator createDefault(TokenFinder tokenFinder,
			String host, int port, String codeset);

	public abstract Communicator createDefault(TokenFinder tokenFinder,
			CommunicationSource source, String host, int port, String codeset);
	
	public abstract Communicator createDefault(TokenFinder tokenFinder,
			List<CommunicationSource> sources);

	public abstract Communicator createPooled(TokenFinder tokenFinder,
			String host, int port, String codeset, String header, int maxpoolsize, int secs);

	public abstract Communicator createPooled(TokenFinder tokenFinder, 
			String host, int port, int maxpoolsize, int secs);
	
	Communicator createPooled(TokenFinder tokenFinder, 
			String host, int port, String codeset, int maxpoolsize, int secs);

	public abstract Communicator createPooled(String user, String group,
			String password, String mandt, String host, int port, int maxpoolsize, int secs);

	Communicator createPooled(String user, String group,
			String password, String mandt, String host, int port, String codeset, 
			int maxpoolsize, int secs);
	
	Communicator createPooled(String user, String group,
			String password, String mandt, String host, int port, String codeset, String header, 
			int maxpoolsize, int secs);

	public Communicator createPooled(String user, String group,
			String password, String mandt, String host, int port,
			String codeset, int maxpoolsize, int secs, boolean useSSL);

	public Communicator createDefault
		(TokenFinder tokenFinder, String host, int port, String codeset, boolean useSSL);

	public Communicator createDefault(TokenFinder tokenFinder, 
				CommunicationSource source, 
				String host, int port, String codeset, boolean useSSL);

	public Communicator createPooled(TokenFinder tokenFinder,
			String host, int port, String codeset, String header,
			int maxpoolsize, int secs, boolean useSSL);
	
	public Communicator createPooled(String user, String group,
			String password, String mandt, String host, int port, String codeset, String header,
			int maxpoolsize, int secs, boolean useSSL);
	
	public void start();
	
	public void stop();
	
}