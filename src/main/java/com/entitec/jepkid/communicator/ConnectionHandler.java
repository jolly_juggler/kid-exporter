package com.entitec.jepkid.communicator;

import com.entitec.wax.wcall.IWCall;

/**
 * Erweitert die Aufrufschnittstelle IWCall um Funktionalit�t zum Auf- 
 * und Abbau einer Kommunikationsverbindung.
 * <pre/>
 * Ablauf der Kommunikation:
 * <li>connect()
 * <li>process()+
 * <li>disconnect
 * <pre/>
 * Das bei connect mitgegebene Token gilt f�r die gesamte Kommunikation,
 * d.h. vom connect() bis disconnect().
 *  
 * @author SD0024
 *
 */

public interface ConnectionHandler extends IWCall {
	void connect(Token userToken);
	void disconnect();
	ConnectionHandler newInstance();
	boolean isConnected();
}
