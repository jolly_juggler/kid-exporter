package com.entitec.jepkid.communicator;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.entitec.wax.wcall.IWCall;
import com.entitec.wax.wcall.WSuccess;
import com.entitec.wax.wdm.WDataModel;

/**
 * 
 * @author sd0024
 *
 */
public class CommunicatorImpl implements Communicator {
	private Log log = LogFactory.getLog(this.getClass());
	
	private DateFormat df = new SimpleDateFormat("yyyyMMdd");
	
	private TokenFinder tokenFinder;
	private List<CommunicationSource> communicationSources; 
	
	public CommunicatorImpl(final Token token, CommunicationSource defaultTreiberSource ) {
		this.tokenFinder = 
			new TokenFinder() {
				public Token getToken() {
					return token;
				}
			};
		this.communicationSources = new ArrayList<CommunicationSource>();
		this.communicationSources.add(defaultTreiberSource);
		this.informSourcesObservers();
		this.informSourcesObservers();
	}
	
	public CommunicatorImpl(TokenFinder tokenFinder, CommunicationSource defaultTreiberSource ) {
		this.tokenFinder = tokenFinder;
		this.communicationSources = new ArrayList<CommunicationSource>();
		this.communicationSources.add(defaultTreiberSource);
		this.informSourcesObservers();
	}

	public CommunicatorImpl(
			TokenFinder tokenFinder,
			List<CommunicationSource> communicationSources) {
		
		this.tokenFinder = tokenFinder;
		this.communicationSources = communicationSources;
		this.informSourcesObservers();
	}

	public CommunicatorImpl(
			final Token token,
			List<CommunicationSource> communicationSources) {
		
		this.tokenFinder = new TokenFinder() {
			public Token getToken() {
				return token;
			};
		};
		this.communicationSources = communicationSources;
		this.informSourcesObservers();
	}
	
	public void setCommunicationSources(List<CommunicationSource> communicationSources) {
		this.communicationSources = communicationSources;
		this.informSourcesObservers();
	}

	public void setTokenFinder(TokenFinder tokenFinder) {
		this.tokenFinder = tokenFinder;
	}
	
	public void setToken(final Token token) {
		this.tokenFinder = new TokenFinder() {
			public Token getToken() {
				return token;
			};
		};
	}
	
	public CommunicatorImpl() {
		;
	}
	
	public WSuccess process(String function, WDataModel in, WDataModel out) {
		
		if ( log.isDebugEnabled()) {
			StringBuffer b = new StringBuffer();
			b.append(">>> Funktion: "+function);
			if ( in != null) {
				b.append("Inview:\n"+in.toString());
			}
			if ( out != null) {
				b.append("Outview:\n"+out.toString());
			}
			log.debug(b.toString());
		}
		IWCall call = null;
		Token token = null;
		
		if ( tokenFinder != null) 
			token = tokenFinder.getToken();
		
		if ( communicationSources != null )
			for (CommunicationSource communicationSource : communicationSources) {
				if ( communicationSource != null) {
					call = communicationSource.getConnection(function, token);
					if ( call != null) break;
				}
			}
	
		if ( call == null ) {
			return new WSuccess("K002");
		} else {
			WSuccess suc = call.process(function, in, out);
			if ( log.isInfoEnabled()) log.info( function+":"+suc.getSuccess() );
			if ( log.isDebugEnabled()) {
				StringBuffer b = new StringBuffer();
				b.append("<<< Funktion: "+function);
				if ( in != null) {
					b.append("Inview:\n"+in.toString());
				}
				if ( out != null) {
					b.append("Outview:\n"+out.toString());
				}
				log.debug(b.toString());
			}
			return suc;	
		}
	}
	
	private void informSourcesObservers(){
		final CommunicationSource defaultsource =
			communicationSources.get(communicationSources.size()-1);
		
		for (CommunicationSource source : communicationSources ) {
			if ( source instanceof CommunicatorFactoryObserver ) {
				final CommunicatorFactoryObserver observer = 
					(CommunicatorFactoryObserver) source;
				
				observer.setCommunicator(this);
				observer.setDefaultSource(defaultsource);
			}
				
		}
	}

	public String getMandt() {
		return tokenFinder.getToken().getMandt();
	}

	public String getSttag() {
		return df.format(new Date());
	}

	public String getUser() {
		return tokenFinder.getToken().getUser();
	}
}
