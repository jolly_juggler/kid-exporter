package com.entitec.jepkid.communicator;

public interface TokenObserver {
	
	/**
	 * Token wurde ung�ltig, d.h. kann nicht mehr verwendet werden
	 * 
	 * @param token
	 */
	void invalidate(Token token);

}
