package com.entitec.jepkid.communicator.bean;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.entitec.jepkid.communicator.bean.annotations.KIDArgs;
import com.entitec.jepkid.communicator.bean.annotations.KIDFunction;
import com.entitec.jepkid.communicator.bean.annotations.KIDResult;
import com.entitec.jepkid.communicator.bean.mapping.BeanAction;
import com.entitec.jepkid.communicator.bean.mapping.BeanActionWAttributes;
import com.entitec.jepkid.communicator.bean.mapping.BeanActionWDataModel;
import com.entitec.jepkid.communicator.bean.mapping.BeanException;
import com.entitec.jepkid.communicator.bean.mapping.BeanUtils2;
import com.entitec.wax.wdm.WDataModel;

public class BeanMappingTools {

	/** erzeugt anhand einer Methode (m) und der Methodenargument-objekte ein gefülltes WDataModel 
	 * unter Beachtung der Annotation KIDArgs an der Methode. 
	 * 
	 * Mit dieser Funktion wird das IN-View einer aquivalenten Kid Funktion gebildet.
	 * 
	 * Anm.: intern
	 * @throws BeanException */
	
	final public static WDataModel createInWDataModel(Method m, Object [] args) 
	throws BeanException {
		
		if ( args == null ) return null;
		
		KIDArgs a = m.getAnnotation(KIDArgs.class);
		Class [] parameterTypes = m.getParameterTypes();
		
		if ( a != null ) {
			ArrayList l = new ArrayList();
			for ( int i=0; i<a.value().length; i++ ) {
				BeanUtils2.pojoToDOM(parameterTypes[i], null, new BeanActionWAttributes(l), a.value()[i]);
			}
			WDataModel dm = BeanActionWAttributes.createWDataModel(l);
		
			for ( int i=0; i<a.value().length; i++ ) {
				BeanUtils2.pojoToDOM(parameterTypes[i], args[i], new BeanActionWDataModel(dm), a.value()[i]);
			}
			return dm;
			
		} else if ( args.length == 1 && BeanUtils2.isBean(parameterTypes[0])){
			ArrayList l = new ArrayList();
			BeanUtils2.pojoToDOM(parameterTypes[0], null, new BeanActionWAttributes(l));
			
			WDataModel dm = BeanActionWAttributes.createWDataModel(l);
			BeanUtils2.pojoToDOM(parameterTypes[0], args[0], new BeanActionWDataModel(dm));
			return dm;
		} else {
			throw new BeanException("Missing KIDArgs Annotaiton:"+m.getName());
		}
	}

	/** erzeugt anhand einer BeanKlasse ein leeres WDataModel */
	final public static WDataModel createOutWDataModel(Class cl) 
	throws BeanException {
		ArrayList l = new ArrayList();
		try {
			BeanUtils2.pojoToDOM(cl, cl.newInstance(), new BeanActionWAttributes(l));
		} catch (Exception e) {
			throw new BeanException(cl.getName(), e.getCause());
		}
		WDataModel dm = BeanActionWAttributes.createWDataModel(l);
		return dm;
	}

	/** anhand einer Methoden Signature wird eine Out DataModel erzeugt,
	 * derzeit wird nur der Fall "einfacher Datentyp" berücksichtigt. 
	 * */
	final public static WDataModel createOutWDataModel(Method method) 
	throws BeanException {
		Class resultType;
		if ( method.getReturnType().isAssignableFrom(List.class) == true ) {
			Type genericReturnType  = method.getGenericReturnType();
			if ( (genericReturnType instanceof ParameterizedType) == false) return null;
	
			ParameterizedType paraType = (ParameterizedType) genericReturnType;
			Type t[] = paraType.getActualTypeArguments();
			resultType = (Class) t[0];
		
		} else {
			resultType = method.getReturnType();
		}
	
		ArrayList l = new ArrayList();
		BeanAction action = new BeanActionWAttributes(l);
		action.set(method.getAnnotation(
				KIDResult.class).value(), null, resultType, null  
				);
		
		return BeanActionWAttributes.createWDataModel(l);
	}

	/** das return Object (bean) einer Methode (m) wird in ein bestehdendes WDataModel (dm) übernommen 
	 * handelt es sich beim return type der methode um ein array , so wird fuer jedes arrayelement eine 
	 * zeile im wdatamodel eingestellt.
	 * */
	final public static WDataModel updateWDataModel(Method m, Object bean, WDataModel dm ) 
	throws BeanException {
			if ( m.getReturnType().isArray() ) {
				Object [] beans = (Object []) bean;
				Class arrayReturnType = BeanUtils2.getArrayElementClass(m.getReturnType());
				BeanActionWDataModel action = new BeanActionWDataModel(dm);
				for ( int i=0; i < beans.length; i++ ) {
					BeanUtils2.pojoToDOM(arrayReturnType, beans[i], action);
					if ( i < beans.length-1 ) action.addRow();
				}
			} else if ( m.getReturnType().isAssignableFrom(List.class) ) {
				Type genericReturnType  = m.getGenericReturnType();
	
				if (genericReturnType instanceof ParameterizedType) {
					ParameterizedType paraType = (ParameterizedType) genericReturnType;
					Type t[] = paraType.getActualTypeArguments();
					Class listReturnType = (Class) t[0];	
					
					List list = (List) bean;
					BeanTools.updateWDataModel(list, listReturnType, dm);
				}
			} else {
				KIDResult a = m.getAnnotation(KIDResult.class);
				if ( a != null ) {
					BeanUtils2.pojoToDOM(m.getReturnType(), bean, new BeanActionWDataModel(dm), a.value());
				} else {
				BeanUtils2.pojoToDOM(m.getReturnType(), bean, new BeanActionWDataModel(dm));
				}
			}
			
		return dm;
	}

	/** ermittelt zu dem Namen einer Kid-Funktion die entsprechende Funktion
	 * aus einem Interface
	 */
	final public static Method getMethod(String kidfnktn, Class serviceclass ) {
		Method [] m = serviceclass.getMethods();
		for ( int i=0; i<m.length; i++) {
			KIDFunction kidFunction = m[i].getAnnotation(KIDFunction.class);
			if ( (kidFunction != null) && ( kidFunction.value().equalsIgnoreCase(kidfnktn)) ) {
				return m[i];
			} else if (m[i].getName().equalsIgnoreCase(kidfnktn)) {
				return m[i];
			}
		}
		for (Class i: serviceclass.getInterfaces()) {
			for (Method m2:i.getMethods()) {
				KIDFunction a = (KIDFunction) m2.getAnnotation(KIDFunction.class);
				if ( (a != null) && (a.value().equalsIgnoreCase(kidfnktn)) ) 
						return m2;
			}
		}
		return null;
	}

	final public static Object[] getArgsFromWDatamodel(Method m, WDataModel dm ) 
	throws BeanException {
		ArrayList<Object> l = new ArrayList<Object>();
		
		KIDArgs args = m.getAnnotation(KIDArgs.class);
		Class [] cls = m.getParameterTypes();
		
		for ( int i=0; i<cls.length; i++ ) {
			if ( BeanUtils2.isBean(cls[i]) ) {
				if ( args != null && args.value()[i] != null && !"".equals(args.value()[i]) ) {
					l.add(BeanUtils2.domToPOJO(new BeanActionWDataModel(dm), cls[i], args.value()[i]));
				} else {	
					l.add(BeanUtils2.domToPOJO(new BeanActionWDataModel(dm), cls[i]));
				}	
			} else {
				if ( args != null && args.value()[i] != null && !"".equals(args.value()[i]) ) {
					l.add(BeanTools.createValueObject(dm, cls[i], args.value()[i]));
				}	
			}
		}
		return l.toArray();
	}

}
