package com.entitec.jepkid.communicator.bean.mapping;

public interface BeanAction {
	
	public void set(String path, Object value, Class type, BeanAlias alias);
	
	public Object get(String path, Class type, BeanAlias alias);
}
