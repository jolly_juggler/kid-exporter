package com.entitec.jepkid.communicator.bean.annotations;

import java.lang.annotation.*;

@Retention (RetentionPolicy.RUNTIME)
@Target (ElementType.METHOD)

/** Verwendete Namen der KID-Attribute in der Reihenfolge der Aufrufparameter */
public @interface KIDArgs {
	String [] value ();
}