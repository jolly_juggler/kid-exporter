package com.entitec.jepkid.communicator.bean.annotations;

import java.lang.annotation.*;

@Retention (RetentionPolicy.RUNTIME)
@Target( { ElementType.TYPE, ElementType.METHOD } )

public @interface KIDExecuteFunction {
	String update() default "";
	String insert() default "";
	String delete() default "";
	String read()   default "";
}