package com.entitec.jepkid.communicator.bean;

import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;


import com.entitec.jepkid.communicator.bean.mapping.BeanActionTransformUtils;
import com.entitec.jepkid.communicator.bean.mapping.BeanActionWDataModel;
import com.entitec.jepkid.communicator.bean.mapping.BeanComparator;
import com.entitec.jepkid.communicator.bean.mapping.BeanException;
import com.entitec.jepkid.communicator.bean.mapping.BeanUtils2;
import com.entitec.jepkid.communicator.bean.mapping.Comparator2;
import com.entitec.wax.wdm.WDataModel;
import com.entitec.wax.wdm.WDataModelReader;
import com.entitec.wax.wdm.WDataModelWriter;

/*
 * Funktionalit�t f�r das Mapping von Bean auf EP/KID und umgekehrt. 
 */
public class BeanTools {
	
	private BeanTools() {
		;
	}
	
	
	/** erzeugt anhand eines WDataModel ein neues Pojo */
	final public static Object createPojo(WDataModel dm, Class cl) 
	throws BeanException {
		Object pojo = BeanUtils2.domToPOJO(new BeanActionWDataModel(dm), cl);
		return pojo;
	}
	
	/** erzeugt anhand eines WDataModel ein neues Array von Pojos, d.h.:f�r jede Zeile 
	 * im WDataModel ein neues Pojo im Array*/
	final public static Object [] createPojoArray(WDataModel dm, Class cl) 
	throws BeanException {
		WDataModelReader reader = new WDataModelReader(dm);
		int rows = reader.rowCount();
		
		Object [] pojos = (Object[]) Array.newInstance(cl, rows);
		
		for ( int i = 0 ; i < rows; i++ ) {
			reader.setRow(i);
			pojos[i] = createPojo(dm, cl);
		}
		return pojos;
	}

	
	/** erzeugt anhand eines WDataModel ein neue Liste von Pojos, d.h.:f�r jede Zeile 
	 * im WDataModel ein neues Pojo in der Liste*/
	final public static List createPojoList(WDataModel dm, Class cl) 
	throws BeanException {
		WDataModelReader reader = new WDataModelReader(dm);
		int rows = reader.rowCount();
		ArrayList<Object> pojos = new ArrayList<Object>();
		
		for ( int i = 0 ; i < rows; i++ ) {
			reader.setRow(i);
			Object o = createPojo(dm, cl);
			if ( o != null) pojos.add(o);
		}
		return pojos;
	}
	
	/** erzeugt f�r die Attributspalte <em>name</em> aus dem WDataModel <em>dm</em> eine Liste
	 * mit Objekten von Type <em>cl</em>
	 * 
	 * @param dm
	 * @param cl
	 * @param name
	 * @return
	 * @throws BeanException
	 */
	final public static List createValueList(WDataModel dm, Class cl, String name) 
	throws BeanException {
		WDataModelReader reader = new WDataModelReader(dm);
		int rows = reader.rowCount();
		ArrayList<Object> valueList = new ArrayList<Object>();
		
		for ( int i = 0 ; i < rows; i++ ) {
			reader.setRow(i);
			Object o = createValueObject(dm, cl, name);
			if ( o != null) valueList.add(o);
		}
		return valueList;
	}
	
		
	/** liefert aus dem WDataModel einen Wert zur�ck */
	final public static Object createValueObject(WDataModel dm, Class cl, String name) 
	throws BeanException {
		Object returnObject = (new BeanActionWDataModel(dm)).get(name.toUpperCase(), cl, null);
		return returnObject;
	}
	
	final public static Object [] createValueArray(WDataModel dm, Class cl, String name) 
	throws BeanException {
		WDataModelReader reader = new WDataModelReader(dm);
		int rows = reader.rowCount();
		
		Object [] values = (Object[]) Array.newInstance(cl, rows);
		
		for ( int i = 0 ; i < rows; i++ ) {
			reader.setRow(i);
			values[i] = createValueObject(dm, cl, name);
		}
		return values;
	}
	
	public static final void updateWDataModel(Collection c, Class cl, WDataModel dm) {
		BeanActionWDataModel actionWDM = new BeanActionWDataModel(dm);
		updateBeanActionWDataModel(c, cl, actionWDM);
	}

	public static final void updateWDataModel(Collection c, Class cl, WDataModelWriter dmw) {
		BeanActionWDataModel actionWDM = new BeanActionWDataModel(dmw);
		updateBeanActionWDataModel(c, cl, actionWDM);
	}
	
	private static final void updateBeanActionWDataModel(Collection l, Class cl, BeanActionWDataModel actionWDM) {
		final Iterator i = l.iterator();
		while ( i.hasNext() ) {
			Object o = i.next();
			BeanUtils2.pojoToDOM(cl, o, actionWDM);
			if ( i.hasNext() ) actionWDM.addRow();
		}
	}
	
	/**
	 * �bernimmt die Werte eines Pojos in ein WDataModel.
	 * 
	 * @param cl	Klasse des Pojos
	 * @param bean	Pojo
	 * @param dm	DataModel
	 */
	public static final void updateWDataModel(Class cl, Object bean, WDataModel dm) {
		BeanUtils2.pojoToDOM(cl, bean, new BeanActionWDataModel(dm));
	}

	final static public Comparator2 createComparator(Class cl, String [] sortedattribs, String separator) {
		return new BeanComparator(cl, sortedattribs, separator);			
	}
	
	final static public Comparator2 createComparator(Class cl) {
		return new BeanComparator(cl);			
	}


	/** bef�llt alle null-Werten in to mit entsprechenden werten aus from */
	final public static void fillupNullValues(Class cl, Object from, Object to) {
		if ( cl.isInstance(from) && cl.isInstance(to) ) {
			final Hashtable<String, Method> getters =
				BeanUtils2.getMethodsToGet(cl);
			final Hashtable<String, Method> setters = 
				BeanUtils2.getMethodsToSet(cl);
			
			final Iterator i = getters.keySet().iterator();
			while ( i.hasNext() ) {
				final Object key = i.next();
				final Method getter = (Method) getters.get(key);
				Object toValue;
				try {
					toValue = getter.invoke(to, (Object[]) null);
					if ( toValue == null ) {
						Object fromValue = getter.invoke(from, (Object[]) null);
						if ( fromValue != null ) {
							Method setter = (Method) setters.get(key);
							setter.invoke(to, new Object[]{fromValue});
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			} /*while end*/
		}
	}
	
	public static String transformJava(Object value, Class type) {
		return BeanActionTransformUtils.transformJava(value, type);
	}

}
