package com.entitec.jepkid.communicator.bean.annotations;

import java.lang.annotation.*;

@Retention (RetentionPolicy.RUNTIME)
@Target (ElementType.METHOD)

/** Resultwert einer Funktion zu KID-Attributname (wird nur bein elementarte Typen ben�tigt) */
public @interface KIDResult {
	String value ();
}