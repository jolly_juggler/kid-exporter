package com.entitec.jepkid.communicator.bean.annotations;

import java.lang.annotation.*;

@Retention (RetentionPolicy.RUNTIME)
@Target (ElementType.PARAMETER)

/** Verwendete Namen der KID-Attributes */
public @interface KIDParameter {
	String value ();
}