package com.entitec.jepkid.communicator.bean.mapping;

import java.util.Comparator;

public interface Comparator2 extends Comparator {
	
	String toString(Object o);
	String[] toStringArray(Object o);
	int matchedKeys(Object o1, Object o2);
}
