package com.entitec.jepkid.communicator.bean.mapping;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Hashtable;


/**
 * Erzeugt zu einem Bean einen Comparator der alle oder eine Auswahl der
 * Attribute des Beans berücksichtigt.
 * 
 * @author SD0024
 *
 */
public class BeanComparator implements Comparator2 {
	
	final private Class cl;
	final private String [] sortedAttribs;
	
	final private Hashtable<String, Method> getters;
	final private Hashtable<String, Method> setters;
	final private String separator;

	public BeanComparator(Class cl, String [] sortedattribs, String separator) {
		super();
		
		this.cl = cl;
		this.separator = separator;
		getters = BeanUtils2.getMethodsToGet(cl);
		setters = BeanUtils2.getMethodsToSet(cl);

		if (sortedattribs == null) {
			this.sortedAttribs = createAllAttribs(setters);
		} else {
			this.sortedAttribs = sortedattribs;
		}
	}
	
	private static final String [] createAllAttribs(Hashtable<String, Method> setters) {
		ArrayList<String> l = new ArrayList<String>(setters.keySet());
		return (String[]) l.toArray(new String[] {});
	}
	
	public BeanComparator(Class cl) {
		this(cl, null, " ");
	}
		
	public int compare(Object o1, Object o2) {
		if ( cl.isInstance(o1) && cl.isInstance(o2) ) {
			int v = 0;
			for ( int i=0; i < sortedAttribs.length; i++ ) {
				Method m = (Method) getters.get(sortedAttribs[i]);
				v = compareMethodValue(m, o1, o2);
				if ( v != 0 ) 	return v;
			}
			return v;
		} else {
			return -1;
		}
	}
	
	/** vergleicht den returnwert zweiter methoden */
	private final static int compareMethodValue(Method m, Object o1, Object o2) {
		try {
			Object v1 = m.invoke(o1, (Object[])null);
			Object v2 = m.invoke(o2, (Object[])null);
			if ( (v1==null) && (v2==null) ) return 0;
			if ( v1 == null ) return -1;
			if ( v2 == null ) return +1;
			
			if ( Comparable.class.isInstance(v1) ) {
				return ((Comparable)v1).compareTo(v2);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return -1; }
		return -1;
	}
/*
	public int compareIgnoreNullValues(Object o1, Object o2) {
		if ( cl.isInstance(o1) && cl.isInstance(o2) ) {
			int v = 0;
			for ( int i=0; i < sortedAttribs.length; i++ ) {
				Method m = (Method) getters.get(sortedAttribs[i]);
				v = compareMethodValueIgnoreNullValues(m, o1, o2);
				if ( v != 0 ) 	return v;
			}
			return v;
		} else {
			return -1;
		}
	}
*/
	/** vergleicht den returnwert zweiter methoden, ignoriert Null-Werte */
	private final static int compareMethodValueIgnoreNullValues(Method m, Object o1, Object o2) {
		try {
			Object v1 = m.invoke(o1, (Object[])null);
			Object v2 = m.invoke(o2, (Object[])null);
			if ( (v1==null) && (v2==null) ) return 0;
			if ( v1 == null ) return 0;
			if ( v2 == null ) return 0;
			
			if ( Comparable.class.isInstance(v1) ) {
				return ((Comparable)v1).compareTo(v2);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return -1; }
		return -1;
	}
	/** gibt zurück, wieviele Attribute der beiden Beans übereinstimmen */
	public int matchedKeys(Object o1, Object o2) {
		int matches=0;
		int v = 0;
		if ( cl.isInstance(o1) && cl.isInstance(o2) ) {
			for ( int i=0; i < sortedAttribs.length; i++ ) {
				Method m = (Method) getters.get(sortedAttribs[i]);
				v = compareMethodValue(m, o1, o2);
				if ( v == 0) matches++; else break;
			}
		}
		return matches;
	}
	
	/** rueckgabe er attributname mit uebereinstimmenden werten */
	public String[] matchedKeyArray(Object o1, Object o2) {
		ArrayList<String> returnArray = new ArrayList<String>();
		if ( cl.isInstance(o1) && cl.isInstance(o2) ) {
			for ( int i=0; i < sortedAttribs.length; i++ ) {
				Method m = (Method) getters.get(sortedAttribs[i]);
				if ( compareMethodValue(m, o1, o2) == 0) returnArray.add(sortedAttribs[i]);
			}
		}
		return (String[]) returnArray.toArray(new String[] {});
	}


	public String toString(Object o) {
		StringBuffer text = new StringBuffer();
		try {
		for ( int i=0; i < sortedAttribs.length; i++ ) {
			if ( i > 0 ) text.append(" - ");
			
			Method getter = (Method) getters.get(sortedAttribs[i]);
			Object value = getter.invoke(o,(Object[]) null);
			if ( value != null ) text.append(value.toString());
		}
		} catch (Exception e) {;}

		return text.toString();
	}
	
	public String[] toStringArray(Object o) {
		ArrayList<String> l = new ArrayList<String>();
		try {
			for ( int i=0; i < sortedAttribs.length; i++ ) {
				Method m = (Method) getters.get(sortedAttribs[i]);
				Class type = m.getReturnType(); 
			
				if ( String.class.equals(type)) {
					String s = (String) m.invoke(o, (Object[]) null); 
					l.add(s);
				}
			}
		} catch(Exception e) {;}
		return l.toArray(new String[] {});
	}
/*
	public Object newInstance(Object o) {
		try {
		if ( cl.isInstance(o) ) {
			Object o2 = cl.newInstance();
			int v = 0;
			for ( int i=0; i < sortedAttribs.length; i++ ) {
				Method m = (Method) getters.get(sortedAttribs[i]);
				Method m2 = (Method) setters.get(sortedAttribs[i]);
				m2.invoke(o2, m.invoke(o, new Object[] {null}));
			}
			return o2;
		} else {
			return null;
		}
		} catch (Exception e) {
			return null;
		}
	}
*/
}
