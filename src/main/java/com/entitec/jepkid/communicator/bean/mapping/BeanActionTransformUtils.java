package com.entitec.jepkid.communicator.bean.mapping;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.entitec.wax.wdm.WDMTypChecker;

public class BeanActionTransformUtils {
	public static Object defaultJavaValue(Class toclass) {
		Object o = null;
		if ( toclass.equals(String.class))
			o = "".toString();
		else if ( toclass.equals(Integer.class) ) 
			o = new Integer(0);
		else if ( toclass.equals(Long.class) ) 
			o = new Long(0);
		else if ( toclass.equals(Double.class) ) 
				o = new Double(0.0);
		else if ( toclass.equals(Boolean.class) ) 
			o = new Boolean(false);
		else 
			o = null;
		
		return o;
	}

	public static String transformJava(Object value, Class type) {
		String o;
		if ( type.equals(Boolean.class)) {
			o = toKidString((Boolean) value);
		} else if ( type.equals(Calendar.class) ) {
			o = toKidString((Calendar) value);
		} else if ( type.equals(Date.class) ) {
			o = toKidString((Date) value);
		}
		else {
			o = value.toString();
		}
		return o;
	}
	
	/** transformiert ein KID Datenwert, in das JAVA equivalent */ 
	public static Object transformKid(Object kidvalue, Class toclass) {
		if ( kidvalue == null ) return null;
		
		Object o = null;
		if ( toclass.equals(String.class))
			o = kidvalue.toString();
		else if ( toclass.equals(Integer.class) ) 
			o = toInteger(kidvalue.toString());
		else if ( toclass.equals(Long.class) ) 
			o = toLong(kidvalue.toString());
		else if ( toclass.equals(Double.class) ) 
				o = toDouble(kidvalue.toString());
		else if ( toclass.equals(Boolean.class) ) 
			o = toBoolean(kidvalue.toString());
		else if ( toclass.equals(Calendar.class) ) 
			o = toCalendar(kidvalue.toString());
		else if ( toclass.equals(Date.class) ) 
			o = toDate(kidvalue.toString());
		else {
			o = kidvalue.toString();
		}
		return o;
	}
	public static Integer toInteger(String str) {
		
		String integerValue = WDMTypChecker.getIntegerPart(str);
		
		if ( str.contains("-") == true ) 
			return new Integer("-"+integerValue);
		else
			return new Integer(integerValue);
	}
	
	public static Long toLong(String str) {
		
		String integerValue = WDMTypChecker.getIntegerPart(str);
		
		if ( str.contains("-") == true ) 
			return new Long("-"+integerValue);
		else
			return new Long(integerValue);
	}
	
	
	public static Double toDouble(String str) {
		String doubleValue = WDMTypChecker.getDecimal(str);
		
		return new Double(doubleValue);
	}
	
	public static Boolean toBoolean(String str) {
		if ( str.equalsIgnoreCase("J"))
			return new Boolean(true);
		else
			return new Boolean(false);
	}
	
	public static String toKidString(Boolean b) {
		if ( b.booleanValue() == true ) 
			return "J";
		else
			return "N";
	}
		
	/** transformiert Java Calendar nach KID Datum (JJJJMMTT) */
	public static String toKidString(Calendar d) {
		int year = d.get(Calendar.YEAR);
		int month = d.get(Calendar.MONTH)+1;
		int day = d.get(Calendar.DAY_OF_MONTH);
		
		StringBuffer sb = new StringBuffer(""+year);
		if ( month < 10 )
			sb.append("0"+month);
		else
			sb.append(month);
		
		if ( day < 10 )
			sb.append("0"+day);
		else
			sb.append(day);
		return sb.toString();
	}

	/** transformiert Java Calendar nach KID Datum (JJJJMMTT) */
	public static String toKidDate(Calendar d) {
		return toKidString(d);
	}
	
	/** transformiert Java Calendar nach KID Zeit (HHMMSS) */
	public static String toKidTime(Calendar cal) {
		int iHour = 0;
		int iMinute = 0;
		int iSecond = 0;
		
		if (cal != null) {
			iHour = cal.get(Calendar.HOUR_OF_DAY);
			iMinute = cal.get(Calendar.MINUTE);
			iSecond = cal.get(Calendar.SECOND);
		}
		String sHour     = "00";
		String sMinute   = "00";
		String sSecond   = "00";
		
		sHour = sHour+String.valueOf(iHour);
		sHour = sHour.substring(0+String.valueOf(iHour).length(),2+String.valueOf(iHour).length());
		sMinute = sMinute+String.valueOf(iMinute);
		sMinute = sMinute.substring(0+String.valueOf(iMinute).length(),2+String.valueOf(iMinute).length());
		sSecond = sSecond+String.valueOf(iSecond);
		sSecond = sSecond.substring(0+String.valueOf(iSecond).length(),2+String.valueOf(iSecond).length());
		
		return sHour+sMinute+sSecond;
			
	}

	/** transformiert Java Date nach KID Datum (JJJJMMTT) */
	public static String toKidString(Date d) {
		Calendar c = Calendar.getInstance();
		c.setTime(d);
		return toKidString(c);
	}
	
	/** transformiert eine Datum im KIDFormat (JJJJMMTT), in ein Calendar Java Type */
	public static Calendar toCalendar(String str) {
		int year = Integer.parseInt(str.substring(0,4));
		int month = Integer.parseInt(str.substring(4,6));
		int day = Integer.parseInt(str.substring(6,8));
		
		return new GregorianCalendar(year,month-1,day);
	}
	
	/** transformiert eine Datum im KIDFormat (JJJJMMTT), in ein DATE Java Type */
	public static Date toDate(String str) {
		return new Date(toCalendar(str).getTimeInMillis());
	}
}
