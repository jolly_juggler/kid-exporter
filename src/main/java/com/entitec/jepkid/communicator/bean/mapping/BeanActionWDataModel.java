package com.entitec.jepkid.communicator.bean.mapping;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.entitec.wax.wdm.IWDataItem;
import com.entitec.wax.wdm.WDMTypChecker;
import com.entitec.wax.wdm.WDataModel;
import com.entitec.wax.wdm.WDataModelReader;
import com.entitec.wax.wdm.WDataModelWriter;

public class BeanActionWDataModel implements BeanAction {
	
	public static boolean constAvoidNullValues = false;
	WDataModelWriter writer;
	WDataModelReader reader;

	public BeanActionWDataModel(WDataModel dm) {
		writer = new WDataModelWriter(dm);
		reader = new WDataModelReader(dm);
	}
	
	public BeanActionWDataModel(WDataModelWriter dmw) {
		writer = dmw;
		reader = dmw;
	}

	public void set(String path, Object value, Class type, BeanAlias alias) {
		if ( path != null ) path = path.toUpperCase();
		if ( value != null && writer.hasAttribute(path)) {
			if ( writer.curRowNr() == -1 ) writer.setRow(0);
			
			String o = BeanActionTransformUtils.transformJava(value, type);
			writer.setValue(path, o, IWDataItem.ENABLED);
		}
	}
	
	/** fuegt eine Zeile hinzu */
	public void addRow() {
		writer.addRow();
	}

	public Object get(String path, Class type, BeanAlias alias) {
		//TODO hier sollte eine WRuntimeException abgefangen werden
		if ( reader.hasAttribute(path)) {
			short state = reader.getState(path);
			if ( (state == IWDataItem.ENABLED) || (state == IWDataItem.UNCHANGED) ) {
				return BeanActionTransformUtils.transformKid(reader.getValue(path), type);
			} else {
				if ( constAvoidNullValues ) {
					return BeanActionTransformUtils.defaultJavaValue(type);
				} else {
					return (null);
				}
			}
		}	
		return null;
	}
	
	
}
