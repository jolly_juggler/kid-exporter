package com.entitec.jepkid.communicator.bean.annotations;

import java.lang.annotation.*;

@Retention (RetentionPolicy.RUNTIME)
@Target (ElementType.METHOD)


public @interface KIDDecl {
	String name();
	char typ();
	int len();
	int dec();
	boolean sign();
}