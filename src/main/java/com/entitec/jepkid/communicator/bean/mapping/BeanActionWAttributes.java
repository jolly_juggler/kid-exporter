package com.entitec.jepkid.communicator.bean.mapping;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import com.entitec.wax.wdm.WAttribute;
import com.entitec.wax.wdm.WDataModel;
import com.entitec.wax.wdm.WDataModelWriter;
import com.entitec.wax.wdm.WSignature;

public class BeanActionWAttributes implements BeanAction {
	Collection c;
	
	public BeanActionWAttributes(Collection c) {
		this.c = c;
	}

	public void set(String path, Object value, Class type, BeanAlias alias) {

		path = path.toUpperCase();
		int len = 255;
		if (alias != null) {
			len = alias.getLen();
		}
		
		WAttribute a;
		if ( String.class.equals(type)) {
			a = new WAttribute(path, path, 'C', len, 0, false);
		} else if ( Integer.class.equals(type) ) {
			a = new WAttribute(path, path, 'D', 8, 0, true);
		} else if ( Double.class.equals(type) ) {
			a = new WAttribute(path, path, 'D', 16, 4, true);
		} else if ( Boolean.class.equals(type) ) {
			a = new WAttribute(path, path, 'C', 1, 0, false);
		} else if ( Date.class.equals(type) ) {
			a = new WAttribute(path, path, 'C', 8, 0, false);
		} else if ( Calendar.class.equals(type) ) {
				a = new WAttribute(path, path, 'C', 8, 0, false);
		} else {
			a = new WAttribute(path, path, 'C', 255, 0, false);
		}
		c.add(a);
	}

	public Object get(String path, Class type, BeanAlias alias) {
		// wird nicht verwendet
		return null;
	}
	
	public WDataModel createWDataModel() {
		return createWDataModel(c);
	}
	
	final public static WDataModel createWDataModel(Collection c) {
		WSignature sig = new WSignature();
		Iterator i = c.iterator();
		while (i.hasNext()) {
			WAttribute a = (WAttribute) i.next();
			sig.addAttribute(a);
		}
		/*
		sig.addAttribute(new WAttribute("MANDT", "MANDT", 'N', 2, 0, false));
		sig.addAttribute(new WAttribute("STTAG", "STTAG", 'N', 8, 0, false));
		*/
		
		WDataModelWriter writer = new WDataModelWriter(new WDataModel(sig));
		return writer.getDataModel(); 	
	}
}
