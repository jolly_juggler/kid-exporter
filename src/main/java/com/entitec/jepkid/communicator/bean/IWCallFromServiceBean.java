package com.entitec.jepkid.communicator.bean;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.entitec.jepkid.communicator.bean.mapping.BeanException;
import com.entitec.wax.wcall.IWCall;
import com.entitec.wax.wcall.WCallException;
import com.entitec.wax.wcall.WSuccess;
import com.entitec.wax.wdm.WDataModel;

/** IWCall zu ServiceBean */
public class IWCallFromServiceBean implements IWCall {

	private final Class serviceClass;
	private final Object serviceObject;

	public IWCallFromServiceBean(Class serviceInterfaces, Object serviceObject) {
		this.serviceClass = serviceInterfaces;
		this.serviceObject = serviceObject;
	}
	
	public WSuccess process(String function, WDataModel in, WDataModel out)
	throws WCallException {
		final Method m = BeanMappingTools.getMethod(function, serviceClass);
		if ( m == null ) return new WSuccess("99", "K002");
		Object ret = null;
		try {
			Object [] args = BeanMappingTools.getArgsFromWDatamodel(m, in);
			try {
				ret = m.invoke(serviceObject, args);
			} catch (IllegalArgumentException e) {
				return new WSuccess("99", "K001");
			} catch (IllegalAccessException e) {
				e.printStackTrace();
				return new WSuccess("99", "K001");
				
			} catch (InvocationTargetException e) {
				e.printStackTrace();
				return new WSuccess("99", "K001");
			}
		} catch (BeanException e) {
			e.printStackTrace();
			return new WSuccess("99");
		}

		if ( m.getReturnType() != void.class ) {
			if ( ret == null ) return new WSuccess("99");
			try {
				BeanMappingTools.updateWDataModel(m, ret, out);
			} catch (BeanException e) {
				e.printStackTrace();
			}
		}
		return new WSuccess("00");
	}
}
