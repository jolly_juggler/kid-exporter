package com.entitec.jepkid.communicator.bean.mapping;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import com.entitec.jepkid.communicator.bean.annotations.InternalValueId;
import com.entitec.jepkid.communicator.bean.annotations.KIDDecl;
import com.entitec.jepkid.communicator.bean.annotations.KIDDeclField;
import com.entitec.jepkid.communicator.bean.annotations.MethodSeqNo;

public class BeanUtils2 {

	final static public BeanAction pojoToDOM(Class cl, Object bean, BeanAction action) {
		return __pojoToDOM(cl, bean, action, "", null);
	}
	
	final static public BeanAction pojoToDOM(Class cl, Object bean, BeanAction action, String prefix) {
		return __pojoToDOM(cl, bean, action, prefix, null);
	}
	
	final static private BeanAction __pojoToDOM(Class cl, Object bean, BeanAction action,  
		String prefix, BeanAlias alias ) {
		
		if ( cl == null ) cl = bean.getClass();
		
		if ( isBean(cl)) {
			Hashtable methods = getMethodsToGet(cl);
			
			Iterator i = methods.keySet().iterator();
			while (i.hasNext()) {
				String name = (String) i.next();
				Method getter = (Method) methods.get(name);
				
				Object value = null;
				try {
					if (bean != null) value = getter.invoke(bean, (Object[]) null);
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}
				
				Class retCl = getter.getReturnType();
				
				BeanAlias myAlias = getBeanAlias(cl, getter);
				if ( (myAlias != null) && (myAlias.getName() != null) && (myAlias.getName().equals("") == false))
					name = myAlias.getName();
				
				if ( retCl.isArray()) {
					Object [] vs = (Object[]) value;
					for ( int ii=0; ii < vs.length; ii++ )
						if ( vs[ii] != null ) {
							__pojoToDOM(getArrayElementClass(retCl), vs[ii], action, getAttribName(prefix, name)+"["+ii+"]", myAlias);
						}	
				} else if ( isBean(retCl)) {
					if ( value != null ) {
						__pojoToDOM(retCl, value, action, getAttribName(prefix, name), myAlias);
					}
				} else if ( action != null ) {
					action.set(getAttribName(prefix, name), value, retCl, myAlias);
				}
			}
		} else if ( cl.isArray() ) {
			Object [] vs = (Object[]) bean;
			for ( int ii=0; ii < vs.length; ii++ )
				if ( vs[ii] != null ){
					__pojoToDOM(cl, vs[ii], action, prefix+"["+ii+"]", alias);
				}
		} else if ( action != null){
			action.set(prefix, bean, cl, alias);
		}
		return action;
	}
		
	final static public Object domToPOJO(BeanAction action, Class cl) 
	throws BeanException {
		return __domToPOJO(action, cl, null);
	}
	
	final static public Object domToPOJO(BeanAction action, Class cl, String prefix) 
	throws BeanException {
		return __domToPOJO(action, cl, prefix);
	}

	final static private Object __domToPOJO(BeanAction action, Class cl, String prefix) 
	throws BeanException  
	{
		
		Object bean = null;
		try {
			bean = cl.newInstance();
			boolean setterWasCalled = false;
		
		
			Hashtable methods = getMethodsToSet(cl);
			Iterator i = methods.keySet().iterator();
			while (i.hasNext()) {
				String name = (String) i.next();
				Method setter = (Method) methods.get(name);
				
				BeanAlias myAlias = getBeanAlias(cl, setter);
				if ( (myAlias != null) && (myAlias.getName() != null) && 
						(myAlias.getName().equals("") == false))
					name = myAlias.getName();
				
				Class [] parameters = setter.getParameterTypes();
				if ( (parameters == null) || (parameters.length > 1) ) continue;
				
				if ( parameters[0].isArray()) {
					Class arrayClass = getArrayElementClass(parameters[0]);
					int arrayLength = 3;
					Object [] array = (Object[]) Array.newInstance(arrayClass, arrayLength); 
					
					for ( int ii=0; ii<arrayLength; ii++) {
						array[ii] = __domToPOJO( action, arrayClass, 
								getAttribName(prefix,name)+"["+ii+"]");
					}
					setter.invoke(bean, new Object[]{array});
				} else if ( isBean(parameters[0])) {
						Object pojo = __domToPOJO( action, parameters[0], 
								getAttribName(prefix,name));
						setter.invoke(bean, new Object[]{pojo});
						
				} else {
					Object value = null;
					if (action != null )
						value = action.get(getAttribName(prefix, name), parameters[0], myAlias);
					if ( value != null ) {
						setter.invoke(bean, new Object [] {value});
						setterWasCalled = true;
					}
				}
			}
			if ( setterWasCalled == false ) {
				bean = null;
			}
		} catch (InstantiationException e) {
			throw new BeanException(cl.getName(), e.getCause());
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) { 
			e.printStackTrace();
		} catch (InvocationTargetException e) { 
			e.printStackTrace();
		}
		return bean;
	}
	
	final private static BeanAlias getBeanAlias(Class cl, Method method) {
		BeanAlias alias = null;
	
		final String name = method.getName();
		
		KIDDecl decl = method.getAnnotation(KIDDecl.class);
		if ( decl != null ) {
			alias = new BeanAlias(decl.name(), ""+decl.typ(), decl.len());
		} else {
			String fieldname = name.substring(3,4).toLowerCase()+name.substring(4);
			try {
				Field field = cl.getDeclaredField(fieldname);
				KIDDeclField fielddecl = field.getAnnotation(KIDDeclField.class);
				if ( fielddecl != null )
					alias = new BeanAlias(fielddecl.name(), ""+fielddecl.typ(), fielddecl.len());
			} catch (Exception e) {;}
		}
		return alias;
	}
	

	final static private String getAttribName(String prefix, String name) {
		if ( (prefix == null) || ("".equals(prefix)))
			return name.toUpperCase();
		else if (prefix.endsWith("-"))
			return (prefix+name).toUpperCase();
		else
			return (prefix+"-"+name).toUpperCase();
	}
		
	final static public Class getArrayElementClass(Class cl) {
		try {
			return Class.forName(cl.getName().substring(2, cl.getName().lastIndexOf(';')));
		} catch (ClassNotFoundException e) { ; }
		return null;
	}
		
	final static public boolean isBean(Class cl) {
		if ( String.class.equals(cl)   ||
			 String[].class.equals(cl) ||
			 Integer.class.equals(cl)  ||
			 Integer[].class.equals(cl)  ||
			 Long.class.equals(cl)  ||
			 Long[].class.equals(cl)  ||
			 Double[].class.equals(cl)  ||
			 Double.class.equals(cl )  ||
			 Boolean[].class.equals(cl)  ||
			 Boolean.class.equals(cl )  ||
			 Calendar[].class.equals(cl)  ||
			 Calendar.class.equals(cl )  ||
			 Date[].class.equals(cl)  ||
			 Date.class.equals(cl ) 
			 
		 //???|| cl.isArray()
		
		) 
		{
			return false;
		} else {
			return true;
		}
	}
		
	final static private Hashtable<String, Method> _getMethodsWithPrefix(Class cl, String prefix) {
		Hashtable<String, Method> result = new Hashtable<String, Method>();
		Method [] m = cl.getMethods();
		for (int i=0; i < m.length ; i++ ) {
			String name = m[i].getName();
			if ( name.startsWith(prefix) && (name.equals("getClass") == false)) {
				result.put(name.substring(3).toLowerCase(), m[i]);
			}
		}
		return result;
	}
	final static public Hashtable<String, Method> getMethodsToSet(Class cl) {
		return _getMethodsWithPrefix(cl, "set");
	}
	
	final static public Hashtable<String, Method> getMethodsToGet(Class cl) {
		return _getMethodsWithPrefix(cl, "get");
	}
	
	/** liste von feldnamen der internen Id. zu feldname auf die diese verweisen */
	final static public Hashtable<String, String> getInternaIds(Class cl) {
		Hashtable<String, String > internalIds = 
			new Hashtable <String, String>(); 
		
		for ( Field field : cl.getDeclaredFields() ) {
			final InternalValueId internalId = field.getAnnotation(InternalValueId.class); 
			if (  internalId != null ) {
				internalIds.put(field.getName().toLowerCase(), internalId.value().toLowerCase());
			}
		}
		return internalIds;
	}
	
	final static public List<Method> getGetters(Class cl) {
		final List<Method> r = new ArrayList<Method>();
		final Method [] m = cl.getMethods();
		for (int i=0; i < m.length ; i++ ) {
			final String name = m[i].getName();
			if ( name.startsWith("get") && (name.equals("getClass") == false)) {
				r.add(m[i]);
			}
		}
		sortBySequenceNo(r);
		return r;
	}
	
	final static private void sortBySequenceNo(List<Method> methods) {
		Collections.sort(methods, new Comparator<Method> () {

			public int compare(Method o1, Method o2) {
				final MethodSeqNo a1 = 
					o1.getAnnotation(MethodSeqNo.class);
				
				final MethodSeqNo a2 = 
					o2.getAnnotation(MethodSeqNo.class);
				
				if ( (o1 == null) && (o2 == null) ) 
					return 0;
				else if ( o1 == null ) 
					return -1; 
				else if ( o2 == null ) 
					return +1;
				else if ( (a1 == null) && ( a2 == null) )
					return o1.getName().compareTo(o2.getName());
				else if ( a1 == null )
					return +1;
				else if ( a2 == null )
					return -1;
				else 
					return a1.value() - a2.value();
			}
		});
	}
	
}
