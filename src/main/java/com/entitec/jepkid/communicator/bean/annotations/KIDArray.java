package com.entitec.jepkid.communicator.bean.annotations;

import java.lang.annotation.*;

@Retention (RetentionPolicy.RUNTIME)
@Target (ElementType.METHOD)


public @interface KIDArray {
	String name ();
	int maxlength ();
}

