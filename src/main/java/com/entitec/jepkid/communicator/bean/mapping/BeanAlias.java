package com.entitec.jepkid.communicator.bean.mapping;

public class BeanAlias {
	
	private String name;
	boolean isArrayAlias;
	
	private int len = 255;
	private int arrayLength = 3;


	public BeanAlias(String name, String typ, int len) {
		this.isArrayAlias = false;
		this.name = name;
		this.len = len;
	}
	
	public BeanAlias(String name, int arrayLength ) {
		this.isArrayAlias = true;
		this.name = name;
		this.arrayLength = arrayLength;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getLen() {
		return len;
	}
	
	public int getArrayLength() {
		return arrayLength;
	}
	
}
