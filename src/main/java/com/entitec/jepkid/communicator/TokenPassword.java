package com.entitec.jepkid.communicator;

public interface TokenPassword extends Token {
	
	String getPassword();
	
	String getGroup();

}
