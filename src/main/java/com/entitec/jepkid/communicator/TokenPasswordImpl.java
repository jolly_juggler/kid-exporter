package com.entitec.jepkid.communicator;

public class TokenPasswordImpl extends AbstractTokenImpl implements TokenPassword {
	
	private String password;
	private String group;	

	public TokenPasswordImpl(String user, String password, String group) {
		super(user);
		this.password = password;
		this.group = group;
	}
	
	public TokenPasswordImpl(String user, String password, String group, String mandt) {
		super(user, mandt);
		this.password = password;
		this.group = group;
	}
	
	public TokenPasswordImpl(String user, String password, String group, String mandt, boolean traceEnabled) {
		super(user, mandt, traceEnabled);
		this.password = password;
		this.group = group;
	}

	@Override
	public String getPassword() {
		return this.password;
	}
	
	@Override
	public String getGroup() {
		return this.group;
	}
	
}
