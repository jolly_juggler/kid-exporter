package com.entitec.jepkid.communicator;


/**
 * 
 * @author sd0024
 *
 */
public interface TokenFinder {

	Token getToken();
}
