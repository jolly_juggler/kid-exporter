package com.entitec.jepkid.communicator;

public interface TokenJWT extends Token {
	
	String getJWT();
	
}
