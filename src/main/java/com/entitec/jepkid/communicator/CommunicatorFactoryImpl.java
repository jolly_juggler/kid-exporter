package com.entitec.jepkid.communicator;

import java.util.ArrayList;
import java.util.List;

import com.entitec.jepkid.communicator.source.PooledConnectionSourceImpl;
import com.entitec.jepkid.communicator.source.SingleConnectionSourceImpl;
import com.entitec.jepkid.communicator.source.TcpConnectionHandlerImpl;

/**
 * Communicator Factory.
 * 
 * Erzeugt f�r verschiedene Anwendungsf�lle die Communicator-Instanz.
 * 
 * @author sd0024
 *
 */
public class CommunicatorFactoryImpl implements CommunicatorFactory {
	
	private boolean isStarted = false;
	
	private List<PooledConnectionSourceImpl> pooledConnectionSources = 
		new ArrayList<PooledConnectionSourceImpl>();

	boolean useSSL() {
		return (System.getProperties().getProperty("useSSL") != null);
	}
	
	/**
	 * Communicator zu einem entfernten EP/KID Server.
	 * <p/>
	 * 
	 * <li/>eine Verbindung zur Zeit
	 * <li/>pro Aufruf ein Verbindungsaufbau und -abbau
	 * <li/>Anmeldung mit einen vorgegebenen Benutzer
	 *  
	 * @param user	Name des Benutzers
	 * @param group	Gruppe des Benutzers
	 * @param password	Kennwort des Benutzers
	 * @param mandt	Mandt
	 * @param host	Name des EP/KID Servers
	 * @param port	Port an dem der EP/KID Servers Anfragen erwartet
	 * @return	Communicator
	 */
	public Communicator createDefault
		(String user, String group, String password, String mandt, 
				String host, int port) {
		return 
			new CommunicatorImpl(
					new TokenPasswordImpl(user, password, group, mandt),
					new SingleConnectionSourceImpl(
						new TcpConnectionHandlerImpl(host, port))
					);
	}
	
	/**
	 * Communicator zu einem entfernten EP/KID Server.
	 * <p/>
	 * 
	 * <li/>eine Verbindung zur Zeit
	 * <li/>pro Aufruf ein Verbindungsaufbau und -abbau
	 * <li/>Benutzer zur Anmeldung wird bei jedem Aufruf ermittelt
	 *  
	 * @param tokenFinder	Ermittlung der Benutzerdaten
	 * @param host	Name des EP/KID Servers
	 * @param port	Port an dem der EP/KID Servers Anfragen erwartet
	 * @return	Communicator
	 */
	public Communicator createDefault(TokenFinder tokenFinder, String host, int port, String codeset ) {
		return createDefault(tokenFinder, host, port, codeset, useSSL());
	}

	public Communicator createDefault
		(TokenFinder tokenFinder, String host, int port, String codeset, boolean useSSL) {
		return 
			new CommunicatorImpl(
					tokenFinder,
					new SingleConnectionSourceImpl(
						new TcpConnectionHandlerImpl(host, port, codeset, null, useSSL))
					);
	}

	public Communicator createDefault(TokenFinder tokenFinder, 
				CommunicationSource source, 
				String host, int port, String codeset ) {
		return createDefault(tokenFinder, source, host, port, codeset, useSSL());
	}

	public Communicator createDefault(TokenFinder tokenFinder, 
				CommunicationSource source, 
				String host, int port, String codeset, boolean useSSL) {
		
		final CommunicationSource defaultsource =
			new SingleConnectionSourceImpl(
					new TcpConnectionHandlerImpl(host, port, codeset, null, useSSL));
		
		final List<CommunicationSource> sources = 
			new ArrayList<CommunicationSource>();
		
		sources.add(source);
		sources.add(defaultsource);
		
		final Communicator communicator =
			new CommunicatorImpl(tokenFinder, sources);
		
		return communicator;
	}

	public Communicator createDefault(TokenFinder tokenFinder,
			List<CommunicationSource> sources) {
		return new CommunicatorImpl(tokenFinder, sources);
	}

	public Communicator createPooled(TokenFinder tokenFinder,
			String host, int port, String codeset, String header,
			int maxpoolsize, int secs) {
		return createPooled(tokenFinder, host, port, codeset, header, maxpoolsize, secs, useSSL());
	}

	public Communicator createPooled(TokenFinder tokenFinder,
			String host, int port, String codeset, String header,
			int maxpoolsize, int secs, boolean useSSL) {
		
		PooledConnectionSourceImpl pooledConnection =
			new PooledConnectionSourceImpl(
					new TcpConnectionHandlerImpl(host, port, codeset, header, useSSL),
					maxpoolsize, secs);
		
		if ( isStarted ) {
			pooledConnection.start();
		}
		pooledConnectionSources.add(pooledConnection);
		
		return 
			new CommunicatorImpl(
					tokenFinder,
				pooledConnection
				);
	}
	
	@Override
	public Communicator createPooled(TokenFinder tokenFinder, String host, int port, int maxpoolsize, int secs) {
		return createPooled(tokenFinder, host, port, null, null, maxpoolsize, secs);
	}
	
	@Override
	public Communicator createPooled(TokenFinder tokenFinder, String host, int port, String codeset, int maxpoolsize,
			int secs) {
		return createPooled(tokenFinder, host, port, codeset, null, maxpoolsize, secs);
	}

	public Communicator createPooled(String user, String group,
			String password, String mandt, String host, int port, String codeset, String header,
			int maxpoolsize, int secs) {
				return createPooled(user, group, password, mandt, host, port, codeset, header, maxpoolsize, secs, useSSL());
	}
	
	public Communicator createPooled(String user, String group,
			String password, String mandt, String host, int port, String codeset, String header,
			int maxpoolsize, int secs, boolean useSSL) {
		
		PooledConnectionSourceImpl pooledConnection =
			new PooledConnectionSourceImpl(
					new TcpConnectionHandlerImpl(host, port, codeset, header, useSSL),
					maxpoolsize, secs);
		
		if ( isStarted ) {
			pooledConnection.start();
		}
		pooledConnectionSources.add(pooledConnection);
		
		return 
			new CommunicatorImpl(
				new TokenPasswordImpl(user, password, group, mandt),
				pooledConnection
				);
	}
	
	public Communicator createPooled(String user, String group,
			String password, String mandt, String host, int port,
			int maxpoolsize, int secs) {
	
		return 
			createPooled(user, group, password, mandt, host, port, null, maxpoolsize, secs);
	}
	
	public Communicator createDefault(String user, String group,
			String password, String mandt, CommunicationSource source) {
		return 
			new CommunicatorImpl(
				new TokenPasswordImpl(user, password, group, mandt),
					source
				);
	}
	
	public synchronized void start() {
		if ( isStarted == false ) {
			for (PooledConnectionSourceImpl pooledSource:pooledConnectionSources) {
				pooledSource.start();
			}
			isStarted = true;
		}
	}

	public synchronized void stop() {
		if ( isStarted == true ) {
			for (PooledConnectionSourceImpl pooledSource:pooledConnectionSources) {
				pooledSource.stop();
			}
			isStarted = false;
		}
	}

	public Communicator createDefault(TokenFinder tokenFinder, String host,
			int port) {
		return
			createDefault(tokenFinder, host, port, null);
	}

	public Communicator createDefault(TokenFinder tokenFinder,
			CommunicationSource source, String host, int port) {
		return
			createDefault(tokenFinder, source, host, port, null);
	}

	public Communicator createPooled(String user, String group,
			String password, String mandt, String host, int port,
			String codeset, int maxpoolsize, int secs) {
		
		return this.createPooled(user, password, group, mandt, host, port, codeset, null, maxpoolsize, secs, useSSL());
	}

	public Communicator createPooled(String user, String group,
			String password, String mandt, String host, int port,
			String codeset, int maxpoolsize, int secs, boolean useSSL) {
		return this.createPooled(user, password, group, mandt, host, port, codeset, null, maxpoolsize, secs, useSSL);
	}
}
