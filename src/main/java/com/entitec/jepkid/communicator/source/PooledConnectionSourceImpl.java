package com.entitec.jepkid.communicator.source;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.entitec.jepkid.communicator.CommunicationSource;
import com.entitec.jepkid.communicator.ConnectionHandler;
import com.entitec.jepkid.communicator.Token;
import com.entitec.wax.wcall.IWCall;
import com.entitec.wax.wcall.WCallException;
import com.entitec.wax.wcall.WSuccess;
import com.entitec.wax.wdm.WDataModel;

public class PooledConnectionSourceImpl 
implements CommunicationSource 
{
	private Log log = LogFactory.getLog(this.getClass());
	private int maxPoolSize = 10;
	private int freeallconvsecs = 60;
	
	private BlockingQueue<ConnectionHandler> pool = null;
	
	private List<ConnectionHandler> connectionsToClose = 
		new ArrayList<ConnectionHandler>();
	
	private boolean isRunning = false;
	
	private Thread freeAllConversationThread = null;
	
	public PooledConnectionSourceImpl(ConnectionHandler connectionhandler, int maxpoolsize, int freeallconvsecs) {
		
		this.maxPoolSize = maxpoolsize;
		this.pool = 
			new PriorityBlockingQueue<ConnectionHandler>(maxPoolSize,
					new Comparator<ConnectionHandler>() {

						public int compare(ConnectionHandler o1,
								ConnectionHandler o2) {
							
							if ( (!o1.isConnected() && !o2.isConnected()) || 
								   (o1.isConnected() && o2.isConnected()) ) {
								return 0;
							} else if ( o1.isConnected() ) {
								return -1;
							} else {
								return 1;
							} 
						}
				
			});
		this.freeallconvsecs = freeallconvsecs;
		
		for ( int i=0; i<this.maxPoolSize; i++ ) {
			this.pool.offer(connectionhandler.newInstance());
		}
	}
	
	public synchronized void start() {
		if ( isRunning == false ) {
			this.freeAllConversationThread = new Thread(new Runnable(){

				public void run() {
					while (true) {
						try {
							Thread.sleep(freeallconvsecs*1000);
						} catch (InterruptedException e) {
							return;
						}
						doFreeallConnectionsToclose();
					}
					
				}});
			this.freeAllConversationThread.start();
			
			isRunning = true;
			if ( log.isInfoEnabled() ) {
				log.info("Free-all-Conversation-Thread started. Interval [secs] = "+freeallconvsecs);
			}
		}
	}
	
	public synchronized void stop() {
		if ( isRunning == true ) {
			if ( this.freeAllConversationThread != null )
				this.freeAllConversationThread.interrupt();
			this.isRunning = false;
		}
		doFreeallUnusedConversations();
		if ( log.isInfoEnabled() ) {
			log.info("Free-all-Conversation-Thread stopped.");
		}
	}
	
	private final class PooledConnectionProxy 
	implements IWCall 
	{
		private final Token token;
		
		private PooledConnectionProxy(Token token) {
			super();
			this.token = token;
		}
		
		public synchronized WSuccess process(String function, WDataModel inview, WDataModel outview) 
		throws WCallException {
			ConnectionHandler connection = 
				takePooledConnection(token);
			
			if ( connection == null ) {
				throw new WCallException("Funktion "+function+" wurde abgebrochen!");
			}
			
			if ( connectionsToClose.contains(connection)) {
				connectionsToClose.remove(connection);
			}
			try {
				connection.connect(token);
				final WSuccess suc = 
					connection.process(function, inview, outview);
				return suc;
			} finally {
				putPooledConnection(connection);
			}
		}
	}
	public IWCall getConnection(String name, Token token) {
		return new PooledConnectionProxy(token);
	}
	
	private ConnectionHandler takePooledConnection(Token token) {
		ConnectionHandler c;
		try {
			c = pool.take();
		} catch (InterruptedException e) {
			return null;
		}
		return c;
	}
	
	private void putPooledConnection(ConnectionHandler connectionhandler) {
		try {
			pool.put(connectionhandler);
		} catch (InterruptedException e) {
			;
		}
	}

	private void doFreeallConnectionsToclose() {
		List<ConnectionHandler> l = 
			new ArrayList<ConnectionHandler>();
		
		pool.drainTo(l);
		int closedConnectionsCount = 0;
		for ( ConnectionHandler c : l) {
			if ( connectionsToClose.contains(c)) {
			    if (c.isConnected()) {
					c.disconnect();
					closedConnectionsCount ++;
			    }
				connectionsToClose.remove(c);
			} else {
				connectionsToClose.add(c);
			}
		}
		pool.addAll(l);
		if ( log.isInfoEnabled() && closedConnectionsCount > 0 ) log.info("FreeAllConversations: "+closedConnectionsCount+ " closed.");
	}

	private void doFreeallUnusedConversations() {
		List<ConnectionHandler> l = 
			new ArrayList<ConnectionHandler>();
		
		pool.drainTo(l);
		int closedConnectionsCount = 0;
		for ( ConnectionHandler c : l) {
			    if (c.isConnected()) {
					c.disconnect();
					closedConnectionsCount ++;
			    }
		}
		pool.addAll(l);
		if ( log.isInfoEnabled() && closedConnectionsCount > 0 ) log.info("FreeAllConversations: "+closedConnectionsCount+ " closed.");
	}
}
