package com.entitec.jepkid.communicator.source;

import java.util.Hashtable;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.directory.Attribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class LDAPHandlerImpl {
	private static LDAPHandlerImpl mInstance = null;
	DirContext ctx = null;
	String searchBase = "";
	SearchControls sc = null;
	String usrdnsdomain = null; 
	Hashtable<String, String> users = new Hashtable<String, String>();

	protected Log log = LogFactory.getLog(this.getClass());
	
	public static String info() { return "$Id: LDAPHandlerImpl.java 19262 2019-12-19 14:48:42Z hm0048 $"; }

	public void init(String usrdnsdomain, String user, String passwd) {
		this.usrdnsdomain = usrdnsdomain;
		Hashtable<String, String> env = new Hashtable<String, String>();
		try {
			env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
			env.put(Context.PROVIDER_URL, "ldap://" + usrdnsdomain);
			env.put(Context.SECURITY_PRINCIPAL, user + "@" + usrdnsdomain);
			env.put(Context.SECURITY_CREDENTIALS, passwd);

			log.debug("ldap://" + usrdnsdomain);

			ctx = new InitialDirContext(env);
			sc = new SearchControls();
			sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
			for (String dc : usrdnsdomain.split("\\.")) {
				if (!searchBase.equals(""))	searchBase += ",";
				searchBase += "DC=" + dc;
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			mInstance = null;
			users = new Hashtable<String, String>();
			e.printStackTrace();
		}
	}

	public String getUid(String user) {
		String rc = users.get(user);
		if (rc == null) {
			try {
				String searchString = "(&(objectClass=User)(userprincipalname=" + user + "@" + usrdnsdomain + "))";
				log.debug("search " + searchString);
				NamingEnumeration<SearchResult> answer = ctx.search(searchBase,
						searchString, sc);
				while (answer.hasMoreElements()) {
					SearchResult sr = answer.next();
					Attribute jatt = sr.getAttributes().get("uid");
					if (jatt != null) {
						String att = (String) jatt.get();
						if (att.indexOf(";") > 0) {
							att = att.substring(0, att.indexOf(";"));
						}
						log.debug("uid=" + att);
						users.put(user, att);
						rc = att;
					}
				}
			} catch (Exception e) {
				mInstance = null;
				users = new Hashtable<String, String>();
				e.printStackTrace();
			}
		}
		return rc;
	}

	public static LDAPHandlerImpl getInstance(String usrdnsdomain, String user, String passwd) {
		if (mInstance != null) return mInstance;
		else {
			synchronized(LDAPHandlerImpl.class) {
				mInstance = new LDAPHandlerImpl();
				mInstance.init(usrdnsdomain, user, passwd);
				return mInstance;
			}
		}
	}
}

