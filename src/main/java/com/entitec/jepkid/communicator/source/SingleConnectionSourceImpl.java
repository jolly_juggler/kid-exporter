package com.entitec.jepkid.communicator.source;

import com.entitec.jepkid.communicator.CommunicationSource;
import com.entitec.jepkid.communicator.ConnectionHandler;
import com.entitec.jepkid.communicator.Token;
import com.entitec.wax.wcall.IWCall;
import com.entitec.wax.wcall.WCallException;
import com.entitec.wax.wcall.WSuccess;
import com.entitec.wax.wdm.WDataModel;

/**
 * Liefert ein IWCall Objekt auf eine einzige Verbindung.
 * 
 *  Bei Aufruf der process-Methode wird die Verbindung hergestellt, die
 *  Funktionalit�t ausgef�hrt und die Verbindung wieder abgebaut.
 *  
 *  Die Ausf�hrung der process-Methode stellt sicher, dass kein weiterer
 *  die Verbindung nutzen kann.
 *  
 *  Beim Aufruf von getConnection wird ein Proxy zur�ckgegeben. In diesem Proxy ist das
 *  zu verwendende Token hinterlegt.
 *  
 *  Jeder Aufruf der process() Methode erzeugt einen Verbindungsaufbau und -abbau.
 *  
 * @author sd0024
 *
 */
public class SingleConnectionSourceImpl implements CommunicationSource {
	private final class ConnectionProxy implements IWCall {
		private final ConnectionHandler connection;
		private final Token token;
		
		private ConnectionProxy(ConnectionHandler c, Token token) {
			super();
			this.connection = c;
			this.token = token;
		}
		
		public synchronized WSuccess process(String function, WDataModel inview, WDataModel outview) throws WCallException {
			synchronized (connection) {
				connection.connect(token);
				final WSuccess suc = connection.process(function, inview, outview);
				connection.disconnect();
				return suc;
			}
		}
	}

	final ConnectionHandler singleConnectionHandler;
	
	public SingleConnectionSourceImpl(final ConnectionHandler connectionHandler) {
		super();
		this.singleConnectionHandler = connectionHandler;
	}

	public IWCall getConnection(String name, Token token) {
		return new ConnectionProxy(singleConnectionHandler, token);
	}

}
