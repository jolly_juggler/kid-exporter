package com.entitec.jepkid.communicator.source;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.X509TrustManager;

public class TrustAllManager implements X509TrustManager {
	public static String info() { return "$Id: TrustAllManager.java 19068 2019-10-25 09:33:47Z hm0048 $"; }
	public  void checkClientTrusted(X509Certificate[] cert, String authType) throws CertificateException {}
	public  void checkServerTrusted(X509Certificate[] cert, String authType) throws CertificateException {}
	public  X509Certificate[] getAcceptedIssuers() { return null; }
}
