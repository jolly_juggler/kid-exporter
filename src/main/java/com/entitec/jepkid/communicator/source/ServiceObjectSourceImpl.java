package com.entitec.jepkid.communicator.source;

import java.lang.reflect.Method;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import com.entitec.jepkid.communicator.CommunicationSource;
import com.entitec.jepkid.communicator.Token;
import com.entitec.jepkid.communicator.bean.IWCallFromServiceBean;
import com.entitec.jepkid.communicator.bean.annotations.KIDFunction;
import com.entitec.wax.wcall.IWCall;

public class ServiceObjectSourceImpl implements CommunicationSource {
	final Map<String, Object> functionsToBeanObjects;
		
	public ServiceObjectSourceImpl(List<Object> beanObjects) {
		this.functionsToBeanObjects = getFunctionsToObjects(beanObjects);
	}

	public IWCall getConnection(String name, Token token) {
		if ( functionsToBeanObjects.containsKey(name)) {
			Object bean = functionsToBeanObjects.get(name);
			return new IWCallFromServiceBean(bean.getClass(), bean);
		} else {
			return null;
		}
	}

	protected static Map<String, Object> getFunctionsToObjects(List<Object> beanObjects) {
		final Map<String, Object> l = new Hashtable<String, Object>();
		
		for (Object bean : beanObjects ) {
			final Class beanCl = bean.getClass();
			for (Method m:beanCl.getMethods()) {
				KIDFunction a = (KIDFunction) m.getAnnotation(KIDFunction.class);
				if ( a != null )
					l.put(a.value(), bean);
			}
			for (Class i: beanCl.getInterfaces()) {
				for (Method m:i.getMethods()) {
					KIDFunction a = (KIDFunction) m.getAnnotation(KIDFunction.class);
					if ( a != null )
						l.put(a.value(), bean);
				}
			}
		}
		return l;
	}
		
}
