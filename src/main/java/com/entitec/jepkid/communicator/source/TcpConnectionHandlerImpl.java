package com.entitec.jepkid.communicator.source;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.SecureRandom;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import java.security.NoSuchAlgorithmException;
import java.security.KeyManagementException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.entitec.jepkid.communicator.ConnectionHandler;
import com.entitec.jepkid.communicator.Token;
import com.entitec.jepkid.communicator.TokenImpl;
import com.entitec.jepkid.communicator.TokenImplWDataModelProcessor;
import com.entitec.jepkid.communicator.TokenJWT;
import com.entitec.jepkid.communicator.TokenJWTWDataModelProcessor;
import com.entitec.jepkid.communicator.TokenPassword;
import com.entitec.jepkid.communicator.TokenPasswordWDataModelProcessor;
import com.entitec.jepkid.communicator.TokenWDataModelProcessor;
import com.entitec.wax.wcall.WCallErrorList;
import com.entitec.wax.wcall.WCallException;
import com.entitec.wax.wcall.WSuccess;
import com.entitec.wax.wcall.wprotocol.WTcpRequest;
import com.entitec.wax.wcall.wprotocol.WTcpResponse;
import com.entitec.wax.wcall.wprotocol.WTcpToolkit;
import com.entitec.wax.wdm.IWDataItem;
import com.entitec.wax.wdm.WDataModel;
import com.entitec.wax.wdm.WDataModelReader;
import com.entitec.wax.wdm.WDataModelWriter;
import com.entitec.wax.wdm.WSignature;

/** 
 * 
 * Protokolltreiber fuer EPKID Aufrufe ueber TCP/IP
 * 
 * @author SD0024
 *
 */
public class TcpConnectionHandlerImpl 
implements ConnectionHandler 
{
	protected Log log = LogFactory.getLog(this.getClass());

	private final String identity;
	
	private String host = "localhost";
	private int port = 1234;
	private String mCodeSet = null;
	/* CICS Header */
	private String header = null;
	
	private Socket mSoc;
	static String useSSL = null;
	
	private String user = null;

	public static String info() {
		return "$Id: TcpConnectionHandlerImpl.java 19348 2020-01-20 14:15:13Z hm0048 $";
	}

	public TcpConnectionHandlerImpl(String host, int port) {
		this(host, port, null, null);
	}
	
	public TcpConnectionHandlerImpl(String host, int port, String codeSet, String header) {
		this(host, port, codeSet, header, false);
	}

	public TcpConnectionHandlerImpl(String host, int port, String codeSet, String header, boolean useSSL) {
		this(useSSL);
		this.host = host;
		this.port = port;
		this.mCodeSet = codeSet;
		this.header = header;
	}

	public TcpConnectionHandlerImpl() {
		this(false);
	}
	
	public TcpConnectionHandlerImpl(boolean useSSL) {
		if (this.useSSL == null) this.useSSL = System.getProperties().getProperty("useSSL");
		if (this.useSSL == null) {
			if (useSSL) this.useSSL = "1";
			else this.useSSL = "0";
		}
		this.identity = Integer.toHexString(System.identityHashCode(this));
	}
	
	
	public ConnectionHandler newInstance() {
		return new TcpConnectionHandlerImpl(this.host, this.port, this.mCodeSet, this.header);
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getCodeSet() {
		return mCodeSet;
	}

	public void setCodeSet(String codeSet) {
		mCodeSet = codeSet;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}
	
	public String getUser() {
		return this.user;
	}
	
	public void reset() {
		this.mSoc = null;
		this.user = null;
	}

	public String getIdentity() {
		return this.identity;
	}
	
	/** stellt eine tcp verbindung zum host her und 
	 * fuehrt mit dem usertoken ein login durch */
	
	public synchronized void connect(Token userToken) {
		if ( (getUser() != null) && (! getUser().equals(userToken.getUser())) ) {
			disconnect();
		}
		
		try {
			if ( this.isReady() == false ) { /* start connect und login */
				connect();
				login(userToken);
			} /* end connect und login */
			
		} catch (WCallException e) {
			closeConnection();
			throw e;
		}		
	}
	
	protected final boolean isUserLogged(Token userToken) {
		return ( (getUser() != null) && getUser().equals(userToken.getUser()) );
	}
	
	/**
	 * Fuehrt einen logoff und einen close aus
	 */
	public synchronized void disconnect() {
		logoff();
		closeConnection();
	}

	public boolean isConnected() {
		return isReady();
	}
	
	public synchronized WSuccess process(String function, WDataModel in, WDataModel out) {
		if ( log.isInfoEnabled() ) {
			log.info("process: ("+identity+") "+function);
		}
		
		/* Korrektur von In- und Out-View */
		if ( in == null ) in = new WDataModel(new WSignature());
		fillUpEmptyInView(in);

		if ( out == null ) out = new WDataModel(new WSignature());

		WSuccess suc;
		try {
			suc = process_internal(function, in, out);
		
			if ( "99".equals(suc.getSuccess()) ) {;}
		} catch (IOException e) {
			closeConnection();
			throw new WCallException(e.getMessage());
		}
		return suc;
	}
	
	protected WDataModel fillUpEmptyInView(WDataModel in) {
		WDataModelWriter wdin = new WDataModelWriter(in);
		if ((wdin.rowCount() == 0) && (wdin.colCount()> 0) ) {
			wdin.addRow();
			for ( int i = 0; i < wdin.colCount(); i++) {
				String name = wdin.getAttributeName(i);
				wdin.setValue(name,"");
				wdin.setState(name,IWDataItem.DISABLED);
			}
		}
		return in;
	}
	
	protected WSuccess process(String function) {
		if ( log.isInfoEnabled() ) {
			log.info("process: ("+identity+") "+function);
		}
		
		if ( isConnected() == false ) {
			throw new UnsupportedOperationException("Connectionhandler isn't connected");
		}
		
		try {
			return process_internal(function, 
					fillUpEmptyInView(new WDataModel(new WSignature())),
					new WDataModel(new WSignature())
					);
		} catch (IOException e) {
			this.mSoc = null;
			throw new WCallException("Function: "+function+" : "+e.getMessage());
		}
	}

	protected final void closeConnection() {
		if (mSoc != null){
			InputStream si = null;
			OutputStream so = null;
			try {
				si = mSoc.getInputStream();
				so = mSoc.getOutputStream();
		
				so.write("5004".getBytes()); so.flush();
				si.read();
			} catch (IOException e) {
				;
			} finally {
				try {
					if ( so != null) so.close(); 
					if ( si != null) si.close();
					if ( mSoc != null ) mSoc.close();
					
				} catch (IOException e1) { ; }
				
				mSoc = null;
				
				if ( log.isInfoEnabled() ) {
					log.info("process: ("+identity+") close");
				}
			}
		}
	}
	
	/** Fuehrt einen EP/KID-Login aus */
	protected final WSuccess login(Token userToken){
		
		if ( log.isInfoEnabled() ) {
			log.info("process: (" + identity + ") LOGIN");
		}
		
		TokenWDataModelProcessor tokenProcessor;
		
		if (userToken instanceof TokenJWT) {
			tokenProcessor = new TokenJWTWDataModelProcessor((TokenJWT) userToken);
		} else if (userToken instanceof TokenPassword) {
			tokenProcessor = new TokenPasswordWDataModelProcessor((TokenPassword) userToken);
			
		// Diese Konvertierung wegen R�ckw�rtskompatibilit�t
		} else if (userToken instanceof TokenImpl) {
			tokenProcessor = new TokenImplWDataModelProcessor((TokenImpl) userToken);
		} else {
			throw new IllegalStateException(String.format("Can't find TokenWDataModelProcessor to convert '%s'", userToken.getClass()));
		}
		
		WDataModel in = tokenProcessor.incomingDataModel();
		WDataModel out = tokenProcessor.outgoingDataModel();
		
		// LOGIN 
		String success = WSuccess.WRK_TECHN_FEHLER;
		WCallErrorList errorList = new WCallErrorList();
		
		try {
			WTcpRequest m = 
				new WTcpRequest("LOGIN", new WDataModelReader(in), new WDataModelReader(out));
			
			/* request */
			OutputStream so = mSoc.getOutputStream();
			if ( (header != null) && (! "".equals(header)) ) {
				WTcpToolkit.write(so, WTcpToolkit.fillRight(header, ' ', 50), mCodeSet);
			} 
			WTcpToolkit.write(so, m.toTcpRequest(), mCodeSet);
	
			/* response */	
			InputStream si = mSoc.getInputStream();
			success = WTcpToolkit.parseTcpResponse(si, so,
				new WTcpResponse(new WDataModelWriter(in), new WDataModelWriter(out), errorList), mCodeSet
			);
		} catch (IOException e) {
			this.mSoc = null;
			throw new WCallException(e.getClass()+"."+e.getMessage());
		}

		this.user = userToken.getUser();
		
		WSuccess suc = new WSuccess(success, errorList);
		if ( WSuccess.WRK_OK.equals(suc.getSuccess()) != true  ) {
			throw new WCallException("Login failed");
		}
		
		return suc;
	}
	
	private final WSuccess process_internal(
	String function, WDataModel in, WDataModel out) 
	throws IOException 
	{

		String success = WSuccess.WRK_TECHN_FEHLER;
		WCallErrorList errorList = new WCallErrorList();
	
		WTcpRequest request = 
			new WTcpRequest(function, new WDataModelReader(in), new WDataModelReader(out));
		
		OutputStream outputStream = null; InputStream inputStream = null;
		
		String requestString = request.toTcpRequest();
	
		outputStream = mSoc.getOutputStream();
		WTcpToolkit.write(outputStream, requestString, mCodeSet);

		/* response */	
		inputStream = mSoc.getInputStream();
		success = WTcpToolkit.parseTcpResponse(inputStream, outputStream,
			new WTcpResponse(new WDataModelWriter(in), new WDataModelWriter(out), 
					errorList), mCodeSet
		);
		
		return new WSuccess(success, errorList);
	}

	
	protected final boolean isReady() {
		return ( mSoc != null);
	}
	
	protected final void connect() {
		try {
			TrustManager[] trustAllCerts = new TrustManager[] { new TrustAllManager() };
			SSLContext context = SSLContext.getInstance("SSL");
			context.init(null, trustAllCerts, new SecureRandom());
			if (useSSL.equals("1")) mSoc = context.getSocketFactory().createSocket(host, port);
			else mSoc = new Socket(host, port);
		} catch (NoSuchAlgorithmException e) {
			throw new WCallException(e.getClass()+"."+e.getMessage());
		} catch (KeyManagementException e) {
			throw new WCallException(e.getClass()+"."+e.getMessage());
		} catch (UnknownHostException e) {
			throw new WCallException(e.getClass()+"."+e.getMessage());
		} catch (IOException e) {
			throw new WCallException(e.getClass()+"."+e.getMessage());
		}
	}
	
	protected final void logoff() {
		this.user = null;
	}

}
