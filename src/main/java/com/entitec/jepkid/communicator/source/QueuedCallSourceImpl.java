package com.entitec.jepkid.communicator.source;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import com.entitec.jepkid.communicator.CommunicationSource;
import com.entitec.jepkid.communicator.Communicator;
import com.entitec.jepkid.communicator.Token;
import com.entitec.wax.wcall.IWCall;
import com.entitec.wax.wcall.WCallException;
import com.entitec.wax.wcall.WSuccess;
import com.entitec.wax.wdm.WDataModel;


public class QueuedCallSourceImpl implements CommunicationSource {
	
	final private Communicator destinationCommunicator;
	final private List<String> queuedFunctionNames;
	
	final private List<QueuedIWCall> queue = new ArrayList<QueuedIWCall>();
	final private Thread worker;
		
	protected class QueuedIWCall {
		final String functionname;
		final WDataModel copyIn;
		final WDataModel copyOut;
		
		public QueuedIWCall(String functionname, WDataModel in, WDataModel out) {
			this.functionname = functionname;
			this.copyIn = (WDataModel) deepCopy(in);
			this.copyOut = (WDataModel) deepCopy(out);
		}
		public boolean process(Communicator communicator) {
			final WSuccess s = communicator.process(
					functionname, 
					copyIn, copyOut);
			System.out.println(functionname);
			return true;
		}
	}

	public QueuedCallSourceImpl(Communicator destination, final int interval) {
		this(null, destination, interval);
	}
	
	public QueuedCallSourceImpl(List<String> queuedFunctionNames, 
			Communicator destination, final int interval) {
		this.destinationCommunicator = destination;
		this.queuedFunctionNames = queuedFunctionNames;
		this.worker = new Thread(new Runnable() {
			public void run() {
				while (true) {
					QueuedIWCall nextIWCall = null;
					synchronized(queue) {
						if ( ! queue.isEmpty())
							nextIWCall = queue.remove(0);
					}
					if ( nextIWCall == null ) {
						try {
							Thread.sleep(interval*1000);
						} catch (InterruptedException e) {;}
					} else {
						if ( nextIWCall.process(destinationCommunicator) ) {
							; //OK
						} else {
							; // push to retry list
						}
					}
				}
			}
		});
		this.worker.start();
	}
	
	public IWCall getConnection(String name, Token token) {
		if ((queuedFunctionNames == null) || queuedFunctionNames.contains(name)) {
			return new IWCall() {
				public WSuccess process(String function, WDataModel inview, WDataModel outview) throws WCallException {
					synchronized(queue) {
						queue.add(new QueuedIWCall(function, inview, outview));
					}
					return new WSuccess("00");
				}
			};
		} else {
			return null;
		}
	}
	
	 private static Object deepCopy(Object o){
			Object newObject = null;
			try {
			  ByteArrayOutputStream outStream = new ByteArrayOutputStream();
			  ObjectOutputStream os = new ObjectOutputStream(outStream);
			  os.writeObject(o);
			  byte[] b = outStream.toByteArray();
			  os.close();
			  ByteArrayInputStream inStream = new ByteArrayInputStream(b);
			  ObjectInputStream is = new ObjectInputStream(inStream);
			  newObject = is.readObject();
			} catch (IOException e) {
			  throw new RuntimeException(e.getClass() + ": " + e.getMessage());
			} catch (ClassNotFoundException ee) {
			  throw new RuntimeException(ee.getClass() + ": " + ee.getMessage());
			}
			return newObject;
		  }  
}
