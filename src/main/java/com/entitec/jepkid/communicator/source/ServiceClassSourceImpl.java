package com.entitec.jepkid.communicator.source;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import com.entitec.jepkid.communicator.CommunicationSource;
import com.entitec.jepkid.communicator.Token;
import com.entitec.jepkid.communicator.bean.IWCallFromServiceBean;
import com.entitec.jepkid.communicator.bean.annotations.KIDFunction;
import com.entitec.wax.wcall.IWCall;

public class ServiceClassSourceImpl implements CommunicationSource {
	final Map<String, Class> functionsToClasses;
		
	public ServiceClassSourceImpl(List<String> beansClassNames) {
		
		this.functionsToClasses = getFunctionsToClasses(
				getNamesToClasses(beansClassNames)
			);
	}

	public IWCall getConnection(String name, Token token) {
		if ( functionsToClasses.containsKey(name)) {
			Class beanCl = functionsToClasses.get(name);
			try {
				Object bean = beanCl.newInstance();
				return new IWCallFromServiceBean(beanCl, bean);
			} catch (Exception e) {
				throw new RuntimeException(e.getCause());
			}
		} else {
			return null;
		}
	}

	protected static Map<String, Class> getFunctionsToClasses(List<Class> beans) {
		final Map<String, Class> l = new Hashtable<String, Class>();
		
		for (Class beanCl : beans ) {
			for (Method m:beanCl.getMethods()) {
				KIDFunction a = (KIDFunction) m.getAnnotation(KIDFunction.class);
				if ( a != null )
					l.put(a.value(), beanCl);
			}
			for (Class i: beanCl.getInterfaces()) {
				for (Method m:i.getMethods()) {
					KIDFunction a = (KIDFunction) m.getAnnotation(KIDFunction.class);
					if ( a != null )
						l.put(a.value(), beanCl);
				}
			}
		}
		return l;
	}
	
	protected static List<Class> getNamesToClasses(List<String> beansClassNames) {
		List<Class> r = new ArrayList<Class>();
		for (String className : beansClassNames ) {
			try {
				Class beanCl = Class.forName(className);
				r.add(beanCl);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		return r;
	}
	
	protected static List<Class> getBeanObjectsToClasses(List<Object> beanObjects) {
		List<Class> r = new ArrayList<Class>();
		for (Object beanObject : beanObjects ) {
			if ( beanObject != null) r.add(beanObject.getClass());
		}
		return r;
	}
	
}
