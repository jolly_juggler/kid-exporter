package com;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.entitec.jepkid.communicator.Communicator;
import com.entitec.jepkid.communicator.CommunicatorFactory;
import com.entitec.jepkid.communicator.CommunicatorFactoryImpl;
import com.entitec.wax.wdm.WAttribute;
import com.entitec.wax.wdm.WDataItem;
import com.entitec.wax.wdm.WDataModel;
import com.entitec.wax.wdm.WDataModelReader;
import com.entitec.wax.wdm.WDataModelWriter;
import com.entitec.wax.wdm.WSignature;
import com.jayway.jsonpath.JsonPath;

import net.minidev.json.JSONArray;

@RestController
@EnableAutoConfiguration
public class Kid_Exporter {
	Logger logger = LoggerFactory.getLogger(Kid_Exporter.class);
	Communicator c = null;
	String server = "sun02";
	Hashtable<String, Integer> htCount = new Hashtable<String, Integer>();

	private final CommunicatorFactory cf = new CommunicatorFactoryImpl();

	/*
curl -H "Content-Type: application/json" -d {'LESEN-FUNKTIONEN':[{'MODUL':'SYKDAT'}]} -X POST http://localhost:8085
	 */
	
	static Connection con = null;
	@PostMapping(value = "/", produces = { "text/plain;version=0.0.4;charset=utf-8" })
	public String alertLogger(@RequestBody String jsonString) {
		String funktion = "UNBEKANNT";
		logger.debug(jsonString);
		
		if (con == null) {
			try {
				Class.forName("org.sqlite.JDBC");
				Properties prop = new Properties();
				prop.put("charSet", "iso-8859-15");
				con = DriverManager.getConnection("jdbc:sqlite:/Temp/outview.db");
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		@SuppressWarnings("unchecked")
		Map<String, Object> mf = (Map<String, Object>) JsonPath.read(jsonString, "$");
		for (Map.Entry<String, Object> entry : mf.entrySet()) funktion = entry.getKey().toUpperCase();
		JSONArray q = JsonPath.read(jsonString, "$.*.*");
		@SuppressWarnings("unchecked")
		Map<String, String> lhm = (Map<String, String>) q.get(0);
		StringBuilder retcode = new StringBuilder("");
		int iport = 1237;
		String user = "x";
		String pass = "y";
		boolean useSSL = false;

		logger.info("******* connect: " + server + ":" + iport + "!" + user + "!" + useSSL);
		if (c == null)
			c = cf.createPooled(user, pass, "AM", "36", server, iport, "ISO-8859-1", 2, 60, useSSL);
/*
		retcode.append(
				"bgstandard{fnktn=\"" + funktion + "\",server=\"" + server + "\",port=\"" + iport + "\",erfolg=\"");
*/
		StringBuilder line = new StringBuilder();
		try {
			WDataModel in;
			WDataModel out;
			WDataModelWriter wdm;
			in = new WDataModel(new WSignature());
			wdm = new WDataModelWriter(in);
			
			for (Map.Entry<String, String> entry : lhm.entrySet()) {
				String key = entry.getKey().toUpperCase();
				String value = entry.getValue();
				wdm.addAttribute(new WAttribute(key, WAttribute.FRM_CHAR, value.length(), 0, false));
				wdm.addRow();
				wdm.setValue(key, value, WDataItem.ENABLED);
			}

			ArrayList<String> attrs = new ArrayList<String>();
			WSignature sig = new WSignature();
			try {
				Statement stmt = con.createStatement();
				ResultSet rs = stmt.executeQuery("select ATTRIBUT,ATLEN from y02 where FNKTN='" + funktion + "'");
				while (rs.next()) {
					String attr = rs.getString(1);
					sig.addAttribute(new WAttribute(attr, attr, WAttribute.FRM_CHAR, rs.getInt(2), 0, false));
					attrs.add(attr);
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}

			//outData
			WDataModel outData = new WDataModel(sig);
			WDataModelReader outView = new WDataModelReader(outData);

			c.process(funktion, in, outData);
			WDataModelWriter dmw = new WDataModelWriter(outData);
			
			
			for (int i=0; i<dmw.rowCount(); ++i) {
				outView.setRow(i);
				if (i>0) line.append(",");
				line.append("{");
				String value = null;
				for (String att : attrs) {
					if (value != null) line.append(",");
					value = (String) dmw.getValue(att);
					line.append("\"" + att + "\":\"" + value + "\"");
				}
				line.append("}");
				System.out.println(line.toString());
			}
			
			// logger.info(outattr + "=" + val);
			c.process("FREE-ALL-CONVERSATIONS", in, outData);
			// retcode.append(wsucc.getSuccess() + "\"}");
		} catch (Exception e) {
			e.printStackTrace();
			// retcode.append("99\"}");
		}
		retcode.append("{\"" + funktion + "\": { \"out\" :[" + line + "]}}");
		return retcode.toString();
	}
}
