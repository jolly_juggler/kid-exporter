package com;

import java.util.List;

public class MQClientList {
	private List<MQClient> mqclient;
 
    public List<MQClient> getMqclient() {
    	return mqclient;
    }
    public void setMqclient(List<MQClient> mqclient) {
    	this.mqclient = mqclient;
    }
}
