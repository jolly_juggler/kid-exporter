package com;

public class MQClient {
    private String hostname;
    private String hostport;
	private String user;
	private String pass;
	private String ssl;

    public String getHostname() {
    	return hostname;
    }
    public void setHostname(String hostname) {
    	this.hostname = hostname;
    }
    public String getHostport() {
    	return hostport;
    }
    public void setHostport(String hostport) {
    	this.hostport = hostport;
    }
    public String getUser() {
    	return user;
    }
    public void setUser(String user) {
    	this.user = user;
    }
    public String getPass() {
    	return pass;
    }
    public void setPass(String pass) {
    	this.pass = pass;
    }
    public String getSsl() {
    	return ssl;
    }
    public void setSsl(String ssl) {
    	this.ssl = ssl;
    }
 }
