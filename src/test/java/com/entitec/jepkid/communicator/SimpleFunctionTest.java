package com.entitec.jepkid.communicator;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.entitec.wax.wdm.WAttribute;
import com.entitec.wax.wdm.WDataItem;
import com.entitec.wax.wdm.WDataModel;
import com.entitec.wax.wdm.WDataModelWriter;
import com.entitec.wax.wdm.WSignature;



public class SimpleFunctionTest {

	private final CommunicatorFactory cf = 
		new CommunicatorFactoryImpl();
	/**
	 * F�hrt die EP/KID Funktion TAGES-DATUM auf dem Server aus.
	 */
	
	@Before
	public void startCommunicatorFactory() {
		cf.start();
	}
	@After
	public void stopCommunicatorFactory() {
		cf.stop();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			;
		}
	}
	
	@Ignore
	@Test
	public void pooledtagesdatum() {
		final ServiceInvocationHandler sh = 
			new GenericServiceFactoryImpl(
					cf.createPooled("sd0024", "123456789", "admin", "31", "entdev1", 1235, 2, 60)
					);
		final EpkidTestService s = 
			sh.newInstance(EpkidTestService.class);
		
		
		List<Thread> threads = new ArrayList<Thread>();
		
		for ( int i=0; i < 5; i++ ) {
			Thread t = new Thread(new Runnable(){

				public void run() {
					for (int i = 0; i < 30; i++) {
						s.tagesDatum();
					}
					System.out.println(Thread.currentThread().getId()+": fertig");
				}});
			threads.add(t);
		}
		for (Thread t : threads) {
			t.start();
		}
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		try {
			s.wrongFunction();
		} catch (CommunicatorRuntimeException e) {
			assertEquals("K002", e.getTextNummer());
		}
		
	}
	
	@Ignore
	@Test
	public void pooledAttributDocLesen() {
		final ServiceInvocationHandler sh = 
			new GenericServiceFactoryImpl(
					cf.createPooled("ip0010", "123456789", "admin", "31", "entdev1", 1235, 2, 60)
					);
		final EpkidTestService s = 
			sh.newInstance(EpkidTestService.class);
		
		
		List<Thread> threads = new ArrayList<Thread>();
		
		for ( int i=0; i < 5; i++ ) {
			Thread t = new Thread(new Runnable(){

				public void run() {
					for (int i = 0; i < 30; i++) {
						String doc = s.attributDescriptionLesen("IPTEST");
						System.out.println("DOKU: " + doc);
					}
					System.out.println(Thread.currentThread().getId()+": fertig");
				}});
			threads.add(t);
		}
		for (Thread t : threads) {
			t.start();
		}
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		try {
			s.wrongFunction();
		} catch (CommunicatorRuntimeException e) {
			assertEquals("K002", e.getTextNummer());
		}
		
	}

	
	@Ignore
	@Test
	public void attributDocLesen() {
		final ServiceInvocationHandler sh = 
			new GenericServiceFactoryImpl(
					cf.createPooled("ip0010", "123456789", "admin", "31", "entdev1", 1235, "ISO-8859-1", 2, 60)
					);
		
		final EpkidTestService s = 
			sh.newInstance(EpkidTestService.class);

		try {
			String doc = s.attributDescriptionLesen("IPTEST");
			System.out.println("DOC:" + doc);
		} catch (CommunicatorRuntimeException e) {
			System.out.println(e.getText());
			assertEquals("K002", e.getTextNummer());
		}

		System.out.println(Thread.currentThread().getId()+": fertig");
		
	}

	@Ignore
	@Test
	public void attributDocSchreiben() {
		final Communicator c = cf.createPooled("ip0010", "123456789", "admin", "31", "entdev1", 1235, "ISO-8859-1", 2, 60);
		WDataModel in;
		WDataModel out;
		WDataModelWriter wdm;

		in = new WDataModel(new WSignature());
		wdm = new WDataModelWriter(in);
		wdm.addAttribute(new WAttribute("ATTYP", WAttribute.FRM_CHAR, 16, 0, false));
		wdm.addAttribute(new WAttribute("ISD-AENDER", WAttribute.FRM_CHAR, 2, 0, false));
		wdm.addAttribute(new WAttribute("ISD-DOKU", WAttribute.FRM_CHAR, 4000, 0, false));
		wdm.addRow();
		wdm.setValue("ATTYP", "IPTEST", WDataItem.UNCHANGED);
		wdm.setValue("ISD-AENDER", "00", WDataItem.ENABLED);
		wdm.setValue("ISD-DOKU", "Ingos Test ohne Umlaute", WDataItem.ENABLED);

		out = new WDataModel(new WSignature());
		wdm = new WDataModelWriter(out);
		wdm.addAttribute(new WAttribute("STRG", WAttribute.FRM_CHAR, 255, 0, false));
		c.process("ATTRIBUT-DESC-AENDERN", in, out);

		
		attributDocLesen();
		
		in = new WDataModel(new WSignature());
		wdm = new WDataModelWriter(in);
		wdm.addAttribute(new WAttribute("ATTYP", WAttribute.FRM_CHAR, 16, 0, false));
		wdm.addAttribute(new WAttribute("ISD-AENDER", WAttribute.FRM_CHAR, 2, 0, false));
		wdm.addAttribute(new WAttribute("ISD-DOKU", WAttribute.FRM_CHAR, 4000, 0, false));
		wdm.addRow();
		wdm.setValue("ATTYP", "IPTEST", WDataItem.UNCHANGED);
		wdm.setValue("ISD-AENDER", "00", WDataItem.ENABLED);
		wdm.setValue("ISD-DOKU", "ÄÖÜäöü", WDataItem.ENABLED);
		
		out = new WDataModel(new WSignature());
		wdm = new WDataModelWriter(out);
		wdm.addAttribute(new WAttribute("STRG", WAttribute.FRM_CHAR, 255, 0, false));
		c.process("ATTRIBUT-DESC-AENDERN", in, out);
		
		
		attributDocLesen();
		
		
		
	}

	
}
