package com.entitec.jepkid.communicator;

import static org.junit.Assert.fail;

import org.junit.Ignore;
import org.junit.Test;

@Ignore
public class SYKPAMFunctionTest {
	
	/*
	private static final String USER = "i5005482";
	private static final String PASSWORD = "entitec0";
	private static final String HOST = "bgsanwt";
	private static final int PORT = 3055;
	*/
		
	private static final String USER = "sd0024";
	private static final String PASSWORD = "ab654321";
	private static final String HOST = "zeus";
	private static final int PORT = 1234;
	
	private static final String NEWPASSWORD = "123hu765";
	
	private static final String GROUP = "admin";
	
	
	private final CommunicatorFactory cf = 
		new CommunicatorFactoryImpl();

	private final ServiceInvocationHandler sh1 = 
		new GenericServiceFactoryImpl(
				cf.createDefault(
						USER, PASSWORD, GROUP, 
						"31", HOST, PORT)
				);
	
	final EpkidTestService s1 = 
		sh1.newInstance(EpkidTestService.class);
	
	private final ServiceInvocationHandler sh2 = 
		new GenericServiceFactoryImpl(
				cf.createDefault(
						USER, NEWPASSWORD, GROUP, 
						"31", HOST, PORT)
				);
	
	final EpkidTestService s2 = 
		sh2.newInstance(EpkidTestService.class);
	
	@Test
	public void changePassword() {
		s1.tagesDatum();
		s1.changePassword(USER, PASSWORD, NEWPASSWORD);
		try {
		s1.tagesDatum();
		fail("Kennwort wurde nicht ge�ndert");
		} catch (Exception e) {;}
		
		s2.tagesDatum();
		s2.changePassword(USER, NEWPASSWORD, PASSWORD);
		
		s1.tagesDatum();
	}
	
	//@Ignore
	@Test
	public void changePasswordToSmall() {
		s1.tagesDatum();
		try {
			s1.changePassword(USER, PASSWORD, "xx");
			fail("Zu kleines Kennwort wurde ge�ndert!");
		} catch (Exception e) {;}
	}
}
