package com.entitec.jepkid.communicator;

import com.entitec.jepkid.communicator.bean.annotations.KIDArgs;
import com.entitec.jepkid.communicator.bean.annotations.KIDFunction;
import com.entitec.jepkid.communicator.bean.annotations.KIDResult;

/**
 * Service Interface f�r die Bereitstellung von 
 * EP/KID Testfunktionalit�t
 * 
 * @author sd0024
 *
 */
public interface EpkidTestService {
	
	/**
	 * Gibt das aktuelle Tagesdatum zur�ck.
	 * 
	 * @return
	 */
	@KIDFunction("TAGES-DATUM")
	@KIDResult("HEUTE")
	
	String tagesDatum();
	
	@KIDFunction("LESEN-FUNKTION")
	@KIDResult("MODUL")
	@KIDArgs({"FNKTN"})
	
	String lesenFunktion(String funktion);
	
	@KIDFunction("PAM-CHANGE-PASSWD")
	@KIDArgs({"USRID", "PASSWD", "NEWPASSWD"})
	void changePassword(String usrid, String password, String newpassword);
	
	@KIDFunction("JKLOOKL")
	void wrongFunction();
	
	
	
	@KIDFunction("ATTRIBUT-DESC-ANZEIGEN")
	@KIDResult("ISD-DOKU")
	@KIDArgs({"ATTYP"})
	String attributDescriptionLesen(String attyp);

	@KIDFunction("ATTRIBUT-DESC-AENDERN")
	@KIDResult("STRG")
	@KIDArgs({"ATTYP", "SD-AENDER", "SD-DOKU"})
	void attributDescriptionSchreiben(String attyp, String sdaendern, String cddoku);
	
	
}
