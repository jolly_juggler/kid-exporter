package com.entitec.jepkid.session;


import org.junit.Test;
import static org.junit.Assert.*;


public class TestSession {

	
	@Test
	public void testSession() {

		SessionManager sm = new SessionManagerImpl();

		Session[] sessions = sm.getSessions();
		assertTrue(sessions.length == 0);
		
		
		Session session1 = sm.createSession("4711");
		Session session2 = sm.createSession("application");
		
		SessionHandler sh = (SessionHandler)sm;

		assertNull(sh.currentSession());

		sm.activateSession(session1.getSessionId());
		Session session = sh.currentSession();
		assertEquals(session1, session);
		sm.deactivateCurrentSession();
		assertNull(sh.currentSession());
		
		
		sm.activateSession(session1.getSessionId());
		session = sh.currentSession();
		session.setProperty("hugo", "hugo");
		assertEquals("hugo", session.getProperty("hugo"));
		
		sm.activateSession(session2.getSessionId());
		assertNotSame(session1, sh.currentSession());
		assertNull(sh.currentSession().getProperty("hugo"));
		
		
	}
}
